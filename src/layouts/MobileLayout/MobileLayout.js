import React, {Fragment} from 'react';
import FooterMobile from './FooterMobile/FooterMobile';
import Broadcaster from '../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../common/constants/broadcastEvents';

class MobileLayout extends React.PureComponent {
  componentDidMount() {
    setTimeout(() => {
      Broadcaster.broadcast(BROADCAST_EVENT.LAYOUT_LOADED);
    }, 1000);
  }

  render() {
    return (
      <Fragment>
        {this.props.children}
        <FooterMobile/>
      </Fragment>
    );
  }
}

export default MobileLayout;