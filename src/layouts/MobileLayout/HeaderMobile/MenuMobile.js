import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import Avatar from '../../../components/Avatar/Avatar';
import HyperLink from '../../../components/HyperLink/HyperLink';
import {openModal} from '../../../redux/actions/modals/modals';
import {logout} from '../../../redux/actions/user';
import './MenuMobile.scss';

class MenuMobile extends React.PureComponent {
  render() {
    const {
      isOpen, handleClose,
      user, auth, openRegisterModal, handleLogout, notifications
    } = this.props;
    let name = '';
    if (user && user.full_name) {
      const arr = user.full_name.split(' ');
      name = arr[arr.length - 1];
    }
    let countUnread = 0;
    notifications.forEach(item => {
      if (!item.seen) {
        countUnread++;
      }
    });
    return (
      <div className={`menu-mobile-container ${isOpen ? 'visible' : ''}`}>
        <div className={`menu-mobile ${isOpen ? 'open' : ''}`}>
          <div className="menu-mobile-header">
            <Avatar size={40} src={user ? user.avatar : ''}/>
            {
              !auth.isAuthenticated &&
              <div className="account-info" onClick={handleClose}>
                <Link to="/login" className="login-link">Đăng nhập</Link>
                <p>Để nhận được nhiều ưu đãi</p>
              </div>
            }
            {
              auth.isAuthenticated &&
              <div className="account-info" onClick={handleClose}>
                <Link className="login-link" to={'/user'}>Chào <strong>{name}</strong></Link>
                <p><HyperLink onClick={handleLogout}>Đăng xuất</HyperLink></p>
              </div>
            }
          </div>
          <div className="menu-mobile-content">
            <ul className="nav-mobile" onClick={handleClose}>
              <li><Link to="/"><i className="fas fa-home"/> Trang chủ</Link></li>
              <li><HyperLink><i className="fas fa-list-ul"/> Danh mục sản phẩm</HyperLink></li>
              <li><Link to="/groups/"><i className="fas fa-users"/> Hội nhóm</Link></li>
              <li><HyperLink><i className="fas fa-fire-alt"/> Khuyến mãi mỗi ngày</HyperLink></li>
              <li><HyperLink><i className="fas fa-award"/> Hàng bán chạy</HyperLink></li>
              <li><HyperLink><i className="fas fa-clock"/> Sản phẩm đã xem</HyperLink></li>
              {
                auth.isAuthenticated &&
                <Fragment>
                  <li><Link to="/user"><i className="fas fa-user-circle"/> Tài khoản của tôi</Link></li>
                  <li><Link to="/user/notifications"><i className="fas fa-bell"/> Thông báo của tôi {
                    countUnread > 0 && <span>({countUnread})</span>
                  }</Link></li>
                </Fragment>
              }
            </ul>
            <div className="list-link">
              <label>LIÊN KẾT</label>
              <ul onClick={handleClose}>
                {
                  !auth.isAuthenticated &&
                  <li><HyperLink onClick={openRegisterModal}>Đăng ký tài khoản mới</HyperLink></li>
                }
                <li><HyperLink>Ưu đãi của đối tác</HyperLink></li>
                <li><HyperLink>Đặt phòng khách sạn</HyperLink></li>
              </ul>
            </div>
            <div className="contact-info">
              <label>Hỗ trợ</label>
              <p>Hotline: <a href="tel:18000006" className="text-orange">1800 0006</a> (miễn phí)</p>
              <p><HyperLink>Hỗ trợ khách hàng</HyperLink></p>
            </div>
          </div>
        </div>
        {
          isOpen &&
          <div className="menu-mobile-backdrop" onClick={handleClose}/>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
  auth: state.user.auth,
  notifications: state.user.notifications
});

const mapDispatchToProps = (dispatch) => ({
  openRegisterModal: () => dispatch(openModal('register')),
  handleLogout: () => dispatch(logout())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MenuMobile));