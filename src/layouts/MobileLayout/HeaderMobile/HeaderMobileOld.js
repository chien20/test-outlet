import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import {history} from '../../../common/utils/router/history';
import {handleInputTextChanged} from '../../../common/helpers';
import MenuMobile from './MenuMobile';
import './HeaderMobile.scss';

class HeaderMobileOld extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      keyword: '',
      isMenuOpen: false
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  toggleMenu = (state) => () => {
    this.setState({
      isMenuOpen: state
    });
  };

  goBack = () => {
    history.goBack();
  };

  handleSearch = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const {keyword} = this.state;
    if (!keyword) {
      return;
    }
    history.push('/tim-kiem/' + encodeURI(keyword));
  };

  render() {
    const {location: {pathname}, cart} = this.props;
    const {isMenuOpen, keyword} = this.state;
    return (
      <Fragment>
        <div className="header-mobile-old">
          {
            pathname === '/' &&
            <div className="menu-toggle" onClick={this.toggleMenu(true)}><i className="fas fa-bars"/></div>
          }
          {
            pathname !== '/' &&
            <div className="menu-toggle" onClick={this.goBack}><i className="fas fa-arrow-left"/></div>
          }
          <form className="search-container" onSubmit={this.handleSearch}>
            <div className="search-icon">
              <i className="fas fa-search"/>
            </div>
            <input
              type="text"
              placeholder="Tìm kiếm"
              value={keyword}
              onChange={handleInputTextChanged('keyword', this.setData)}/>
          </form>
          <div className="cart-container">
            <Link to="/cart"><i className="fas fa-shopping-cart badge-container">{cart.qty > 0 &&
            <span className="badge">{cart.qty}</span>}</i></Link>
          </div>
        </div>
        <MenuMobile isOpen={isMenuOpen} handleClose={this.toggleMenu(false)}/>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  cart: state.pages.order.cart
});

export default withRouter(connect(mapStateToProps)(HeaderMobileOld));