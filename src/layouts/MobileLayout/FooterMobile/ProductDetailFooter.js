import React from 'react';
import './ProductDetailFooter.scss';
import Broadcaster from '../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../common/constants/broadcastEvents';
import {withRouter} from 'react-router-dom';

class ProductDetailFooter extends React.PureComponent {
  handleClickAddToCart = () => {
    const {match: {params: {id}}} = this.props;
    Broadcaster.broadcast(BROADCAST_EVENT.CLICK_ADD_TO_CART, {
      product_id: id,
      qty: 1
    });
  };

  handleClickBuyNow = () => {
    const {match: {params: {id}}} = this.props;
    Broadcaster.broadcast(BROADCAST_EVENT.CLICK_ADD_TO_CART, {
      product_id: id,
      qty: 1,
      buy_now: true
    });
  };

  render() {
    return (
      <div className="product-detail-footer">
        <div className="product-detail-bottom-toolbar">
          <div className="btn-add btn-warning" onClick={this.handleClickAddToCart}>
            <i className="fas fa-cart-plus"/> Thêm vào giỏ hàng
          </div>
          <div className="btn-buy btn-danger" onClick={this.handleClickBuyNow}>
            <i className="fas fa-cart-arrow-down"/> Mua ngay
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(ProductDetailFooter);
