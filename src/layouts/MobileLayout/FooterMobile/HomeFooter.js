import React from 'react';
import {publicUrl} from '../../../common/helpers';
import HTMLView from '../../../components/View/HTMLView';
import './FooterMobile.scss';

const HomeFooter = ({menus, settings}) => (
  <div className="home-footer-mobile">
    <div className="ft-logo">
      <img src={publicUrl('/assets/images/logo-st.png')}
           alt="Outlet"/>
    </div>
    <HTMLView className="company-info" html={settings['footer_company_info']}/>
    {/* <HTMLView className="certificate" html={settings['footer_business_code']}/> */}
  </div>
);

export default HomeFooter;
