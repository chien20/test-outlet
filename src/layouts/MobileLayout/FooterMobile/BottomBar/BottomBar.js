import React, {Fragment} from 'react';
import './BottomBar.scss';
import TabItem from './TabItem';
import {publicUrl} from '../../../../common/helpers';

const iconList = ['home', 'category', 'bell', 'message', 'profile'];

const PreLoad = () => (
  <div style={{display: 'none'}}>
    {
      iconList.map((icon, index) => (
        <Fragment key={index}>
          <img src={publicUrl(`/assets/images/icons/bottom-bar/${icon}.svg`)} alt="Icon"/>
          <img src={publicUrl(`/assets/images/icons/bottom-bar/${icon}-active.svg`)} alt="Icon"/>
        </Fragment>
      ))
    }
  </div> 
);

class BottomBar extends React.PureComponent {
  render() {
    const {path, pathname, notifications, messages} = this.props;
    return (
      <div className="bottom-bar">
        <TabItem
          icon="home"
          link="/"
          label="Trang chủ"
          isActive={path === '/'}
        />
        <TabItem
          icon="category"
          link="/categories"
          label="Danh mục"
          isActive={`${path}`.startsWith('/categories')}
        />
        <TabItem
          icon="bell"
          link="/user/notifications/all"
          label={(
            <Fragment>
              Thông báo
              {
                !!notifications?.length &&
                <div className="badge-number">{notifications.length}</div>
              }
            </Fragment>
          )}
          isActive={`${pathname}`.startsWith('/user/notifications/')}
        />
        <TabItem
          icon="message"
          link="/user/chat/"
          label={(
            <Fragment>
              Tin nhắn
              {
                !!messages?.length &&
                <div className="badge-number">{messages.length}</div>
              }
            </Fragment>
          )}
          isActive={`${pathname}`.startsWith('/user/chat/')}
        />
        {/* <TabItem
          icon="group"
          link="/groups"
          label="Hội nhóm"
          isActive={`${path}`.startsWith('/groups')}
        /> */}
        <TabItem
          icon="profile"
          link="/user"
          label="Tài khoản"
          isActive={path === '/user' && !`${pathname}`.startsWith('/user/chat/') && !`${pathname}`.startsWith('/user/notifications/')}
        />
        <PreLoad/>
      </div>
    );
  }
}

export default BottomBar;
