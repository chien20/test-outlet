import React from 'react';
import {publicUrl} from '../../../../common/helpers';
import {Link} from 'react-router-dom';

const TabItem = ({icon, link, isActive, label}) => (
  <Link className={`tab-item ${isActive ? 'active' : ''}`} to={link}>
    <div className="tab-icon" style={{
      backgroundImage: `url(${publicUrl(`/assets/images/icons/bottom-bar/${icon}${isActive ? '-active' : ''}.svg`)})`
    }}/>
    <label>{label}</label>
  </Link>
);

export default TabItem;