import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import HomeFooter from './HomeFooter';
import {withRouter} from 'react-router-dom';
import BottomBar from './BottomBar/BottomBar';

class FooterMobile extends React.PureComponent {
  render() {
    const {menus, settings} = this.props;
    const {match: {path}, location: {pathname}, notifications, messages} = this.props;
    return (
      <Fragment>
        {
          path !== '/:alias/p:id' && path !== '/cart' &&
          path !== '/checkout' &&
          path !== '/product-list-filter' &&
          path !== '/forgot-password' &&
          path !== '/register' &&
          path !== '/login' &&
          <BottomBar path={path} pathname={pathname} messages={messages} notifications={notifications}/>
        }
        {
          path === '/' &&
          <HomeFooter menus={menus} settings={settings}/>
        }
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  menus: state.app.menus,
  settings: state.app.settings,
  notifications: state.user.notifications,
  messages: state.user.messages,
});
export default withRouter(connect(mapStateToProps)(FooterMobile));
