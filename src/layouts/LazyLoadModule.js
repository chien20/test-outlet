import * as React from 'react';
import {Fragment} from 'react';
import PropTypes from 'prop-types';

export class LazyLoadModule extends React.Component {
  constructor() {
    super();
    this.state = {
      module: null,
      hasError: null
    };
  }

  componentDidCatch(error) {
    this.setState({hasError: error});
  }

  componentDidMount() {
    try {
      const {resolve} = this.props;
      resolve().then(module => this.setState({module: module.default}));
    } catch (error) {
      this.setState({hasError: error});
    }
  }

  render() {
    const {module: LazyComponent, hasError} = this.state;
    const {props} = this.props;

    if (hasError) {
      return hasError.message;
    }

    if (!LazyComponent) {
      return <Fragment/>
    }

    return <LazyComponent {...props}/>
  }
}

LazyLoadModule.propTypes = {
  resolve: PropTypes.func,
  props: PropTypes.object
};
