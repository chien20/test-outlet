import React from "react";
import "./ServicesFooter.scss"
import {publicUrl} from "../../../common/helpers";

class ServicesFooter extends React.PureComponent {
  render() {
    return (
      <section className="section services-footer-section">
        <div className="container">
          <div className="services-footer">
            <div className="service">
              <div className="service-icon">
                <img src={publicUrl(`/assets/images/icons/service-protection.svg`)} alt="service-icon"/>
              </div>
              <div className="service-name">Cam kết hàng chính hãng 100%</div>
            </div>
            <div className="service">
              <div className="service-icon">
                <img src={publicUrl(`/assets/images/icons/service-refund.svg`)} alt="service-icon"/>
              </div>
              <div className="service-name">Hoàn tiền nếu 100% là hàng giả</div>
            </div>
            <div className="service">
              <div className="service-icon">
                <img src={publicUrl(`/assets/images/icons/service-shipping.svg`)} alt="service-icon"/>
              </div>
              <div className="service-name">Giao hàng toàn quốc</div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default ServicesFooter;