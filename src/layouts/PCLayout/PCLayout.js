import React, {Fragment} from 'react';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import Broadcaster from '../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../common/constants/broadcastEvents';
import ServicesFooter from "./ServicesFooter/ServicesFooter";

class PCLayout extends React.PureComponent {
  componentDidMount() {
    setTimeout(() => {
      Broadcaster.broadcast(BROADCAST_EVENT.LAYOUT_LOADED);
    }, 1000);
  }

  render() {
    return (
      <Fragment>
        <Header/>
        <div className="main">
          {this.props.children}
        </div>
        <ServicesFooter/>
        <Footer/>
      </Fragment>
    );
  }
}

export default PCLayout;