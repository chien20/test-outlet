import React from 'react';
import {publicUrl} from '../../../common/helpers';
import {Link} from 'react-router-dom';
import Subscribe from './Subscribe';
import {connect} from 'react-redux';
import HTMLView from '../../../components/View/HTMLView';
import './Footer.scss';
import HyperLink from '../../../components/HyperLink/HyperLink';

class Footer extends React.PureComponent {
  goToBCT = () => {
    const a = document.createElement('a');
    a.href = "http://online.gov.vn/Home/WebDetails/71346";
    a.target = "_blank";
    a.click();
  }

  render() {
    const {menus, settings} = this.props;
    return (
      <footer className="footer-page">
        <div className="footer-top">
          <div className="container">
            <div className="row">
              <div className="col-3">
                {
                  menus && menus['footer_1'] &&
                  <div className="block">
                    <h3>VỀ OUTLET</h3>
                    <div className="footer-link">
                      {
                        menus['footer_1'].map((item) => (
                          <Link
                            key={item.id}
                            to={item.link}
                            target={item.target}>{item.name}</Link>
                        ))
                      }
                    </div>
                  </div>
                }
              </div>
              <div className="col-3">
                {
                  menus && menus['footer_2'] &&
                  <div className="block">
                    <h3>CHÍNH SÁCH VÀ QUY ĐỊNH</h3>
                    <div className="footer-link">
                      {
                        menus['footer_2'].map((item) => (
                          <Link
                            key={item.id}
                            to={item.link}
                            target={item.target}>{item.name}</Link>
                        ))
                      }
                    </div>
                  </div>
                }
              </div>
              <div className="col-3">
                {
                  menus && menus['footer_3'] &&
                  <div className="block">
                    <h3>HỖ TRỢ KHÁCH HÀNG</h3>
                    <div className="footer-link">
                      {
                        menus['footer_3'].map((item) => (
                          <Link
                            key={item.id}
                            to={item.link}
                            target={item.target}>{item.name}</Link>
                        ))
                      }
                    </div>
                  </div>
                }
              </div>
              <div className="col-3">
                <div className="block">
                  <h3>ĐĂNG KÝ NHẬN THÔNG TIN</h3>
                  <Subscribe/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-mid">
          <div className="container">
            <div className="footer-mid-content">
              <div className="company-info">
                <h3>Công Ty Cổ Phần SmileTech Công Nghệ Số</h3>
                <HTMLView className="address" html={settings['footer_company_info']}/>
                {/* <HTMLView className="mo" html={settings['footer_business_code']}/> */}
              </div>
              <div className="dkbct">
                <HyperLink onClick={this.goToBCT}>
                  <img alt="" title="" src={publicUrl(`assets/images/logoCCDV.png`)}/>
                </HyperLink>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="container">
            <div className="copyright">Copyright © 2021 - Bản quyền thuộc Outlet.vn</div>
          </div>
        </div>
      </footer>
    )
  }
}

const mapStateToProps = (state) => ({
  menus: state.app.menus,
  settings: state.app.settings
});

export default connect(mapStateToProps)(Footer);
