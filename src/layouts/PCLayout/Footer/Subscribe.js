import React from 'react';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {subscribeEmailAPI} from '../../../api/users';
import './Subscribe.scss';

class Subscribe extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      isSubmitting: false,
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  submit = () => {
    const {email} = this.state;
    this.setState({
      isSubmitting: true
    });
    subscribeEmailAPI({email}).then(() => {
      this.setState({
        isSubmitting: false
      });
      showAlert({
        type: 'success',
        message: `Đăng ký thành công!`
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
      this.setState({
        isSubmitting: false
      });
    });
  };

  render() {
    const {name, email, isSubmitting} = this.state;
    return (
      <div className="newsletter-box">
        <div className="subscription">
          <input
            type="text"
            className="form-control rounded-0"
            value={name}
            onChange={handleInputTextChanged('name', this.setData)}
            placeholder="Họ tên"
          />
          <input
            type="text"
            className="form-control rounded-0"
            value={email}
            onChange={handleInputTextChanged('email', this.setData)}
            placeholder="Địa chỉ email"
          />
          <button
            className="btn btn-red btn-rounded"
            disabled={isSubmitting}
            onClick={this.submit}
          >
            Đăng ký
          </button>
        </div>
      </div>
    )
  }
}

export default Subscribe;
