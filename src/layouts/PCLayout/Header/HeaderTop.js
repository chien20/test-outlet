import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {openModal} from '../../../redux/actions/modals/modals';
import {Link} from 'react-router-dom';
import HyperLink from '../../../components/HyperLink/HyperLink';
import Avatar from '../../../components/Avatar/Avatar';
import iconBell from '../../../assets/images/icons/icon-bell-white.svg';
import iconPlayStore from '../../../assets/images/icons/icon-play-store-white.svg';
import iconAppStore from '../../../assets/images/icons/icon-app-store-white.svg';

const underConstruction = (event) => {
  event.preventDefault();
  alert('Tính năng đang được phát triển');
};

const HeaderTop = ({user, auth, openLoginModal, openRegisterModal, notifications}) => {
  return (
    <div className="header-top">
      <div className="container">
        <div className="header-top-content">
          <ul className="list-links">
            <li>
              <Link to="/user/store">Bán hàng cùng Outlet</Link>
            </li>
            <li>
              Tải ứng dụng
              <HyperLink onClick={underConstruction}><img src={iconAppStore} alt="App Store" className="m-l-8"/></HyperLink>
              <HyperLink onClick={underConstruction}><img src={iconPlayStore} alt="Play Store" className="m-l-8"/></HyperLink>
            </li>
          </ul>
          <ul className="list-links">
            <li>
              <Link to="/user/notifications">
                <div className="notification-icon-wrap m-r-8">
                  <img src={iconBell} alt="Notifications"/>
                  {
                    !!notifications?.length &&
                    <div className="badge-number small">{notifications.length}</div>
                  }
                </div> Thông báo
              </Link>
            </li>
            {
              !auth.isAuthenticated &&
              <Fragment>
                <li>
                  <HyperLink onClick={openRegisterModal}>Đăng ký</HyperLink>
                </li>
                <li>
                  <HyperLink onClick={openLoginModal}>Đăng nhập</HyperLink>
                </li>
              </Fragment>
            }
            {
              auth.isAuthenticated && user &&
              <Fragment>
                <li>
                  <Link to="/user/profile" className="user-link">
                    <Avatar src={user.avatar || ''} size={24}/> {user.full_name || ''}
                  </Link>
                </li>
                {/*<li>*/}
                {/*<HyperLink onClick={logout}>Đăng xuất</HyperLink>*/}
                {/*</li>*/}
              </Fragment>
            }
          </ul>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.user.info,
  notifications: state.user.notifications,
  auth: state.user.auth,
});

const mapDispatchToProps = (dispatch) => ({
  openRegisterModal: () => dispatch(openModal('register')),
  openLoginModal: () => dispatch(openModal('login')),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderTop);
