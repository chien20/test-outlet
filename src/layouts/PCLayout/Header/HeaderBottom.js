import React from 'react';
import TopCategoryMenu from './TopCategoryMenu';
import {Link} from 'react-router-dom';
import HyperLink from '../../../components/HyperLink/HyperLink';

const underConstruction = (event) => {
  event.preventDefault();
  alert('Tính năng đang được phát triển');
};

class HeaderBottom extends React.PureComponent {
  render() {
    return (
      <div className="header-bottom">
        <div className="container">
          <div className="e-row row-wrap">
            <div className="col-left">
              <TopCategoryMenu/>
            </div>
            <div className="col-main">
              <ul className="list-links">
                <li>
                  <Link to="/groups/">Hội nhóm</Link>
                </li>
                <li>
                  <Link to="/products/flash-sale">Khuyến mãi mỗi ngày</Link>
                </li>
                <li>
                  <Link to="/products/best-selling">Hàng bán chạy</Link>
                </li>
                <li>
                  <Link to="/products/history">Sản phẩm đã xem</Link>
                </li>
                <li>
                  <Link to="/user/orders">Theo dõi đơn hàng</Link>
                </li>
                <li>
                  <Link to="/help-center">Trợ giúp</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default HeaderBottom;
