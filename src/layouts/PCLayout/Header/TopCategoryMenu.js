import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import {imageUrl} from '../../../common/helpers';
import './TopCategoryMenu.scss';

const MenuItem = ({item, depth = 0}) => {
  const hasChild = !!(item.children && item.children.length);
  return (
    <li>
      <Link to={`/${item.slug}/c${item.id}`}>
        {depth === 0 && item.icon_url && <img src={imageUrl(item.icon_url)} alt=""/>} {item.name}
      </Link>
      {
        hasChild &&
        (
          <ul>
            {
              item.children.map((child, childIndex) => (
                <MenuItem key={childIndex} item={child} depth={depth + 1}/>
              ))
            }
          </ul>
        )
      }
    </li>
  )
};

class TopCategoryMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    }
  }

  componentWillMount() {
    this.checkVisible(this.props);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.location !== this.props.location) {
      this.checkVisible(nextProps);
    }
  }

  isHomePage = (props) => {
    const {location: {pathname}} = props;
    return pathname === '/';
  };

  checkVisible = (props) => {
    if (this.isHomePage(props)) {
      this.setState({
        isOpen: true
      });
    } else {
      this.setState({
        isOpen: false
      });
    }
  };

  toggleMenu = () => {
    if (this.isHomePage(this.props)) {
      return;
    }
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  onMouseEnter = () => {
    if (this.isHomePage(this.props)) {
      return;
    }
    this.setState({
      isOpen: true
    });
  };

  onMouseLeave = () => {
    if (this.isHomePage(this.props)) {
      return;
    }
    this.setState({
      isOpen: false
    });
  };

  render() {
    const {category} = this.props;
    const {isOpen} = this.state;
    const isHomePage = this.isHomePage(this.props);
    return (
      <Fragment>
        <div
          className={`category-wrapper ${isOpen ? 'open' : 'hidden'}`}
          onMouseLeave={this.onMouseLeave}>
          <div
            className="category-button"
            onClick={this.toggleMenu}
            onMouseEnter={this.onMouseEnter}>
            <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
              <rect x="1.5" y="1" width="7.15789" height="7.15789" rx="2" stroke="white" strokeWidth="1.8"/>
              <rect x="11.3418" y="1" width="7.15789" height="7.15789" rx="2" stroke="white" strokeWidth="1.8"/>
              <rect x="1.5" y="10.842" width="7.15789" height="7.15789" rx="2" stroke="white" strokeWidth="1.8"/>
              <rect x="11.3418" y="10.842" width="7.15789" height="7.15789" rx="2" stroke="white" strokeWidth="1.8"/>
            </svg>
            <span>Danh mục sản phẩm</span>
          </div>
          <div className="product-category-menu">
            <ul>
              {
                category.map((item, index) => (
                  <MenuItem key={index} item={item}/>
                ))
              }
            </ul>
          </div>
        </div>
        {
          !isHomePage && <div className="category-menu-backdrop"/>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  category: state.common.category.tree
});

export default withRouter(connect(mapStateToProps)(TopCategoryMenu));
