import React from 'react';
import {connect} from 'react-redux';
import {imageUrl, publicUrl} from '../../../common/helpers';
import {Link} from 'react-router-dom';
import history from '../../../common/utils/router/history';
import iconSearch from '../../../assets/images/icons/icon-search.svg';
import iconCart from '../../../assets/images/icons/icon-cart-white.svg';
import iconHeart from '../../../assets/images/icons/icon-heart-white.svg';
import HyperLink from '../../../components/HyperLink/HyperLink';
import {convertToSlug, numberAsCurrentcy} from "../../../common/helpers/format";
import NoData from '../../../components/NoData/NoData';

const underConstruction = (event) => {
  event.preventDefault();
  alert('Tính năng đang được phát triển');
};

class HeaderMid extends React.PureComponent {
  state = {
    keyword: '',
  };

  onKeywordChange = (e) => {
    this.setState({
      keyword: e.target.value || '',
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const {keyword} = this.state;
    if (!keyword) {
      return;
    }
    history.push('/tim-kiem/' + encodeURI(keyword));
  };

  render() {
    const {cart} = this.props;
    const {keyword} = this.state;
    return (
      <div className="header-mid">
        <div className="container header-mid-container">
          <div className="e-row align-items-center">
            <div className="col-left">
              {/* <Link to="/" className="logo"><img src={publicUrl('/assets/images/v2/logo.png')} alt="Eplaza"/></Link> */}
              <Link style={{paddingLeft: '50px', paddingBottom: '10px'}} to="/" className="logo"><img style={{transform: 'scale(2.3)'}} src={publicUrl('/assets/images/logo/outlet-white.svg')} alt="OutLet"/></Link>
            </div>
            <div className="col-main">
              <div className="header-mid-main-content">
                <form className="search-box" onSubmit={this.handleSearch}>
                  <input
                    type="text"
                    value={keyword}
                    maxLength="128"
                    onChange={this.onKeywordChange}
                    placeholder="Tìm sản phẩm, danh mục hoặc thương hiệu mong muốn"
                  />
                  <img src={iconSearch} alt="Search"/>
                  <button type="submit"/>
                </form>
                <div className="icon-buttons">
                  <div className="icon-button favorite">
                    <HyperLink onClick={underConstruction} title="Yêu thích">
                      <img src={iconHeart} alt="Heart"/>
                    </HyperLink>
                  </div>
                  <div className="icon-button cart">
                    <Link to="/cart" title="Giỏ hàng">
                      <img src={iconCart} alt="Cart"/>{cart.qty > 0 && <span className="qty">{cart.qty}</span>}
                    </Link>
                    {
                      cart.qty === 0 &&
                      <div className="quick-view-cart">
                        <NoData
                          title="Không có sản phẩm"
                          description="Không có sản phẩm nào trong giỏ hàng của bạn."
                        >
                          <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
                        </NoData>
                      </div>
                    }
                    {
                      cart.qty > 0 &&
                      <div className="quick-view-cart">
                        <ul className="cart-detail">
                          {
                            cart.data.map((item, index) => {
                              let imgUrl = null;
                              if (item.product.images && item.product.images.length) {
                                imgUrl = imageUrl(item.product.images[0].thumbnail_url);
                              }
                              const bgImage = {};
                              if (imgUrl) {
                                bgImage.backgroundImage = `url(${imgUrl})`;
                              }
                              const slug = item.product.slug ? item.product.slug : convertToSlug(item.product.name);
                              const productUrl = `/${slug}/p${item.product.id}`;
                              return (
                                <li key={index}>
                                  <Link className="cart-item" to={productUrl}>
                                    <div className="product-avatar" style={bgImage}/>
                                    <div className="product-info">
                                      <div className="product-name">
                                        {item.display_name}
                                      </div>
                                      <div className="product-price">
                                        {numberAsCurrentcy(item.display_price)} đ
                                        {
                                          item.qty > 1 &&
                                          <small> x {item.qty} </small>
                                        }
                                      </div>
                                    </div>
                                  </Link>
                                </li>
                              );
                            })
                          }
                        </ul>
                        <div className="total">
                          <p>Tổng cộng: <strong>{numberAsCurrentcy(cart.sub_total)} đ</strong></p>
                        </div>
                        <div className="d-flex-center">
                          <Link to={`/cart`} className="btn">
                            Xem giỏ hàng và thanh toán
                          </Link>
                        </div>
                      </div>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.pages.order.cart
});

export default connect(mapStateToProps)(HeaderMid);
