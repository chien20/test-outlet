import React from 'react';
import {withRouter} from 'react-router-dom';
import HeaderTop from './HeaderTop';
import HeaderMid from './HeaderMid';
import HeaderBottom from './HeaderBottom';
import './Header.scss';

class Header extends React.PureComponent {
  render() {
    return (
      <div className="header">
        <HeaderTop/>
        <HeaderMid/>
        <HeaderBottom/>
      </div>
    )
  }
}

export default withRouter(Header);
