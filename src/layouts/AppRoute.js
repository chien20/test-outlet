import React, {Suspense} from 'react';
import {Route} from 'react-router-dom';

/**
 * App route with layout
 * @param Component
 * @param Layout
 * @param Fallback fallback component for lazy load
 * @param others
 * @returns {*}
 * @constructor
 */
const AppRoute = ({component: Component, layout: Layout, fallback: Fallback, ...others}) => (
  <Route {...others} render={props => (
    <Layout>
      {
        Fallback &&
        <Suspense fallback={Fallback}>
          <Component {...props} />
        </Suspense>
      }
      {
        !Fallback &&
        <Component {...props} />
      }
    </Layout>
  )}/>
);

export default AppRoute;
