import React, {Suspense} from 'react';
import {Redirect, Route} from 'react-router-dom';
import Store from '../redux/store';

/**
 * App route with layout
 * @param Component
 * @param Layout
 * @param Fallback fallback component for lazy load
 * @param others
 * @returns {*}
 * @constructor
 */
const PrivateAppRoute = ({component: Component, layout: Layout, fallback: Fallback, ...others}) => {
  const render = (props) => {
    const user = Store.getState().user;
    if (!user.auth.isAuthenticated) {
      if (user.auth.refresh_token) {
        return null;
      }
      return <Redirect to={`/login?redirectTo=${encodeURIComponent(others.location.pathname + others.location.search)}`}/>;
    }
    if (!user.info) {
      return null;
    }
    return (
      <Layout>
        {
          Fallback &&
          <Suspense fallback={Fallback}>
            <Component {...props} />
          </Suspense>
        }
        {
          !Fallback &&
          <Component {...props} />
        }
      </Layout>
    );
  };
  return (
    <Route {...others} render={render}/>
  );
};

export default PrivateAppRoute;
