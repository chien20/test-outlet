import React from 'react';
import {isMobile} from '../common/helpers/browser';
import {LazyLoadModule} from './LazyLoadModule';

const AppLayout = ({...props}) => {
  if (isMobile) {
    return <LazyLoadModule resolve={() => (import('./MobileLayout/MobileLayout'))} props={props}/>
  }
  return (
    <LazyLoadModule resolve={() => (import('./PCLayout/PCLayout'))} props={props}/>
  );
};

export default AppLayout;
