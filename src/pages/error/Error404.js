import React from 'react';
import Broadcaster from '../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../common/constants/broadcastEvents';
import {publicUrl} from '../../common/helpers';
import {Link} from 'react-router-dom';
import './Error404.scss';

class Error404 extends React.PureComponent {
  componentDidMount() {
    setTimeout(() => {
      Broadcaster.broadcast(BROADCAST_EVENT.LAYOUT_LOADED);
    });
  }

  render() {
    return (
      <div className="error-page error-404">
        <div className="container">
          <div className="error-container">
            <div className="error-col error-img">
              <img src={publicUrl('/assets/images/monitor-sad.svg')} alt={`404 error`}/>
            </div>
            <div className="error-col error-text">
              <h1>Trang không tồn tại</h1>
              <p>Xin lỗi, liên kết mà bạn vừa nhấp vào hiện không còn tồn tại hoặc đã bị hỏng!<br/>
                Bạn có thể quay trở lại trang trước đó hoặc về trang chủ của chúng tôi bằng cách click vào nút dưới đây.
              </p>
              <div className="error-buttons">
                <Link to="/" className="btn btn-primary"><i className="fas fa-home"/> Trang chủ</Link>
                <Link to="/contact" className="btn btn-secondary"><i className="fas fa-envelope"/> Liên hệ chúng tôi</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Error404;