import React, {Fragment} from "react";
import "./RegisterShop.scss";
import {Helmet} from "react-helmet";
import {Formik} from 'formik';
import {registerShop} from '../../api/utils';
import {showAlert} from '../../common/helpers';
import history from '../../common/utils/router/history';

const initialValues = {
  business_name: '',
  business_tax_code: '',
  business_issued_date: '',
  business_issued_by: '',
  business_website: '',
  address: '',
  phone: '',
  email: '',
  legal_name: '',
  role: '',
  fields: '',
  products: '',
  coupon: '',
  register: {
    open_shop: true,
    co_operate: true,
    group: true,
    donate: false,
    donate_value: '',
  },
  request: {
    advisory_law: false,
    advisory_protection: false,
    create_website: false,
    global_business: false,
  },
};

class RegisterShop extends React.PureComponent {
  handleSubmit = (values, {setSubmitting}) => {
    registerShop(values).then(() => {
      showAlert({
        type: 'success',
        message: 'Đăng ký thành công! Chờ xác nhận.'
      });
      setTimeout(() => {
        history.push('/');
      }, 1000);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: error.message,
      });
      setSubmitting(false);
    });
  };

  render() {
    return (
      <Fragment>
        <Helmet>
          <title>Đăng ký tham gia</title>
        </Helmet>
        <div className="container">
          <div className="register-shop-page">
            <div className="section-heading">
              <h1>Đăng ký tham gia</h1>
            </div>
            <div className="section-inner">
              <Formik
                initialValues={initialValues}
                onSubmit={this.handleSubmit}
              >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                  }) => (
                  <form className="register-shop-form" onSubmit={handleSubmit}>
                    <div className="form-group">
                      <label>Tên DN/Hộ KD:</label>
                      <input
                        className="form-control"
                        type="text"
                        name="business_name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.business_name}
                      />
                    </div>
                    <div className="d-flex-1-1">
                      <div className="form-group">
                        <label>Đăng ký KD số:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="business_tax_code"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.business_tax_code}
                        />
                      </div>
                      <div className="form-group">
                        <label>Ngày cấp:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="business_issued_date"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.business_issued_date}
                        />
                      </div>
                    </div>
                    <div className="d-flex-1-1">
                      <div className="form-group">
                        <label>Nơi cấp:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="business_issued_by"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.business_issued_by}
                        />
                      </div>
                      <div className="form-group">
                        <label>Website:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="business_website"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.business_website}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>Địa chỉ:</label>
                      <textarea
                        className="form-control"
                        rows="3"
                        name="address"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.address}
                      />
                    </div>
                    <div className="d-flex-1-1">
                      <div className="form-group">
                        <label>Điện thoại:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="phone"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.phone}
                        />
                      </div>
                      <div className="form-group">
                        <label>Email:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                      </div>
                    </div>
                    <div className="d-flex-1-1">
                      <div className="form-group">
                        <label>Đại diện:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="legal_name"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.legal_name}
                        />
                      </div>
                      <div className="form-group">
                        <label>Chức vụ:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="role"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.role}
                        />
                      </div>
                    </div>
                    <div className="d-flex-1-1">
                      <div className="form-group">
                        <label>Lĩnh vực HĐ:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="fields"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.fields}
                        />
                      </div>
                      <div className="form-group">
                        <label>Sản phẩm:</label>
                        <input
                          className="form-control"
                          type="text"
                          name="products"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.products}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label>Mã quà tặng:</label>
                      <input
                        className="form-control"
                        type="text"
                        name="coupon"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.coupon}
                      />
                    </div>
                    <div className="form-group checkbox">
                      <label>Đăng ký:</label>
                      <div className="checkbox-list">
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="register.open_shop"
                            onChange={handleChange}
                            checked={values.register.open_shop}
                            id="open_shop"
                            disabled={true}
                          />
                          <label htmlFor="open_shop">Mở gian hàng trên Outlet.vn</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="register.co_operate"
                            onChange={handleChange}
                            checked={values.register.co_operate}
                            id="co_operate"
                          />
                          <label htmlFor="co_operate">Tham gia hợp tác kích cầu thiêu dùng trên Outlet.vn</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="register.group"
                            onChange={handleChange}
                            checked={values.register.group}
                            id="group"
                          />
                          <label htmlFor="group">Tham gia bán chéo sản phẩm, giao thương kinh tế chia sẻ</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            className="t-8"
                            name="register.donate"
                            onChange={handleChange}
                            checked={values.register.donate}
                            id="donate"
                          />
                          <label htmlFor="donate">Đầu tư cộng đồng cho Outlet só tiền</label>
                          <input
                            className="form-control"
                            type="number"
                            name="register.donate_value"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.register.donate_value}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="form-group checkbox">
                      <label>Yêu cầu:</label>
                      <div className="checkbox-list">
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="request.advisory_law"
                            onChange={handleChange}
                            checked={values.request.advisory_law}
                            id="advisory_law"
                          />
                          <label htmlFor="advisory_law">Tư vấn luật từ Công ty Luật TNHH Quốc tế ICT (ICTLAW)</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="request.advisory_protection"
                            onChange={handleChange}
                            checked={values.request.advisory_protection}
                            id="advisory_protection"
                          />
                          <label htmlFor="advisory_protection">Tư vấn bảo hộ nhãn hiệu, sáng chế, kiểu dáng công nghiệp</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="request.create_website"
                            onChange={handleChange}
                            checked={values.request.create_website}
                            id="create_website"
                          />
                          <label htmlFor="create_website">Mở trang web và địa chỉ email miễn phí</label>
                        </div>
                        <div className="checkbox-item custom-checkbox-2">
                          <input
                            type="checkbox"
                            name="request.global_business"
                            onChange={handleChange}
                            checked={values.request.global_business}
                            id="global_business"
                          />
                          <label htmlFor="global_business">Hỗ trợ xúc tiến thương mại quốc tế và
                            www.globalbusiness.vn</label>
                        </div>
                      </div>
                    </div>
                    <div className="text-center">
                      <button
                        className="btn  btn-register"
                        disabled={isSubmitting}
                        type="submit"
                      >
                        Đăng ký
                      </button>
                    </div>
                  </form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default RegisterShop;
