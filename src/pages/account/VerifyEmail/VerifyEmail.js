import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import qs from 'qs';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import {verifyEmailAPI} from '../../../api/users';
import successIcon from '../../../assets/images/icons/icon_success.png';
import errorIcon from '../../../assets/images/icons/icon-error.svg';

class VerifyEmail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
      result: null,
      error: null
    };
  }

  componentDidMount() {
    this.verifyEmail();
  }

  verifyEmail = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    this.setState({
      isFetching: true,
      error: null,
      result: null
    });
    verifyEmailAPI(params).then(res => {
      this.setState({
        isFetching: false,
        result: res.data
      });
    }).catch(error => {
      let errorMessage = error.message;
      if (error.error_fields) {
        error.error_fields.forEach(item => {
          if (`${item.message}`.indexOf('expired') >= 0) {
            errorMessage = 'Liên kết đã hết hạn';
          }
        });
      }
      this.setState({
        isFetching: false,
        result: false,
        error: errorMessage
      });
    });
  };

  render() {
    const {isFetching, result, error} = this.state;
    return (
      <Fragment>
        <Helmet>
          <title>Xác nhận email</title>
        </Helmet>
        <Breadcrumb>
          <BreadcrumbItem text="Xác nhận email" isActive={true}/>
        </Breadcrumb>
        <div className="verify-email-page common-page">
          <div className="container">
            {
              isFetching &&
              <SkeletonTheme color="#e6e6e6" highlightColor="#f0f0f0">
                <Skeleton height={100}/>
                <Skeleton height={25} count={5}/>
              </SkeletonTheme>
            }
            {
              !isFetching &&
              <Fragment>
                {
                  result &&
                  <div className="no-data">
                    <img src={successIcon} alt={`Thành công`}/>
                    <p className="title">Xác thực email thành công!</p>
                    <p>Email của bạn đã được xác thực, bây giờ bạn có thể đăng nhập.</p>
                  </div>
                }
                {
                  !result &&
                  <div className="no-data">
                    <img src={errorIcon} alt={`Lỗi`}/>
                    <p className="title">Lỗi!</p>
                    <p>{error}</p>
                  </div>
                }
              </Fragment>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

VerifyEmail.propTypes = {};

export default VerifyEmail;
