import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import qs from 'qs';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import {handleInputTextChanged, publicUrl, showAlert} from '../../common/helpers';
import {loginAPI, loginByFacebookAPI, loginByGoogleAPI} from '../../api/users';
import {getCurrentUserSuccessAC} from '../../redux/actions/user/info';
import {loginSuccess} from '../../redux/actions/user';
import HyperLink from '../../components/HyperLink/HyperLink';
import history from '../../common/utils/router/history';
import {openModal} from '../../redux/actions/modals/modals';
import './Login.scss';
import {isMobile} from '../../common/helpers/browser';
import HeaderMobileLogin from '../../components/HeaderMobile/HeaderMobileLogin';
import ButtonFacebookLogin from '../../components/Button/ButtonFacebookLogin';
import ButtonGoogleLogin from '../../components/Button/ButtonGoogleLogin';

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email_phone: '',
        password: '',
        fb_access_token: '',
        google_id: '',
        google_access_token: ''
      },
      isLoading: false,
      isLoginSuccess: null,
      isForgotPassword: false
    };
    this.formEl = React.createRef();
  }

  componentDidMount() {
    this.checkAuthState();
  }

  componentDidUpdate() {
    this.checkAuthState();
  }

  checkAuthState = () => {
    const {auth, location: {search}} = this.props;
    if (auth && auth.isAuthenticated) {
      if (search && typeof search === 'string') {
        const params = qs.parse(search || '', {ignoreQueryPrefix: true});
        if (params && params.redirectTo) {
          history.replace(decodeURIComponent(params.redirectTo));
          return;
        }
      }
      history.push('/');
    }
  };

  /**
   * Set form data
   * @param data
   */
  setData = (data) => {
    if (!data || typeof data !== 'object') {
      return;
    }
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  
  handleLogin = () => {
    const {data: {email_phone, password}} = this.state;
    const {handleLoginSuccess, handleGetCurrentUserSuccess} = this.props;
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    loginAPI({
      email_phone,
      password
    }).then(res => {
      this.setState({
        isLoading: false,
        isLoginSuccess: true
      });
      showAlert({
        message: 'Đăng nhập thành công!',
        type: 'success'
      });
      handleLoginSuccess(res.data.oauth);
      handleGetCurrentUserSuccess(res.data.info, true);
    }).catch(error => {
      this.setState({
        isLoading: false,
        isLoginSuccess: false
      });
      if (error && error.error === 'invalid_grant') {
        showAlert({
          message: `Đăng nhập không thành công: Sai tên truy cập hoặc mật khẩu`,
          type: 'error'
        });
      } else {
        showAlert({
          message: `Đăng nhập thất bại!`,
          type: 'error'
        });
      }
    });
  };

  handleClickFacebookLogin = () => {
    console.log('a');
  };

  handleFacebookLogin = (res) => {
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    if (res?.accessToken) {
      loginByFacebookAPI({
        access_token: res.accessToken,
      })
      .then(this.handleLoginResponse)
      .catch(this.handleLoginResponse);
    } else {
      this.handleLoginResponse();
    }
  };

  handleGoogleLogin = (res) => {
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    if (res?.accessToken) {
      loginByGoogleAPI({
        access_token: res.accessToken,
      })
      .then(this.handleLoginResponse)
      .catch(this.handleLoginResponse);
    } else {
      this.handleLoginResponse();
    }
  };

  
  handleLoginResponse = (res) => {
    const {handleClose, handleLoginSuccess, handleGetCurrentUserSuccess} = this.props;
    if (res?.data?.oauth) {
      this.setState({
        isLoading: false,
        isLoginSuccess: true
      });
      showAlert({
        message: 'Đăng nhập thành công!',
        type: 'success'
      });
      handleLoginSuccess(res.data.oauth);
      handleGetCurrentUserSuccess(res.data.info, true);
      handleClose();
    } else {
      this.setState({
        isLoading: false,
        isLoginSuccess: false
      });
      if (res?.error === 'invalid_grant') {
        showAlert({
          message: `Đăng nhập không thành công: Sai tên truy cập hoặc mật khẩu`,
          type: 'error'
        });
      } else {
        showAlert({
          message: `Đăng nhập thất bại!`,
          type: 'error'
        });
      }
    }
  };

  forgotPassword = () => {
    this.props.openLoginModal({isForgotPassword: true});
  };

  register = () => {
    this.props.openRegisterModal();
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleLogin();
  };

  render() {
    const {data, isLoading} = this.state;
    const isBtnLoginDisabled = isLoading || !data.email_phone || !data.password;
    return (
      <Fragment>
        <Helmet>
          <title>Đăng nhập</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Đăng nhập" isActive={true}/>
          </Breadcrumb>
        }
        {
          isMobile &&
          <HeaderMobileLogin/>
        }
        <div className="login-page common-page">
          <div className="container">
            {
              isMobile &&
              <div className="login-header">
                <h1 className="title">Đăng nhập tài khoản</h1>
                <p className="description">Nhập thông tin vào các trường dưới đây</p>
              </div>
            }
            <form onSubmit={this.submit} ref={this.formEl} className="login-form">
              <div className="form-group">
                <input type="text"
                       value={data.email_phone}
                       onChange={handleInputTextChanged('email_phone', this.setData)}
                       className="form-control"
                       required={true}
                       placeholder="Email"
                       />
                <div className="invalid-feedback">
                  Vui lòng nhập email hoặc số điện thoại
                </div>
              </div>
              <div className="form-group">
                <input type="password"
                       value={data.password}
                       onChange={handleInputTextChanged('password', this.setData)}
                       className="form-control"
                       required={true}
                       placeholder="Mật Khẩu"
                       />
                <div className="invalid-feedback">
                  Vui lòng nhập mật khẩu
                </div>
              </div>
              {
                isMobile &&
                <Fragment>
                  <p className="text-right text-active">
                    <Link to="forgot-password">
                      Quên mật khẩu
                    </Link>
                  </p>
                  <div className="action">
                    <button
                        type="submit"
                        className="btn btn-primary"
                        onClick={this.submit}
                        disabled={isBtnLoginDisabled}>
                      Đăng nhập
                    </button>
                  </div>
                  <p className="text-center others-login">Hoặc đăng nhập bằng</p>
                  <div className="by-socials text-center">
                    <ButtonGoogleLogin
                      isDisabled={isLoading}
                      onSuccess={this.handleGoogleLogin}
                      onFailure={this.handleGoogleLogin}
                    />
                    <ButtonFacebookLogin
                      isDisabled={isLoading}
                      onClick={this.handleClickFacebookLogin}
                      callback={this.handleFacebookLogin}
                    />
                  </div>
                  <p className="text-center">
                      <span className="color-black">Bạn chưa có tài khoản? </span> 
                      <Link to="register">
                        <span className="text-active">Đăng ký</span>
                      </Link>
                  </p>
                </Fragment>
              }
              {
                !isMobile &&
                <Fragment>
                  <p><Link to="forgot-password" className="text-primary">Quên mật khẩu?</Link></p>
                  <p>
                    <HyperLink className="text-primary" onClick={this.register}>
                      Bạn chưa có tài khoản? Click để Đăng ký!
                    </HyperLink>
                  </p>
                  <div className="action">
                    <button
                        type="submit"
                        className="btn btn-primary"
                        onClick={this.submit}
                        disabled={isBtnLoginDisabled}>
                      Đăng nhập
                    </button>
                  </div>
                  <p className="text-center">HOẶC</p>
                  <div className="by-socials text-center">
                    <ButtonFacebookLogin
                      isDisabled={isLoading}
                      onClick={this.handleClickFacebookLogin}
                      callback={this.handleFacebookLogin}
                    />
                    <ButtonGoogleLogin
                      isDisabled={isLoading}
                      onSuccess={this.handleGoogleLogin}
                      onFailure={this.handleGoogleLogin}
                    />
                  </div>
                </Fragment>
              }

            </form>
          </div>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  auth: state.user.auth
});

const mapDispatchToProps = (dispatch) => ({
  handleLoginSuccess: (data) => dispatch(loginSuccess(data)),
  handleGetCurrentUserSuccess: (data) => dispatch(getCurrentUserSuccessAC(data)),
  openRegisterModal: () => dispatch(openModal('register')),
  openLoginModal: (data) => dispatch(openModal('login', data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
