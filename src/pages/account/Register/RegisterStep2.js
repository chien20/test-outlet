import React from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged} from '../../../common/helpers';

class RegisterStep2 extends React.PureComponent {
  render() {
    const {data, setData} = this.props;
    return (
      <>
        <div className="form-group">
            <input type="number"
                   value={data.phone}
                   onChange={handleInputTextChanged('phone', setData)}
                   className="form-control"
                   required
                   placeholder="Số điện thoại"
                   />
          </div>
          <div className="form-group">
            <input type="number"
                   value={data.otp}
                   onChange={handleInputTextChanged('otp', setData)}
                   className="form-control"
                   required
                   placeholder=""
                   />
          </div>
      </>
    )
  }
}

RegisterStep2.propTypes = {
  setData: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default RegisterStep2;
