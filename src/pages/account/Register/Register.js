import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {Link} from 'react-router-dom';
import {isMobile} from '../../../common/helpers/browser';
import {closeModal} from '../../../redux/actions/modals/modals';
import {cleanAlertAC} from '../../../redux/actions/common';
import RegisterByEmail from './RegisterByEmail';
import HeaderMobileLogin from '../../../components/HeaderMobile/HeaderMobileLogin';
import "./Register.scss";
import RegisterByPhone from './RegisterByPhone';

class Register extends React.PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        registerBy: 'email'
      };
    }
  
    setData = (data) => {
      this.setState(data);
    };
  
    render() {
      const {registerBy} = this.state;
      return (
      <>
        <Helmet>
          <title>Đăng kí</title>
        </Helmet>
        {
          isMobile &&
          <HeaderMobileLogin/>
        }
        <div className="login-page register-page common-page">
          <div className="container">
            {
              isMobile &&
              <div className="login-header">
                <h1 className="title">Đăng kí tài khoản</h1>
                <p className="description">Nhập thông tin vào các trường dưới đây</p>
              </div>
            }
            {
                registerBy === 'email' && 
                <RegisterByEmail setData={this.setData}/>
            }
            {
                registerBy === 'phone' && 
                <RegisterByPhone setData={this.setData}/>
            }
          </div>
        </div>
      </>
    )
  }
}


const mapStateToProps = (state) => ({
    isOpen: state.modals.register.isOpen
  });
  
  const mapDispatchToProps = (dispatch) => ({
    handleClose: () => dispatch(closeModal('register')),
    cleanAlert: (containers) => dispatch(cleanAlertAC(containers))
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(Register);
