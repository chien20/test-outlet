import React, {Fragment} from 'react';
import {Button, Modal} from 'react-bootstrap';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {registerByEmailAPI} from '../../../api/users';
import {Link} from 'react-router-dom';
import history from '../../../common/utils/router/history';
import HyperLink from '../../../components/HyperLink/HyperLink';
import '../Login.scss';

class RegisterByEmail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      re_password:'',
      full_name: '',
      isSubmitting: false,
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  registerByPhone = () => {
    this.props.setData({
      registerBy: 'phone'
    });
  };

  handleSubmit = () => {
    const {email, password, re_password, full_name} = this.state;
    this.setState({
      isSubmitting: true
    });
    registerByEmailAPI({
      email,
      password,
      full_name,
      re_password
    }).then(() => {
      showAlert({
        message: 'Đăng ký thành công! Bạn có thể đăng nhập!',
        type: 'success'
      });
      this.setState({
        isSubmitting: false
      });
      history.push('/login');
    }).catch(error => {
      let message = `Lỗi! ${error.message}`;
      if (error.error_fields) {
        error.error_fields.forEach(e => {
          if (e.field_name === 'email' && e.message === 'FIELD_EXIST') {
            message = 'Email đã tồn tại!';
          }
        });
      }
      showAlert({
        message,
        type: 'error',
        timeout: 0,
        id: 'register-by-email',
        container: 'register-by-email'
      });
      this.setState({
        isSubmitting: false
      });
    });
  };

  render() {
    const {email, password, re_password, full_name, isSubmitting} = this.state;
    const isBtnSubmitDisabled = !email || !password || !full_name || isSubmitting || password !== re_password;
    return (
      <>
        <form className="login-form register-form register-email-form">
          <div className="form-group">
            <input type="text"
                   value={full_name}
                   onChange={handleInputTextChanged('full_name', this.setData)}
                   className="form-control"
                   required
                   placeholder="Họ và tên"/>
          </div>
          <div className="form-group">
            <input type="text"
                   value={email}
                   onChange={handleInputTextChanged('email', this.setData)}
                   className="form-control"
                   required
                   placeholder="Email"
                   />
          </div>
          <div className="form-group">
            <input type="password"
                   value={password}
                   onChange={handleInputTextChanged('password', this.setData)}
                   className="form-control"
                   required
                   placeholder="Mật khẩu"
                   />
          </div>
          <div className="form-group">
            <input type="password"
                   value={re_password}
                   onChange={handleInputTextChanged('re_password', this.setData)}
                   className="form-control"
                   required
                   placeholder="Nhập lại mật khẩu"
                   />
          </div>
          <p className="text-right text-active">
            <HyperLink onClick={this.registerByPhone}>Đăng ký bằng số điện thoại</HyperLink>
          </p>
          <div className="action">
            <button
                type="submit"
                className="btn btn-primary"
                onClick={this.handleSubmit}
                disabled={isBtnSubmitDisabled}>
                Đăng ký
            </button>
          </div>
          <p className="text-center">
                      <span className="color-black">Bạn đã có tài khoản? </span> 
                      <Link to="login">
                        <span className="text-active">Đăng nhập</span>
                      </Link>
                  </p>
          <p className="agree-policy">Bằng việc đăng ký, bạn đã đồng ý với <Link to="/p/dieu-khoan-dich-vu" target="_blank">Điều khoản
            dịch vụ</Link> và <Link to="/p/chinh-sach-bao-mat" target="_blank">Chính sách bảo mật</Link> của chúng tôi.</p>
        </form>
      </>
    )
  }
}

export default RegisterByEmail;
