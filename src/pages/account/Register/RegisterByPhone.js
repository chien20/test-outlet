import React, {Fragment} from 'react';
import {REGISTER_TYPES} from '../../../common/constants/app';
import {registerAPI, registerPhoneNumberAPI, verifyPhoneNumberAPI} from '../../../api/users';
import {showAlert} from '../../../common/helpers';
import RegisterStep1 from './RegisterStep1';
import RegisterStep2 from './RegisterStep2';
import {Link} from 'react-router-dom';
import HyperLink from '../../../components/HyperLink/HyperLink';
import {Button, Modal} from 'react-bootstrap';

class RegisterByPhone extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        full_name: '',
        email: '',
        register_token: '', // Required, get from #1.2
        password: '',
        fb_id: '',
        fb_access_token: '',
        google_id: '',
        google_access_token: '',
        re_password: '',
        phone: '',
        otp: ''
      },
      isSentOTP: false,
      isEmailExisted: false,
      isLoading: false,
      registerType: REGISTER_TYPES.NORMAL,
      step: 1,
      isSubmitting: false,
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.isOpen !== nextProps.isOpen && nextProps.isOpen) {
      this.setState(JSON.parse(JSON.stringify(this.defaultState)));
    }
    if (this.props.isOpen !== nextProps.isOpen && !nextProps.isOpen) {
      this.props.cleanAlert(['register-step1', 'register-step2'])
    }
  }

  /**
   * Set form data
   * @param data
   */
  setData = (data) => {
    if (!data || typeof data !== 'object') {
      return;
    }
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  /**
   * Handle user click continue button
   */
  handleContinue = () => {
    const {step} = this.state;
    if (step === 1) {
      this.handleVerifyPhone();
      return;
    }
    this.handleRegister();
  };

  handleSendOTP = () => {
    this.setState({
      isLoading: true,
      isSentOTP: false
    });
    const {data: {phone}} = this.state;
    registerPhoneNumberAPI({phone}).then(() => {
      this.setState({
        isSentOTP: true,
        isLoading: false,
      });
      showAlert({
        type: 'success',
        message: 'Đã gửi mã xác nhận, vui lòng kiểm tra điện thoại!',
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    });
  };

  handleVerifyPhone = () => {
    const {data: {phone, otp}} = this.state;
    this.setState({
      isLoading: true,
      isSubmitting: true
    });
    verifyPhoneNumberAPI({
      phone,
      otp
    }).then(res => {
      this.setState(prevState => ({
        isLoading: false,
        data: {
          ...prevState.data,
          register_token: res.data.register_token
        },
        step: 2
      }));
    }).catch(error => {
      this.setState({
        isLoading: false,
        isSubmitting: false,
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error',
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    });
  };

  handleRegister = () => {
    const {data} = this.state;
    const {handleClose} = this.props;
    this.setState({
      isLoading: true
    });
    registerAPI(data).then(() => {
      this.setState({
        isLoading: false
      });
      handleClose();
      showAlert({
        message: 'Đăng ký thành công! Bạn có thể đăng nhập!',
        type: 'success'
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error',
        timeout: 0,
        id: 'register-step2',
        container: 'register-step2'
      });
    });
  };

  registerByEmail = () => {
    this.props.setData({
      registerBy: 'email'
    });
  };

  handleSubmit = () => {
    const {handleClose} = this.props;
    const {phone, full_name} = this.state;
    this.setState({
      isSubmitting: true
    });
    registerPhoneNumberAPI({
      phone,
      full_name
    }).then(() => {
      handleClose();
      showAlert({
        message: 'Đăng ký thành công! Bạn có thể đăng nhập!',
        type: 'success'
      });
      this.setState({
        isSubmitting: false
      });
    }).catch(error => {
      let message = `Lỗi! ${error.message}`;
      if (error.error_fields) {
        error.error_fields.forEach(e => {
          if (e.field_name === 'phone' && e.message === 'FIELD_EXIST') {
            message = 'Số điện thoại đã tồn tại!';
          }
        });
      }
      showAlert({
        message,
        type: 'error',
        timeout: 0,
        id: 'register-by-phone',
        container: 'register-by-phone'
      });
      this.setState({
        isSubmitting: false
      });
    });
  };

  render() {
    const {step, data, isSentOTP, isEmailExisted, isLoading} = this.state;
    let isBtnContinueDisabled = isLoading || isEmailExisted;
    if (step === 1) {
      if (!data.phone || !data.full_name || isLoading || isSentOTP) {
        isBtnContinueDisabled = true;
      } 
    }
    if (step === 2) {
      if (!data.full_name || !data.password || !data.re_password || data.password !== data.re_password) {
        isBtnContinueDisabled = true;
      }
    }
    return (
      <>
        <form className="login-form register-form register-phone-form">
          {
            step === 1 &&
            <RegisterStep1
              data={data}
              isSentOTP={isSentOTP}
              setData={this.setData}
              registerByEmail={this.registerByEmail}
              handleSendOTP={this.handleSendOTP}
              isLoading={isLoading}
              isBtnContinueDisabled={isBtnContinueDisabled}/>
          }
          {
            step === 2 &&
            <RegisterStep2
              data={data}
              setData={this.setData}
              isLoading={isLoading}/>
          }
          <p className="text-center">
                      <span className="color-black">Bạn đã có tài khoản? </span> 
                      <Link to="login">
                        <span className="text-active">Đăng nhập</span>
                      </Link>
                  </p>
          <p className="agree-policy">Bằng việc đăng ký, bạn đã đồng ý với <Link to="/p/dieu-khoan-dich-vu" target="_blank">Điều khoản
            dịch vụ</Link> và <Link to="/p/chinh-sach-bao-mat" target="_blank">Chính sách bảo mật</Link> của chúng tôi.</p>
        </form>
      </>
    )
  }
}

export default RegisterByPhone;
