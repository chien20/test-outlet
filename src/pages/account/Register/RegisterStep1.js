import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {registerPhoneNumberAPI} from '../../../api/users';
import {Link} from 'react-router-dom';
import HyperLink from '../../../components/HyperLink/HyperLink';




class RegisterStep1 extends React.PureComponent {
  render() {
    const {data, setData, isBtnContinueDisabled, handleSendOTP} = this.props;
    return (
      <>
          <div className="form-group">
            <input type="text"
                   value={data.full_name}
                   onChange={handleInputTextChanged('full_name', setData)}
                   className="form-control"
                   required
                   placeholder="Họ và tên"/>
          </div>
          <div className="form-group">
            <input type="number"
                   value={data.phone}
                   onChange={handleInputTextChanged('phone', setData)}
                   className="form-control"
                   required
                   placeholder="Số điện thoại"
                   />
          </div>
          <p className="text-right text-active">
                <HyperLink onClick={isBtnContinueDisabled}>Đăng ký bằng email</HyperLink>
              </p>
          <div className="action">
            <button
                type="submit"
                className="btn btn-primary"
                onClick={handleSendOTP}
                disabled={isBtnContinueDisabled}>
                Gửi mã xác nhận
            </button>
          </div>
      </>
    )
  }
}

RegisterStep1.propTypes = {
  setData: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default RegisterStep1;
