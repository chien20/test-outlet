import React from 'react';
import {Helmet} from 'react-helmet/es/Helmet';
import {Link} from 'react-router-dom';
import {publicUrl} from '../../../common/helpers';
import {isMobile} from '../../../common/helpers/browser';
import HeaderMobileLogin from '../../../components/HeaderMobile/HeaderMobileLogin';
import {forgotPasswordAPI} from '../../../api/users';
import "./ForgotPassword.scss"

function ForgotPassword() {
  const [isSuccess, setIsSuccess] = React.useState();
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [receiver, setReceiver] = React.useState('');

  const handleInputChange = (event) => {
    setReceiver(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitting(true);
    forgotPasswordAPI({
      email_phone: receiver,
    }).then(function () {
      setIsSubmitting(false);
      setIsSuccess(true);
    }).catch(function () {
      setIsSubmitting(false);
      setIsSuccess(false);
    });
  };

  let styleFailed = {}
    if (isSuccess === false) {
      styleFailed = { borderColor: '#F01F0E', display: 'flex'}
  }
  let styleSuccess = {}
    if (isSuccess === true) {
      styleSuccess = { display: 'flex'}
  }

  const isBtnLoginDisabled = !receiver || isSubmitting;

  return (
    <>
      <Helmet>
        <title>Quên mật khẩu</title>
      </Helmet>
      {
        isMobile &&
        <HeaderMobileLogin/>
      }
      <div className="login-page common-page forgot-password">
        <div className="container">
          {
            isMobile &&
            <div className="login-header">
              <h1 className="title">Quên mật khẩu</h1>
              <p className="description">Điền email đăng ký của bạn.Chúng tôi sẽ gửi link tạo mật khẩu mới qua email này.</p>
            </div>
          }
          <form onSubmit={handleSubmit} className="login-form">
              <div style={styleFailed} className="form-group">
                <input type="text" className="form-control" value={receiver} onChange={handleInputChange} placeholder="Email"/>
                <img style={styleFailed} className="icon failed" src={publicUrl(`./assets/images/icons/icon-failed.svg`)} alt="icon"/>
                <img style={styleSuccess} className="icon success" src={publicUrl(`./assets/images/icons/icon-success.svg`)} alt="icon"/>
              </div>
            {
              isSuccess === true &&
              <span className="forgot-alert success">Mật khẩu đã được gửi đến email của bạn</span>
            }
            {
              isSuccess === false &&
                <span className="forgot-alert failed">Định dạng email không đúng</span>
            }
            <div className="action">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={isBtnLoginDisabled}
              >
                Gửi
              </button>
            </div>
            <p className="text-center">
              <Link to="register">
                <span className="color-black">Bạn chưa có tài khoản?</span> <span className="text-active">Đăng ký</span>
              </Link>
            </p>
          </form>
        </div>
      </div>
    </>
  );
}

export default ForgotPassword;
