import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {resetPasswordAPI} from '../../../api/users';
import history from '../../../common/utils/router/history';

class ResetPassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      new_password: '',
      re_type_password: ''
    };
    this.formEl = React.createRef();
  }

  setData = (data) => {
    this.setState(data);
  };

  handleReset = () => {
    const {match: {params: {token}}} = this.props;
    const {new_password} = this.state;
    resetPasswordAPI({
      reset_token: token,
      new_password
    }).then(res => {
      showAlert({
        type: 'success',
        message: `Đã cập nhật mật khẩu mới! Vui lòng đăng nhập!`
      });
      setTimeout(() => {
        history.replace('/login');
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleReset();
  };

  render() {
    const {new_password, re_type_password} = this.state;
    return (
      <Fragment>
        <Helmet>
          <title>Đặt lại mật khẩu</title>
        </Helmet>
        <Breadcrumb>
          <BreadcrumbItem text="Đặt lại mật khẩu" isActive={true}/>
        </Breadcrumb>
        <div className="login-page common-page">
          <div className="container">
            <form onSubmit={this.submit} ref={this.formEl} className="login-form">
              <div className="form-group">
                <label>Mật khẩu mới</label>
                <input type="password"
                       value={new_password}
                       onChange={handleInputTextChanged('new_password', this.setData)}
                       className="form-control"
                       required={true}/>
                <div className="invalid-feedback">
                  Vui lòng nhập mật khẩu
                </div>
              </div>
              <div className="form-group">
                <label>Nhập lại mật khẩu mới</label>
                <input type="password"
                       value={re_type_password}
                       onChange={handleInputTextChanged('re_type_password', this.setData)}
                       className="form-control"
                       required={true}/>
                <div className="invalid-feedback">
                  Vui lòng nhập mật khẩu
                </div>
                {
                  !!re_type_password && new_password !== re_type_password &&
                  <p className="text-danger">Mật khẩu chưa giống</p>
                }
              </div>
              <div className="action">
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={this.submit}
                  disabled={!new_password || new_password !== re_type_password}>
                  Đặt lại mật khẩu
                </button>
              </div>
            </form>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default ResetPassword;
