import {combineReducers} from 'redux';
import account from '../account/_redux/reducer';
import store from '../store/_redux/reducer';

export default combineReducers({
  account,
  store
});
