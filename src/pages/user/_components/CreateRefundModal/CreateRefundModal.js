import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from 'react-bootstrap';
import {handleInputTextChanged, handleSelectChange, showAlert} from '../../../../common/helpers';
import {createRefundAPI, getRefundMetadataAPI} from '../../../../api/refunds';
import Select from '../../../../components/Form/Select/Select';
import history from '../../../../common/utils/router/history';
import InputNumber from '../../../../components/Form/InputNumber/InputNumber';
import UploadImages from '../../../../components/images/UploadImages';
import {uploadMediaAPI} from '../../../../api/media';
import {OBJECT_TYPES} from '../../../../common/constants/objectTypes';

const paymentMethods = [
  {
    id: 20,
    name: 'Chuyển khoản',
  },
  {
    id: 30,
    name: 'Epoint',
  }
];

class CreateRefundModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        customer_email: '',
        customer_phone: '',
        refund_reason_id: 0, // Tham khảo API meta // lí do trả hàng
        refund_results_id: 0, // tham khảo API meta (kết quả mong muốn)
        total_price: 0, // số tiền cần hoàn
        customer_note_reason: '', // chú thích của người mua
        payment_method: 30, // 20: ATM, 30: Epoint // phương thức hoàn tiền
        order_detail_ids: [],  // thuộc order chưa có status success và đã thanh toán (đang ở trạng thái giao hàng, user đòi hoàn trước khi xác nhận đã nhận hàng)
        media: [],
      },
      metadata: {
        reasons: [],
        results: [],
      },
      isOpen: false,
      isSubmitting: false,
      products: [],
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
    this.formEl = React.createRef();
  }

  componentDidMount() {
    this.getRefundMetadata();
  }

  getRefundMetadata = () => {
    getRefundMetadataAPI().then(res => {
      this.setState({
        metadata: res.data || {},
      }, () => {
        this.defaultState = JSON.parse(JSON.stringify(this.state));
      });
    }).catch(() => {
      this.setState({
        metadata: {},
      });
    });
  };

  setData = (data) => {
    this.setState(prevState => {
      if (data.order_detail_ids) {
        const {products} = prevState;
        const selectedProducts = products.filter(item => data.order_detail_ids.includes(item.order_detail_id));
        data.total_price = selectedProducts.reduce((result, item) => {
          return result + (item.sub_total || 0);
        }, 0);
      }
      return {
        formData: {
          ...prevState.formData,
          ...data,
        },
      };
    });
  };

  handleOpen = () => {
    const {order} = this.props;
    const state = JSON.parse(JSON.stringify(this.defaultState));
    const products = order.cart_info.map(item => {
      return {
        id: item.product_snapshot.id,
        name: item.product_snapshot.name,
        order_detail_id: item.order_detail_id,
        sub_total: item.sub_total,
      };
    });
    this.setState({
      ...state,
      isOpen: true,
      products,
    }, () => {
      const {user} = this.props;
      this.setData({
        customer_email: user.email || '',
        customer_phone: user.phone || '',
        order_detail_ids: [],
      });
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.setState({
      isSubmitting: true,
    });
    this.handleSubmitAsync(this.state.formData).then(({errorCount}) => {
      if (errorCount > 0) {
        showAlert({
          type: 'success',
          message: `Đã gửi yêu cầu nhưng một số file không tải lên được!`,
        });
      } else {
        showAlert({
          type: 'success',
          message: `Đã gửi yêu cầu! Vui lòng chờ chúng tôi liên hệ!`,
        });
      }
      this.handleClose();
      this.setState({
        isSubmitting: false,
      });
      history.push('/user/refunds');
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
      });
      this.setState({
        isSubmitting: false,
      });
    });
  };

  handleSubmitAsync = async (values) => {
    const {media, ...rest} = values;
    const {data: {id}} = await createRefundAPI(rest);
    let errorCount = 0;
    if (media?.length) {
      for (let i = 0; i < media.length; i++) {
        try {
          await uploadMediaAPI(media[i], {
            object_type: OBJECT_TYPES.Refund,
            object_id: id,
            type: 'customer',
          });
        } catch (error) {
          errorCount++;
        }
      }
    }
    return {
      errorCount,
    };
  };

  render() {
    const {isOpen, formData, metadata, isSubmitting, products} = this.state;
    const selectedReason = (metadata.reasons || []).find(item => item.id === formData.refund_reason_id) || null;
    const selectedResult = (metadata.results || []).find(item => item.id === formData.refund_results_id) || null;
    const selectedPaymentMethod = paymentMethods.find(item => item.id === formData.payment_method) || null;
    const selectedProducts = products.filter(item => formData.order_detail_ids.includes(item.order_detail_id));
    return (
      <Modal
        size="md"
        show={isOpen}
        onHide={this.handleClose}
        className="create-refund-modal common-modal"
        centered
        backdrop="static"
      >
        <form onSubmit={this.submit} ref={this.formEl}>
          <Modal.Header closeButton>
            <Modal.Title>Yêu cầu trả hàng/hoàn tiền</Modal.Title>
          </Modal.Header>
          <Modal.Body className="common-form">
            <div className="form-group">
              <label>Sản phẩm cần trả (*)</label>
              <Select
                options={products}
                value={selectedProducts}
                placeholder="Chọn sản phẩm cần trả"
                isMulti={true}
                onChange={handleSelectChange('order_detail_ids', 'order_detail_id', this.setData, true)}
              />
            </div>
            <div className="form-group">
              <label>Email (*)</label>
              <input
                type="text"
                value={formData.customer_email}
                onChange={handleInputTextChanged('customer_email', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập email
              </div>
            </div>
            <div className="form-group">
              <label>Số điện thoại (*)</label>
              <input
                type="text"
                value={formData.customer_phone}
                onChange={handleInputTextChanged('customer_phone', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập số điện thoại
              </div>
            </div>
            <div className="form-group">
              <label>Lý do trả hàng</label>
              <Select
                options={metadata.reasons}
                value={selectedReason}
                placeholder="Chọn lý do trả hàng"
                onChange={handleSelectChange('refund_reason_id', 'id', this.setData)}
              />
            </div>
            <div className="form-group">
              <label>Kết quả mong muốn</label>
              <Select
                options={metadata.results}
                value={selectedResult}
                placeholder="Chọn kết quả mong muốn"
                onChange={handleSelectChange('refund_results_id', 'id', this.setData)}
              />
            </div>
            <div className="form-group">
              <label>Số tiền cần hoàn (*)</label>
              <InputNumber
                value={formData.total_price}
                onChange={handleInputTextChanged('total_price', this.setData, 'int')}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập số tiền
              </div>
            </div>
            <div className="form-group">
              <label>Phương thức thanh toán (*)</label>
              <Select
                options={paymentMethods}
                value={selectedPaymentMethod}
                placeholder="Chọn phương thức thanh toán"
                onChange={handleSelectChange('payment_method', 'id', this.setData)}
              />
            </div>
            <div className="form-group">
              <label>Ghi chú (*)</label>
              <textarea
                value={formData.customer_note_reason}
                onChange={handleInputTextChanged('customer_note_reason', this.setData)}
                className="form-control"
                required={true}
              />
            </div>
            <div className="form-group">
              <label>Hình ảnh</label>
              <UploadImages
                value={formData.media}
                onChange={this.setData}
                fieldName="media"
                multiple={true}
                maxFiles={10}
                useType="customer"
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="default" disabled={isSubmitting} onClick={this.handleClose}>
              Hủy bỏ
            </Button>
            <button className="btn btn-primary" disabled={isSubmitting} onClick={this.submit}>
              Gửi yêu cầu
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }
}

CreateRefundModal.propTypes = {
  order: PropTypes.object,
  user: PropTypes.object,
  onSuccess: PropTypes.func,
};

export default CreateRefundModal;
