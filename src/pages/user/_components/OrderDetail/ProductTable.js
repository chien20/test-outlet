import React from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import ProductTableRow from './ProductTableRow';
import './ProductTable.scss';

class CartTable extends React.PureComponent {
  render() {
    const {cart_info, sub_total} = this.props;
    return (
      <div className="product-table">
        <div className="t-head">
          <div className="t-row">
            <div className="c-name">
              Sản phẩm
            </div>
            <div className="c-price">
              Đơn giá
            </div>
            <div className="c-qty">
              Số lượng
            </div>
            <div className="c-total">
              Thành tiền
            </div>
          </div>
        </div>
        <div className="t-body">
          {
            cart_info.map((item, index) => (
              <ProductTableRow data={item} key={index}/>
            ))
          }
        </div>
        <div className="t-foot">
          <div className="t-row">
            <div className="c-foot-name">Tổng: <strong
              className="price-color">{cart_info.length}</strong> sản phẩm</div>
            <div className="c-sub-total">Thành tiền: <strong
              className="price-color">{numberAsCurrentcy(sub_total)} đ</strong></div>
          </div>
        </div>
      </div>
    );
  }
}

export default CartTable;
