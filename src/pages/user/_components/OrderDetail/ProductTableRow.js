import React from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import {imageUrl} from '../../../../common/helpers';

class ProductTableRow extends React.PureComponent {
  render() {
    const {data} = this.props;
    const {product_snapshot: product} = data;
    let imgUrl = null;
    if (product.images && product.images.length) {
      imgUrl = imageUrl(product.images[0].thumbnail_url);
    }
    const imageBg = {};
    if (imgUrl) {
      imageBg.backgroundImage = `url(${imgUrl})`;
    }
    return (
      <div className="t-row">
        <div className="c-name">
          <div className="p-image" style={imageBg}/>
          {data.display_name}
        </div>
        <div className="c-price">
          {numberAsCurrentcy(data.display_price)} đ
        </div>
        <div className="c-qty">
          {data.qty}
        </div>
        <div className="c-total price-color">
          {numberAsCurrentcy(data.sub_total)} đ
        </div>
      </div>
    )
  }
}

export default ProductTableRow;