import React from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import {PaymentMethodName} from './OrderDetail';
import OrderStatus from '../OrdersTable/OrderStatus';

class OrderOverview extends React.PureComponent {
  render() {
    const {order} = this.props;
    return (
      <div className="order-overview">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Người nhận hàng</h5>
            <p className="card-text">
              Địa chỉ: {order.receiver.address}<br/>
              Điện thoại: {order.receiver.phone}
            </p>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Hình thức giao hàng</h5>
            <p className="card-text">
              Giao hàng tiết kiệm<br/>
              {!order.shipping_free ? 'Miễn phí vận chuyển' : `Phí vận chuyển: ${numberAsCurrentcy(order.shipping_free)} đ`}
            </p>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Hình thức thanh toán</h5>
            <p className="card-text">
              <PaymentMethodName payment_method_id={order.payment_method_id}/>
            </p>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">Trạng thái đơn hàng</h5>
            <p className="card-text">
              <OrderStatus status={order.status}/>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default OrderOverview;