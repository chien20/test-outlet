import React from 'react';

export const PaymentMethodName = ({payment_method_id}) => {
  switch (payment_method_id) {
    case 0:
      return 'Thanh toán bằng tiền mặt khi nhận hàng';
    case 10:
      return 'Thanh toán bằng thẻ quốc tế (Visa, Master, JCB)';
    case 20:
      return 'Thanh toán bằng thẻ ATM nội địa/Internet banking';
    default:
      return <span/>;
  }
};
