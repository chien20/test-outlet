import React from 'react';
import {Link} from 'react-router-dom';

const OrderName = ({cartInfo, url}) => {
  if (cartInfo.length === 1) {
    return (
      <Link to={url} className="order-name text-ellipsis text-black">
        <span className="first-product">{cartInfo[0].product_snapshot.name}</span>
      </Link>
    )
  }
  if (cartInfo.length > 1) {
    const products = [];
    cartInfo.forEach(item => {
      products.push(item.product_snapshot.name);
    });
    return (
      <Link to={url} className="order-name text-ellipsis text-black">
        [{cartInfo.length} sản phẩm] <span className="first-product">{products.join(', ')}</span>
      </Link>
    )
  }
  return '';
};

export default OrderName;
