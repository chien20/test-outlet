import React from 'react';

const OrderStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chưa xác nhận</span>;
    case 10:
      return <span className="text-orange">Đang chờ lấy hàng</span>;
    case 20:
      return <span className="text-orange">Đang giao hàng</span>;
    case 30:
      return <span className="text-green">Giao hàng thành công</span>;
    case 35:
      return <span className="text-green">Đang hoàn trả</span>;
    case 40:
      return <span className="text-red">Đã hủy</span>;
    case 41:
      return <span className="text-red">Đã hủy</span>;
    case 42:
      return <span className="text-red">Không giao được hàng</span>;
    case 50:
      return <span className="text-green">Hoàn thành</span>;
    case 80:
      return <span className="text-orange">Đã hoàn tiền</span>;
    default:
      return '';
  }
};

export default OrderStatus;
