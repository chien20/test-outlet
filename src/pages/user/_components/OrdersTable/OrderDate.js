import React from 'react';
import {dateFormat} from '../../../../common/helpers/index';

const OrderDate = ({date}) => {
  try {
    date = dateFormat('d-m-Y', Math.floor(Date.parse(date) / 1000));
  } catch (e) {
    date = '';
  }
  return (
    <div className="order-date">
      {date}
    </div>
  )
};

export default OrderDate;
