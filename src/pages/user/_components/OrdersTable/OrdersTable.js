import React from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import OrderStatus from './OrderStatus';
import OrderName from './OrderName';
import './OrdersTable.scss';
import OrderDate from './OrderDate';

class OrdersTable extends React.PureComponent {
  render() {
    const {orders, isStore} = this.props;
    return (
      <div className="orders-table table-responsive">
        <table className="table table-bordered">
          <thead>
          <tr>
            <th>Mã đơn hàng</th>
            <th>Ngày mua</th>
            <th>Sản phẩm</th>
            <th>Tổng tiền</th>
            <th>Trạng thái đơn hàng</th>
          </tr>
          </thead>
          <tbody>
          {
            orders.map((item, index) => (
              <tr key={index}>
                <td>{item.order_code}</td>
                <td><OrderDate date={item.created_at}/></td>
                <td><OrderName cartInfo={item.cart_info} id={item.id} isStore={isStore}/></td>
                <td>{numberAsCurrentcy(item.total || 0)} đ</td>
                <td><OrderStatus status={item.status}/></td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default OrdersTable;
