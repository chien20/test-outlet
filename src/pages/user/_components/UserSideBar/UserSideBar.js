import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import './UserSideBar.scss';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import Avatar from '../../../../components/Avatar/Avatar';
import {logout} from '../../../../redux/actions/user';
import {isMobile} from '../../../../common/helpers/browser';

const MenuLink = ({path, currentPath, exact = true, children}) => {
  let isActive = path === currentPath;
  if (!exact) {
    isActive = isActive || currentPath.indexOf(path) >= 0;
  }
  return (
    <li className={`${isActive ? 'active' : ''}`}>
      <Link to={path}>{children}</Link>
    </li>
  );
};

class UserSideBar extends React.PureComponent {
  handleLogout = () => {
    this.props.dispatch(logout());
  };

  render() {
    const {location: {pathname}, user, notifications} = this.props;
    return (
      <div className="user-side-bar">
        <div className="user-info">
          <div className="user-info-wrapper">
            <Avatar size={isMobile ? 80 : 40} src={user ? user.avatar : ''}/>
            <div className="welcome">
              <p>Xin chào !!</p>
              <h5>{user.full_name}</h5>
              <p className="email">{user.email}</p>
            </div>
          </div>
        </div>
        <ul>
          <MenuLink path="/user/profile" currentPath={pathname}>
            <i className="fas fa-user bg-green-m"/> Thông tin tài khoản
          </MenuLink>
          <MenuLink path="/user/notifications" currentPath={pathname} exact={false}>
            <i className="fas fa-bell bg-red-m"/> Thông báo của tôi
            {
              !!notifications?.length &&
              <div className="badge-number bg-primary m-l-8">{notifications.length}</div>
            }
          </MenuLink>
          <MenuLink path="/user/chat" currentPath={pathname} exact={false}>
            <i className="far fa-envelope bg-orange-m"/> Tin nhắn của tôi
          </MenuLink>
          <MenuLink path="/user/e-points" currentPath={pathname} exact={false}>
            <i className="fas fa-coins bg-or-o-m"/> EPoint của tôi <span className="tag tag-blue m-l-5">Beta</span>
          </MenuLink>
          <MenuLink path="/user/addresses" currentPath={pathname} exact={false}>
            <i className="fas fa-map-marker-alt bg-purple-m"/> Sổ địa chỉ
          </MenuLink>
          <li className="menu-title">
            Mua hàng
          </li>
          <MenuLink path="/user/orders" currentPath={pathname} exact={false}>
            <i className="fas fa-receipt bg-orange-light-m"/> Quản lý đơn hàng
          </MenuLink>
          <MenuLink path="/user/ratings" currentPath={pathname} exact={false}>
            <i className="far fa-comment bg-blue-o-m"/> Đánh giá sản phẩm
          </MenuLink>
          <MenuLink path="/user/refunds" currentPath={pathname} exact={false}>
            <i className="fas fa-exchange-alt bg-blue-dark-m"/> Yêu cầu trả hàng
          </MenuLink>
          <li className="menu-title">
            Bán hàng
          </li>
          {
            user.store_id &&
            <Fragment>
              <MenuLink path="/user/store" currentPath={pathname}>
                <i className="fas fa-store bg-blue-sky-m"/> Thông tin cửa hàng
              </MenuLink>
              <MenuLink path="/user/store/shipping-fee" currentPath={pathname}>
                <i className="fas fa-truck-moving bg-red-m"/> Thông tin vận chuyển
              </MenuLink>
              <MenuLink path="/user/store/products" currentPath={pathname} exact={false}>
                <i className="fas fa-tshirt bg-pink-m"/> Quản lý sản phẩm
              </MenuLink>
              <MenuLink path="/user/store/orders" currentPath={pathname} exact={false}>
                <i className="fas fa-receipt bg-orange-light-m"/> Quản lý đơn hàng
              </MenuLink>
              <MenuLink path="/user/store/ratings" currentPath={pathname} exact={false}>
                <i className="far fa-comment bg-blue-m"/> Đánh giá của khách
              </MenuLink>
              <MenuLink path="/user/store/vouchers" currentPath={pathname} exact={false}>
                <i className="fas fa-ticket-alt bg-orange-m"/> Ví voucher
              </MenuLink>
              <MenuLink path="/user/store/refunds" currentPath={pathname} exact={false}>
                <i className="fas fa-exchange-alt bg-purple-m"/> Yêu cầu trả hàng
              </MenuLink>
              <MenuLink path="/user/store/certs" currentPath={pathname} exact={false}>
                <i className="fas fa-shield-alt bg-or-o-m"/> Chứng nhận
              </MenuLink>
            </Fragment>
          }
          {
            !user.store_id &&
            <MenuLink path="/user/store/register" currentPath={pathname}>
              <i className="fas fa-store bg-blue-sky-m"/> Đăng ký bán hàng
            </MenuLink>
          }
          <li className="menu-title">
            Đăng xuất
          </li>
          <li>
            <HyperLink onClick={this.handleLogout}><i className="fas fa-sign-out-alt bg-blue-dark-m"/> Đăng xuất</HyperLink>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
  notifications: state.user.notifications,
});

export default withRouter(connect(mapStateToProps)(UserSideBar));
