import React, {Fragment} from 'react';
import './UserPage.scss';
import UserSideBar from './_components/UserSideBar/UserSideBar';
import {Redirect, Route, Switch} from 'react-router-dom';
import AccountPage from './account';
import StorePage from './store';
import {isMobile} from '../../common/helpers/browser';
import UserPageMobile from './UserPageMobile';
import {Helmet} from 'react-helmet';

class UserPage extends React.PureComponent {
  render() {
    if (isMobile) {
      return <UserPageMobile/>
    }
    return (
      <Fragment>
        <Helmet>
          <title>Tài khoản của tôi</title>
        </Helmet>
        <div className="user-page">
          <div className="container">
            <div className="e-row">
              <div className="col-left">
                <UserSideBar/>
              </div>
              <div className="col-main">
                <div className="user-page-content">
                  <Switch>
                    <Route path="/user/store" component={StorePage}/>
                    <Route path="/user/notifications" exact render={() => <Redirect to="/user/notifications/all"/>}/>
                    <Route path="/user" exact render={() => <Redirect to="/user/profile"/>}/>
                    <Route path="/user/" component={AccountPage}/>
                  </Switch>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default UserPage;
