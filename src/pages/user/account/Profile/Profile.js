import React from 'react';
import './Profile.scss';
import Avatar from '../../../../components/Avatar/Avatar';
import {connect} from 'react-redux';
import {checkDirtyFields, handleInputTextChanged, handleRadioChanged, showAlert} from '../../../../common/helpers';
import Drawer from '../../../../components/Drawer/Drawer';
import EditAvatarDrawer from '../_components/EditAvatarDrawer/EditAvatarDrawer';
import {getCurrentUserAPI, updateUserProfileAPI} from '../../../../api/users';
import {getCurrentUserAC, getCurrentUserSuccessAC} from '../../../../redux/actions/user/info';
import DatePicker, {registerLocale} from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import vi from 'date-fns/locale/vi';
import {format} from 'date-fns';
import EditPhoneDrawer from '../_components/EditPhoneDrawer/EditPhoneDrawer';
import EditPasswordModal from '../_components/EditPasswordModal/EditPasswordModal';
import HyperLink from '../../../../components/HyperLink/HyperLink';

registerLocale('vi', vi);

const defaultUser = {
  id_card_number: '',
  tax_code: '',
};

const now = new Date();

class Profile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      initialData: {},
      metaData: {
        dirtyFields: {},
        errorFields: {}
      },
      changePassword: false,
      isDrawerOpen: {
        avatar: false,
        phone: false,
      },
    };
    this.formEl = React.createRef();
    this.editPasswordModal = React.createRef();
  }

  componentWillMount() {
    const {user} = this.props;
    this.initData(user);
  }

  componentWillReceiveProps(nexProps, nextContext) {
    if (nexProps.user !== this.props.user && nexProps.user) {
      this.initData(nexProps.user);
    }
  }

  initData = (user) => {
    let data = JSON.parse(JSON.stringify(user));
    data = {...defaultUser, ...data};
    if (data.dob && typeof data.dob === 'string') {
      const arr = data.dob.split('-');
      if (arr.length === 3) {
        data.dob = new Date(arr[0], parseInt(arr[1]) - 1, arr[2]);
      } else {
        data.dob = null;
      }
    } else {
      data.dob = null;
    }
    const initialData = JSON.parse(JSON.stringify(data));
    if (data.dob) {
      initialData.dob = data.dob;
    }
    this.setState({
      data: data,
      initialData: initialData,
      metaData: {
        dirtyFields: {},
        errorFields: {}
      }
    });
  };

  getUser = () => {
    this.props.dispatch(getCurrentUserAC());
  };

  setData = (data) => {
    if (data.gender !== undefined) {
      data.gender = parseInt(data.gender);
    }
    this.setState(prevState => {
      const newState = {
        data: {...prevState.data, ...data}
      };
      newState.metaData = {
        ...prevState.metaData,
        dirtyFields: this.checkDirtyFields(newState.data, prevState.initialData),
        errorFields: this.checkErrorFields(newState.data)
      };
      return newState;
    });
  };

  checkDirtyFields = (data, initialData) => {
    const data1 = {...data};
    const initialData1 = {...initialData};
    if (data1.dob) {
      data1.dob = data1.dob.getTime();
    }
    if (initialData1.dob) {
      initialData1.dob = initialData1.dob.getTime();
    }
    return checkDirtyFields(data1, initialData1);
  };

  checkErrorFields = () => {
    return {}
  };

  toggleDrawer = (name, state) => () => {
    this.setState(prevState => ({
      isDrawerOpen: {
        ...prevState.isDrawerOpen,
        [name]: state
      }
    }));
  };

  toggleChangePassword = () => {
    this.editPasswordModal.current.handleOpen();
  };

  onDateChange = (date) => {
    this.setData({
      dob: date
    });
  };

  handleSave = () => {
    const data = {...this.state.data};
    if (data.dob) {
      data.dob = format(data.dob, 'yyyy-MM-dd');
    }
    this.setState({
      isLoading: true
    });
    updateUserProfileAPI(data).then(() => {
      showAlert({
        type: 'success',
        message: 'Đã lưu!'
      });
      return getCurrentUserAPI();
    }).then(res => {
      this.setState({
        isLoading: false
      });
      this.props.dispatch(getCurrentUserSuccessAC(res.data));
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message_code}`
      });
    });
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleSave();
  };

  render() {
    const {user} = this.props;
    const {data, metaData: {dirtyFields, errorFields}, isDrawerOpen} = this.state;
    const isBtnSubmitDisabled = Object.keys(dirtyFields).length === 0
      || Object.keys(errorFields).length > 0
      || Object.keys(errorFields).length > 0;
    return (
      <div className="profile-page drawer-container">
        <div className="page-title">
          <h1>Thông tin tài khoản</h1>
        </div>
        <div className="page-content">
          <div className="avatar-container">
            <Avatar
              size={138}
              src={user.avatar}
              canEdit={true}
              handleClickEdit={this.toggleDrawer('avatar', true)}/>
          </div>
          <form className="form-container" ref={this.formEl} onSubmit={this.submit}>
            <div className="form-group">
              <label>Họ tên</label>
              <input
                type="text"
                className="form-control"
                name="full_name"
                required={true}
                value={data.full_name}
                onChange={handleInputTextChanged('full_name', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập họ tên
              </div>
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                name="email"
                className="form-control"
                value={data.email}
                disabled={true}
                onChange={handleInputTextChanged('email', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập email của bạn
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>Số CMND</label>
                <div className="input-group">
                  <input
                    type="text"
                    name="id_card_number"
                    className="form-control"
                    value={data.id_card_number}
                    maxLength={12}
                    onChange={handleInputTextChanged('id_card_number', this.setData)}/>
                </div>
                <div className="invalid-feedback">
                  Vui lòng nhập số CMND
                </div>
              </div>
              <div className="form-group col-md-6">
                <label>Mã số thuế TNCN</label>
                <div className="input-group">
                  <input
                    type="text"
                    name="tax_code"
                    className="form-control"
                    value={data.tax_code}
                    maxLength={20}
                    onChange={handleInputTextChanged('tax_code', this.setData)}/>
                </div>
                <div className="invalid-feedback">
                  Vui lòng nhập mã số thuế TNCN
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>Số điện thoại</label>
                <div className="input-group">
                  <input
                    type="text"
                    name="phone"
                    className="form-control"
                    value={data.phone}
                    maxLength={20}
                    readOnly={true}
                    onChange={handleInputTextChanged('phone', this.setData)}/>
                  <div className="input-group-append">
                    <i className="fas fa-edit input-group-text" onClick={this.toggleDrawer('phone', true)}/>
                  </div>
                </div>
                <div className="invalid-feedback">
                  Vui lòng nhập số điện thoại
                </div>
              </div>
              <div className="form-group col-md-6">
                <label style={{display: 'block'}}>Ngày sinh</label>
                <DatePicker
                  selected={data.dob}
                  onChange={this.onDateChange}
                  dateFormat="dd/MM/yyyy"
                  maxDate={now}
                  locale="vi" className="form-control"/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>Giới tính</label>
                <div className="form-inline-group">
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      name="gender"
                      type="radio"
                      id="gender-male"
                      value={1}
                      checked={data.gender === 1}
                      onChange={handleRadioChanged('gender', this.setData)}/>
                    <label
                      className="form-check-label"
                      htmlFor="gender-male">Nam</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      name="gender"
                      type="radio"
                      id="gender-female"
                      value={0}
                      checked={data.gender === 0}
                      onChange={handleRadioChanged('gender', this.setData)}/>
                    <label
                      className="form-check-label"
                      htmlFor="gender-female">Nữ</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      name="gender"
                      type="radio"
                      id="gender-other"
                      value={3}
                      checked={data.gender === 3}
                      onChange={handleRadioChanged('gender', this.setData)}/>
                    <label
                      className="form-check-label"
                      htmlFor="gender-other">Khác</label>
                  </div>
                </div>
              </div>
              <div className="form-group col-md-6">
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <HyperLink onClick={this.toggleChangePassword}>Đổi mật khẩu</HyperLink>
              </div>
            </div>
            <div className="form-submit">
              <button
                className="btn btn-primary"
                type="submit"
                disabled={isBtnSubmitDisabled}
                onClick={this.submit}>Cập nhật
              </button>
            </div>
          </form>
        </div>
        <Drawer
          isOpen={isDrawerOpen.avatar}
          handleClose={this.toggleDrawer('avatar', false)}
          width={375}
          className="edit-avatar-drawer"
          title="Cập nhật avatar">
          <EditAvatarDrawer
            userId={user.id}
            avatarUrl={user.avatar}
            handleClose={this.toggleDrawer('avatar', false)}/>
        </Drawer>
        <Drawer
          isOpen={isDrawerOpen.phone}
          handleClose={this.toggleDrawer('phone', false)}
          width={375}
          className="edit-phone-drawer"
          title="Cập nhật số điện thoại">
          <EditPhoneDrawer
            onSuccess={this.getUser}
            handleClose={this.toggleDrawer('phone', false)}/>
        </Drawer>
        <EditPasswordModal ref={this.editPasswordModal}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info
});

export default connect(mapStateToProps)(Profile);
