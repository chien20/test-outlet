import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {imageUrl, publicUrl, showAlert} from '../../../../../common/helpers';
import './EditAvatarDrawer.scss';
import {getCurrentUserAC} from '../../../../../redux/actions/user/info';
import {updateUserAvatarAPI} from '../../../../../api/users';

class EditAvatarDrawer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      previewUrl: null
    };
  }

  handleFileChange = (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.revokePreview();
      this.setState({
        selectedFile: file,
        previewUrl: URL.createObjectURL(file)
      });
    }
  };

  revokePreview() {
    const {previewUrl} = this.state;
    if (previewUrl) {
      URL.revokeObjectURL(previewUrl);
      this.setState({previewUrl: null});
    }
  }

  handleSubmit = () => {
    const {handleClose} = this.props;
    const {selectedFile} = this.state;
    updateUserAvatarAPI(selectedFile).then(res => {
      showAlert({
        message: 'Đã lưu!',
        type: 'success'
      });
      this.props.dispatch(getCurrentUserAC());
      handleClose();
    }).catch(error => {
      showAlert({
        message: `Lỗi: ${error.message}`,
        type: 'error'
      });
    });
  };

  render() {
    const {avatarUrl, handleClose} = this.props;
    const {previewUrl, selectedFile} = this.state;
    const hasImage = selectedFile || avatarUrl;
    let url = publicUrl('/assets/images/no-image/avatar_400x400.png');
    if (!!hasImage) {
      url = previewUrl || imageUrl(avatarUrl);
    }
    return (
      <Fragment>
        <div className="drawer-content">
          {
            url &&
            <div className="avatar-preview" style={{backgroundImage: `url(${url})`}}/>
          }
          <div className="form-group">
            <input type="file" className="form-control-file" onChange={this.handleFileChange}/>
          </div>
        </div>
        <div className="drawer-footer text-right">
          <button className="btn btn-default" onClick={handleClose}>Hủy bỏ</button>
          <button className="btn btn-primary" onClick={this.handleSubmit}>Cập nhật</button>
        </div>
      </Fragment>
    )
  }
}

export default connect()(EditAvatarDrawer);