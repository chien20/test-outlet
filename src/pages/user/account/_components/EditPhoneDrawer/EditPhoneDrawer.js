import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged, showAlert} from '../../../../../common/helpers';
import {updatePhoneAPI} from '../../../../../api/users';

class EditPhoneDrawer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      password: '',
      isSubmitting: false,
    }
  }

  setData = (data) => {
    this.setState(data);
  };

  handleSubmit = () => {
    const {handleClose, onSuccess} = this.props;
    const {phone, password} = this.state;
    this.setState({
      isSubmitting: false,
    });
    updatePhoneAPI({
      phone,
      password
    }).then(() => {
      this.setState({
        isSubmitting: false,
      });
      handleClose();
      onSuccess();
      showAlert({
        type: 'success',
        message: `Đã cập nhật`
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
      this.setState({
        isSubmitting: false,
      });
    });
  };

  render() {
    const {handleClose} = this.props;
    const {phone, password, isSubmitting} = this.state;
    return (
      <Fragment>
        <div className="drawer-content">
          <div className="form-group">
            <label>Số điện thoại mới</label>
            <input
              type="text"
              className="form-control"
              value={phone}
              onChange={handleInputTextChanged('phone', this.setData)}
            />
          </div>
          <div className="form-group">
            <label>Mật khẩu</label>
            <input
              type="password"
              className="form-control"
              onChange={handleInputTextChanged('password', this.setData)}
            />
          </div>
        </div>
        <div className="drawer-footer text-right">
          <button
            className="btn btn-default"
            onClick={handleClose}
            disabled={isSubmitting}
          >
            Hủy bỏ
          </button>
          <button
            className="btn btn-primary"
            disabled={isSubmitting || !phone || !password}
            onClick={this.handleSubmit}
          >
            Cập nhật
          </button>
        </div>
      </Fragment>
    );
  }
}

EditPhoneDrawer.propTypes = {
  handleClose: PropTypes.func,
  onSuccess: PropTypes.func,
};

export default EditPhoneDrawer;