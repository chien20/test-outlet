import React from 'react';
import {handleInputTextChanged, showAlert} from '../../../../../common/helpers';
import {updateUserPasswordAPI} from '../../../../../api/users';
import {Modal} from 'react-bootstrap';

class EditPasswordModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      old_password: '',
      new_password: '',
      re_type_password: '',
      isSubmitting: false,
      error: '',
    };
    this.defaultState = {...this.state};
  }

  setData = (data) => {
    this.setState(prevState => {
      const newState = {...prevState, ...data};
      newState.error = '';
      if (newState.new_password && newState.re_type_password && newState.new_password !== newState.re_type_password) {
        newState.error = 'Mật khẩu không khớp';
      }
      return newState;
    });
  };

  handleOpen = () => {
    this.setState({
      ...this.defaultState,
      isOpen: true,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  handleSubmit = () => {
    const {old_password, new_password} = this.state;
    this.setState({
      isSubmitting: true,
    });
    updateUserPasswordAPI({old_password, new_password}).then(() => {
      this.setState({
        isSubmitting: false,
      });
      showAlert({
        type: 'success',
        message: `Đã cập nhật`
      });
      this.handleClose();
    }).catch(error => {
      let message = error.message;
      if (message === 'old password not match') {
        message = 'Mật khẩu cũ không chính xác!';
      }
      this.setState({
        error: message,
        isSubmitting: false,
      });
    });
  };

  render() {
    const {isOpen, error, old_password, new_password, re_type_password, isSubmitting} = this.state;
    return (
      <Modal size="md" show={isOpen} onHide={this.handleClose} className="alert-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>Đổi mật khẩu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            error &&
            <div className="alert alert-danger">{error}</div>
          }
          <div className="form-group">
            <label>Mật khẩu hiện tại</label>
            <input
              type="password"
              className="form-control"
              onChange={handleInputTextChanged('old_password', this.setData)}
            />
          </div>
          <div className="form-group">
            <label>Mật khẩu mới</label>
            <input
              type="password"
              className="form-control"
              onChange={handleInputTextChanged('new_password', this.setData)}
            />
          </div>
          <div className="form-group">
            <label>Nhập lại mật khẩu mới</label>
            <input
              type="password"
              className="form-control"
              onChange={handleInputTextChanged('re_type_password', this.setData)}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-default"
            onClick={this.handleClose}
            disabled={isSubmitting}
          >
            Hủy bỏ
          </button>
          <button
            className="btn btn-primary"
            disabled={isSubmitting || !old_password || !new_password || !re_type_password || new_password !== re_type_password}
            onClick={this.handleSubmit}
          >
            Cập nhật
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default EditPasswordModal;
