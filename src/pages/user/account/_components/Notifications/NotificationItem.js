import React from 'react';
import {connect} from 'react-redux';
import {NotificationContent} from '../../../../../components/Notification/NotificationContent';
import {imageUrl, showAlert} from '../../../../../common/helpers';
import {updateNotificationSeenStatusAPI} from '../../../../../api/notifications';
import NotificationWrapper from '../../../../../components/Notification/NotificationWrapper';
import NotificationImage from '../../../../../components/Notification/NotificationImage';
import {getNotificationsAC} from '../../../../../redux/actions/user/notifications';
import NotifyUnread from '../../../../../assets/images/icons/icon-notify-unread.svg';
import NotifyReaded from '../../../../../assets/images/icons/icon-notify-readed.svg';
import {formatDate} from '../../../../../common/helpers/format';

class NotificationItem extends React.PureComponent {
  // deleteNotification = (id) => () => {
  //   deleteNotificationAPI([id]).then(() => {
  //     this.props.handleReload();
  //   }).catch(error => {
  //     showAlert({
  //       type: 'error',
  //       message: `Lỗi: ${error.message}`
  //     });
  //   });
  // };

  onClickTitle = () => {
    const {notification, dispatch} = this.props;
    if (!notification.seen) {
      updateNotificationSeenStatusAPI(notification.id, true).then(res => {
        dispatch(getNotificationsAC());
        this.props.handleReload();
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    }
  };

  getNotificationImage = () => {
    const {notification} = this.props;
    if (notification.body.image_url) {
      return imageUrl(notification.body.image_url);
    }
    if (notification.seen) {
      return NotifyReaded;
    }
    return NotifyUnread;
  };

  render() {
    const {notification} = this.props;
    return (
      <NotificationWrapper
        action={notification.action}
        onClick={this.onClickTitle}
        className={`notification-item ${notification.seen ? '' : 'unread'}`}
      >
        <div className="notification-img">
          <NotificationImage url={this.getNotificationImage()}/>
        </div>
        <div className="notification-body">
          <NotificationContent
            text={notification.body.text}
            params={notification.body.extra_data}
            actions={notification.action}
          />
          <div className="created-date">{formatDate(notification.created_at)}</div>
        </div>
      </NotificationWrapper>
    )
  }
}

export default connect()(NotificationItem);
