import React from 'react';
import NotificationItem from './NotificationItem';

class NotificationList extends React.PureComponent {
  render() {
    const {notifications, handleReload} = this.props;
    return (
      <ul className="notification-list">
        {
          notifications.map((item, index) => (
            <NotificationItem key={index} notification={item} handleReload={handleReload}/>
          ))
        }
      </ul>
    )
  }
}

export default NotificationList;