import React, {Fragment} from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import NotificationsTab from './NotificationsTab';
import './Notifications.scss';
import {isMobile} from '../../../../common/helpers/browser';
import IconBackMobile from '../../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../../components/HeaderMobile/TitleMobile/TitleMobile';
import CartIconMobile from '../../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import HeaderMobile from '../../../../components/HeaderMobile/HeaderMobile';

export const TABS = [
  {
    typeId: '',
    alias: 'all',
    name: 'Tất cả'
  },
  {
    typeId: 0,
    alias: 'system',
    name: 'Hệ thống'
  },
  {
    typeId: 80,
    alias: 'order',
    name: 'Đơn hàng'
  },
  {
    typeId: 20,
    alias: 'store',
    name: 'Cửa hàng'
  },
  {
    typeId: 30,
    alias: 'group',
    name: 'Hội nhóm'
  },
  {
    typeId: 90,
    alias: 'event',
    name: 'Khuyến mãi'
  }
];

class Notifications extends React.PureComponent {
  render() {
    const {match: {params: {type}}} = this.props;
    return (
      <Fragment>
        {
          isMobile &&
          <HeaderMobile className="fixed">
            <IconBackMobile/>
            <TitleMobile>Thông báo của tôi</TitleMobile>
            <CartIconMobile/>
          </HeaderMobile>
        }
        <div className="notifications-page">
          {
            !isMobile &&
            <div className="page-title">
              <h1>Thông báo của tôi</h1>
            </div>
          }
          <div className="page-content">
            <ul className="notifications-tabs-nav d-mobile-none">
              {
                TABS.map((item, index) => (
                  <li key={index} className={type === item.alias ? 'active' : ''}>
                    <Link to={`/user/notifications/${item.alias}`}>{item.name}</Link>
                  </li>
                ))
              }
            </ul>
            <div className="notifications-tabs-content p-b-10">
              <Switch>
                <Route exact path="/user/notifications/:type/:page?" component={NotificationsTab}/>
              </Switch>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default Notifications;
