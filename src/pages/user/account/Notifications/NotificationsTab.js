import React, {Fragment} from 'react';
import {showAlert} from '../../../../common/helpers';
import {getNotificationListAPI} from '../../../../api/notifications';
import {history} from '../../../../common/utils/router/history';
import Pagination from '../../../../components/Pagination/Pagination';
import {TABS} from './Notifications';
import NotificationList from '../_components/Notifications/NotificationList';
import NoData from '../../../../components/NoData/NoData';

const ITEMS_PER_PAGE = 10;

class NotificationsTab extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      notifications: [],
      totalItems: 0
    };
  }

  componentDidMount() {
    this.getNotifications();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props !== prevProps) {
      this.getNotifications();
    }
  }

  getNotifications = () => {
    const {match: {params: {page, type}}} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE,
      sort_by: 'id',
      sort_order: 'DESC'
    };
    params.current_page -= 1;
    if (type && type !== 'all') {
      const tab = TABS.find(item => item.alias === type);
      if (tab) {
        params.type = tab.typeId;
      }
    }
    getNotificationListAPI(params).then(res => {
      if (res && res.data && res.data.list) {
        this.setState({
          isLoading: false,
          notifications: res.data.list,
          totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false,
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
        id: 'get-notifications'
      });
    });
  };

  onPageChanged = (page) => {
    const {match: {params: {type}}} = this.props;
    let url = '';
    if (page > 1) {
      url = `/user/notifications/${type}/${page}`;
    } else {
      url = `/user/notifications/${type}`;
    }
    history.push(url);
  };

  render() {
    const {match: {params: {page}}} = this.props;
    const {isLoading, notifications, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <Fragment>
        {
          !isLoading && !notifications.length &&
          <NoData
            title="Không có thông báo"
            description="Bạn không có thông báo nào."
          />
        }
        {
          !!notifications.length &&
          <NotificationList notifications={notifications} handleReload={this.getNotifications}/>
        }
        <div className="item-pagination hidden-mobile">
          {
            totalPage > 1 &&
            <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
          }
        </div>
      </Fragment>
    );
  }
}

export default NotificationsTab;
