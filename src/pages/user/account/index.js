import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Profile from './Profile/Profile';
import Orders from './Orders/Orders';
import OrderDetail from './Orders/OrderDetail';
import Notifications from './Notifications/Notifications';
import Ratings from './Ratings/Ratings';
import Refunds from './Refunds/Refunds';
import Addresses from './Addresses/Addresses';
import EPoints from './EPoints/EPoints';
import Chat from './Chat/Chat';
import RefundDetail from './Refunds/RefundDetail/RefundDetail';

const AccountPage = () => (
  <Switch>
    <Route exact path="/user/profile" component={Profile}/>
    <Route exact path="/user/orders/view/:id" component={OrderDetail}/>
    <Route exact path="/user/orders/:page?" component={Orders}/>
    <Route exact path="/user/notifications/:type/:page?" component={Notifications}/>
    <Route exact path="/user/ratings/:page?" component={Ratings}/>
    <Route exact path="/user/refunds/:page?" component={Refunds}/>
    <Route exact path="/user/refunds/view/:id?" component={RefundDetail}/>
    <Route exact path="/user/addresses" component={Addresses}/>
    <Route exact path="/user/e-points/:page?" component={EPoints}/>
    <Route exact path="/user/chat" component={Chat}/>
    <Route exact path="/user/chat/conversation/:id" component={Chat}/>
  </Switch>
);

export default AccountPage;
