import React, {Fragment} from "react";
import HeaderMobile from "../../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../components/HeaderMobile/TitleMobile/TitleMobile";
import CartIconMobile from "../../../../components/HeaderMobile/CartIconMobile/CartIconMobile";
import {Tab, Tabs} from "react-bootstrap";
import ListOrdersMobile from "./ListOrdersMobile/ListOrdersMobile";
import './OrdersMobile.scss';

class OrdersMobile extends React.PureComponent {
  render() {
    const {orders, loadMores, hasMore} = this.props;
    return (
      <Fragment>
        <HeaderMobile className="fixed">
          <IconBackMobile/>
          <TitleMobile>Đơn hàng của tôi</TitleMobile>
          <CartIconMobile/>
        </HeaderMobile>
        <div className="orders-page">
          <Tabs defaultActiveKey="all">
            <Tab eventKey="all" title="Toàn bộ">
              <ListOrdersMobile orders={orders} loadMores={loadMores} hasMore={hasMore}/>
            </Tab>
            <Tab eventKey="confirmed" title="Đã xác nhận">
              Comming soon...
            </Tab>
            <Tab eventKey="transport" title="Đang giao">
              Comming soon...
            </Tab>
            <Tab eventKey="cancel" title="Hủy">
              Comming soon...
            </Tab>
            <Tab eventKey="success" title="Hoàn thành">
              Comming soon...
            </Tab>
          </Tabs>
        </div>
      </Fragment>
    )
  }
}

export default OrdersMobile;
