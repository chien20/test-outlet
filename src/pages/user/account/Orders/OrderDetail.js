import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {getOrderDetailAPI, updateOrderDetailAPI} from '../../../../api/orders';
import {showAlert} from '../../../../common/helpers';
import ProductTable from '../../_components/OrderDetail/ProductTable';
import './OrderDetail.scss';
import OrderOverview from '../../_components/OrderDetail/OrderOverview';
import CreateRefundModal from '../../_components/CreateRefundModal/CreateRefundModal';

class OrderDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      order: null
    };
    this.createRefundModal = React.createRef();
  }

  componentDidMount() {
    this.getOrderDetail();
  }

  getOrderDetail = () => {
    const {match: {params: {id}}} = this.props;
    this.setState({
      isLoading: true
    });
    getOrderDetailAPI(id).then(res => {
      if (res && res.data) {
        let sub_total = 0;
        res.data.cart_info.forEach(item => {
          const {product_snapshot: product} = item;
          item.display_price = product.price;
          item.display_original_price = product.original_price;
          item.display_name = product.name;
          if (item.product_option_id && product.options) {
            const option = product.options.find(option => option.id === item.product_option_id);
            if (option) {
              item.display_price = option.price;
              item.display_original_price = option.original_price;
              if (option.groups_index && option.groups_index.length) {
                if (option.groups_index.length === 1 && product.option_groups[0].values) {
                  const variantName = product.option_groups[0].values[option.groups_index[0]];
                  item.display_name = `${product.name} (${variantName})`;
                } else if (option.groups_index.length === 2
                  && product.option_groups[0].values
                  && product.option_groups[1].values) {
                  const variantName = `${product.option_groups[0].values[option.groups_index[0]]} - ${product.option_groups[1].values[option.groups_index[1]]}`;
                  item.display_name = `${product.name} (${variantName})`;
                }
              }
            }
          }
          item.sub_total = item.display_price * item.qty;
          sub_total += item.sub_total;
        });
        res.data.sub_total = sub_total;
        this.setState({
          isLoading: false,
          order: res.data
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  updateOrder = (status, reason = '') => {
    const {match: {params: {id}}} = this.props;
    this.setState({
      isSubmitting: true
    });
    updateOrderDetailAPI(id, {
      status,
      reason
    }).then(() => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'success',
        message: `Đã cập nhật`
      });
      this.getOrderDetail();
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  cancelOrder = () => {
    this.updateOrder(40, 'User cancel');
  };

  confirmCompleted = () => {
    this.updateOrder(50, 'User confirm completed');
  };

  opencreateRefundModal = () => {
    this.createRefundModal.current.handleOpen();
  };

  render() {
    const {match: {params: {id}}, user} = this.props;
    const {order} = this.state;
    return (
      <div className="order-detail-page">
        <div className="page-title">
          <h1>Chi tiết đơn hàng #{id}</h1>
        </div>
        {
          order &&
          <div className="page-content">
            <OrderOverview order={order}/>
            <ProductTable cart_info={order.cart_info} total={order.total || 0} sub_total={order.sub_total}/>
            <div className="order-actions">
              {
                (order.status === 0 || order.status === 10) &&
                <button className="btn btn-danger" onClick={this.cancelOrder}>Hủy đơn hàng</button>
              }
              {
                order.status === 30 &&
                <Fragment>
                  <button className="btn btn-success" onClick={this.confirmCompleted}>Xác nhận hoàn thành</button>
                  <button className="btn btn-danger" onClick={this.opencreateRefundModal}>Yêu cầu hoàn tiền</button>
                </Fragment>
              }
            </div>
          </div>
        }
        {
          order &&
          <CreateRefundModal
            ref={this.createRefundModal}
            order={order}
            user={user}
          />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
});

export default connect(mapStateToProps)(OrderDetail);
