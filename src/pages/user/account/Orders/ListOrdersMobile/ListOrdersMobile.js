import React from "react";
import PropTypes from "prop-types";
import OrderItem from "./OrderItem";
import InfiniteScroll from 'react-infinite-scroll-component';

class ListOrdersMobile extends React.PureComponent {

  fetchData = () => {
    this.props.loadMores();
  };
  render() {
    const {orders,hasMore} = this.props;
    return (
      <InfiniteScroll
        dataLength={orders.length}
        next={this.fetchData}
        hasMore={hasMore}
        loader={<div>Loading...</div>}
      >
        <div className="list-orders">
          {
            orders.map((item, index) => (<OrderItem order={item} key={index}/>))
          }
        </div>
      </InfiniteScroll>
    )
  }
}

ListOrdersMobile.propTypes = {
  orders: PropTypes.array,
};

export default ListOrdersMobile;
