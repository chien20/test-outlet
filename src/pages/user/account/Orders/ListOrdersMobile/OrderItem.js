import React from "react";
import PropTypes from "prop-types";
import {imageUrl} from "../../../../../common/helpers";
import {formatDate, numberAsCurrentcy} from "../../../../../common/helpers/format";
import OrderStatus from "../../../_components/OrdersTable/OrderStatus";
import {Link} from "react-router-dom";

class OrderItem extends React.PureComponent {
  render() {
    const {order} = this.props;
    let imgUrl = null;
    const styles = {};
    if (order.cart_info && order.cart_info.length) {
      const {product_snapshot: product} = order.cart_info[0];
      if (product.images && product.images.length) {
        imgUrl = imageUrl(product.images[0].thumbnail_url);
      }
      if (imgUrl) {
        styles.backgroundImage = `url(${imgUrl})`;
      }
    }
    return (
      <div className="order-item">
        <div className="avatar-order" style={styles}/>
        <div className="overview-order">
          <div className="code-order">#{order.order_code}</div>
          <div className="status-order"><OrderStatus status={order.status}/></div>
          <div className="overview-order-bottom">
            <span className="created-order">{formatDate(order.created_at, 'dd/MM/yyyy - H:i:s')}</span>
            <span className="float-right">
              <span className="price-order">{numberAsCurrentcy(order.total || 0)}đ</span>
              <Link to={order.url} className="link-more"><i className="fas fa-chevron-right"/></Link>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

OrderItem.propTypes = {
  order: PropTypes.object,
};

export default OrderItem;

