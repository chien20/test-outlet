import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import {handleInputTextChanged, showAlert} from '../../../../common/helpers';
import {registerHIACEventAPI} from '../../../../api/hiac';

class DisputeModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        PDisplayName: '',
        PEmail: '',
        PIDCard: '',
        DDisplayName: '',
        DEmail: '',
        DIDCard: '',
        EventName: '',
        // EventContent: '',
        // EventPRequest: '',
        EventLink: '',
      },
      isOpen: false,
      isSubmitting: false,
    };
    this.formEl = React.createRef();
  }

  setData = (data) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        ...data,
      },
    }));
  };

  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.setState({
      isSubmitting: true,
    });
    registerHIACEventAPI(this.state.formData).then(res => {
      console.log(res);
      showAlert({
        type: 'success',
        message: `Đã gửi! Vui lòng chờ chúng tôi liên hệ!`,
      });
      this.handleClose();
      this.setState({
        isSubmitting: false,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
      });
      this.setState({
        isSubmitting: false,
      });
    })
  };

  render() {
    const {isOpen, formData, isSubmitting} = this.state;
    return (
      <Modal
        size="md"
        show={isOpen}
        onHide={this.handleClose}
        className="dispute-modal common-modal"
        centered
      >
        <form onSubmit={this.submit} ref={this.formEl}>
          <Modal.Header closeButton>
            <Modal.Title>Đăng ký tranh chấp</Modal.Title>
          </Modal.Header>
          <Modal.Body className="common-form">
            <p className="text-muted" style={{marginBottom: 10}}>
              Thông tin tranh chấp sẽ được gửi đến Trung tâm Trọng tài Quốc tế Hà Nội (HIAC). <br/>
              HIAC sẽ liên hệ với bạn ngay sau khi nhận được thông tin.
            </p>
            <h3 className="form-section-title">Thông tin của bạn</h3>
            <div className="form-group">
              <label>Họ và tên (*)</label>
              <input
                type="text"
                value={formData.PDisplayName}
                onChange={handleInputTextChanged('PDisplayName', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập họ tên
              </div>
            </div>
            <div className="form-group">
              <label>Email (*)</label>
              <input
                type="text"
                value={formData.PEmail}
                onChange={handleInputTextChanged('PEmail', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập email
              </div>
            </div>
            <div className="form-group">
              <label>CMT/Thẻ căn cước/Hộ chiếu (*)</label>
              <input
                type="text"
                value={formData.PIDCard}
                onChange={handleInputTextChanged('PIDCard', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập CMT/Thẻ căn cước/Hộ chiếu
              </div>
            </div>

            <h3 className="form-section-title">Thông tin bên được yêu cầu</h3>
            <div className="form-group">
              <label>Họ và tên cá nhân/công ty (*)</label>
              <input
                type="text"
                value={formData.DDisplayName}
                onChange={handleInputTextChanged('DDisplayName', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập họ tên cá nhân/công ty
              </div>
            </div>
            <div className="form-group">
              <label>Email (*)</label>
              <input
                type="text"
                value={formData.DEmail}
                onChange={handleInputTextChanged('DEmail', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập email
              </div>
            </div>
            <div className="form-group">
              <label>CMT/Thẻ căn cước/Hộ chiếu - Số giấy phép kinh doanh (công ty) (*)</label>
              <input
                type="text"
                value={formData.DIDCard}
                onChange={handleInputTextChanged('DIDCard', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập CMT/Thẻ căn cước/Hộ chiếu/Số GPKD
              </div>
            </div>

            <h3 className="form-section-title">Thông tin vụ việc</h3>
            <div className="form-group">
              <label>Tên vụ việc (*)</label>
              <input
                type="text"
                value={formData.EventName}
                onChange={handleInputTextChanged('EventName', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập tên vụ việc
              </div>
            </div>
            {/*<div className="form-group">*/}
            {/*<label>Nội dung vụ việc (*)</label>*/}
            {/*<textarea*/}
            {/*value={formData.EventContent}*/}
            {/*onChange={handleInputTextChanged('EventContent', this.setData)}*/}
            {/*className="form-control"*/}
            {/*required={true}*/}
            {/*/>*/}
            {/*<div className="invalid-feedback">*/}
            {/*Vui lòng nhập nội dung vụ việc*/}
            {/*</div>*/}
            {/*</div>*/}
            {/*<div className="form-group">*/}
            {/*<label>Yêu cầu của bạn (*)</label>*/}
            {/*<textarea*/}
            {/*value={formData.EventPRequest}*/}
            {/*onChange={handleInputTextChanged('EventPRequest', this.setData)}*/}
            {/*className="form-control"*/}
            {/*required={true}*/}
            {/*/>*/}
            {/*<div className="invalid-feedback">*/}
            {/*Vui lòng nhập yêu cầu của bạn*/}
            {/*</div>*/}
            {/*</div>*/}
            <div className="form-group">
              <label>Đường dẫn mua hàng (*)</label>
              <input
                type="text"
                value={formData.EventLink}
                onChange={handleInputTextChanged('EventLink', this.setData)}
                className="form-control"
                required={true}
              />
              <div className="invalid-feedback">
                Vui lòng nhập đường dẫn mua hàng
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="default" disabled={isSubmitting} onClick={this.handleClose}>
              Hủy bỏ
            </Button>
            <button className="btn btn-primary" disabled={isSubmitting} onClick={this.submit}>
              Gửi yêu cầu
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }
}

DisputeModal.propTypes = {};

export default DisputeModal;