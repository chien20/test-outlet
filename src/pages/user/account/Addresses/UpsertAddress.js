import React from 'react';
import PropTypes from 'prop-types';
import {
  handleCheckBoxChanged,
  handleInputTextChanged,
  handleSelectChange,
  showAlert
} from '../../../../common/helpers/index';
import {getListDistrictAPI, getListProvinceAPI, getListWardAPI} from '../../../../api/locations';
import Select from '../../../../components/Form/Select/Select';
import {addAddressesAPI, updateAddressesAPI} from '../../../../api/users';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import LoadScript from '../../../../components/GoogleMap/LoadScript';
import LocationPicker from '../../../../components/GoogleMap/LocationPicker';

class UpsertAddress extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        full_name: '',
        phone: '',
        province_id: '',
        district_id: '',
        ward_id: '',
        address: '',
        is_default: true, // true or false
        is_company_address: false, // true or false
        save_this_address: true,
        geocoding: null,
      },
      initialFormData: {},
      provinces: [{
        id: null,
        name: 'Chọn Tỉnh/Thành phố'
      }],
      districts: [{
        id: null,
        name: 'Chọn Quận/Huyện'
      }],
      wards: [{
        id: null,
        name: 'Chọn Phường/Xã/Thị trấn'
      }]
    };
    this.state.initialFormData = {...this.state.formData};
    this.unmounted = false;
  }

  componentDidMount() {
    this.getProvinces();
    this.getFormData();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.formData.province_id !== this.state.formData.province_id) {
      this.getDistricts();
    }
    if (prevState.formData.district_id !== this.state.formData.district_id) {
      this.getWards();
    }
    if (prevProps.address !== this.props.address) {
      this.getFormData();
    }
    if (prevState.formData !== this.state.formData) {
      if (this.props.onFormDataChanged) {
        this.props.onFormDataChanged(this.state.formData);
      }
    }
  }

  getFormData = () => {
    const {address} = this.props;
    if (!address) {
      return null;
    }
    this.setState(prevState => {
      const formData = {};
      Object.keys(prevState.formData).forEach(key => {
        formData[key] = address[key];
      });
      const initialFormData = {...formData};
      return {
        formData,
        initialFormData
      };
    });
  };

  getProvinces = () => {
    getListProvinceAPI().then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Tỉnh/Thành phố'
        });
        this.setState({
          provinces: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getDistricts = () => {
    const {formData} = this.state;
    getListDistrictAPI(formData.province_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Quận/Huyện'
        });
        this.setState({
          districts: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getWards = () => {
    const {formData} = this.state;
    getListWardAPI(formData.district_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Phường/Xã/Thị trấn'
        });
        this.setState({
          wards: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  setData = (data) => {
    if (data && data.province_id !== undefined) {
      data.district_id = null;
      data.ward_id = null;
      data.geocoding = null;
    }
    if (data && data.district_id !== undefined) {
      data.ward_id = null;
      data.geocoding = null;
    }
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  handleAddressChanged = (geocoding) => {
    this.setData({
      geocoding,
    });
  };

  handleSubmit = () => {
    const {formData} = this.state;
    const {address, onSuccess, onCancel} = this.props;
    if (!address) {
      addAddressesAPI(formData).then((res) => {
        if (onSuccess) {
          onSuccess(res?.data);
        }
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    } else {
      updateAddressesAPI(address.id, formData).then(() => {
        if (onSuccess) {
          onSuccess({
            ...address,
            formData,
          });
        }
        showAlert({
          type: 'success',
          message: `Đã lưu`
        });
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    }
    onCancel();
  };

  render() {
    const {onCancel, forCheckout} = this.props;
    const {formData, provinces, districts, wards} = this.state;
    const selectedProvince = provinces.find(item => item.id === formData.province_id) || null;
    const selectedDistrict = districts.find(item => item.id === formData.district_id) || null;
    const selectedWard = wards.find(item => item.id === formData.ward_id) || null;
    const isButtonSaveDisabled = !formData.full_name || !formData.phone || !formData.address || !formData.ward_id;
    return (
      <div className="upsert-address">
        <div className="row">
          <div className="col-6 full-width-mobile">
            <div className="form-group">
              <label>Họ tên</label>
              <input
                className="form-control"
                required={true}
                maxLength={128}
                value={formData.full_name}
                onChange={handleInputTextChanged('full_name', this.setData)}/>
            </div>
          </div>
          <div className="col-6 full-width-mobile">
            <div className="form-group">
              <label>Điện thoại</label>
              <input
                className="form-control"
                required={true}
                maxLength={15}
                value={formData.phone}
                onChange={handleInputTextChanged('phone', this.setData)}/>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-4 full-width-mobile">
            <div className="form-group">
              <label>Tỉnh/Thành phố</label>
              <Select
                options={provinces}
                value={selectedProvince}
                placeholder="Chọn tỉnh thành phố"
                onChange={handleSelectChange('province_id', 'id', this.setData)}/>
            </div>
          </div>
          <div className="col-4 full-width-mobile">
            <div className="form-group">
              <label>Quận/Huyện</label>
              <Select
                options={districts}
                value={selectedDistrict}
                placeholder="Chọn quận huyện"
                onChange={handleSelectChange('district_id', 'id', this.setData)}/>
            </div>
          </div>
          <div className="col-4 full-width-mobile">
            <div className="form-group">
              <label>Phường/Xã/Thị trấn</label>
              <Select
                options={wards}
                value={selectedWard}
                placeholder="Chọn xã"
                onChange={handleSelectChange('ward_id', 'id', this.setData)}/>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 additional-options">
            <div className="form-group">
              <label>Địa chỉ</label>
              <input
                className="form-control"
                required={true}
                maxLength={256}
                value={formData.address}
                onChange={handleInputTextChanged('address', this.setData)}/>
            </div>
            {
              selectedProvince && selectedDistrict && selectedWard && formData.province_id && formData.district_id && formData.ward_id &&
              <div className="form-group">
                <label>Chọn địa chỉ trên bản đồ để giao hàng nhanh nhất</label>
                <p className="hint">(Kéo thả điểm đánh dấu để chọn vị trí)</p>
                {
                  formData.geocoding &&
                  <p className="text-green"><i className="fas fa-check-circle"/> {formData.geocoding.address}</p>
                }
                <LoadScript>
                  <LocationPicker
                    province={selectedProvince}
                    district={selectedDistrict}
                    ward={selectedWard}
                    geocoding={formData.geocoding}
                    onAddressChanged={this.handleAddressChanged}
                  />
                </LoadScript>
              </div>
            }
            <div className="form-group">
              {
                forCheckout &&
                <Checkbox
                  label="Lưu lại địa chỉ này cho lần sau"
                  checked={formData.save_this_address}
                  onChange={handleCheckBoxChanged('save_this_address', this.setData, true, 'bool')}/>
              }
              {
                !forCheckout &&
                <Checkbox
                  label="Đặt làm địa chỉ mặc định"
                  checked={formData.is_default}
                  onChange={handleCheckBoxChanged('is_default', this.setData, true, 'bool')}/>
              }
              <Checkbox
                label="Đây là địa chỉ công ty"
                checked={formData.is_company_address}
                onChange={handleCheckBoxChanged('is_company_address', this.setData, true, 'bool')}/>
            </div>
          </div>
        </div>
        {
          !forCheckout &&
          <div className="row">
            <div className="col-12">
              <div className="button-group">
                {
                  onCancel &&
                  <button className="btn btn-default" type="button" onClick={onCancel}>Hủy bỏ</button>
                }
                <button
                  type="button"
                  className="btn btn-primary"
                  disabled={isButtonSaveDisabled}
                  onClick={this.handleSubmit}>Lưu lại
                </button>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

UpsertAddress.propTypes = {
  onCancel: PropTypes.func,
  onSuccess: PropTypes.func,
  address: PropTypes.object,
  forCheckout: PropTypes.bool,
  onFormDataChanged: PropTypes.func
};

export default UpsertAddress;
