import React from 'react';
import PropTypes from 'prop-types';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {deleteAddressesAPI} from '../../../../api/users';
import {showAlert} from '../../../../common/helpers';
import './AddressCard.scss';
import {isMobile} from "../../../../common/helpers/browser";
import {Link} from "react-router-dom";

class AddressCard extends React.PureComponent {
  editAddress = (event) => {
    event.stopPropagation();
    this.props.onEdit(this.props.address);
  };

  deleteAddress = (event) => {
    event.stopPropagation();
    const r = window.confirm('Địa chỉ này sẽ bị xóa. Bạn có chắc chắn?');
    if (r) {
      deleteAddressesAPI(this.props.address.id).then(() => {
        this.props.onDeleteSuccess();
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    }
  };

  render() {
    const {address, onClick, isSelected} = this.props;
    if (!address) {
      return null;
    }
    if (isMobile) {
      return (
        <div
          className="address-card-mobile">
          <div className="address-card-header">
            <span>{address.is_company_address ? `Công ty` : `Nhà riêng`}</span>
            <Link className="edit" to={`checkout-address/edit?address_id=${address.id}`}><i className="fas fa-pen"/></Link>
          </div>
          <div className="address-card-content">
            <p><span>{address.full_name}</span> | <span>{address.phone}</span></p>
            <p>{address.address}</p>
            <div className={`address-selected ${isSelected ? 'selected' : ''}`} onClick={onClick}>
              {/*<div className="checkbox d-flex-center">*/}
              {/*  {*/}
              {/*    isSelected &&*/}
              {/*    <i className="fas fa-check"/>*/}
              {/*  }*/}
              {/*</div>*/}
              {/*<span>Giao tới địa chỉ này</span>*/}
            </div>
          </div>
        </div>
      )
    }
    return (
      <div
        className={`address-card ${isSelected ? 'selected' : ''}`}
        onClick={onClick}
        style={{cursor: onClick ? 'pointer' : 'default'}}>
        <h4>{address.full_name}</h4>
        <div className="address-info">
          <p><i className="fas fa-map-marker-alt"/> Địa chỉ: <strong>{address.address}</strong></p>
          <p><i className="fas fa-mobile-alt"/> Điện thoại: {address.phone}</p>
        </div>
        {
          address.is_default &&
          <i className="default-address fas fa-star"/>
        }
        <div className="actions">
          <HyperLink className="edit-link" onClick={this.editAddress}>Sửa</HyperLink>
          <HyperLink className="delete-link" onClick={this.deleteAddress}>Xóa</HyperLink>
          {
            isSelected &&
            <i className="selected-address fas fa-check-circle"/>
          }
        </div>
      </div>
    )
  }
}

AddressCard.propTypes = {
  address: PropTypes.object,
  onEdit: PropTypes.func,
  onDeleteSuccess: PropTypes.func,
  onClick: PropTypes.func,
  isSelected: PropTypes.bool
};

export default AddressCard;
