import React, {Fragment} from "react";
import HeaderMobile from "../../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../components/HeaderMobile/TitleMobile/TitleMobile";
import qs from 'qs';
import {getAddressDetailAPI} from "../../../../api/users";
import {handleInputTextChanged} from "../../../../common/helpers";

class AddressEditMobile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      selectedAddress: null,
      selectedId: null,
      addNew: false
    };
    this.unmounted = false;
  }

  componentDidMount() {
    this.getSelectedId();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  setData = (data) => {
    this.setState(prevState => {
      const {selectedAddress} = prevState;
      return {
        selectedAddress: {
          ...selectedAddress,
          ...data,
        }
      };
    });
  };

  getSelectedId = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    if (!params.address_id) {
      return;
    }
    const addressId = params.address_id * 1;
    this.setState({
      isFetching: true,
      selectedId: addressId,
    }, this.getAddressDetail);
  };

  getAddressDetail = () => {
    const {selectedId} = this.state;
    getAddressDetailAPI(selectedId).then(res => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isFetching: false,
        selectedAddress: res.data,
      });
    }).catch(() => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isFetching: false
      });
    });
  };

  render() {
    const {selectedAddress} = this.state;
    console.log(this.state);
    return (
      <Fragment>
        <Fragment>
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile>Chỉnh sửa địa chỉ</TitleMobile>
          </HeaderMobile>
          <div className="container">
            <div className="address-edit-page page-content">
              {
                selectedAddress &&
                <form>
                  <div className="form-group">
                    <input
                      className="form-control"
                      name="textName"
                      type="text"
                      required={true}
                      maxLength={128}
                      placeholder="Tên người nhận"
                      value={selectedAddress.full_name}
                      onChange={handleInputTextChanged('full_name', this.setData)}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control"
                      name="numberPhone"
                      type="number"
                      required={true}
                      maxLength={15}
                      placeholder="Số điện thoại"
                    />
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control"
                      name="textName"
                      type="text"
                      required={true}
                      maxLength={128}
                      placeholder="Tên người nhận"
                    />
                  </div>
                </form>
              }
            </div>
          </div>
        </Fragment>
      </Fragment>
    )
  }
}

export default AddressEditMobile;