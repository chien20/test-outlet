import React, {Fragment} from 'react';
import {getAddressesAPI} from '../../../../api/users';
import {showAlert} from '../../../../common/helpers';
import DivLoading from '../../../../components/DivLoading/DivLoading';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import UpsertAddress from './UpsertAddress';
import AddressCard from './AddressCard';
import './Addresses.scss';
import NoData from '../../../../components/NoData/NoData';
import {isMobile} from "../../../../common/helpers/browser";
import HeaderMobile from "../../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../components/HeaderMobile/TitleMobile/TitleMobile";

class Addresses extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      addresses: [],
      selectedAddress: null,
      isFetching: false,
      addAddress: false
    };
  }

  componentDidMount() {
    this.getAddresses();
  }

  getAddresses = () => {
    this.setState({
      isFetching: true
    });
    getAddressesAPI().then(res => {
      this.setState({
        isFetching: false,
        addresses: res.data.list || []
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  toggleAddAddress = () => {
    this.setState(prevState => ({
      addAddress: !prevState.addAddress,
      selectedAddress: null
    }));
  };

  onUpsertAddressSuccess = () => {
    this.setState({
      addAddress: false,
      selectedAddress: null
    });
    this.getAddresses();
  };

  onEdit = (address) => {
    this.setState({
      addAddress: true,
      selectedAddress: address
    });
  };
  

  render() {
    const {isFetching, addresses, addAddress, selectedAddress} = this.state;
    return (
      <div className="addresses-page">
        {
          !isMobile &&
            <Fragment>
              <div className="page-title">
                <h1>Sổ địa chỉ</h1>
              </div>
              <div className="page-content">
                {
                  isFetching && <DivLoading/>
                }
                {
                  !isFetching &&
                  <Fragment>
                    {
                      !addresses.length && !addAddress &&
                      <NoData
                        title="Không có địa chỉ"
                        description="Bạn chưa có địa chỉ nào, click vào nút dưới đây để thêm địa chỉ mới.">
                        <HyperLink className="btn btn-primary" onClick={this.toggleAddAddress}>Thêm địa chỉ mới</HyperLink>
                      </NoData>
                    }
                    <div className="row address-list">
                      {
                        addresses.map((item) => (
                          <div
                            className="col-12 col-sm-6"
                            key={item.id}>
                            <AddressCard
                              address={item}
                              onEdit={this.onEdit}
                              onDeleteSuccess={this.getAddresses}
                            />
                          </div>
                        ))
                      }
                    </div>
                    {
                      addresses.length > 0 && !addAddress &&
                      <HyperLink onClick={this.toggleAddAddress}>Thêm địa chỉ mới</HyperLink>
                    }
                    {
                      addAddress &&
                      <UpsertAddress
                        address={selectedAddress}
                        onCancel={this.toggleAddAddress}
                        onSuccess={this.onUpsertAddressSuccess}/>
                    }
                  </Fragment>
                }
              </div>
            </Fragment>
        }
        {
          isMobile &&
            <Fragment>
              <HeaderMobile className="fixed">
                <IconBackMobile/>
                <TitleMobile>Sổ địa chỉ</TitleMobile>
              </HeaderMobile>
              <div className="page-content address-page">
                {
                  isFetching && <DivLoading/>
                }
                {
                  !isFetching &&
                  <Fragment>
                    {
                      !addresses.length && !addAddress &&
                      <NoData
                        title="Không có địa chỉ"
                        description="Bạn chưa có địa chỉ nào, click vào nút dưới đây để thêm địa chỉ mới.">
                        <HyperLink className="btn btn-primary" onClick={this.toggleAddAddress}>Thêm địa chỉ mới</HyperLink>
                      </NoData>
                    }
                    <div className="row address-list">
                      {
                        addresses.map((item) => (
                          <div
                            className="col-12 col-sm-6"
                            key={item.id}>
                            <AddressCard
                              address={item}
                              onEdit={this.onEdit}
                              onDeleteSuccess={this.getAddresses}
                            />
                          </div>
                        ))
                      }
                    </div>
                    {
                      addresses.length > 0 && !addAddress &&
                      <HyperLink onClick={this.toggleAddAddress}>Thêm địa chỉ mới</HyperLink>
                    }
                    {
                      addAddress &&
                      <UpsertAddress
                        address={selectedAddress}
                        onCancel={this.toggleAddAddress}
                        onSuccess={this.onUpsertAddressSuccess}/>
                    }
                  </Fragment>
                }
              </div>
            </Fragment>
        }
      </div>
    )
  }
}

export default Addresses;
