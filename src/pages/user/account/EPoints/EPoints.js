import React, {Fragment} from 'react';
import './EPoints.scss';
import {getUserPointsAPI, getUserPointsHistoryAPI} from '../../../../api/users';
import StatsCard from '../../../group/GroupDetail/Tabs/ManageGroup/_components/StatsCard/StatsCard';
import {convertNotification, formatDate, numberAsCurrentcy} from '../../../../common/helpers/format';
import EPointStatus from './EPointStatus';
import CommonTable from '../../../../components/Table/CommonTable';
import Pagination from '../../../../components/Pagination/Pagination';
import {history} from '../../../../common/utils/router/history';

const ITEMS_PER_PAGE = 10;

class EPoints extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      expiration: null,
      history: [],
      isFetching: false,
      totalItems: 0,
      page: 0
    }
  }

  componentDidMount() {
    this.getPoints();
    this.getPointsHistory();
  }

  getPoints = () => {
    this.setState({
      isFetching: true
    });
    getUserPointsAPI().then(res => {
      this.setState({
        ...res.data,
        isFetching: false
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    });
  };

  getPointsHistory = () => {
    const {match: {params: {page}}} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE
    };
    params.current_page -= 1;
    getUserPointsHistoryAPI(params).then(res => {
      this.setState({
        history: res.data.list || [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {

    });
  };

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/e-points/${page}`);
    } else {
      history.push(`/user/e-points`);
    }
  };

  columns = [
    {
      Header: 'Giá trị',
      id: 'value',
      width: 100,
      className: 'text-center',
      accessor: (item) => (
        <Fragment>
          {item.value < 0 && <span className="text-red">{numberAsCurrentcy(item.value)}</span>}
          {item.value > 0 && <span className="text-green">+{numberAsCurrentcy(item.value)}</span>}
        </Fragment>
      )
    },
    {
      Header: 'Thời gian',
      width: 100,
      id: 'created_at',
      className: 'text-right',
      accessor: (item) => (
        <div className="date">
          {formatDate(item.created_at, 'dd/MM/yyyy')}
        </div>
      )
    },
    {
      Header: 'Nội dung',
      id: 'total',
      accessor: (item) => (
        <div dangerouslySetInnerHTML={{__html: convertNotification(item.message, item.extra_data)}}/>
      )
    },
    {
      Header: 'Trạng thái',
      width: 120,
      id: 'status',
      className: 'text-center',
      accessor: (item) => (
        <EPointStatus status={item.status}/>
      )
    }
  ];

  render() {
    const {total, history, totalItems} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <div className="e-points-page">
        <div className="page-title">
          <h1>ePoint của tôi</h1>
        </div>
        <div className="page-content">
          <StatsCard
            icon="fas fa-coins"
            name="Tổng số ePoint hiện có"
            value={numberAsCurrentcy(total)}
            className="orange"/>
          <h3>Lịch sử ePoint</h3>
          <CommonTable
            className="points-history"
            data={history}
            columns={this.columns}
            showPagination={false}/>
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default EPoints;
