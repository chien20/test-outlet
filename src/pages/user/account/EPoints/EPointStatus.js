import React from 'react';

const EPointStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chưa xác nhận</span>;
    case 1:
      return <span className="text-green">Đã chấp nhận</span>;
    case 2:
      return <span className="text-red">Đã hủy</span>;
    default:
      return '';
  }
};

export default EPointStatus;
