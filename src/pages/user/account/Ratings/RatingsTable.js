import React from 'react';
import {dateFormat} from '../../../../common/helpers';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import StarRating from '../../../../components/StarRating/StarRating';
import {Link} from 'react-router-dom';

class RatingsTable extends React.PureComponent {
  render() {
    const {ratings, handleDeleteItem} = this.props;
    return (
      <div className="table-responsive ratings-table">
        <table className="table table-bordered">
          <thead>
          <tr>
            <th className="w-250px">Sản phẩm</th>
            <th>Đánh giá</th>
            <th className="w-150px">Ngày</th>
            <th className="w-80px">Thao tác</th>
          </tr>
          </thead>
          <tbody>
          {
            ratings.map((item, index) => (
              <tr key={index}>
                <td><Link to={`/${item.product.slug}/p${item.product.id}`}>{item.product.name}</Link></td>
                <td>
                  <StarRating value={item.rate || 0} isDisabled={true}/>
                  <p>{item.comment || ''}</p>
                </td>
                <td>{dateFormat('Y-m-d H:i:s', Math.floor(Date.parse(item.created_at) / 1000))}</td>
                <td><HyperLink onClick={handleDeleteItem(item.id)}>Xóa</HyperLink></td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default RatingsTable;
