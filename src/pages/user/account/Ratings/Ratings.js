import React, {Fragment} from 'react';
import {deleteProductRatingAPI, getRatingListAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import Pagination from '../../../../components/Pagination/Pagination';
import {history} from '../../../../common/utils/router/history';
import {Link} from 'react-router-dom';
import {formatDate} from '../../../../common/helpers/format';
import StarRating from '../../../../components/StarRating/StarRating';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import CommonTable from '../../../../components/Table/CommonTable';
import NoData from '../../../../components/NoData/NoData';

const ITEMS_PER_PAGE = 10;

class Ratings extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      ratings: [],
      totalItems: 0
    };
  }

  componentDidMount() {
    this.getRatings();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props !== prevProps) {
      this.getRatings();
    }
  }

  getRatings = () => {
    const {match: {params: {page}}} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE
    };
    params.current_page -= 1;
    getRatingListAPI(params).then(res => {
      if (res && res.data && res.data.list) {
        this.setState({
          isLoading: false,
          ratings: res.data.list,
          totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/ratings/${page}`);
    } else {
      history.push(`/user/ratings`);
    }
  };

  handleDeleteItem = (id) => () => {
    deleteProductRatingAPI([id]).then(() => {
      showAlert({
        type: 'success',
        message: `Đã xóa!`
      });
      this.getRatings();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  columns = [
    {
      Header: 'Sản phẩm',
      id: 'product',
      className: 'text-left',
      width: 220,
      accessor: (item) => (
        <Link
          className="text-ellipsis"
          to={`/${item.product.slug}/p${item.product.id}`}>
          {item.product.name}
        </Link>
      )
    },
    {
      Header: 'Bình luận',
      className: 'text-left',
      accessor: 'comment'
    },
    {
      Header: 'Đánh giá',
      id: 'rating',
      width: 150,
      className: 'text-center',
      accessor: (item) => (
        <StarRating value={item.rate || 0} isDisabled={true}/>
      )
    },
    {
      Header: 'Ngày',
      width: 100,
      id: 'created_at',
      className: 'text-center',
      accessor: (item) => (
        <div className="date">
          {formatDate(item.created_at, 'dd/MM/yyyy')}
        </div>
      )
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 80,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <Fragment>
          <HyperLink onClick={this.handleDeleteItem(item.id)} className="text-red">
            <i className="fas fa-trash"/> Xóa
          </HyperLink>
        </Fragment>
      )
    }
  ];

  render() {
    const {match: {params: {page}}} = this.props;
    const {isLoading, ratings, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <div className="ratings-page">
        <div className="page-title">
          <h1>Đánh giá của tôi</h1>
        </div>
        <div className="page-content">
          {
            !isLoading && !ratings.length &&
            <NoData
              title="Không có đánh giá nào"
              description="Bạn chưa đánh giá sản phẩm nào."
            >
              <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
            </NoData>
          }
          {
            !!ratings.length &&
            <CommonTable
              className="vouchers-table"
              data={ratings}
              columns={this.columns}
              showPagination={false}/>
          }
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Ratings;
