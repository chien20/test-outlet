import React, {Fragment} from 'react';
import './Chat.scss';
import {getConversationsAPI} from '../../../../api/chat';
import Avatar from '../../../../components/Avatar/Avatar';
import {formatDate} from '../../../../common/helpers/format';
import Conversation from './Conversation/Conversation';
import qs from 'qs';
import {getBasicUserInfoAPI} from '../../../../api/users';
import NoData from '../../../../components/NoData/NoData';
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {isMobile} from "../../../../common/helpers/browser";
import HyperLink from "../../../../components/HyperLink/HyperLink";
import HeaderMobile from "../../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../components/HeaderMobile/TitleMobile/TitleMobile";
import CartIconMobile from '../../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import history from "../../../../common/utils/router/history";

class Chat extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      keyword: '',
      conversations: [],
      selectedConversationId: null
    }
  }

  componentDidMount() {
    this.getConversations().then().catch(error => {

    });
  }

  getConversations = async () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    this.setState({
      isFetching: true
    });
    const {data: {list: conversations}} = await getConversationsAPI();
    const state = {
      conversations: conversations,
      selectedConversationId: conversations.length ? conversations[0].user.id : null,
      isFetching: false
    };
    if (params && params.new && params.new === 'true' && params.user_id) {
      const userId = parseInt(params.user_id);
      const selectedConversation = conversations.find(item => item.user.id === userId);
      if (selectedConversation) {
        state.selectedConversationId = selectedConversation.user.id;
      } else {
        const {data} = await getBasicUserInfoAPI(userId);
        console.log(data);
        state.conversations.splice(0, 0, {
          id: 0,
          user: data,
          unread_count: 0,
          last_message: '',
          updated_at: null
        });
        state.selectedConversationId = userId;
      }
    }
    this.setState(state);
  };

  selectConversation = (selectedConversation) => () => {
    this.setState({
      selectedConversationId: selectedConversation.user.id
    });
  };

  handleOnchange = (e) => {
    this.setState({
      keyword: e.target.value.toLowerCase(),
    })


  }
  render() {
    const {conversations,keyword, selectedConversationId, isFetching} = this.state;
    const conversation = conversations.find(item => item.user.id === selectedConversationId);
    const {user} = this.props;
    let conversationsTemp = conversations;
    if(keyword) {
      conversationsTemp = conversations.filter((item) => {
        return item.user.full_name.toLowerCase().indexOf(keyword) !== -1;
      })
    }


    return (
      <Fragment>
      {
          isMobile &&
          <HeaderMobile className="fixed">
            <IconBackMobile/>
            <TitleMobile>Tin nhắn của tôi</TitleMobile>
            <CartIconMobile/>
          </HeaderMobile>
        }
        <div className="chat-page">
          {
            !0 &&
            <div className="page-content padding-0">
              {
                !isFetching && !conversations.length &&
                <NoData description="Bạn chưa có cuộc hội thoại nào"/>
              }
              {
                !!conversations.length &&
                <div className={`chat-window ${isFetching ? 'hidden' : ''}`}>
                  <div className="conversations">
                    <div className="conversations__top">
                      <div className="user-info">
                        {
                          isMobile &&
                          <div className="m-r-8" onClick={history.goBack}><i className="material-icons">keyboard_backspace</i></div>
                        }
                        <Avatar src={user.avatar}  size={50} alt="avatar" className="profile-image"/>
                        <div className="user-name-wrapper">
                          <div className="user-name">{user.full_name}</div>
                          <div className="online">Online</div>
                        </div>
                      </div>
                      <div className="actions">
                        <HyperLink title="Làm mới" onClick={this.getConversations}><i className="material-icons">cached</i></HyperLink>
                        {/*<HyperLink title="Thu gọn"><i className="material-icons">menu</i></HyperLink>*/}
                      </div>

                    </div>

                    <div className="search-box">
                      <div className="input-wrapper">
                        <input type="text" value={keyword}  onChange={this.handleOnchange} placeholder="Tìm kiếm"/>
                        <i className="material-icons">search</i>
                      </div>
                    </div>
                    {
                      !conversationsTemp.length &&
                      <NoData description="Không tìm  thấy tin nhắn phù hợp"/>
                    }

                    {
                      !!conversationsTemp.length &&
                      <ul>
                        {
                          !isMobile &&

                          <Fragment>
                            {
                              conversationsTemp.map((item, index) => (

                                <li
                                  key={index}
                                  className={`${item.user.id === selectedConversationId ? 'active' : ''}`}
                                  onClick={this.selectConversation(item)}>
                                  <div className="user-avatar">
                                    <Avatar src={item.user.avatar} size={50}/>
                                  </div>
                                  <div className="conversation-content">
                                    <h4>{item.user.full_name}</h4>
                                    <div className="chat-excerpt">
                                      <p>{item.last_message}</p>
                                      <span className="time">{formatDate(item.created_at, 'dd/MM/yyyy')}</span>
                                    </div>
                                  </div>
                                </li>
                              ))
                            }
                          </Fragment>
                        }

                        {
                          isMobile &&
                          <Fragment>
                            {
                              conversationsTemp.map((item, index) => (

                                <li
                                  key={index}
                                  // className={`${item.user.id === selectedConversationId ? 'active' : ''}`}
                                  onClick={this.selectConversation(item)}>
                                  <Link to={`/user/chat/${item.user.id}`}>
                                    <div className="user-avatar">
                                      <Avatar src={item.user.avatar} size={50}/>
                                    </div>
                                    <div className="conversation-content">
                                      <h4>{item.user.full_name}</h4>
                                      <div className="chat-excerpt">
                                        <p>{item.last_message}</p>
                                        <span className="time">{formatDate(item.created_at, 'dd/MM/yyyy')}</span>
                                      </div>
                                    </div>
                                  </Link>

                                </li>
                              ))
                            }
                          </Fragment>
                        }


                      </ul>
                    }

                  </div>
                  <div className="conversation-wrapper">
                    {
                      selectedConversationId &&
                      <Conversation conversation={conversation} id={selectedConversationId} key={selectedConversationId}/>
                    }
                  </div>
                </div>
              }
            </div>
          }



        </div>
      </Fragment>

    );
  }
}

const mapSateToProps = (state) => ({
  user: state.user.info
});

export default withRouter(connect(mapSateToProps)(Chat));
