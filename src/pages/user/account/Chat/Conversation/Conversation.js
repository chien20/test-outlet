import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './Conversation.scss';
import {getConversationMessagesAPI} from '../../../../../api/chat';
import {connect} from 'react-redux';
import {uuidv4} from '../../../../../common/helpers';
import __debounce from 'lodash/debounce';
import Avatar from "../../../../../components/Avatar/Avatar";
import HyperLink from "../../../../../components/HyperLink/HyperLink";
import 'emoji-mart/css/emoji-mart.css';
import {Picker} from 'emoji-mart';
import Socket from '../../../../../common/utils/network/Socket';
import Broadcaster from '../../../../../common/helpers/broadcaster';

const ITEMS_PER_PAGE = 20;

class Conversation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      messages: [],
      message: '',
      isEmoji: false,
      isSubmitting: false,
      hasMore: false,
      nextPage: 0,
      typing: false,
      isHeTyping: false,
    };
    this.listMessageEl = React.createRef();
    this.inputEl = React.createRef();
  }

  componentDidMount() {
    this.getMessages();
    Broadcaster.on('socket.message.receive', this.onMessageReceived);
    Broadcaster.on('socket.message.send.success', this.onMessageSendSuccess);
    Broadcaster.on('socket.message.send.failed', this.onMessageSendFailed);
    Broadcaster.on('socket.message.typing', this.onTypingReceived);
  }

  componentWillUnmount() {
    Broadcaster.off('socket.message.receive', this.onMessageReceived);
    Broadcaster.off('socket.message.send.success', this.onMessageSendSuccess);
    Broadcaster.off('socket.message.send.failed', this.onMessageSendFailed);
    Broadcaster.off('socket.message.typing', this.onTypingReceived);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.typing !== this.state.typing) {
      this.handleTypingChanged();
    }
  }

  getMessages = (loadMore = false) => {
    const {id} = this.props;
    const {nextPage, isFetching} = this.state;
    if (isFetching) {
      return;
    }
    this.setState({
      isFetching: true
    });
    const params = {
      current_page: nextPage,
      page_size: ITEMS_PER_PAGE
    };
    getConversationMessagesAPI(id, params, {__showLoading: !loadMore}).then(res => {
      const messages = res.data.list || [];
      const pageInfo = res.data.page_info;
      this.setState(prevState => {
        const newMessages = messages.reverse();
        return {
          isFetching: false,
          messages: [...newMessages, ...prevState.messages],
          hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
          nextPage: pageInfo.current_page + 1
        };
      }, () => {
        if (!loadMore) {
          this.scrollMessage();
          this.inputEl.current.focus();
        }
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    });
  };

  scrollMessage = () => {
    const listEl = this.listMessageEl.current;
    listEl.scrollTop = listEl.scrollHeight;
  };

  onScroll = () => {
    const {hasMore} = this.state;
    if (!hasMore) {
      return;
    }
    if (this.listMessageEl.current.scrollTop === 0) {
      this.getMessages(true);
    }
  };

  __onScroll = __debounce(this.onScroll, 300);

  onInputChange = (event) => {
    const message = event.target.value || '';
    this.setState({
      message,
      typing: !!message,
    });
  };

  handleTypingChanged = () => {
    const {id} = this.props;
    const {typing} = this.state;
    Socket.emit('message.typing', {
      to: id,
      typing,
    });
  };

  onTypingReceived = (data) => {
    const {id} = this.props;
    if (data?.user?.id !== id) {
      return;
    }
    this.setState({
      isHeTyping: data?.typing,
    });
  };

  onMessageReceived = (data) => {
    const {id} = this.props;
    if (data?.user?.id !== id) {
      return;
    }
    this.setState(prevState => {
      const messages = [...prevState.messages, data];
      return {
        messages,
      };
    }, this.scrollMessage);
  };

  onMessageSent = (status) => (data) => {
    this.setState(prevState => {
      const messages = [...prevState.messages];
      const idx = messages.findIndex(item => item.local_id === data?.local_id);
      if (idx >= 0) {
        messages[idx] = {
          ...messages[idx],
          sent: status,
        };
        return {
          messages,
        };
      }
      return null;
    });
  };

  onMessageSendSuccess = this.onMessageSent(true);
  onMessageSendFailed = this.onMessageSent(false);

  sendMessage = (event) => {
    event.preventDefault();
    const {id, user} = this.props;
    const {message} = this.state;
    const postData = {
      to: id,
      body: message,
      local_id: uuidv4(),
    };
    Socket.emit('message.send', postData);
    this.setState(prevState => {
      const newMessage = {
        body: message,
        created_at: new Date().getTime(),
        user,
        sent: null,
        local_id: postData.local_id,
      };
      return {
        messages: [...prevState.messages, newMessage],
        message: '',
        typing: false,
      };
    }, this.scrollMessage);
  };


  // refreshMessages
  refreshMessages = () => {
    this.getMessages();
  };

  // emoji

  addEmoji = e => {
    let emoji = e.native;
    this.setState(prevState => ({
      message: prevState.message + emoji,
      typing: true,
    }));
  };

  toggleEmoji = () => {
    const {isEmoji} = this.state;
    this.setState({
      isEmoji: !isEmoji
    });
  };

  closeEmoji = () => {
    this.setState({
      isEmoji: false
    });
  };

  render() {
    const {user, conversation} = this.props;
    const {messages, message, isEmoji, isSubmitting, isFetching, isHeTyping, hasMore} = this.state;

    return (
      <div className="conversation">
        <div className="conversation__top">
          <div className="profile d-flex align-items-center">
            <Avatar src={conversation.user.avatar} size={50}/>
            <strong className="m-l-8">{conversation.user.full_name}</strong>
          </div>

          <div className="actions">
            <HyperLink title="Làm mới " onClick={this.refreshMessages}><i className="material-icons">cached</i></HyperLink>
          </div>
        </div>
        <div className="messages">
          <ul className="custom-scrollbar list-message" ref={this.listMessageEl} onScroll={this.__onScroll}>
            {
              hasMore &&
              <li className="loading-message text-center">...loading...</li>
            }
            {
              !isFetching && !messages.length &&
              <li className="text-muted text-center">Hãy nói gì đó để bắt đầu</li>
            }
            {
              messages.map((item, index) => (
                <li
                  key={index}
                  className={`bubble ${item.user.id !== user.id ? 'you' : 'me'}`}
                >
                  {item.body}
                  {
                    (item.sent === null || item.sent === false) &&
                    <Fragment>
                      <br/>
                      {
                        item.sent === null && <small>Đang gửi...</small>
                      }
                      {
                        item.sent === false && <small>Không gửi được!</small>
                      }
                    </Fragment>
                  }
                </li>
              ))
            }
          </ul>
          {
            isHeTyping &&
            <div className="typing-message">{conversation?.user?.full_name} đang gõ...</div>
          }
        </div>
        <div className="write-message">
          <form className="input-wrap" onSubmit={this.sendMessage}>
            <div className="emoji-chat">
              <i className="material-icons" onClick={this.toggleEmoji}>sentiment_very_satisfied</i>
            </div>
            <div className={isEmoji ? `show` : ``} onBlur={this.closeEmoji}>
              {
                isEmoji &&
                <Picker set='google' onSelect={this.addEmoji}/>
              }
            </div>
            <input
              ref={this.inputEl}
              type="text"
              value={message}
              onChange={this.onInputChange}
              placeholder="Nhập tin nhắn..."
            />
            <button
              className="send-icon"
              type="submit"
              title="Gửi tin nhắn"
              disabled={!message || isSubmitting}>
              <i className="material-icons">send</i>
            </button>
          </form>
        </div>
      </div>
    );
  }
}

Conversation.propTypes = {
  id: PropTypes.number,
  conversation: PropTypes.object
};

const mapStateToProps = (state) => ({
  user: state.user.info,
});

export default connect(mapStateToProps)(Conversation);
