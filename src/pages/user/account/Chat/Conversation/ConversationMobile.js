import React, {Fragment} from 'react';
import IconBackMobile from "../../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../../components/HeaderMobile/TitleMobile/TitleMobile";
import CartIconMobile from "../../../../../components/HeaderMobile/CartIconMobile/CartIconMobile";
import HeaderMobile from "../../../../../components/HeaderMobile/HeaderMobile";
import {getConversationMessagesAPI, sendMessageAPI} from "../../../../../api/chat";
import __debounce from "lodash/debounce";
import {showAlert} from "../../../../../common/helpers";
import Avatar from "../../../../../components/Avatar/Avatar";
import HyperLink from "../../../../../components/HyperLink/HyperLink";
import {Picker} from "emoji-mart";
import {connect} from "react-redux";
import {getBasicUserInfoAPI} from "../../../../../api/users";
import './ConversationMobile.scss';
import history from "../../../../../common/utils/router/history";


const ITEMS_PER_PAGE = 20;

class ConversationMobile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      messages: [],
      message: '',
      isEmoji: false,
      isSubmitting: false,
      hasMore: false,
      nextPage: 0,
      conversation: '',
    };
    this.listMessageEl = React.createRef();
    this.inputEl = React.createRef();
  }

  componentDidMount() {
    this.getBasicUser();
    this.getMessages();
  }

  getBasicUser () {
    const {match: {params: {id}}} = this.props;
    getBasicUserInfoAPI(id).then(res => {
      const data = res.data;
      this.setState({
        conversation: data
      })
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    })
  };



  getMessages = (loadMore = false) => {
    const {match: {params: {id}}} = this.props;
    const {nextPage, isFetching} = this.state;
    if (isFetching) {
      return;
    }
    this.setState({
      isFetching: true
    });
    const params = {
      current_page: nextPage,
      page_size: ITEMS_PER_PAGE
    };
    getConversationMessagesAPI(id, params, {__showLoading: !loadMore}).then(res => {
      const messages = res.data.list || [];
      const pageInfo = res.data.page_info;
      this.setState(prevState => {
        const newMessages = messages.reverse();
        return {
          isFetching: false,
          messages: [...newMessages, ...prevState.messages],
          hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
          nextPage: pageInfo.current_page + 1
        };
      }, () => {
        if (!loadMore) {
          this.scrollMessage();
          this.inputEl.current.focus();
        }
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    });
  };

  scrollMessage = () => {
    const listEl = this.listMessageEl.current;
    listEl.scrollTop = listEl.scrollHeight;
  };

  onScroll = () => {
    const {hasMore} = this.state;
    if (!hasMore) {
      return;
    }
    if (this.listMessageEl.current.scrollTop === 0) {
      this.getMessages(true);
    }
  };

  __onScroll = __debounce(this.onScroll, 300);

  onInputChange = (event) => {
    this.setState({
      message: event.target.value
    });
  };

  sendMessage = (event) => {
    event.preventDefault();
    const {match: {params: {id}}, user} = this.props;
    const {message, isSubmitting} = this.state;
    if (isSubmitting) {
      return;
    }
    this.setState({
      isSubmitting: true
    });
    sendMessageAPI({
      user_id: id,
      body: message
    }).then(res => {
      this.setState(prevState => {
        const newMessage = {
          body: message,
          created_at: res.data,
          user
        };
        return {
          messages: [...prevState.messages, newMessage],
          message: '',
          isSubmitting: false
        };
      }, this.scrollMessage);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Không gửi được tin nhắn, vui lòng thử lại sau`
      });
      this.setState({
        isSubmitting: false
      });
    });
  };


  // refreshMessages
  refreshMessages = () => {
    this.getMessages();
  }

  // emoji

  addEmoji = e => {
    let emoji = e.native;
    this.setState({
      message: this.state.message + emoji
    });
  };

  toggleEmoji = () => {
    const {isEmoji} = this.state;
    this.setState({
      isEmoji: !isEmoji
    })
  }

  closeEmoji = () => {
    this.setState({
      isEmoji: false
    })
  }

  render() {
    const {match: {params: {id}}, user} = this.props;

    const {messages, message, isEmoji, isSubmitting, isFetching, hasMore, conversation} = this.state;
    console.log(conversation);

    console.log(id);
    return (
      <Fragment>
        <div className="conversation">
          <div className="conversation__top">
            <div className="profile d-flex align-items-center">
              <div className="m-r-8" onClick={history.goBack}><i className="material-icons">keyboard_backspace</i></div>
              <Avatar src={conversation.avatar} size={50}/>
              <strong className="m-l-8">{conversation.full_name}</strong>
            </div>

            <div className="actions">
              <HyperLink title="Làm mới " onClick={this.refreshMessages}><i className="material-icons">cached</i></HyperLink>
            </div>
          </div>
          <div className="messages">
            <ul className="custom-scrollbar" ref={this.listMessageEl} onScroll={this.__onScroll}>
              {
                hasMore &&
                <li className="loading-message text-center">...loading...</li>
              }
              {
                !isFetching && !messages.length &&
                <li className="text-muted text-center">Hãy nói gì đó để bắt đầu</li>
              }
              {
                messages.map((item, index) => (
                  <li
                    key={index}
                    className={`bubble ${item.user.id !== user.id ? 'you' : 'me'}`}>
                    {item.body}
                  </li>
                ))
              }
            </ul>
          </div>
          <div className="write-message">
            <form className="input-wrap" onSubmit={this.sendMessage}>
              <div className="emoji-chat" >
                <i className="material-icons" onClick={this.toggleEmoji}>sentiment_very_satisfied</i>
              </div>
              <div className={isEmoji ? `show` : ``} onBlur={this.closeEmoji}>
                {
                  isEmoji &&

                  <Picker set='google' onSelect={this.addEmoji} />
                }
              </div>
              <input
                ref={this.inputEl}
                type="text"
                value={message}
                onChange={this.onInputChange}
                placeholder="Nhập tin nhắn..."
              />
              <button
                className="send-icon"
                type="submit"
                title="Gửi tin nhắn"
                disabled={!message || isSubmitting}>
                <i className="material-icons">send</i>
              </button>
            </form>
          </div>
        </div>
      </Fragment>


    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
});

export default connect(mapStateToProps)(ConversationMobile);