import React from 'react';
import {showAlert} from '../../../../common/helpers';
import {history} from '../../../../common/utils/router/history';
import {getRefundsAPI} from '../../../../api/refunds';
import {formatDate, numberAsCurrentcy} from '../../../../common/helpers/format';
import RefundItems from './RefundItems';
import RefundStatus from './RefundStatus';
import CommonTable from '../../../../components/Table/CommonTable';
import Pagination from '../../../../components/Pagination/Pagination';
import {connect} from 'react-redux';
import NoData from '../../../../components/NoData/NoData';

const ITEMS_PER_PAGE = 10;

class Refunds extends React.PureComponent {
  state = {
    isLoading: true,
    items: [],
    totalItems: 0,
  };

  componentDidMount() {
    this.getItems();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props !== prevProps) {
      this.getItems();
    }
  }

  getItems = () => {
    const {match: {params: {page}, path}, storeId} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE,
      // store_id: storeId,
    };
    if (`${path}`.includes('/store/')) {
      params.store_id = storeId;
      params.from_store = true;
    }
    params.current_page -= 1;
    getRefundsAPI(params).then(res => {
      if (res && res.data && res.data.list) {
        this.setState({
          isLoading: false,
          items: res.data.list,
          totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    const {match: {path}} = this.props;
    if (`${path}`.includes('/store/')) {
      if (page > 1) {
        history.push(`/user/store/refunds/${page}`);
      } else {
        history.push(`/user/store/refunds`);
      }
      return;
    }
    if (page > 1) {
      history.push(`/user/refunds/${page}`);
    } else {
      history.push(`/user/refunds`);
    }
  };

  columns = [
    {
      Header: 'Sản phẩm',
      id: 'products',
      className: 'text-left',
      accessor: (item) => {
        const {match: {path}} = this.props;
        const isShop = `${path}`.includes('/store/');
        return (
          <RefundItems itemId={item.id} orderDetailInfo={item.order_detail_info} isShop={isShop}/>
        );
      }
    },
    {
      Header: 'Giá trị',
      className: 'text-right',
      id: 'total',
      width: 150,
      accessor: (item) => (
        <span>{numberAsCurrentcy(item.total_price)}đ</span>
      )
    },
    {
      Header: 'Trạng thái',
      id: 'status',
      width: 200,
      className: 'text-center',
      accessor: (item) => (
        <RefundStatus item={item}/>
      )
    },
    {
      Header: 'Ngày',
      width: 100,
      id: 'created_at',
      className: 'text-center',
      accessor: (item) => (
        <div className="date">
          {formatDate(item.created_at, 'dd/MM/yyyy')}
        </div>
      )
    }
  ];

  render() {
    const {match: {params: {page}}} = this.props;
    const {isLoading, items, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <div className="ratings-page">
        <div className="page-title">
          <h1>Yêu cầu trả hàng/hoàn tiền</h1>
        </div>
        <div className="page-content">
          {
            !isLoading && !items.length &&
            <NoData
              title="Wow"
              description="Không có yêu cầu trả hàng nào. Thật tuyệt!"
            >
            </NoData>
          }
          {
            !!items.length &&
            <CommonTable
              className="vouchers-table"
              data={items}
              columns={this.columns}
              showPagination={false}
              minRows={0}
            />
          }
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
});

export default connect(mapStateToProps)(Refunds);
