import React from 'react';

export const REFUND_STATUS = {
  CUSTOMER_CREATED: {
    id: 0,
    name: 'Khách hàng gửi yêu cầu',
    actionName: 'Gửi yêu cầu',
    nextStatus: ['E-10', 'E-11', 'U-30'],
  },
  EPLAZA_ACCEPT: {
    id: 10,
    name: 'Chờ người bán chấp nhận',
    actionName: 'Điều chỉnh yêu cầu',
    nextStatus: ['S-20', 'S-21', 'U-30'],
  },
  EPLAZA_REJECT: {
    id: 11,
    name: 'Outlet từ chối xử lý',
    actionName: 'Từ chối yêu cầu',
    className: 'text-red',
  },
  SHOP_ACCEPT: {
    id: 20,
    name: 'Shop chấp nhận yêu cầu',
    actionName: 'Chấp nhận yêu cầu',
    className: 'text-blue',
    nextStatus: ['U-22', 'U-30'],
  },
  SHOP_REJECT: {
    id: 21,
    name: 'Shop từ chối yêu cầu',
    actionName: 'Từ chối yêu cầu',
    className: 'text-red',
    nextStatus: ['U-10', 'U-31', 'U-30'],
  },
  CUSTOMER_SENT_PRODUCT: {
    id: 22,
    name: 'Khách hàng đã gửi sản phẩm',
    actionName: 'Tôi đã gửi sản phẩm',
    className: 'text-blue',
    nextStatus: ['S-23', 'S-24'],
  },
  SHOP_RECEIVED_PRODUCT: {
    id: 23,
    name: 'Shop đã nhận sản phẩm',
    actionName: 'Tôi đã nhận được sản phẩm',
    className: 'text-blue',
    nextStatus: ['E-40'],
  },
  SHOP_RECEIVED_BAD_PRODUCT: {
    id: 24,
    name: 'Hàng shop nhận được bị lỗi',
    actionName: 'Tôi đã nhận được sản phẩm nhưng sản phẩm bị lỗi',
    className: 'text-red',
    nextStatus: ['U-31'],
  },
  CUSTOMER_CANCEL: {
    id: 30,
    actionName: 'Hủy yêu cầu',
    name: 'Yêu cầu bị hủy',
  },
  CUSTOMER_RESEND: {
    id: 31,
    name: 'Yêu cầu kiểm tra lại',
    actionName: 'Yêu cầu Outlet xem xét',
  },
  CUSTOMER_REQUEST_HIAC: {
    id: 32,
    name: 'Yêu cầu HIAC xử lý',
    actionName: 'Yêu cầu HIAC xử lý',
  },
  EPLAZA_REVIEW_RESULT: {
    id: 33,
    name: 'Xác nhận kết quả',
    actionName: 'Xác nhận kết quả',
    className: 'text-orange',
    nextStatus: ['U-33', 'S-33'],
  },
  EPLAZA_REFUND: {
    id: 40,
    name: 'Đã hoàn tiền',
    actionName: 'Đã hoàn tiền cho khách hàng',
    className: 'text-green',
    nextStatus: ['U-50'],
  },
  COMPLETED: {
    id: 50,
    name: 'Hoàn thành',
    actionName: 'Đã hoàn tất',
    className: 'text-green',
  },
};

const RefundStatus = React.memo(({item}) => {
  const status = Object.values(REFUND_STATUS).find(i => i.id === item.status);
  if (status) {
    return (
      <span className={status.className ? status.className : ''}>
        {status.name}
      </span>
    );
  }
  return null;
});

export default RefundStatus;
