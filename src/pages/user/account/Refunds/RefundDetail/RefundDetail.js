import React, {Fragment} from 'react';
import Skeleton from 'react-loading-skeleton';
import NoData from '../../../../../components/NoData/NoData';
import {getRefundDetailAPI, getRefundMetadataAPI} from '../../../../../api/refunds';
import {getErrorMessage} from '../../../../../common/helpers/language';
import {dateFormat, showAlert} from '../../../../../common/helpers';
import './RefundDetail.scss';
import {getBasicUserInfoAPI} from '../../../../../api/users';
import {getStoreDetailAPI} from '../../../../../api/stores';
import {numberAsCurrentcy} from '../../../../../common/helpers/format';
import RefundStatus, {REFUND_STATUS} from '../RefundStatus';
import UpdateRefundStatus from './UpdateRefundStatus';
import PreviewImages from '../../../../../components/images/PreviewImages';

class RefundDetail extends React.PureComponent {
  state = {
    isFetching: true,
    refundDetail: null,
  };

  componentDidMount() {
    this.getRefund();
  }

  getRefund = () => {
    this.getRefundAsync().then(refund => {
      this.setState({
        isFetching: false,
        refundDetail: refund,
      });
    }).catch(error => {
      this.setState({
        isFetching: false,
        refundDetail: null,
      });
      showAlert({
        type: 'error',
        message: `${getErrorMessage(error)}`,
      });
    });
  };

  getRefundAsync = async () => {
    const {match: {params: {id}}} = this.props;
    const {data: refund} = await getRefundDetailAPI(id);
    if (refund) {
      const {data: user} = await getBasicUserInfoAPI(refund.user_id);
      const {data: shop} = await getStoreDetailAPI(refund.store_id);
      refund.user = user;
      refund.shop = shop;
      refund.products = [];
      refund.order_detail_info.forEach(orderDetail => {
        refund.products.push(JSON.parse(orderDetail.order_detail.product_snapshot));
      });
      if (refund.eplaza_result_id) {
        const {data: {results}} = await getRefundMetadataAPI();
        refund.eplaza_result = results.find(item => item.id === refund.eplaza_result_id);
      }
    }
    return refund;
  };

  render() {
    const {isFetching, refundDetail} = this.state;
    return (
      <div className="refund-detail-page">
        <div className="page-title">
          <h1>Chi tiết yêu cầu</h1>
        </div>
        <div className="page-content">
          {
            isFetching && <Skeleton/>
          }
          {
            !isFetching && !refundDetail &&
            <NoData/>
          }
          {
            !isFetching && !!refundDetail &&
            <Fragment>
              <div className="d-flex justify-content-between">
                <div className="refund-section ">
                  <h2>Thông tin khách hàng</h2>
                  <div className="info-table">
                    <div className="info"><strong>Họ tên:</strong> <span>{refundDetail.user.full_name}</span></div>
                    <div className="info"><strong>Email:</strong> <span>{refundDetail.customer_email}</span></div>
                    <div className="info"><strong>Số điện thoại:</strong> <span>{refundDetail.customer_phone}</span>
                    </div>
                  </div>
                </div>
                <div className="refund-section ">
                  <h2>Thông tin shop</h2>
                  <div className="info-table">
                    <div className="info"><strong>Tên shop:</strong> <span>{refundDetail.shop.name}</span></div>
                    <div className="info"><strong>Email:</strong> <span>{refundDetail.shop.email}</span></div>
                    <div className="info"><strong>Số điện thoại:</strong> <span>{refundDetail.shop.phone}</span></div>
                    {/*<div className="info"><strong>Địa chỉ:</strong> <span>{refundDetail.shop.address}</span></div>*/}
                  </div>
                </div>
              </div>
              <div className="refund-section refund-info">
                <h2>Thông tin yêu cầu</h2>
                <div className="info-table">
                  <div className="info"><strong>Thời gian tạo:</strong>
                    <span>{dateFormat('Y-m-d H:i:s', Math.floor(Date.parse(refundDetail.created_at) / 1000))}</span>
                  </div>
                  <div className="info"><strong>Nguyên nhân:</strong> <span>{refundDetail.reason.name}</span></div>
                  <div className="info"><strong>Yêu cầu:</strong> <span>{refundDetail.result.name}</span></div>
                  <div className="info"><strong>Số tiền:</strong>
                    <span>{numberAsCurrentcy(refundDetail.total_price)}đ</span></div>
                  <div className="info"><strong>Trạng thái xử lý:</strong> <RefundStatus item={refundDetail}/></div>
                </div>
              </div>
              <div className="products-table refund-section">
                <h2>Sản phẩm</h2>
                <table className="table table-bordered">
                  <tbody>
                  <tr>
                    <th className="text-center">STT</th>
                    <th>Tên sản phẩm</th>
                    <th className="text-center">Số lượng</th>
                    <th className="text-center">Đơn giá</th>
                    <th className="text-center">Thành tiền</th>
                  </tr>
                  {
                    refundDetail.products.map((item, index) => (
                      <tr key={index}>
                        <td className="text-center">{index + 1}</td>
                        <td>{item.name}</td>
                        <td className="text-right">{item.qty}</td>
                        <td className="text-right">{numberAsCurrentcy(item.price)}đ</td>
                        <td className="text-right">{numberAsCurrentcy(item.price * item.qty)}đ</td>
                      </tr>
                    ))
                  }
                  </tbody>
                </table>
              </div>
              <div className="refund-section">
                <h2>Thông tin khác</h2>
                <div className="other-info">
                  <label>Khách hàng ghi chú:</label>
                  <p>{refundDetail.customer_note_reason || 'N/A'}</p>
                </div>
                <div className="other-info">
                  <label>Shop ghi chú:</label>
                  <p>{refundDetail.seller_note_reason || 'N/A'}</p>
                </div>
                {
                  refundDetail?.customer_media?.length &&
                  <div className="other-info">
                    <label>Hình ảnh khách hàng cung cấp:</label>
                    <PreviewImages images={refundDetail.customer_media}/>
                  </div>
                }
              </div>
              {
                (refundDetail.status === REFUND_STATUS.EPLAZA_REVIEW_RESULT.id || refundDetail.status === REFUND_STATUS.CUSTOMER_REQUEST_HIAC.id) &&
                <div className="refund-section refund-info">
                  <h2>Phán quyết của Outlet</h2>
                  <div className="other-info">
                    <label>Quyết định:</label>
                    <p>{refundDetail.eplaza_result?.name}</p>
                  </div>
                  <div className="other-info">
                    <label>Outlet ghi chú:</label>
                    <p>{refundDetail.eplaza_note || 'N/A'}</p>
                  </div>
                </div>
              }
              {
                refundDetail.hiac_event_id &&
                <div className="refund-section refund-info">
                  <h2>Thông tin vụ việc chuyển sang HIAC</h2>
                  <div className="other-info">
                    <label>ID:</label>
                    <p>{refundDetail.hiac_event_id}</p>
                  </div>
                  <div className="other-info">
                    <label>Liên kết:</label>
                    <p><a href={refundDetail.hiac_event_link} target="_blank">{refundDetail.hiac_event_link}</a></p>
                  </div>
                </div>
              }
              <UpdateRefundStatus
                refundId={refundDetail.id}
                refund={refundDetail}
                key={refundDetail.status}
                currentStatusId={refundDetail.status}
                onSuccess={this.getRefund}
              />
            </Fragment>
          }
        </div>
      </div>
    );
  }
}

export default RefundDetail;
