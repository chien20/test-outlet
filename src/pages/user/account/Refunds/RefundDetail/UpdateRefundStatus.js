import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import {REFUND_STATUS} from '../RefundStatus';
import Select from '../../../../../components/Form/Select/Select';
import {
  handleCheckBoxChanged,
  handleInputTextChanged,
  handleSelectChange,
  showAlert
} from '../../../../../common/helpers';
import {updateRefundAPI} from '../../../../../api/refunds';
import {getErrorMessage} from '../../../../../common/helpers/language';
import Checkbox from '../../../../../components/Form/Checkbox/Checkbox';

const formFields = {
  10: [
    {
      label: 'Số tiền',
      field: 'total_price',
      type: 'inputNumber',
    },
  ],
  20: [
    {
      label: 'Địa chỉ nhận hàng',
      field: 'seller_address_refund',
      type: 'inputText',
    }
  ],
  21: [
    {
      label: 'Ghi chú',
      field: 'seller_note_reason',
      type: 'textarea',
    }
  ],
  22: [
    {
      label: 'Mã vận đơn',
      field: 'waybill_code',
      type: 'inputText',
    },
    {
      label: 'Ghi chú',
      field: 'customer_note_sent_product',
      type: 'textarea',
    },
    // {
    //   label: 'Phương thức giao hàng',
    //   field: 'shipment_method',
    //   type: 'hidden',
    //   defaultValue: 0,
    // },
  ],
  24: [
    {
      label: 'Ghi chú',
      field: 'seller_note_received_product',
      type: 'textarea',
    },
  ],
  33: [
    {
      label: 'Tôi đồng ý với kết quả trên',
      field: 'customer_confirmed_status',
      type: 'checkbox',
    }
  ],
};

const EnumConfirm = {
  DECLINE: 10,
  AGREE: 20,
};

class UpdateRefundStatus extends React.PureComponent {
  state = {
    hasError: false,
    type: 'U',
    nextStatuses: [],
    selectedStatusId: null,
    customer_confirmed_status: true,
    isSubmitting: false,
    hideCheckbox: false,
  };

  componentDidMount() {
    this.initData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.refund !== this.props.refund) {
      this.initData();
    }
  }

  initData = () => {
    this.initDataAsync().then(data => {
      this.setState({
        ...data,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: error.message,
      });
    });
  };

  initDataAsync = async () => {
    const {location: {pathname}, currentStatusId, refund} = this.props;
    const state = {
      type: 'U',
      nextStatuses: [],
    };
    if (pathname.includes('store')) {
      state.type = 'S';
    }
    const currentStatus = Object.values(REFUND_STATUS).find(i => i.id === currentStatusId);
    if (!currentStatus) {
      return {
        ...state,
        hasError: true,
      };
    }
    if (currentStatus.nextStatus) {
      let nextStatuses = currentStatus.nextStatus.filter(item => item.indexOf(state.type) === 0);
      if (nextStatuses.length) {
        const nextStatusIds = nextStatuses.map(item => {
          const arr = item.split('-');
          return parseInt(arr[1]);
        });
        nextStatuses = Object.values(REFUND_STATUS).filter(item => nextStatusIds.includes(item.id));
        if (nextStatuses.length === 1 && nextStatuses[0].id === REFUND_STATUS.EPLAZA_REVIEW_RESULT.id) {
          if (state.type === 'U' && refund?.customer_confirmed_status) {
            nextStatuses = [];
          } else if (state.type !== 'U' && refund?.seller_confirmed_status) {
            nextStatuses = [];
          }
          if (state.type === 'U' && refund?.seller_confirmed_status === EnumConfirm.DECLINE) {
            state.hideCheckbox = true;
            state.customer_confirmed_status = false;
          }
          if (state.type === 'S' && refund?.customer_confirmed_status === EnumConfirm.DECLINE) {
            state.hideCheckbox = true;
            state.customer_confirmed_status = false;
          }
        }
        if (nextStatuses.length === 1) {
          state.selectedStatusId = nextStatuses[0].id;
        }
        state.nextStatuses = nextStatuses;
      }
    }
    return {
      ...state,
    };
  };

  setData = (data) => {
    this.setState(data);
  };

  handleSubmit = () => {
    const {refundId, onSuccess} = this.props;
    const postData = {
      ...this.state,
      status: this.state.selectedStatusId,
      shipment_method: this.state.selectedStatusId === 22 ? 0 : undefined,
    };
    if (postData.status !== REFUND_STATUS.EPLAZA_REVIEW_RESULT.id) {
      delete postData.customer_confirmed_status;
    } else {
      if (postData.type === 'U') {
        postData.customer_pid = postData.cmt;
        postData.customer_confirmed_status = !this.state.customer_confirmed_status ? EnumConfirm.DECLINE : EnumConfirm.AGREE;
      } else {
        postData.seller_pid = postData.cmt;
        postData.seller_confirmed_status = !this.state.customer_confirmed_status ? EnumConfirm.DECLINE : EnumConfirm.AGREE;
        delete postData.customer_confirmed_status;
      }
    }
    delete postData.nextStatuses;
    delete postData.selectedStatusId;
    delete postData.hasError;
    delete postData.isSubmitting;
    delete postData.hideCheckbox;
    this.setState({
      isSubmitting: true,
    });
    updateRefundAPI(refundId, postData).then(() => {
      this.setState({
        isSubmitting: false,
      });
      showAlert({
        type: 'success',
        message: 'Đã cập nhật',
      });
      onSuccess();
    }).catch(error => {
      this.setState({
        isSubmitting: false,
      });
      showAlert({
        type: 'error',
        message: `${getErrorMessage(error)}`,
      });
    });
  };

  submit = (event) => {
    const {selectedStatusId} = this.state;
    event.preventDefault();
    event.stopPropagation();
    if (!selectedStatusId) {
      showAlert({
        type: 'error',
        message: 'Vui lòng chọn trạng thái yêu cầu',
      });
      return;
    }
    this.handleSubmit();
  };

  render() {
    const {hasError, nextStatuses, selectedStatusId, isSubmitting, hideCheckbox} = this.state;
    if (hasError) {
      return (
        <p className="text-red">Đã có lỗi xảy ra, vui lòng liên hệ quản trị viên!</p>
      );
    }
    if (!nextStatuses.length) {
      return null;
    }
    const selectedStatus = nextStatuses.find(item => item.id === selectedStatusId) || null;
    const fields = selectedStatus && formFields[selectedStatusId] ? formFields[selectedStatusId] : [];
    return (
      <div className="refund-section actions">
        <h2>Thao tác</h2>
        <form className="update-status" onSubmit={this.submit}>
          <div className="form-group">
            <label>Cập nhật trạng thái:</label>
            <Select
              options={nextStatuses}
              value={selectedStatus}
              optionLabelKey={'actionName'}
              optionValueKey={'id'}
              className="mw-400"
              placeholder="Vui lòng chọn một trạng thái"
              onChange={handleSelectChange('selectedStatusId', 'id', this.setData)}
            />
          </div>
          {
            fields.map((item, index) => (
              <Fragment key={index}>
                {
                  item.type === 'inputText' &&
                  <div className="form-group">
                    <label>{item.label}</label>
                    <input
                      type="text"
                      name={item.field}
                      className="form-control"
                      value={this.state[item.field]}
                      onChange={handleInputTextChanged(item.field, this.setData)}/>
                  </div>
                }
                {
                  item.type === 'inputNumber' &&
                  <div className="form-group">
                    <label>{item.label}</label>
                    <input
                      type="number"
                      name={item.field}
                      className="form-control"
                      value={this.state[item.field]}
                      onChange={handleInputTextChanged(item.field, this.setData)}/>
                  </div>
                }
                {
                  item.type === 'textarea' &&
                  <div className="form-group">
                    <label>{item.label}</label>
                    <textarea
                      name={item.field}
                      className="form-control"
                      value={this.state[item.field]}
                      onChange={handleInputTextChanged(item.field, this.setData)}/>
                  </div>
                }
                {
                  item.type === 'checkbox' && !this.state.hideCheckbox &&
                  <div className="form-group">
                    <Checkbox
                      name={item.field}
                      checked={this.state[item.field]}
                      label={item.label}
                      value={true}
                      type="bool"
                      onChange={handleCheckBoxChanged(item.field, this.setData)}
                    />
                  </div>
                }
              </Fragment>
            ))
          }
          {
            selectedStatus?.id === REFUND_STATUS.EPLAZA_REVIEW_RESULT.id && !this.state.customer_confirmed_status &&
            <Fragment>
              <p>Tranh chấp sẽ được chuyển sang HIAC giải quyết, vui lòng cung cấp số CMND của bạn để tiếp tục.</p>
              <div className="form-group">
                <label>Số CMND</label>
                <input
                  type="text"
                  name="cmt"
                  className="form-control"
                  value={this.state.cmt || ''}
                  onChange={handleInputTextChanged('cmt', this.setData)}/>
              </div>
            </Fragment>
          }
          <div className="form-group">
            <button
              className="btn btn-primary"
              type="submit"
              disabled={isSubmitting}
            >
              Xác nhận
            </button>
          </div>
        </form>
      </div>
    );
  }
}

UpdateRefundStatus.propTypes = {
  refundId: PropTypes.number,
  currentStatusId: PropTypes.number,
  onSuccess: PropTypes.func,
};

export default withRouter(UpdateRefundStatus);
