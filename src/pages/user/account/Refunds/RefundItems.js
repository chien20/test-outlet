import React from 'react';
import {Link} from 'react-router-dom';

const RefundItems = React.memo(({itemId, orderDetailInfo, isShop}) => {
  let productNames = {};
  let productCount = 0;
  (orderDetailInfo || []).forEach(item => {
    if (item && item.order_detail && item.order_detail.product_snapshot) {
      if (typeof item.order_detail.product_snapshot === 'string') {
        const product = JSON.parse(item.order_detail.product_snapshot);
        productNames[product.name] = product.name;
        productCount++;
      } else if (typeof item.order_detail.product_snapshot === 'object') {
        productNames[item.order_detail.product_snapshot.name] = item.order_detail.product_snapshot.name;
        productCount++;
      }
    }
  });
  productNames = Object.values(productNames);
  if (productCount > 0) {
    return (
      <Link to={`/user/${isShop ? 'store/' : ''}refunds/view/${itemId}`}>
        <span>{productNames[0]}</span>
        <span className="text-black-50">{productCount > 1 ? ` và ${productCount - 1} sản phẩm khác` : ''}</span>
      </Link>
    )
  }
  return null;
});

export default RefundItems;
