import {call, put, takeEvery} from 'redux-saga/effects'
import {updateNotificationSeenStatusAC} from './actions';
import {updateNotificationSeenStatusAPI} from '../../../../api/notifications';
import {getNotificationsAC} from '../../../../redux/actions/user/notifications';

function* updateNotificationSeenStatusFlow(action) {
  try {
    yield call(updateNotificationSeenStatusAPI, action.id, action.seen);
    yield put(getNotificationsAC());
  } catch (error) {

  }
}

export default function* pageWatcher() {
  yield takeEvery(updateNotificationSeenStatusAC().type, updateNotificationSeenStatusFlow);
}
