export const UPDATE_NOTIFICATION_SEEN_STATUS = 'UPDATE_NOTIFICATION_SEEN_STATUS';

export const updateNotificationSeenStatusAC = (id, seen) => ({
  type: UPDATE_NOTIFICATION_SEEN_STATUS,
  id,
  seen
});
