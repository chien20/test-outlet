import createReducer from '../../../../common/utils/redux/createReducer';
import {LOGOUT} from '../../../../redux/actions/user';

const initialState = {
  profile: {}
};

const handlers = {
  [LOGOUT]: () => {
    return {}
  }
};

export default createReducer(initialState, handlers);
