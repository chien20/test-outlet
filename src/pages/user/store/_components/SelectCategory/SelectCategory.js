import React from 'react';
import {TreeSelect} from '../../../../../components/Form/TreeSelect';

const SelectItemWrapper = ({depth, rootLabel, childLabel, children}) => (
  <div className="form-row">
    <div className="form-group col-md-8">
      <label>{!depth ? 'Danh mục sản phẩm' : 'Danh mục con'} (*)</label>
      {
        children
      }
    </div>
  </div>
);


const getOptionLabel = (item) => {
  return item.name || '';
};

const getOptionValue = (item) => {
  return item.id || '';
};

const SelectCategory = ({...props}) => (
  <TreeSelect
    wrapperComponent={SelectItemWrapper}
    getOptionLabel={getOptionLabel}
    getOptionValue={getOptionValue}
    placeholder="Chọn danh mục"
    {...props}/>
);

export default SelectCategory;