import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './UploadImage.scss';

class UploadImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      previewUrl: null
    };
    this.inputRef = React.createRef();
  }

  componentWillUnmount() {
    this.revokePreview();
  }

  revokePreview() {
    const {previewUrl} = this.state;
    if (previewUrl) {
      URL.revokeObjectURL(previewUrl);
      this.setState({previewUrl: null});
    }
  }

  handleClick = () => {
    const {selectedFile} = this.state;
    if (selectedFile) {
      return false;
    }
    this.inputRef.current.click();
  };

  handleEdit = () => {
    this.inputRef.current.click();
  };

  handleDelete = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      selectedFile: null,
      previewUrl: null
    }, () => {
      this.onChange(null);
    });
  };

  handleChange = (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.revokePreview();
      this.setState({
        selectedFile: file,
        previewUrl: URL.createObjectURL(file)
      }, () => {
        this.onChange(file);
      });
    }
  };

  onChange = (file) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(file);
    }
  };

  render() {
    const {className, data, isAvatar} = this.props;
    const {previewUrl, selectedFile} = this.state;
    const hasImage = selectedFile || data;
    let url = null;
    if (hasImage) {
      url = previewUrl || data.thumbnail_url;
    }
    return (
      <div
        className={`upload-image ${className ? className : ''}`}
        onClick={this.handleClick}>
        <input
          type="file"
          style={{display: 'none'}}
          ref={this.inputRef}
          onChange={this.handleChange}/>
        {
          !hasImage && <span>+</span>
        }
        {
          url &&
          <Fragment>
            <img src={url} alt={``}/>
            {
              (!data || !data.id) &&
              <span className="edit-btn" onClick={this.handleEdit}><i className="fas fa-edit"/></span>
            }
            <span className="delete-btn" onClick={this.handleDelete}><i className="fas fa-trash-alt"/></span>
          </Fragment>
        }
        {
          isAvatar &&
          <div className="info-message">(Ảnh đại diện)</div>
        }
      </div>
    )
  }
}

UploadImage.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func
};

export default UploadImage;