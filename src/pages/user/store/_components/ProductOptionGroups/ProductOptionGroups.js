import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import ProductOptionGroup from './ProductOptionGroup';

class ProductOptionGroups extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      list: []
    };
  }

  componentWillMount() {
    this.setList(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data && nextProps.data && nextProps.data.length && this.state.list.length === 0) {
      this.setList(nextProps);
    }
  }

  setList = (props) => {
    const {data} = props;
    if (!data || !data.length) {
      this.setState({
        list: []
      });
    } else {
      this.setState({
        list: data
      });
    }
  };

  handleAddOptionGroup = () => {
    this.setState(prevState => ({
      list: [...prevState.list, {
        name: '',
        values: ['']
      }]
    }), this.onChanged);
  };

  handleDeleteOptionGroup = (index) => () => {
    this.setState(prevState => {
      const list = [...prevState.list];
      list.splice(index, 1);
      return {
        list: list
      }
    }, this.onChanged);
  };

  setOptionGroupData = (index) => (data) => {
    this.setState(prevState => {
      const list = [...prevState.list];
      list[index] = {...list[index], ...data};
      return {
        list: list
      }
    }, this.onChanged);
  };

  onChanged = () => {
    const {list} = this.state;
    if (this.props.onChanged) {
      this.props.onChanged(list);
    }
  };

  render() {
    const {list} = this.state;
    return (
      <Fragment>
        {
          list.map((item, index) => (
            <ProductOptionGroup
              key={index}
              data={item}
              setData={this.setOptionGroupData(index)}
              handleDelete={this.handleDeleteOptionGroup(index)}/>
          ))
        }
        {
          list.length < 2 &&
          <div className="add-product-option-group" onClick={this.handleAddOptionGroup}>
            <span>+</span> Thêm phân loại sản phẩm
          </div>
        }
      </Fragment>
    )
  }
}

ProductOptionGroups.propTypes = {
  onChanged: PropTypes.func
};

export default ProductOptionGroups;