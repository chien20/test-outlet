import React from 'react';
import PropTypes from 'prop-types';
import './ProductOptionGroups.scss';
import {handleInputTextChanged} from '../../../../../common/helpers';

class ProductOptionGroup extends React.PureComponent {
  handleOptionChange = (index) => (event) => {
    const {data, setData} = this.props;
    const values = [...data.values];
    values[index] = event.target.value;
    setData({values: values});
  };

  handleAddOption = () => {
    const {data, setData} = this.props;
    const values = [...data.values];
    values.push('');
    setData({values: values});
  };

  handleDeleteOption = (index) => () => {
    const {data, setData} = this.props;
    const values = [...data.values];
    if (values.length === 1) {
      return false;
    }
    values.splice(index, 1);
    setData({values: values});
  };

  render() {
    const {handleDelete, setData, data} = this.props;
    return (
      <div className="product-option-group">
        <span className="btn-delete" onClick={handleDelete}>X</span>
        <div className="form-group">
          <label>Tên phân loại (*)</label>
          <input
            type="text"
            placeholder="Ví dụ: Màu sắc, Size,..."
            value={data.name}
            onChange={handleInputTextChanged('name', setData)}
            className="form-control"/>
        </div>
        {
          data.values.map((item, index) => (
            <div className="form-group" key={index}>
              <label>Lựa chọn {index + 1}</label>
              <div className="option-input">
                <input
                  type="text"
                  value={item}
                  placeholder="Ví dụ: Đỏ, Xanh, Vàng,..."
                  onChange={this.handleOptionChange(index)}
                  className="form-control"/>
                {
                  data.values.length > 1 &&
                  <span
                    className="btn-delete-option"
                    onClick={this.handleDeleteOption(index)}>X</span>
                }
              </div>
            </div>
          ))
        }
        {
          data.values.length < 10 &&
          <div
            className="add-product-option-value"
            onClick={this.handleAddOption}>
            <span>+</span> Thêm lựa chọn
          </div>
        }
      </div>
    )
  }
}

ProductOptionGroup.propTypes = {
  handleDelete: PropTypes.func,
  setData: PropTypes.func,
  data: PropTypes.any
};

export default ProductOptionGroup;
