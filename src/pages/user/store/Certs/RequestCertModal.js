import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import {createCertRequestAPI} from '../../../../api/certs';
import {handleInputTextChanged, handleSelectChange, showAlert} from '../../../../common/helpers';
import Select from '../../../../components/Form/Select/Select';
import {OBJECT_TYPES} from '../../../../common/constants/objectTypes';
import {uploadMediaAPI} from '../../../../api/media';
import {Link} from 'react-router-dom';

class RequestCertModal extends React.PureComponent {
  state = {
    isOpen: false,
    isSubmitting: false,
    formData: {},
  };

  formEl = React.createRef();

  handleOpen = () => {
    this.setState({
      isOpen: true,
      isSubmitting: false,
      formData: {},
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  setData = (data) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        ...data,
      },
    }));
  };

  handleFileChange = (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      this.setState(prevState => {
        return {
          ...prevState,
          formData: {
            ...prevState.formData,
            files: files,
          },
        };
      });
    }
  };

  isOptionDisabled = (option) => {
    const {certRequests} = this.props;
    const item = (certRequests || []).find(item => item.certificate_id === option.id);
    return !!item;
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.setState({
      isSubmitting: true,
    });
    this.handleSubmit().then((errorCount) => {
      if (errorCount > 0) {
        showAlert({
          type: 'warning',
          message: `Có ${errorCount} file không upload được, vui lòng xóa đi và thử lại!`,
        });
      } else {
        showAlert({
          type: 'success',
          message: `Đã gửi yêu cầu! Vui lòng chờ phê duyệt!`,
        });
      }
      this.handleClose();
      this.setState({
        isSubmitting: false,
      });
      this.props.onSuccess();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
      });
      this.setState({
        isSubmitting: false,
      });
    });
  };

  handleSubmit = async () => {
    const {formData: {files, ...postData}} = this.state;
    const {data: {id}} = await createCertRequestAPI(postData);
    const fileLength = files ? files.length : 0;
    const data = {
      object_type: OBJECT_TYPES.CertRequest,
      object_id: id,
      type: 'avatar',
    };
    let errorCount = 0;
    for (let i = 0; i < fileLength; i++) {
      try {
        await uploadMediaAPI(files[i], data);
      } catch (e) {
        errorCount++;
      }
    }
    return errorCount;
  };

  render() {
    const {certs} = this.props;
    const {isOpen, isSubmitting, formData} = this.state;
    const selectedCert = (certs || []).find(item => item.id === formData.certificate_id) || null;
    return (
      <Modal
        size="md"
        show={isOpen}
        onHide={this.handleClose}
        className="create-refund-modal common-modal"
        centered
      >
        <form onSubmit={this.submit} ref={this.formEl}>
          <Modal.Header closeButton>
            <Modal.Title>Yêu cầu chứng nhận</Modal.Title>
          </Modal.Header>
          <Modal.Body className="common-form">
            <div className="form-group">
              <label>Loại chứng nhận (*)</label>
              <Select
                options={certs}
                value={selectedCert}
                placeholder="Chọn chứng nhận cần yêu cầu"
                optionLabelKey="name"
                optionValueKey="id"
                onChange={handleSelectChange('certificate_id', 'id', this.setData)}
                isOptionDisabled={this.isOptionDisabled}
              />
              {
                selectedCert &&
                <Link
                  to={`/cert/${selectedCert.id}`}
                  target="_blank"
                  style={{
                    marginTop: 10,
                    textDecoration: 'underline',
                    display: 'block'
                  }}
                >
                  Hướng dẫn đăng ký
                </Link>
              }
            </div>
            <div className="form-group">
              <label>Ghi chú</label>
              <textarea
                value={formData.note}
                onChange={handleInputTextChanged('note', this.setData)}
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label>Tài liệu đính kèm</label>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  id="attachments"
                  multiple={true}
                  onChange={this.handleFileChange}
                  accept="image/*"
                />
                <label className="custom-file-label" htmlFor="attachments">
                  {formData.files ? `${formData.files.length} file` : 'Chọn file...'}
                </label>
                <div className="invalid-feedback">Vui lòng chọn file đính kèm</div>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="default" disabled={isSubmitting} onClick={this.handleClose}>
              Hủy bỏ
            </Button>
            <button className="btn btn-primary" disabled={isSubmitting} onClick={this.submit}>
              Gửi yêu cầu
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }
}

export default RequestCertModal;
