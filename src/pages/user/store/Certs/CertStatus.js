import React from 'react';

const CertStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chờ duyệt</span>;
    case 10:
      return <span className="text-green">Đã duyệt</span>;
    case 20:
      return <span className="text-red">Bị từ chối</span>;
    default:
      return '';
  }
};

export default CertStatus;
