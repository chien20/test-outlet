import React from 'react';
import {getCertRequestsAPI, getCertsAPI} from '../../../../api/certs';
import {showAlert} from '../../../../common/helpers';
import {formatDate} from '../../../../common/helpers/format';
import CertStatus from './CertStatus';
import CommonTable from '../../../../components/Table/CommonTable';
import NoData from '../../../../components/NoData/NoData';
import RequestCertModal from './RequestCertModal';

class Certs extends React.PureComponent {
  state = {
    certs: [],
    certRequests: [],
    certById: {},
    isLoaded: false,
    isLoading: false,
  };

  requestCertModal = React.createRef();

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    this.setState({isLoading: true});
    this.getDataAsync().then(res => {
      this.setState({
        ...res,
        isLoaded: true,
        isLoading: false,
      });
    }).catch(error => {
      this.setState({
        isLoaded: true,
        isLoading: false,
      });
      showAlert({
        type: 'error',
        message: error.message,
      });
    });
  };

  getDataAsync = async () => {
    const {data: {list: certs}} = await getCertsAPI({
      page_size: 100,
    });
    const {data: {list: certRequests}} = await getCertRequestsAPI({
      page_size: 100,
    });
    const certById = (certs || []).reduce((result, item) => ({
      ...result,
      [item.id]: item,
    }), {});
    (certRequests || []).forEach(item => {
      item.certificate_name = certById[item.certificate_id] ? certById[item.certificate_id].name : 'N/A';
    });
    return {
      certs,
      certRequests,
      certById,
    };
  };

  createCert = () => {
    this.requestCertModal.current.handleOpen();
  };

  columns = [
    {
      Header: 'ID',
      width: 60,
      accessor: 'id',
      className: 'text-center',
    },
    {
      Header: 'Tên chứng nhận',
      accessor: 'certificate_name',
    },
    {
      Header: 'Ngày tạo',
      width: 180,
      id: 'created_at',
      accessor: (item) => (
        <div className="date">
          {formatDate(item.created_at, 'dd/MM/yyyy - H:i:s')}
        </div>
      ),
    },
    {
      Header: 'Trạng thái',
      width: 150,
      id: 'status',
      className: 'text-center',
      accessor: (item) => (
        <CertStatus status={item.status}/>
      ),
    },
  ];

  render() {
    const {isLoaded, isLoading, certs, certRequests, certById} = this.state;
    return (
      <div className="orders-page">
        <div className="page-title">
          <h1>Chứng nhận</h1>
          <div className="action-group">
            <button className="btn btn-secondary" onClick={this.createCert}>
              <i className="fa fa-plus"/> Gửi yêu cầu
            </button>
          </div>
        </div>
        <div className="page-content">
          {
            isLoaded && !certRequests.length &&
            <NoData description="Bạn chưa có chứng nhận nào"/>
          }
          {
            !!certRequests.length &&
            <CommonTable
              className="vouchers-table"
              data={certRequests}
              columns={this.columns}
              showPagination={false}
              loading={isLoading}
            />
          }
        </div>
        <RequestCertModal
          certs={certs}
          certRequests={certRequests}
          ref={this.requestCertModal}
          onSuccess={this.getData}
        />
      </div>
    );
  }
}

export default Certs;
