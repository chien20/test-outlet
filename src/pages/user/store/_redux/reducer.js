import createReducer from '../../../../common/utils/redux/createReducer';
import {GET_STORE_DETAIL_SUCCESS, UPDATE_PRODUCT_SUCCESS, UPLOAD_PRODUCT_SUCCESS} from './actions';
import {LOGOUT} from '../../../../redux/actions/user';

const getFromStorage = () => {
  let data = {};
  if (sessionStorage.getItem('store')) {
    try {
      data = JSON.parse(sessionStorage.getItem('store'));
    } catch (e) {
      data = {};
    }
  }
  return data;
};

const saveToStorage = (data) => {
  sessionStorage.setItem('store', JSON.stringify(data));
};

const initialState = {
  info: null,
  productList: [],
  ...getFromStorage()
};

const handlers = {
  [LOGOUT]: () => {
    return {
      info: null,
      productList: []
    }
  },
  [GET_STORE_DETAIL_SUCCESS]: (state, action) => {
    return {
      ...state,
      info: action.data
    }
  },
  [UPLOAD_PRODUCT_SUCCESS]: (state, action) => {
    const newState = {
      ...state,
      productList: [...state.productList, action.data]
    };
    saveToStorage(newState);
    return newState;
  },
  [UPDATE_PRODUCT_SUCCESS]: (state, action) => {
    const productList = [...state.productList];
    const index = productList.findIndex(item => item.id === action.productId);
    if (index >= 0) {
      productList[index] = {...productList[index], ...action.data};
      const newState = {
        ...state,
        productList: productList
      };
      saveToStorage(newState);
      return newState;
    }
    return state;
  }
};

export default createReducer(initialState, handlers);
