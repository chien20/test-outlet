export const REGISTER_STORE_SUCCESS = 'REGISTER_STORE_SUCCESS';
export const GET_STORE_DETAIL_SUCCESS = 'GET_STORE_DETAIL_SUCCESS';
export const UPLOAD_PRODUCT_SUCCESS = 'UPLOAD_PRODUCT_SUCCESS';
export const UPDATE_PRODUCT_SUCCESS = 'UPDATE_PRODUCT_SUCCESS';

export const getStoreDetailSuccessAC = (data) => ({
  type: GET_STORE_DETAIL_SUCCESS,
  data
});

export const registerStoreSuccessAC = (store_id) => ({
  type: REGISTER_STORE_SUCCESS,
  store_id
});

export const uploadProductSuccessAC = (data) => ({
  type: UPLOAD_PRODUCT_SUCCESS,
  data
});

export const updateProductSuccessAC = (productId, data) => ({
  type: UPDATE_PRODUCT_SUCCESS,
  productId,
  data
});

