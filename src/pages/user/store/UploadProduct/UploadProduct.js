import React from 'react';
import {connect} from 'react-redux';
import {history} from '../../../../common/utils/router/history';
import Nprogress from 'nprogress';
import './UploadProduct.scss';
import {handleEditorChanged, handleInputTextChanged, imageUrl, showAlert} from '../../../../common/helpers';
import ProductOptionGroups from '../_components/ProductOptionGroups/ProductOptionGroups';
import {
  getProductDetailAPI,
  getProductFeatureListAPI,
  updateProductAPI,
  uploadProductAPI
} from '../../../../api/products';
import {updateProductSuccessAC, uploadProductSuccessAC} from '../_redux/actions';
import SelectCategory from '../_components/SelectCategory/SelectCategory';
import {deleteMediaAPI, uploadMediaAPI} from '../../../../api/media';
import CKEditor from '../../../../components/Editor/CKEditor/CKEditor';
import InputNumber from '../../../../components/Form/InputNumber/InputNumber';
import UploadImages from '../../../../components/images/UploadImages';
import {OBJECT_TYPES} from '../../../../common/constants/objectTypes';

class UploadProduct extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: '',
        category_id: 0,
        store_id: 0,
        full_description: '',
        short_description: '',
        guarantee_info: '{list_items: "",month:12}',
        price: 0,
        original_price: 0,
        sku: '',
        qty: 0,
        weight: 0, // cân nặng
        height: 0, // cao
        width: 0, // rộng
        length: 0, // dài
        features: [],
        option_groups: [],
        options: [],
        images: [],
      },
      initialImageIds: [],
      isLoading: false,
      selectedCategory: null
    };
    this.formEl = React.createRef();
  }

  componentDidMount() {
    this.setData({
      store_id: this.props.storeId
    });
    if (this.props.isEdit) {
      this.getProductDetail(this.props.productId);
    }
  }

  getProductDetail = async (id) => {
    this.setState({
      isLoading: true
    });
    try {
      const {data: product} = await getProductDetailAPI(id, {type: 'own'}, {__auth: true});
      (product.images || []).forEach(item => {
        if (item.type === 'default') {
          item.type = 'avatar';
        }
        item.use_type = item.type;
        item.preview_url = imageUrl(item.url);
      });
      const initialImageIds = (product.images || []).map(item => item.id);
      this.setState({
        selectedCategory: {
          id: product.category_id,
          children: [],
        },
        initialImageIds,
      }, () => {
        if (product.options && product.options.length && product.option_groups && product.option_groups.length) {
          product.options.forEach(item => {
            if (item.groups_index && item.groups_index.length) {
              if (item.groups_index.length === 1 && product.option_groups[0].values) {
                item.name = product.option_groups[0].values[item.groups_index[0]];
              } else if (item.groups_index.length === 2
                && product.option_groups[0].values
                && product.option_groups[1].values) {
                item.name = `${product.option_groups[0].values[item.groups_index[0]]} - ${product.option_groups[1].values[item.groups_index[1]]}`;
              }
            }
          });
        }
        this.setData(product);
      });
    } catch (error) {
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error'
      });
    }
    this.setState({
      isLoading: false
    });
  };

  setData = (data) => {
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  handleCategoryChanged = (selectedCategory) => {
    this.setState({
      selectedCategory
    }, () => {
      if (selectedCategory) {
        this.setData({
          category_id: selectedCategory.id
        });
        if (selectedCategory.children.length === 0) {
          this.getListFeatures(selectedCategory.id);
        }
      } else {
        this.setData({
          category_id: 0
        });
      }
    });
  };

  getListFeatures = (categoryId) => {
    getProductFeatureListAPI(categoryId).then(res => {
      if (res && res.data && res.data.list) {
        const {data} = this.state;
        const features = res.data.list || [];
        const values = {};
        if (data.features && data.features.length) {
          data.features.forEach(f => {
            values[f.global_feature_id] = f.value;
          });
        }
        features.forEach(item => {
          item.value = values[item.id] || '';
          item.global_feature_id = item.id;
          delete item.id;
        });
        this.setData({
          features: features
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: error.message
      });
    });
  };

  handleProductFeatureChanged = (index) => (event) => {
    const value = event.target.value;
    this.setState(prevState => {
      const {features} = prevState.data;
      if (!features || !features.length || !features[index]) {
        return null;
      }
      const newFeatures = [...features];
      newFeatures[index].value = value;
      return {
        data: {
          ...prevState.data,
          features: newFeatures
        }
      };
    });
  };

  handleOptionGroupsChanged = (optionGroups) => {
    const options = [];
    if (optionGroups.length === 1) {
      for (let i = 0; i < optionGroups[0].values.length; i++) {
        if (!optionGroups[0].values[i]) {
          continue;
        }
        options.push({
          name: optionGroups[0].values[i],
          sku: '',
          qty: 0,
          price: 0,
          original_price: 0,
          groups_index: [i]
        });
      }
    }
    if (optionGroups.length === 2) {
      for (let i = 0; i < optionGroups[0].values.length; i++) {
        if (!optionGroups[0].values[i]) {
          continue;
        }
        for (let j = 0; j < optionGroups[1].values.length; j++) {
          if (!optionGroups[1].values[j]) {
            continue;
          }
          options.push({
            name: `${optionGroups[0].values[i]} - ${optionGroups[1].values[j]}`,
            sku: '',
            qty: 0,
            price: 0,
            original_price: 0,
            groups_index: [i, j]
          });
        }
      }
    }
    this.setData({
      option_groups: optionGroups,
      options: options
    });
  };

  handleOptionDataChanged = (index, key) => (event) => {
    const value = typeof event === 'string' ? event : event.target.value;
    const options = [...this.state.data.options];
    options[index][key] = value;
    this.setData({options});
  };

  handleSubmit = () => {
    const {isEdit} = this.props;
    if (!isEdit) {
      this.uploadProduct();
    } else {
      this.updateProduct();
    }
  };

  uploadProduct = () => {
    this.setState({
      isLoading: true
    });
    Nprogress.start();
    const {data: {images, ...postData}} = this.state;
    uploadProductAPI(postData).then(res => {
      let productId = 0;
      if (res && res.data) {
        if (typeof res.data === 'object' && res.data.id) {
          productId = res.data.id;
        } else {
          productId = res.data;
        }
      }
      this.uploadImages(productId);
    }).catch(error => {
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error'
      });
      Nprogress.done();
      this.setState({
        isLoading: false
      });
    });
  };

  updateProduct = () => {
    const {productId} = this.props;
    const {data: {images, ...postData}} = this.state;
    this.setState({
      isLoading: true
    });
    updateProductAPI(productId, postData).then(() => {
      this.setState({
        isLoading: false
      });
      this.uploadImages(productId);
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error'
      });
    });
  };

  onUploadSuccess = () => {
    const {productId} = this.props;
    Nprogress.done();
    this.setState({
      isLoading: false
    });
    if (productId) {
      showAlert({
        message: 'Cập nhật sản phẩm thành công!',
        type: 'success'
      });
      setTimeout(() => {
        history.push('/user/store/products');
      });
    } else {
      showAlert({
        message: 'Upload sản phẩm thành công!',
        type: 'success'
      });
      setTimeout(() => {
        history.push('/user/store/products');
      });
    }
  };

  uploadImages = (productId) => {
    this.uploadImagesAsync(productId).then((errorCount) => {
      this.onUploadSuccess();
      if (errorCount > 0) {
        showAlert({
          message: 'Không cập nhật được ảnh sản phẩm',
          type: 'error'
        });
      }
    }).catch(() => {
      showAlert({
        message: 'Không cập nhật được ảnh sản phẩm',
        type: 'error'
      });
    });
  };

  uploadImagesAsync = async (productId) => {
    const {data: {images}, initialImageIds} = this.state;
    let errorCount = 0;
    const mediaIds = (images || []).map(item => item.id);
    const newMedia = (images || []).filter(item => !item.id);
    const deletedMediaIds = initialImageIds.filter(id => !mediaIds.includes(id));
    if (newMedia.length) {
      for (let i = 0; i < newMedia.length; i++) {
        try {
          await uploadMediaAPI(newMedia[i], {
            object_type: OBJECT_TYPES.Product,
            object_id: productId,
            type: 'avatar',
          });
        } catch (error) {
          errorCount++;
        }
      }
    }
    if (deletedMediaIds.length) {
      try {
        await deleteMediaAPI(deletedMediaIds);
      } catch (e) {
        errorCount++;
      }
    }
    return errorCount;
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (!this.state.data.category_id) {
      showAlert({
        type: 'error',
        message: 'Vui lòng chọn danh mục sản phẩm!'
      });
      return;
    }
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleSubmit();
  };

  render() {
    const {category, isEdit} = this.props;
    const {isLoading, data, selectedCategory} = this.state;
    const isBtnUploadDisabled = isLoading || 0;
    if (isEdit && !data.id) {
      return null;
    }
    return (
      <form className="upload-product-page" ref={this.formEl} onSubmit={this.submit}>
        <div className="page-title">
          <h1>{isEdit ? 'Cập nhật sản phẩm' : 'Upload sản phẩm'}</h1>
        </div>
        <div className="page-content">
          <div className="form-row">
            <div className="form-group col-md-8">
              <label>Tên sản phẩm (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.name}
                onChange={handleInputTextChanged('name', this.setData)}
                required={true}/>
            </div>
          </div>
          <SelectCategory
            value={selectedCategory}
            options={category}
            onChange={this.handleCategoryChanged}
            className={`r-select ${(!selectedCategory || selectedCategory.children.length) ? 'is-invalid' : ''}`}/>
          {
            data.features.map((item, index) => (
              <div className="form-group" key={index}>
                <label>{item.name}</label>
                <input
                  type={item.value_type === 'integer' || item.value_type === 'double' ? 'number' : 'text'}
                  className="form-control"
                  value={item.value}
                  onChange={this.handleProductFeatureChanged(index)}/>
              </div>
            ))
          }
          <div className="form-group">
            <label>Mô tả ngắn</label>
            <textarea
              className="form-control"
              rows={3}
              value={data.short_description}
              onChange={handleInputTextChanged('short_description', this.setData)}
              required={true}/>
          </div>
          <div className="form-group">
            <label>Mô tả chi tiết (*)</label>
            <CKEditor
              value={data.full_description}
              onChange={handleEditorChanged('full_description', this.setData)}/>
          </div>
          <div className="form-group">
            <label>Ảnh sản phẩm (*)</label>
            <div className="product-images">
              <UploadImages
                value={data.images}
                onChange={this.setData}
                fieldName="images"
                multiple={true}
                maxFiles={10}
                useType="avatar"
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-3">
              <label>Giá niêm yết (*)</label>
              <InputNumber
                type="text"
                min={0}
                max={1000000000}
                className="form-control"
                disabled={data.options.length > 0}
                value={data.original_price}
                onChange={handleInputTextChanged('original_price', this.setData, 'int')}
                required={true}/>
            </div>
            <div className="form-group col-md-3">
              <label>Giá bán ra (*)</label>
              <InputNumber
                type="text"
                min={0}
                max={data.original_price}
                className="form-control"
                disabled={data.options.length > 0}
                value={data.price}
                onChange={handleInputTextChanged('price', this.setData, 'int')}
                required={true}/>
              {
                data.price > data.original_price &&
                <div className="invalid-feedback">Giá bán ra phải nhỏ hơn giá niêm yết</div>
              }
            </div>
            <div className="form-group col-md-3">
              <label>SKU (*)</label>
              <input
                type="text"
                className="form-control"
                disabled={data.options.length > 0}
                value={data.sku}
                onChange={handleInputTextChanged('sku', this.setData)}
                required={true}/>
            </div>
            <div className="form-group col-md-3">
              <label>Số lượng (*)</label>
              <InputNumber
                type="text"
                className="form-control"
                disabled={data.options.length > 0}
                value={data.qty}
                pattern="[0-9\.]*"
                onChange={handleInputTextChanged('qty', this.setData, 'int', data.qty)}
                required={true}/>
            </div>
          </div>
          <div className="form-group">
            <label>Phân loại sản phẩm</label>
            <ProductOptionGroups data={data.option_groups} onChanged={this.handleOptionGroupsChanged}/>
          </div>
          {
            data.options.length > 0 &&
            <div className="form-group">
              <label>Danh sách phân loại (*)</label>
              <div className="table-responsive">
                <table className="table table-bordered">
                  <thead>
                  <tr>
                    <td>Tên phân loại</td>
                    <td>Số lượng (*)</td>
                    <td>Giá niêm yết (*)</td>
                    <td>Giá bán ra (*)</td>
                    <td>SKU (*)</td>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    data.options.map((item, index) => (
                      <tr key={index}>
                        <td>{item.name}</td>
                        <td>
                          <InputNumber
                            type="text"
                            className="form-control"
                            min={0}
                            max={1000000}
                            value={item.qty}
                            onChange={this.handleOptionDataChanged(index, 'qty')}/>
                        </td>
                        <td>
                          <InputNumber
                            type="text"
                            className="form-control"
                            min={0}
                            max={1000000000}
                            value={item.original_price}
                            onChange={this.handleOptionDataChanged(index, 'original_price')}/>
                        </td>
                        <td>
                          <InputNumber
                            type="text"
                            className="form-control"
                            min={0}
                            max={item.original_price}
                            value={item.price}
                            onChange={this.handleOptionDataChanged(index, 'price')}/>
                          {
                            item.price > item.original_price &&
                            <div className="invalid-feedback">Giá bán ra phải nhỏ hơn giá niêm yết</div>
                          }
                        </td>
                        <td>
                          <input
                            type="text"
                            className="form-control"
                            value={item.sku}
                            onChange={this.handleOptionDataChanged(index, 'sku')}/>
                        </td>
                      </tr>
                    ))
                  }
                  </tbody>
                </table>
              </div>
            </div>
          }
          <div className="form-row">
            <div className="form-group col-md-3">
              <label>Cân nặng (g) <sup>(*)</sup></label>
              <InputNumber
                type="text"
                className="form-control"
                value={data.weight}
                pattern="[0-9\.]*"
                onChange={handleInputTextChanged('weight', this.setData, 'int', data.weight)}
                required={true}/>
              <p className="form-text text-muted">(*) Cân nặng sau khi đóng gói</p>
            </div>
            <div className="form-group col-md-3">
              <label>Dài (cm) <sup>(*)</sup></label>
              <InputNumber
                type="text"
                className="form-control"
                value={data.length}
                pattern="[0-9\.]*"
                onChange={handleInputTextChanged('length', this.setData, 'int', data.length)}
                required={true}/>
            </div>
            <div className="form-group col-md-3">
              <label>Rộng (cm) <sup>(*)</sup></label>
              <InputNumber
                type="text"
                className="form-control"
                value={data.width}
                pattern="[0-9\.]*"
                onChange={handleInputTextChanged('width', this.setData, 'int', data.width)}
                required={true}/>
            </div>
            <div className="form-group col-md-3">
              <label>Cao (cm) <sup>(*)</sup></label>
              <InputNumber
                type="text"
                className="form-control"
                value={data.height}
                pattern="[0-9\.]*"
                onChange={handleInputTextChanged('height', this.setData, 'int', data.height)}
                required={true}/>
            </div>
          </div>
          <div className="form-submit text-right">
            <button
              className="btn btn-primary"
              type="submit"
              disabled={isBtnUploadDisabled}
              onClick={this.submit}>{isEdit ? 'Cập nhật' : 'Upload'}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  category: state.common.category.tree,
  productId: ownProps.match.params.id,
  isEdit: !!ownProps.match.params.id,
  storeId: state.user.info.store_id
});

const mapDispatchToProps = (dispatch) => ({
  uploadProductSuccess: (data) => dispatch(uploadProductSuccessAC(data)),
  updateProductSuccess: (productId, data) => dispatch(updateProductSuccessAC(productId, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UploadProduct);
