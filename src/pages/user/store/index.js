import React from 'react';
import {connect} from 'react-redux';
import {Link, Route, Switch} from 'react-router-dom';
import StoreInfo from './StoreInfo/StoreInfo';
import ProductList from './ProductList/ProductList';
import CreateStore from './CreateStore/CreateStore';
import UploadProduct from './UploadProduct/UploadProduct';
import Orders from './Orders/Orders';
import OrderDetail from './Orders/OrderDetail';
import Vouchers from './Vouchers/Vouchers';
import UpsertVoucher from './Vouchers/UpsertVoucher/UpsertVoucher';
import Ratings from './Ratings/Ratings';
import Refunds from '../account/Refunds/Refunds';
import RefundDetail from '../account/Refunds/RefundDetail/RefundDetail';
import Certs from './Certs/Certs';
import ShippingFee from './Shipping/ShippingFee';
import UpsertShippingFee from './Shipping/UpsertShippingFee';

const StorePage = ({storeId, location: {pathname}}) => {
  if (!storeId && pathname !== '/user/store/register') {
    return (
      <div className="register-store-page">
        <div className="page-content">
          Bạn chưa khởi tạo cửa hàng!&nbsp;
          <Link to="/user/store/register">Click vào đây</Link> để bắt đầu bán hàng cùng Eplaza!
        </div>
      </div>
    )
  }
  return (
    <Switch>
      <Route exact path="/user/store" component={StoreInfo}/>
      <Route exact path="/user/store/register" component={CreateStore}/>
      <Route exact path="/user/store/products/upload" component={UploadProduct}/>
      <Route exact path="/user/store/products/update/:id" component={UploadProduct}/>
      <Route exact path="/user/store/products/:page?" component={ProductList}/>
      <Route exact path="/user/store/orders/view/:id" component={OrderDetail}/>
      <Route exact path="/user/store/orders/:page?" component={Orders}/>
      <Route exact path="/user/store/vouchers/add" component={UpsertVoucher}/>
      <Route exact path="/user/store/vouchers/:id" component={UpsertVoucher}/>
      <Route exact path="/user/store/vouchers" component={Vouchers}/>
      <Route exact path="/user/store/vouchers/p/:page" component={Vouchers}/>
      <Route exact path="/user/store/ratings/:page?" component={Ratings}/>
      <Route exact path="/user/store/shipping-fee/new" component={UpsertShippingFee}/>
      <Route exact path="/user/store/shipping-fee/update/:id" component={UpsertShippingFee}/>
      <Route exact path="/user/store/shipping-fee/:page?" component={ShippingFee}/>
      <Route exact path="/user/store/refunds/:page?" component={Refunds}/>
      <Route exact path="/user/store/refunds/view/:id?" component={RefundDetail}/>
      <Route exact path="/user/store/certs" component={Certs}/>
    </Switch>
  );
};

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id
});

export default connect(mapStateToProps)(StorePage);
