import React from 'react';
import './StoreInfo.scss';
import {connect} from 'react-redux';
import {
  handleCheckBoxChanged, handleEditorChanged,
  handleInputTextChanged,
  handleSelectChange,
  imageUrl,
  showAlert
} from '../../../../common/helpers';
import {getStoreDetailSuccessAC} from '../_redux/actions';
import {getStoreDetailAPI, updateStoreAPI} from '../../../../api/stores';
import {BUSINESS_TYPES} from '../../../../common/constants/app';
import Select from '../../../../components/Form/Select/Select';
import DatePicker, {registerLocale} from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import vi from 'date-fns/locale/vi';
import {format} from 'date-fns';
import {getListDistrictAPI, getListProvinceAPI, getListWardAPI} from "../../../../api/locations";
import LoadScript from "../../../../components/GoogleMap/LoadScript";
import LocationPicker from "../../../../components/GoogleMap/LocationPicker";
import UploadImages from '../../../../components/images/UploadImages';
import {deleteMediaAPI, getObjectMediaAPI, uploadMediaAPI} from '../../../../api/media';
import {OBJECT_TYPES} from '../../../../common/constants/objectTypes';
import CKEditor from '../../../../components/Editor/CKEditor/CKEditor';

registerLocale('vi', vi);

const now = new Date();

class StoreInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: {
        name: '',
        description: '',
        address: '',
        email: '',
        phone: '',
        website: '',
        published: 0,
        other_info: {
          authentic: false,
          refund: false
        },
        business_info: {
          business_registration_number: '',
          date_registration: null,
          issued_by: '',
          tax_code: '',
          type: 0, //0: doanh nghiep, 10: ho kinh doanh, 20: ca nhan
        },
        media: [],
      },
      initialData: {},
      provinces: [{
        id: null,
        name: 'Chọn Tỉnh/Thành phố'
      }],
      districts: [{
        id: null,
        name: 'Chọn Quận/Huyện'
      }],
      wards: [{
        id: null,
        name: 'Chọn Phường/Xã/Thị trấn'
      }]
    };
    this.state.initialData = JSON.parse(JSON.stringify(this.state.data));
    this.formEl = React.createRef();
  }

  componentWillMount() {
    if (this.props.info) {
      this.initData(this.props);
    }
  }

  componentDidMount() {
    this.getProvinces();
    if (!this.props.info) {
      this.getStoreDetail();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.data.province_id !== this.state.data.province_id) {
      this.getDistricts();
    }
    if (prevState.data.district_id !== this.state.data.district_id) {
      this.getWards();
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.info !== nextProps.info && nextProps.info) {
      this.initData(nextProps);
    }
  }

  getProvinces = () => {
    getListProvinceAPI().then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Tỉnh/Thành phố'
        });
        this.setState({
          provinces: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getDistricts = () => {
    const {data} = this.state;
    getListDistrictAPI(data.province_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Quận/Huyện'
        });
        this.setState({
          districts: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getWards = () => {
    const {data} = this.state;
    getListWardAPI(data.district_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Phường/Xã/Thị trấn'
        });
        this.setState({
          wards: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  initData = (props) => {
    const {info} = props;
    const data = JSON.parse(JSON.stringify(info));
    if (data.business_info.date_registration) {
      data.business_info.date_registration = new Date(data.business_info.date_registration);
    } else {
      data.business_info.date_registration = null;
    }
    if (!data.media) {
      data.media = [];
    }
    const initialData = JSON.parse(JSON.stringify(info));
    if (!initialData.media) {
      initialData.media = [];
    }
    this.setState({
      data,
      initialData
    });
  };

  setData = (data) => {
    if (data && data.province_id !== undefined) {
      data.district_id = null;
      data.geocoding = null;
    }
    if (data && data.district_id !== undefined) {
      data.geocoding = null;
    }
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  handleAddressChanged = (geocoding) => {
    const data = {
      geocoding,
    };
    // if (!this.state.data.address) {
    //   data.address = data.geocoding.address;
    // }
    this.setData(data);
  };

  setOtherInfoData = (data) => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        other_info: {
          ...prevState.data.other_info,
          ...data
        }
      }
    }));
  };

  setBusinessInfoData = (data) => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        business_info: {
          ...prevState.data.business_info,
          ...data
        }
      }
    }));
  };

  onDateChange = (date) => {
    this.setBusinessInfoData({
      date_registration: date
    });
  };

  getStoreDetail = () => {
    const {getStoreDetailSuccess} = this.props;
    this.setState({isLoading: true});
    this.handleGetStoreAsync().then(store => {
      this.setState({isLoading: false}, () => {
        getStoreDetailSuccess(store);
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message_code}`
      });
      this.setState({isLoading: false});
    });
  };

  handleGetStoreAsync = async () => {
    const {storeId} = this.props;
    const {data: store} = await getStoreDetailAPI(storeId);
    if (store) {
      const {data: {list: media}} = await getObjectMediaAPI(storeId, OBJECT_TYPES.Store, null, {
        page_size: 100,
      });
      store.media = media || [];
      store.media.forEach(item => {
        if (!item.type || `${item.type}`.includes('image')) {
          item.use_type = 'cover';
        } else {
          item.use_type = item.type;
        }
        item.preview_url = imageUrl(item.url);
      });
    }
    return store;
  };

  handleSubmitAsync = async () => {
    const {data: {media, ...data}, initialData} = this.state;
    const {storeId} = this.props;
    data.business_info = {
      ...data.business_info,
    };
    if (data.business_info && data.business_info.date_registration) {
      data.business_info.date_registration = format(data.business_info.date_registration, 'yyyy-MM-dd');
    }
    try {
      data.published = parseInt(data.published);
    } catch (e) {
      // nothing
    }
    let errorCount = 0;
    try {
      await updateStoreAPI(storeId, data);
    } catch (error) {
      errorCount++;
    }
    const initialMediaIds = (initialData?.media || []).map(item => item.id);
    const mediaIds = (media || []).map(item => item.id);
    const newMedia = (media || []).filter(item => !item.id);
    const deletedMediaIds = initialMediaIds.filter(id => !mediaIds.includes(id));
    if (newMedia.length) {
      for (let i = 0; i < newMedia.length; i++) {
        try {
          await uploadMediaAPI(newMedia[i], {
            object_type: OBJECT_TYPES.Store,
            object_id: storeId,
            type: newMedia[i].use_type,
          });
        } catch (error) {
          errorCount++;
        }
      }
    }
    if (deletedMediaIds.length) {
      try {
        await deleteMediaAPI(deletedMediaIds);
      } catch (e) {
        errorCount++;
      }
    }
    return {
      errorCount,
    };
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.setState({isLoading: true});
    this.handleSubmitAsync().then(() => {
      showAlert({
        type: 'success',
        message: 'Đã lưu!'
      });
      this.getStoreDetail();
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message_code}`
      });
    });
  };

  render() {
    const {data, provinces, districts, wards} = this.state;
    const selectedType = BUSINESS_TYPES.find(item => item.id === data.business_info.type) || null;
    const selectedProvince = provinces.find(item => item.id === data.province_id) || null;
    const selectedDistrict = districts.find(item => item.id === data.district_id) || null;
    const selectedWard = wards.find(item => item.id === data.ward_id) || null;
    return (
      <div className="store-info-page">
        <div className="page-title">
          <h1>Thông tin cửa hàng</h1>
        </div>
        <form className="page-content" ref={this.formEl} onSubmit={this.submit}>
          <div className="form-group">
            <label>Tên cửa hàng (*)</label>
            <input
              type="text"
              className="form-control"
              value={data.name}
              name="name"
              required={true}
              onChange={handleInputTextChanged('name', this.setData)}/>
            <div className="invalid-feedback">
              Vui lòng nhập tên cửa hàng, tối thiểu 5 ký tự, tối đa 100 ký tự.
            </div>
          </div>
          <div className="form-row">
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Tỉnh/Thành phố</label>
                <Select
                  options={provinces}
                  value={selectedProvince}
                  placeholder="Chọn tỉnh thành phố"
                  onChange={handleSelectChange('province_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Quận/Huyện</label>
                <Select
                  options={districts}
                  value={selectedDistrict}
                  placeholder="Chọn quận huyện"
                  onChange={handleSelectChange('district_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Phường/Xã/Thị trấn</label>
                <Select
                  options={wards}
                  value={selectedWard}
                  placeholder="Chọn xã"
                  onChange={handleSelectChange('ward_id', 'id', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 full-width-mobile">
              <div className="form-group">
                <label>Địa chỉ (*)</label>
                <input
                  type="text"
                  className="form-control"
                  value={data.address}
                  name="address"
                  required={true}
                  onChange={handleInputTextChanged('address', this.setData)}/>
                <div className="invalid-feedback">
                  Vui lòng nhập địa chỉ cửa hàng, tối thiểu 10 ký tự, tối đa 100 ký tự.
                </div>
              </div>
            </div>
          </div>
          {
            selectedProvince && selectedDistrict && data.province_id && data.district_id &&
            <div className="form-group">
              <label>Chọn địa chỉ trên bản đồ để giao hàng nhanh nhất</label>
              <p className="hint">(Kéo thả điểm đánh dấu để chọn vị trí)</p>
              {
                data.geocoding && data.geocoding.address &&
                <p className="text-green"><i className="fas fa-check-circle"/> {data.geocoding.address}</p>
              }
              <LoadScript>
                <LocationPicker
                  province={selectedProvince}
                  district={selectedDistrict}
                  geocoding={data.geocoding}
                  onAddressChanged={this.handleAddressChanged}
                />
              </LoadScript>
            </div>
          }
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>Email liên hệ (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.email}
                name="email"
                required={true}
                onChange={handleInputTextChanged('email', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập email liên hệ.
              </div>
            </div>
            <div className="form-group col-md-6">
              <label>Số điện thoại liên hệ (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.phone}
                name="phone"
                required={true}
                onChange={handleInputTextChanged('phone', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập số điện thoại liên hệ.
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label>Loại hình kinh doanh (*)</label>
              <Select
                value={selectedType}
                options={BUSINESS_TYPES}
                optionLabelKey="name"
                optionValueKey="id"
                onChange={handleSelectChange('type', 'id', this.setBusinessInfoData)}
              />
            </div>
            <div className="form-group col-md-4">
              <label>Mã số thuế</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.tax_code}
                name="tax_code"
                required={true}
                onChange={handleInputTextChanged('tax_code', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập mã số thuế.
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label>Số ĐKKD (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.business_registration_number}
                name="business_registration_number"
                required={true}
                onChange={handleInputTextChanged('business_registration_number', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập số đăng ký kinh doanh.
              </div>
            </div>
            <div className="form-group col-md-4">
              <label>Nơi cấp (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.issued_by}
                name="issued_by"
                required={true}
                onChange={handleInputTextChanged('issued_by', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập nơi cấp.
              </div>
            </div>
            <div className="form-group col-md-4">
              <label>Ngày cấp (*)</label>
              <DatePicker
                selected={data.business_info.date_registration}
                onChange={this.onDateChange}
                dateFormat="dd/MM/yyyy"
                maxDate={now}
                locale="vi"
                className="form-control"
              />
              <div className="invalid-feedback">
                Vui lòng nhập ngày cấp.
              </div>
            </div>
          </div>
          <div className="form-group">
            <label>Mô tả ngắn</label>
            <CKEditor
              value={data.description}
              onChange={handleEditorChanged('description', this.setData)}
            />
          </div>
          {
            data.media &&
            <div className="form-group">
              <label>Hình ảnh cover</label>
              <UploadImages
                value={data.media}
                onChange={this.setData}
                fieldName="media"
                multiple={true}
                maxFiles={10}
                useType="cover"
              />
            </div>
          }
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                id="published"
                checked={`${data.published}` === '1'}
                value={1}
                name="published"
                onChange={handleCheckBoxChanged('published', this.setData, 0)}/>
              <label
                className="form-check-label"
                htmlFor="published">Hiển thị</label>
            </div>
          </div>
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                id="authentic"
                checked={data.other_info.authentic}
                value={true}
                name="authentic"
                onChange={handleCheckBoxChanged('authentic', this.setOtherInfoData, false, 'bool')}/>
              <label
                className="form-check-label"
                htmlFor="authentic">Cam kết hàng chính hãng</label>
            </div>
          </div>
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                id="refund"
                checked={data.other_info.refund}
                value={true}
                name="refund"
                onChange={handleCheckBoxChanged('refund', this.setOtherInfoData, false, 'bool')}/>
              <label
                className="form-check-label"
                htmlFor="refund">Hoàn tiền nếu phát hiện hàng giả</label>
            </div>
          </div>
          <div className="form-submit text-right">
            <button className="btn btn-primary" type="submit" onClick={this.submit}>Cập nhật</button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
  info: state.pages.user.store.info
});

const mapDispatchToProps = (dispatch) => ({
  getStoreDetailSuccess: (data) => dispatch(getStoreDetailSuccessAC(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(StoreInfo);
