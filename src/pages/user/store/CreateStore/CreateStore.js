import React from 'react';
import {connect} from 'react-redux';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {Redirect} from 'react-router-dom';
import {
  handleCheckBoxChanged,
  handleEditorChanged,
  handleInputTextChanged,
  handleSelectChange,
  showAlert
} from '../../../../common/helpers';
import {registerStoreAPI} from '../../../../api/stores';
import {registerStoreSuccessAC} from '../_redux/actions';
import {getListDistrictAPI, getListProvinceAPI, getListWardAPI} from '../../../../api/locations';
import Select from '../../../../components/Form/Select/Select';
import {BUSINESS_TYPES} from '../../../../common/constants/app';
import DatePicker, {registerLocale} from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import vi from 'date-fns/locale/vi';
import {format} from 'date-fns';
import LoadScript from '../../../../components/GoogleMap/LoadScript';
import LocationPicker from '../../../../components/GoogleMap/LocationPicker';
import CKEditor from '../../../../components/Editor/CKEditor/CKEditor';

registerLocale('vi', vi);

const now = new Date();

class CreateStore extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isAgreed: false,
      isLoading: false,
      data: {
        name: '',
        description: '',
        address: '',
        email: '',
        phone: '',
        website: '',
        province_id: null,
        district_id: null,
        ward_id: null,
        other_info: {
          authentic: false,
          refund: false,
        },
        business_info: {
          business_registration_number: '',
          date_registration: null,
          issued_by: '',
          tax_code: '',
          type: 0, //0: doanh nghiep, 10: ho kinh doanh, 20: ca nhan
        },
      },
      provinces: [],
      districts: [],
      wards: [],
    };
    this.formEl = React.createRef();
  }

  componentDidMount() {
    this.getProvinces();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.data.province_id !== this.state.data.province_id) {
      this.getDistricts();
    }
    if (prevState.data.district_id !== this.state.data.district_id) {
      this.getWards();
    }
  }

  setData = (data) => {
    this.setState(prevState => ({
      data: {...prevState.data, ...data},
    }));
  };

  handleAddressChanged = (geocoding) => {
    const data = {
      geocoding,
    };
    // if (!this.state.data.address) {
    //   data.address = data.geocoding.address;
    // }
    this.setData(data);
  };

  onDateChange = (date) => {
    this.setBusinessInfoData({
      date_registration: date,
    });
  };

  setOtherInfoData = (data) => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        other_info: {
          ...prevState.data.other_info,
          ...data,
        },
      },
    }));
  };

  setBusinessInfoData = (data) => {
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        business_info: {
          ...prevState.data.business_info,
          ...data,
        },
      },
    }));
  };

  getProvinces = () => {
    getListProvinceAPI().then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Tỉnh/Thành phố',
        });
        this.setState({
          provinces: list,
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
      });
    });
  };

  getDistricts = () => {
    const {data} = this.state;
    getListDistrictAPI(data.province_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Quận/Huyện',
        });
        this.setState({
          districts: list,
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
      });
    });
  };

  getWards = () => {
    const {data} = this.state;
    getListWardAPI(data.district_id).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Phường/Xã/Thị trấn'
        });
        this.setState({
          wards: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handleToggleAgree = () => {
    this.setState(prevState => ({
      isAgreed: !prevState.isAgreed,
    }));
  };

  handleSave = () => {
    const data = {...this.state.data};
    data.business_info = {
      ...data.business_info,
    };
    if (data.business_info && data.business_info.date_registration) {
      data.business_info.date_registration = format(data.business_info.date_registration, 'yyyy-MM-dd');
    }
    this.setState({
      isLoading: true,
    });
    registerStoreAPI(data).then(res => {
      this.setState({
        isLoading: false,
      }, () => {
        this.props.registerStoreSuccess(res.data.id);
        showAlert({
          message: 'Thành công!',
          type: 'success',
        });
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error',
      });
    });
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleSave();
  };

  render() {
    const {storeId} = this.props;
    const {isAgreed, isLoading, data, provinces, districts, wards} = this.state;
    if (storeId) {
      return <Redirect to="/user/store"/>;
    }
    const selectedType = BUSINESS_TYPES.find(item => item.id === data.business_info.type) || null;
    const selectedProvince = provinces.find(item => item.id === data.province_id) || null;
    const selectedDistrict = districts.find(item => item.id === data.district_id) || null;
    const selectedWard = wards.find(item => item.id === data.ward_id) || null;
    return (
      <div className="register-store-page">
        <div className="page-title">
          <h1>Tạo cửa hàng</h1>
        </div>
        <form className="page-content" onSubmit={this.submit} ref={this.formEl}>
          <div className="form-group">
            <label>Tên cửa hàng (*)</label>
            <input
              type="text"
              className="form-control"
              value={data.name}
              name="name"
              required={true}
              minLength={5}
              maxLength={100}
              onChange={handleInputTextChanged('name', this.setData)}/>
            <div className="invalid-feedback">
              Vui lòng nhập tên cửa hàng, tối thiểu 5 ký tự, tối đa 100 ký tự.
            </div>
          </div>
          <div className="form-row">
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Tỉnh/Thành phố</label>
                <Select
                  options={provinces}
                  value={selectedProvince}
                  placeholder="Chọn tỉnh thành phố"
                  onChange={handleSelectChange('province_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Quận/Huyện</label>
                <Select
                  options={districts}
                  value={selectedDistrict}
                  placeholder="Chọn quận huyện"
                  onChange={handleSelectChange('district_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Phường/Xã/Thị trấn</label>
                <Select
                  options={wards}
                  value={selectedWard}
                  placeholder="Chọn xã"
                  onChange={handleSelectChange('ward_id', 'id', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="col-12 full-width-mobile">
              <div className="form-group">
                <label>Địa chỉ (*)</label>
                <input
                  type="text"
                  className="form-control"
                  value={data.address}
                  name="address"
                  required={true}
                  onChange={handleInputTextChanged('address', this.setData)}/>
                <div className="invalid-feedback">
                  Vui lòng nhập địa chỉ cửa hàng, tối thiểu 10 ký tự, tối đa 100 ký tự.
                </div>
              </div>
            </div>
          </div>
          {
            selectedProvince && selectedDistrict && data.province_id && data.district_id &&
            <div className="form-group">
              <label>Chọn địa chỉ trên bản đồ để giao hàng nhanh nhất</label>
              <p className="hint">(Kéo thả điểm đánh dấu để chọn vị trí)</p>
              {
                data.geocoding && data.geocoding.address &&
                <p className="text-green"><i className="fas fa-check-circle"/> {data.geocoding.address}</p>
              }
              <LoadScript>
                <LocationPicker
                  province={selectedProvince}
                  district={selectedDistrict}
                  geocoding={data.geocoding}
                  onAddressChanged={this.handleAddressChanged}
                />
              </LoadScript>
            </div>
          }
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>Email liên hệ (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.email}
                name="email"
                required={true}
                onChange={handleInputTextChanged('email', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập email liên hệ.
              </div>
            </div>
            <div className="form-group col-md-6">
              <label>Số điện thoại liên hệ (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.phone}
                name="phone"
                required={true}
                minLength={10}
                maxLength={11}
                onChange={handleInputTextChanged('phone', this.setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập số điện thoại liên hệ.
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label>Loại hình kinh doanh (*)</label>
              <Select
                value={selectedType}
                options={BUSINESS_TYPES}
                optionLabelKey="name"
                optionValueKey="id"
                onChange={handleSelectChange('type', 'id', this.setBusinessInfoData)}
              />
            </div>
            <div className="form-group col-md-4">
              <label>Mã số thuế</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.tax_code}
                name="tax_code"
                onChange={handleInputTextChanged('tax_code', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập mã số thuế.
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label>Số ĐKKD (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.business_registration_number}
                name="business_registration_number"
                required={true}
                onChange={handleInputTextChanged('business_registration_number', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập số đăng ký kinh doanh.
              </div>
            </div>
            <div className="form-group col-md-4">
              <label>Nơi cấp (*)</label>
              <input
                type="text"
                className="form-control"
                value={data.business_info.issued_by}
                name="issued_by"
                required={true}
                onChange={handleInputTextChanged('issued_by', this.setBusinessInfoData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập nơi cấp.
              </div>
            </div>
            <div className="form-group col-md-4">
              <label>Ngày cấp (*)</label>
              <DatePicker
                selected={data.business_info.date_registration}
                onChange={this.onDateChange}
                dateFormat="dd/MM/yyyy"
                maxDate={now}
                locale="vi"
                className="form-control"
              />
              <div className="invalid-feedback">
                Vui lòng nhập ngày cấp.
              </div>
            </div>
          </div>
          <div className="form-group">
            <label>Mô tả ngắn</label>
            <CKEditor
              value={data.description}
              onChange={handleEditorChanged('description', this.setData)}
            />
          </div>
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                id="authentic"
                checked={data.other_info.authentic}
                value={true}
                name="authentic"
                onChange={handleCheckBoxChanged('authentic', this.setOtherInfoData, false, 'bool')}/>
              <label
                className="form-check-label"
                htmlFor="authentic">Cam kết hàng chính hãng</label>
            </div>
          </div>
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                id="refund"
                checked={data.other_info.refund}
                value={true}
                name="refund"
                onChange={handleCheckBoxChanged('refund', this.setOtherInfoData, false, 'bool')}/>
              <label
                className="form-check-label"
                htmlFor="refund">Hoàn tiền nếu phát hiện hàng giả</label>
            </div>
          </div>
          <div className="form-group">
            <div className="form-check">
              <input
                className="form-check-input"
                type="checkbox"
                checked={isAgreed}
                name="agree"
                required={true}
                onChange={this.handleToggleAgree}
                id="agree"/>
              <label
                className="form-check-label"
                htmlFor="agree">Tôi đã đọc và đồng ý với <strong><HyperLink>Điều khoản sử dụng</HyperLink></strong> của
                Eplaza</label>
              <div className="invalid-feedback">
                Vui lòng đọc và đồng ý với điều khoản sử dụng
              </div>
            </div>
          </div>
          <div className="form-submit text-right">
            <button
              className="btn btn-primary"
              type="submit"
              disabled={isLoading}
              onClick={this.submit}>Đăng ký
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
});

const mapDispatchToProps = (dispatch) => ({
  registerStoreSuccess: (id) => dispatch(registerStoreSuccessAC(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateStore);
