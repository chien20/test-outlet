export const VOUCHER_TYPES = [
  {
    id: 0,
    name: 'Giảm giá đơn hàng'
  },
  {
    id: 10,
    name: 'Giảm giá phí vận chuyển'
  }
];

export const VOUCHER_SCOPES = [
  {
    id: 0,
    name: 'Tất cả sản phẩm'
  },
  {
    id: 1,
    name: 'Một số sản phẩm'
  }
];

export const VOUCHER_DISCOUNT_TYPES = [
  {
    id: 0,
    name: 'Theo số tiền',
    unit: 'vnđ'
  },
  {
    id: 1,
    name: 'Theo phần trăm',
    unit: '%'
  }
];

export const VOUCHER_CONDITION_TYPES = [
  {
    id: 0,
    name: 'Giá trị đơn hàng tối thiểu'
  },
  {
    id: 1,
    name: 'Số lượng sản phẩm tối thiểu'
  }
];
