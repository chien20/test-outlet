import React from 'react';
import {handleInputTextChanged, handleRadioChanged, handleSelectChange, showAlert} from '../../../../../common/helpers';
import {addVoucherAPI, getVoucherDetailAPI, updateVoucherAPI} from '../../../../../api/vouchers';
import {VOUCHER_CONDITION_TYPES, VOUCHER_DISCOUNT_TYPES, VOUCHER_SCOPES} from './const';
import history from '../../../../../common/utils/router/history';
import Select from '../../../../../components/Form/Select/Select';
import SelectProducts from '../../../../../components/SelectProducts/SelectProducts';
import HyperLink from '../../../../../components/HyperLink/HyperLink';
import {Link} from 'react-router-dom';
import DatePicker, {registerLocale} from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import vi from 'date-fns/locale/vi';
import {getStoreDetailAPI} from '../../../../../api/stores';
import {getStoreDetailSuccessAC} from '../../_redux/actions';
import connect from 'react-redux/es/connect/connect';
import Skeleton from 'react-loading-skeleton';
import './UpsertVoucher.scss';

registerLocale('vi', vi);
const now = new Date();

class UpsertVoucher extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: '',
        voucher: '', // discount code, uppercase, A-Z, 0-9, length: 5-10
        description: '',
        voucher_type: 0, // 0: giảm giá đơn hàng, 10: giảm phí vận chuyển (chỉ admin mới add được type=10)
        scope: 0, // 0: tất cả sản phẩm trong shop, 1: một số sản phẩm cụ thể
        discount_type: 0, // 0: theo số tiền, 1: theo phần trăm
        condition_type: 0, // 0: giá trị đơn hàng tối thiểu, 1: số lượng sản phẩm tối thiểu
        conditions: [
          {
            min: 0,
            discount: 0,
            max_discount: 0 // số tiền tối đa được giảm, áp dụng cho trường hợp condition_type = 1
          }
        ],
        additional_conditions: [],
        from: 0, // Unix time
        to: 0, // Unix time
        qty: 100, // số lượng
        qty_user: 1, // số lượng tối đa mỗi user được dùng
        product_ids: [],
        public: true
      },
      fetchError: false,
      fetching: false
    };
  }

  componentDidMount() {
    const {match: {params: {id}}} = this.props;
    if (id) {
      this.getVoucherDetail();
    }
    if (!this.props.storeInfo) {
      this.getStoreDetail();
    }
  }

  getStoreDetail = () => {
    const {storeId} = this.props;
    getStoreDetailAPI(storeId).then(res => {
      this.props.getStoreDetailSuccess(res.data);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message_code}`
      });
    });
  };

  getVoucherDetail = () => {
    const {match: {params: {id}}} = this.props;
    this.setState({
      fetching: false
    });
    getVoucherDetailAPI(id).then(res => {
      const voucher = res.data;
      let date = new Date();
      date.setTime(voucher.from);
      voucher.from = date;
      date = new Date();
      date.setTime(voucher.to);
      voucher.to = date;
      this.setState({
        fetching: false,
        fetchError: false,
        formData: voucher
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
      this.setState({
        fetching: false,
        fetchError: true
      });
    });
  };

  setData = (data) => {
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  setCondition = (index) => (data) => {
    this.setState(prevState => {
      const conditions = [...prevState.formData.conditions];
      conditions[index] = {
        ...conditions[index],
        ...data
      };
      return {
        formData: {
          ...prevState.formData,
          conditions: conditions
        }
      };
    });
  };

  addCondition = () => {
    this.setState(prevState => {
      const conditions = [...prevState.formData.conditions];
      conditions.push({
        min: 0,
        discount: 0,
        max_discount: 0
      });
      return {
        formData: {
          ...prevState.formData,
          conditions: conditions
        }
      };
    });
  };

  deleteCondition = (index) => () => {
    this.setState(prevState => {
      const conditions = [...prevState.formData.conditions];
      conditions.splice(index, 1);
      return {
        formData: {
          ...prevState.formData,
          conditions: conditions
        }
      };
    });
  };

  onDateChange = (key) => (date) => {
    this.setData({
      [key]: date
    });
  };

  onProductChange = (productIds) => {
    this.setData({
      product_ids: productIds
    });
  };

  handleSave = () => {
    const {match: {params: {id}}} = this.props;
    const {formData} = this.state;
    const postData = JSON.parse(JSON.stringify(formData));
    if (formData.from) {
      postData.from = formData.from.getTime();
    }
    if (formData.to) {
      postData.to = formData.to.getTime();
    }
    if (!id) {
      addVoucherAPI(postData).then(res => {
        showAlert({
          type: 'success',
          message: 'Thành công!'
        });
        history.push('/user/store/vouchers');
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    } else {
      updateVoucherAPI(id, postData).then(res => {
        showAlert({
          type: 'success',
          message: 'Đã lưu!'
        });
        history.push('/user/store/vouchers');
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    }
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    // if (this.formEl.current.checkValidity() === false) {
    //   this.formEl.current.classList.add('was-validated');
    //   return;
    // }
    this.handleSave();
  };

  render() {
    const {match: {params: {id}}, storeInfo} = this.props;
    const {formData, fetching, fetchError} = this.state;
    const isEdit = !!id;
    const selectedScope = VOUCHER_SCOPES.find(item => item.id === formData.scope) || null;
    const selectedDiscountType = VOUCHER_DISCOUNT_TYPES.find(item => item.id === formData.discount_type) || null;
    const selectedConditionType = VOUCHER_CONDITION_TYPES.find(item => item.id === formData.condition_type) || null;
    const isValid = !formData.voucher || /^([A-Z0-9_-]){5,10}$/.test(formData.voucher);
    if (!storeInfo) {
      return (
        <div className="upsert-voucher-page">
          <div className="page-title">
            <h1>Tạo voucher</h1>
          </div>
          <div className="page-content">
            <Skeleton height={30}/>
          </div>
        </div>
      )
    }
    return (
      <div className="upsert-voucher-page">
        <div className="page-title">
          <h1>Tạo voucher</h1>
        </div>
        <div className="page-content">
          <div className="form-section">
            <div className="form-section-label">Thông tin cơ bản</div>
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Tên chương trình giảm giá (*)</label>
                <input
                  type="text"
                  className="form-control"
                  value={formData.name}
                  onChange={handleInputTextChanged('name', this.setData)}
                  required={true}/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-5">
                <label>Mã giảm giá (*)</label>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <div className="input-group-text">{storeInfo && storeInfo.code}</div>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    value={formData.voucher}
                    onChange={handleInputTextChanged('voucher', this.setData)}
                    required={true}/>
                </div>
                <div className={`description small ${!isValid ? 'text-red' : ''}`}>
                  Viết hoa, chỉ gồm chữ và số, độ dài từ 5-10 ký tự
                </div>
              </div>
              <div className="form-group col-md-3">
                <label>Số lượng (*)</label>
                <input
                  type="number"
                  className="form-control"
                  value={formData.qty}
                  onChange={handleInputTextChanged('qty', this.setData, 'number', formData.qty)}
                  required={true}/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Mô tả (*)</label>
                <textarea
                  className="form-control"
                  value={formData.description}
                  onChange={handleInputTextChanged('description', this.setData)}
                  required={true}/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label style={{display: 'block'}}>Ngày bắt đầu</label>
                <DatePicker
                  selected={formData.from}
                  onChange={this.onDateChange('from')}
                  dateFormat="dd/MM/yyyy"
                  minDate={now}
                  maxDate={formData.to}
                  locale="vi"
                  className="form-control"/>
              </div>
              <div className="form-group col-md-4">
                <label style={{display: 'block'}}>Ngày kết thúc</label>
                <DatePicker
                  selected={formData.to}
                  onChange={this.onDateChange('to')}
                  dateFormat="dd/MM/yyyy"
                  minDate={formData.from}
                  locale="vi"
                  className="form-control"/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Hình thức giảm giá</label>
                <Select
                  optionLabelKey="name"
                  optionValueKey="id"
                  value={selectedDiscountType}
                  options={VOUCHER_DISCOUNT_TYPES}
                  onChange={handleSelectChange('discount_type', 'id', this.setData)}/>
              </div>
              <div className="form-group col-md-4">
                <label>Điều kiện giảm giá</label>
                <Select
                  optionLabelKey="name"
                  optionValueKey="id"
                  value={selectedConditionType}
                  options={VOUCHER_CONDITION_TYPES}
                  onChange={handleSelectChange('condition_type', 'id', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="form-section">
            <div className="form-section-label">Mức giảm giá</div>
            {
              formData.conditions.map((item, index) => (
                <div className="form-row" key={index}>
                  {
                    formData.conditions.length > 1 &&
                    <div className="form-group col-md-1 condition-name">
                      <label>Mức {index + 1}</label>
                      <p className="small">(<HyperLink onClick={this.deleteCondition(index)}>Xóa</HyperLink>)</p>
                    </div>
                  }
                  <div className="form-group col-md-3">
                    <label>{selectedConditionType.name}</label>
                    <input
                      type="number"
                      className="form-control"
                      min={0}
                      value={item.min}
                      onChange={handleInputTextChanged('min', this.setCondition(index), 'number', item.min)}
                      required={true}/>
                  </div>
                  <div className="form-group col-md-3">
                    <label>Giảm giá ({selectedDiscountType.unit})</label>
                    <input
                      type="number"
                      className="form-control"
                      min={0}
                      value={item.discount}
                      onChange={handleInputTextChanged('discount', this.setCondition(index), 'number', item.discount)}
                      required={true}/>
                  </div>
                  {
                    selectedDiscountType.id === 1 &&
                    <div className="form-group col-md-3">
                      <label>Giảm giá tối đa (vnđ)</label>
                      <input
                        type="number"
                        className="form-control"
                        min={0}
                        value={item.max_discount}
                        onChange={handleInputTextChanged('max_discount', this.setCondition(index), 'number', item.max_discount)}
                        required={true}/>
                    </div>
                  }
                </div>
              ))
            }
            <HyperLink onClick={this.addCondition}>+ Thêm mức giảm giá</HyperLink>
          </div>
          <div className="form-section">
            <div className="form-section-label">Sản phẩm áp dụng</div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Select
                  optionLabelKey="name"
                  optionValueKey="id"
                  value={selectedScope}
                  options={VOUCHER_SCOPES}
                  onChange={handleSelectChange('scope', 'id', this.setData)}/>
              </div>
            </div>
            {
              selectedScope.id === 1 &&
              <SelectProducts
                productIds={formData.product_ids}
                onChange={this.onProductChange}/>
            }
          </div>
          <div className="form-section">
            <div className="form-section-label">Thông tin khác</div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Số lượng voucher tối đa mỗi user được sử dụng</label>
                <input
                  type="number"
                  className="form-control"
                  value={formData.qty_user}
                  onChange={handleInputTextChanged('qty_user', this.setData, 'number', formData.qty_user)}/>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <label>Hiển thị</label>
                <div className="form-inline-group">
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="public"
                      id="public"
                      value={true}
                      checked={formData.public}
                      onChange={handleRadioChanged('public', this.setData)}/>
                    <label
                      className="form-check-label"
                      htmlFor="public">Công khai</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="public"
                      id="private"
                      value={false}
                      checked={!formData.public}
                      onChange={handleRadioChanged('public', this.setData)}/>
                    <label
                      className="form-check-label"
                      htmlFor="private">Không công khai</label>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-submit">
              <Link to={'/user/store/vouchers'} className="btn btn-default">Hủy bỏ</Link>
              <button
                className="btn btn-primary"
                type="submit"
                disabled={fetching || fetchError || !isValid}
                onClick={this.submit}>
                {isEdit ? 'Lưu lại' : 'Tạo voucher'}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
  storeInfo: state.pages.user.store.info
});

const mapDispatchToProps = (dispatch) => ({
  getStoreDetailSuccess: (data) => dispatch(getStoreDetailSuccessAC(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UpsertVoucher);
