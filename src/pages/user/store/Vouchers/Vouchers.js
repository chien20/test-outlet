import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import Pagination from '../../../../components/Pagination/Pagination';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {history} from '../../../../common/utils/router/history';
import {showAlert} from '../../../../common/helpers';
import {deleteVoucherAPI, getVouchersAPI} from '../../../../api/vouchers';
import CustomCellWithTitle from '../../../../components/Table/components/cells/CustomCellWithTitle';
import CommonTable from '../../../../components/Table/CommonTable';
import './Vouchers.scss';
import AlertModal from '../../../../components/modals/AlertModal/AlertModal';
import Broadcaster from '../../../../common/helpers/broadcaster';

const PAGE_SIZE = 10;

class Vouchers extends React.PureComponent {
  constructor(props, totalItems = 0) {
    super(props);
    this.state = {
      isLoading: false,
      vouchers: [],
      keyword: '',
      totalItems: totalItems,
      selectedVoucherId: null
    }
  }

  componentDidMount() {
    this.getListVouchers();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.match !== this.props.match) {
      this.getListVouchers();
    }
  }

  getListVouchers() {
    const {match: {params: {page}}} = this.props;
    const {keyword} = this.state;
    const params = {
      current_page: page || 1,
      page_size: PAGE_SIZE,
      q: keyword || ''
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getVouchersAPI(params).then(res => {
      this.setState({
        isLoading: false,
        vouchers: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được dữ liệu!`
      });
    });
  }

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/store/vouchers/p/${page}`);
    } else {
      history.push(`/user/store/vouchers`);
    }
  };

  handleDelete = () => {
    Broadcaster.broadcast('CONFIRM_DELETE_VOUCHER', {
      isLoading: true
    });
    const {selectedVoucherId} = this.state;
    deleteVoucherAPI(selectedVoucherId).then(res => {
      showAlert({
        type: 'success',
        message: `Đã xóa`
      });
      Broadcaster.broadcast('CONFIRM_DELETE_VOUCHER', {
        isLoading: false,
        isOpen: false
      });
      this.getListVouchers();
    }).catch(error => {
      Broadcaster.broadcast('CONFIRM_DELETE_VOUCHER', {
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onDelete = (id) => () => {
    this.setState({
      selectedVoucherId: id
    }, () => {
      Broadcaster.broadcast('CONFIRM_DELETE_VOUCHER', {
        title: 'Xóa voucher',
        message: 'Bạn có chắc muốn xóa voucher này?',
        isLoading: false,
        buttons: [
          {
            name: 'Đồng ý',
            className: 'btn btn-danger',
            onClick: this.handleDelete
          }
        ]
      });
    });
  };

  columns = [
    {
      Header: 'Mã giảm giá',
      id: 'voucher',
      accessor: (item) => (
        <CustomCellWithTitle
          title={item.name}
          content={item.voucher}
          className="voucher-name"/>
      )
    },
    {
      Header: 'Sản phẩm áp dụng',
      id: 'scope',
      width: 150,
      accessor: (item) => (
        <span>
          {item.scope === 0 ? 'Tất cả sản phẩm' : `${item.product_ids && item.product_ids.length} sản phẩm`}
        </span>
      )
    },
    {
      Header: 'Số lượng',
      width: 80,
      accessor: 'qty',
      className: 'text-center'
    },
    {
      Header: 'Đã lưu',
      width: 80,
      accessor: 'pending_count',
      className: 'text-center'
    },
    {
      Header: 'Đã dùng',
      width: 80,
      accessor: 'used_count',
      className: 'text-center'
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 120,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <Fragment>
          <Link to={`/user/store/vouchers/${item.id}`} className="text-green">
            <i className="fas fa-edit"/> Sửa
          </Link>
          <HyperLink onClick={this.onDelete(item.id)} className="text-red">
            <i className="fas fa-trash"/> Xóa
          </HyperLink>
        </Fragment>
      )
    }
  ];

  render() {
    const {match: {params: {page}}} = this.props;
    const {vouchers, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <div className="vouchers-page">
        <div className="page-title">
          <h1>Quản lý voucher</h1>
          <div className="action-group">
            {/*<form className="search-box" onSubmit={this.handleSearch}>*/}
            {/*<input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>*/}
            {/*<button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>*/}
            {/*</form>*/}
            <Link to={`/user/store/vouchers/add`} className="btn btn-secondary">
              <i className="fa fa-plus"/> Tạo voucher
            </Link>
          </div>
        </div>
        <div className="page-content">
          <CommonTable
            className="vouchers-table"
            data={vouchers}
            columns={this.columns}
            showPagination={false}
            defaultPageSize={PAGE_SIZE}/>
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
        <AlertModal eventName="CONFIRM_DELETE_VOUCHER"/>
      </div>
    );
  }
}

export default Vouchers;
