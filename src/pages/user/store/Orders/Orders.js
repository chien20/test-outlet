import React from 'react';
import {getOrderListAPI} from '../../../../api/orders';
import {showAlert} from '../../../../common/helpers';
import Pagination from '../../../../components/Pagination/Pagination';
import {history} from '../../../../common/utils/router/history';
import {Link} from 'react-router-dom';
import OrderName from '../../_components/OrdersTable/OrderName';
import {formatDate, numberAsCurrentcy} from '../../../../common/helpers/format';
import OrderStatus from '../../_components/OrdersTable/OrderStatus';
import CommonTable from '../../../../components/Table/CommonTable';
import NoData from '../../../../components/NoData/NoData';

const ITEMS_PER_PAGE = 10;

class Orders extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      orders: [],
      totalItems: 0
    };
  }

  componentDidMount() {
    this.getOrders();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props !== prevProps) {
      this.getOrders();
    }
  }

  getOrders = () => {
    const {match: {params: {page}}} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE,
      is_store: true
    };
    params.current_page -= 1;
    getOrderListAPI(params).then(res => {
      if (res && res.data && res.data.list) {
        res.data.list.forEach(item => {
          item.url = '/user/store/orders/view/' + item.id;
        });
        this.setState({
          isLoading: false,
          orders: res.data.list,
          totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/store/orders/${page}`);
    } else {
      history.push(`/user/store/orders`);
    }
  };

  columns = [
    {
      Header: 'Mã đơn hàng',
      id: 'orderCode',
      width: 180,
      accessor: (item) => (
        <Link to={item.url}>{item.order_code}</Link>
      )
    },
    {
      Header: 'Sản phẩm',
      id: 'scope',
      accessor: (item) => (
        <OrderName cartInfo={item.cart_info} url={item.url}/>
      )
    },
    {
      Header: 'Ngày mua',
      width: 140,
      id: 'created_at',
      accessor: (item) => (
        <div className="date">
          {formatDate(item.created_at, 'dd/MM/yyyy - H:i:s')}
        </div>
      )
    },
    {
      Header: 'Tổng tiền',
      width: 100,
      id: 'total',
      className: 'text-right',
      accessor: (item) => (
        <div className="text-right">{numberAsCurrentcy(item.total || 0)}đ</div>
      )
    },
    {
      Header: 'Trạng thái',
      width: 120,
      id: 'status',
      className: 'text-center',
      accessor: (item) => (
        <OrderStatus status={item.status}/>
      )
    }
  ];

  render() {
    const {match: {params: {page}}} = this.props;
    const {isLoading, orders, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <div className="orders-page">
        <div className="page-title">
          <h1>Đơn hàng của khách</h1>
        </div>
        <div className="page-content">
          {
            !isLoading && !orders.length &&
            <NoData
              title="Không có đơn hàng"
              description="Bạn chưa có đơn hàng nào."
            >
              <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
            </NoData>
          }
          {
            !!orders.length &&
            <CommonTable
              className="vouchers-table"
              data={orders}
              columns={this.columns}
              showPagination={false}/>
          }
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Orders;
