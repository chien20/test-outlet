import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import './ProductList.scss';
import {Link} from 'react-router-dom';
import {deleteProductAPI, getProductListAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import {history} from '../../../../common/utils/router/history';
import Pagination from '../../../../components/Pagination/Pagination';
import ProductTableRT from './ProductTableRT';

const ITEM_PER_PAGE = 10;

class ProductList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      totalItems: 0,
      isLoading: false,
      keyword: ''
    };
  }

  componentDidMount() {
    this.getListProduct(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match !== this.props.match) {
      this.getListProduct(nextProps);
    }
  }

  getListProduct = (props) => {
    const {match: {params: {page}}, storeId} = props;
    const {keyword} = this.state;
    const params = {
      type: 'own',
      store_id: storeId,
      current_page: page || 1,
      page_size: ITEM_PER_PAGE,
      q: keyword || '',
      sort_by: 'id',
      sort_order: 'desc',
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getProductListAPI(params, {__auth: true}).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách sản phẩm!`
      });
    });
  }

  handleDelete = (product) => () => {
    // eslint-disable-next-line no-restricted-globals
    const r = confirm(`Sản phẩm "${product.name}" sẽ bị xóa, bạn có chắc?`);
    if (r) {
      deleteProductAPI(product.id).then(res => {
        if (res.data) {
          showAlert({
            type: 'success',
            message: 'Đã xóa'
          });
          this.getListProduct(this.props);
        } else {
          showAlert({
            type: 'error',
            message: 'Lỗi: không xóa được sản phẩm!'
          });
        }
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    } else {

    }
  };

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListProduct(this.props);
  };

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/store/products/${page}`);
    } else {
      history.push(`/user/store/products`);
    }
  };

  render() {
    const {productList, totalItems, keyword} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / ITEM_PER_PAGE);
    return (
      <div className="product-list-page">
        <div className="page-title">
          <h1>Quản lý sản phẩm</h1>
          <form className="search-box" onSubmit={this.handleSearch}>
            <input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>
            <button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>
          </form>
        </div>
        <div className="page-content">
          {
            !productList.length &&
            <p>Bạn chưa có sản phẩm nào!&nbsp;
              <Link to="/user/store/products/upload">Click vào đây</Link> để upload sản phẩm mới!</p>
          }
          {
            !!productList.length &&
            <Fragment>
              <ProductTableRT productList={productList} handleDelete={this.handleDelete}/>
              <div className="bottom">
                <Link to="/user/store/products/upload" className="btn btn-primary btn-upload-product">Thêm sản
                  phẩm</Link>
                <div className="item-pagination hidden-mobile">
                  {
                    totalPage > 1 &&
                    <Pagination totalPages={totalPage} currentPage={page || 1} onPageChanged={this.onPageChanged}/>
                  }
                </div>
              </div>
            </Fragment>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
  productList: state.pages.user.store.productList
});

export default connect(mapStateToProps)(ProductList);
