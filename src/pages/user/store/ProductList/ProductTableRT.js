import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import CommonTable from '../../../../components/Table/CommonTable';
import './ProductTableRT.scss';
import HyperLink from '../../../../components/HyperLink/HyperLink';

const ProductName = ({product}) => {
  const imageBg = {
    backgroundImage: `url(${product.avatar_thumbnail_url})`
  };
  if (product.status !== 10) {
    return (
      <div className="product-info">
        <div className="product-image">
          <span className="product-image-wrapper" style={imageBg}/>
        </div>
        <span className="product-name">
          {product.name}
        </span>
      </div>
    )
  }
  return (
    <div className="product-info">
      <div className="product-image">
        <Link to={product.url} target="_blank">
          <span className="product-image-wrapper" style={imageBg}/>
        </Link>
      </div>
      <Link className="product-name" to={product.url} target="_blank">
        {product.name}
      </Link>
    </div>
  );
};

const ProductStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chờ phê duyệt</span>;
    case 10:
      return <span className="text-green">Đã phê duyệt</span>;
    case 30:
      return <span className="text-red">Đã bị từ chối</span>;
    default:
      return '';
  }
};

const MutipleVariants = ({item}) => (
  <span className="text-info">{item.options.length} phân loại</span>
);

class ProductTable extends React.PureComponent {
  columns = [
    {
      Header: 'Tên sản phẩm',
      id: 'name',
      accessor: (item) => (
        <ProductName product={item}/>
      )
    },
    {
      Header: 'SKU',
      width: 150,
      id: 'sku',
      accessor: (item) => (
        <div className="text-ellipsis">
          {item.options.length > 0 ? <MutipleVariants item={item}/> : item.sku}
        </div>
      )
    },
    {
      Header: 'Giá bán',
      width: 100,
      id: 'price',
      className: 'text-right',
      accessor: (item) => (
        <div className="text-ellipsis">
          {item.options.length > 0 ? <MutipleVariants item={item}/> : `${numberAsCurrentcy(item.price || 0)}đ`}
        </div>
      )
    },
    {
      Header: 'Số lượng',
      width: 100,
      id: 'qty',
      className: 'text-center',
      accessor: (item) => (
        <div className="text-ellipsis">
          {item.options.length > 0 ? <MutipleVariants item={item}/> : item.qty}
        </div>
      )
    },
    {
      Header: 'Trạng thái',
      width: 80,
      id: 'status',
      className: 'text-center',
      accessor: (item) => (
        <ProductStatus status={item.status}/>
      )
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 130,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <Fragment>
          <Link to={`/user/store/products/update/${item.id}`} className="text-green">
            <i className="fas fa-edit"/> Sửa
          </Link>
          <HyperLink onClick={this.props.handleDelete(item)} className="text-red">
            <i className="fas fa-trash"/> Xóa
          </HyperLink>
        </Fragment>
      )
    }
  ];

  render() {
    const {productList} = this.props;
    return (
      <CommonTable
        className="product-table-rt"
        data={productList}
        columns={this.columns}
        showPagination={false}
      />
    )
  }
}

export default ProductTable;
