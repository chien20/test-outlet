import React from 'react';
import Skeleton from 'react-loading-skeleton';
import {addStoreShippingFeeAPI, getStoreShippingFeeAPI, updateStoreShippingFeeAPI} from '../../../../api/stores';
import {connect} from 'react-redux';
import {showAlert} from '../../../../common/helpers';
import {Formik} from 'formik';
import InputNumberField from '../../../../components/Form/InputNumber/InputNumberField';
import history from '../../../../common/utils/router/history';

class UpsertShippingFee extends React.PureComponent {
  state = {
    isLoaded: false,
    initialValues: null,
  };

  componentDidMount() {
    this.initData();
  }

  initData = () => {
    const {match: {params: {id}}, storeId} = this.props;
    this.setState({
      isLoaded: false,
    });
    if (!id) {
      setTimeout(() => {
        this.setState({
          isLoaded: true,
          initialValues: {
            low: 0,
            high: 0,
            value: 0,
          },
        });
      });
      return;
    }
    getStoreShippingFeeAPI(storeId, id).then(res => {
      this.setState({
        isLoaded: true,
        initialValues: res.data,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handleSubmit = (values) => {
    const {match: {params: {id}}, storeId} = this.props;
    if (!id) {
      addStoreShippingFeeAPI(storeId, values).then(() => {
        showAlert({
          type: 'success',
          message: 'Đã lưu'
        });
        setTimeout(() => {
          history.push('/user/store/shipping-fee');
        }, 300);
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    } else {
      updateStoreShippingFeeAPI(storeId, id, values).then(() => {
        showAlert({
          type: 'success',
          message: 'Đã lưu'
        });
        setTimeout(() => {
          history.push('/user/store/shipping-fee');
        }, 300);
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    }
  };

  renderContent = () => {
    const {initialValues} = this.state;
    return (
      <Formik
        initialValues={initialValues}
        onSubmit={this.handleSubmit}
      >
        {({handleSubmit}) => (
          <form className="shipping-fee-form" onSubmit={handleSubmit}>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label>Từ (km):</label>
                <InputNumberField
                  name="low"
                  className="form-control"
                />
              </div>
              <div className="form-group col-md-6">
                <label>Đến (km):</label>
                <InputNumberField
                  name="high"
                  className="form-control"
                />
              </div>
            </div>
            <div className="form-group">
              <label>Giá vận chuyển (VND/km):</label>
              <InputNumberField
                name="value"
                className="form-control"
              />
            </div>
            <div className="form-actions">
              <button className="btn btn-primary">Lưu</button>
            </div>
          </form>
        )}
      </Formik>
    )
  };

  render() {
    const {isLoaded} = this.state;
    return (
      <div className="shipping-fee-page">
        <div className="page-title">
          <h1>Thông tin vận chuyển</h1>
        </div>
        <div className="page-content">
          {
            !isLoaded &&
            <Skeleton count={5}/>
          }
          {
            isLoaded && this.renderContent()
          }
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
});

export default connect(mapStateToProps)(UpsertShippingFee);
