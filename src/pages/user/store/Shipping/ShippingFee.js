import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {deleteProductRatingAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import Pagination from '../../../../components/Pagination/Pagination';
import {history} from '../../../../common/utils/router/history';
import {formatDate, numberAsCurrentcy} from '../../../../common/helpers/format';
import CommonTable from '../../../../components/Table/CommonTable';
import NoData from '../../../../components/NoData/NoData';
import {deleteStoreShippingFeeAPI, getStoreShippingFeesAPI} from '../../../../api/stores';
import {Link} from 'react-router-dom';
import HyperLink from '../../../../components/HyperLink/HyperLink';

const ITEMS_PER_PAGE = 10;

class ShippingFee extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      totalItems: 0
    };
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.match !== prevProps.match || this.props.storeId !== prevProps.storeId) {
      this.getData();
    }
  }

  getData = () => {
    const {match: {params: {page}}, storeId} = this.props;
    const params = {
      current_page: page || 1,
      page_size: ITEMS_PER_PAGE
    };
    params.current_page -= 1;
    getStoreShippingFeesAPI(storeId, params).then(res => {
      if (res && res.data && res.data.list) {
        this.setState({
          isLoading: false,
          data: res.data.list,
          totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false,
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/user/store/shipping-fee/${page}`);
    } else {
      history.push(`/user/store/shipping-fee`);
    }
  };

  handleDeleteItem = (id) => () => {
    const {storeId} = this.props;
    deleteStoreShippingFeeAPI(storeId, [id]).then(() => {
      showAlert({
        type: 'success',
        message: `Đã xóa!`
      });
      this.getData();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  columns = [
    {
      Header: 'Từ (km)',
      className: 'text-center',
      accessor: 'low'
    },
    {
      Header: 'Đến (km)',
      className: 'text-center',
      accessor: 'high'
    },
    {
      Header: 'Giá cước',
      className: 'text-center',
      id: 'price',
      accessor: (item) => (
        <div>{`${numberAsCurrentcy(item.value || 0)}đ`}</div>
      )
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 150,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <Fragment>
          <Link to={`/user/store/shipping-fee/update/${item.id}`} className="text-green">
            <i className="fas fa-edit"/> Sửa
          </Link>
          <HyperLink onClick={this.handleDeleteItem(item.id)} className="text-red">
            <i className="fas fa-trash"/> Xóa
          </HyperLink>
        </Fragment>
      )
    }
  ];

  render() {
    const {match: {params: {page}}} = this.props;
    const {isLoading, data, totalItems} = this.state;
    const totalPage = Math.ceil(totalItems / ITEMS_PER_PAGE);
    return (
      <div className="shipping-fee-page">
        <div className="page-title">
          <h1>Thông tin vận chuyển</h1>
        </div>
        <div className="page-content">
          {
            !isLoading && !data.length &&
            <NoData
              title="Chưa thiết lập phí vận chuyển"
              description="Click vào nút dưới đây để thêm phí vận chuyển nếu bạn muốn tự vận chuyển cho khách"
            >
              <Link to="/user/store/shipping-fee/new" className="btn btn-primary"><i className="fas fa-plus"/> Thêm</Link>
            </NoData>
          }
          {
            !!data.length &&
            <CommonTable
              className="shipping-fee-table"
              data={data}
              columns={this.columns}
              showPagination={false}
            />
          }
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  storeId: state.user.info.store_id,
});

export default connect(mapStateToProps)(ShippingFee);
