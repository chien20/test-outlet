import React from 'react';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import UserSideBar from './_components/UserSideBar/UserSideBar';
import './UserPageMobile.scss';
import AccountPage from './account';
import StorePage from './store';

import ConversationMobile from "./account/Chat/Conversation/ConversationMobile";

class UserPageMobile extends React.PureComponent {
  render() {
    const {location: {pathname}} = this.props; 
    return (
      <div className="user-page user-page-mobile">
        {
          pathname === '/user' && <UserSideBar/>
        }
        <div className="user-page-content">
          <Switch>
            <Route path="/user/store" component={StorePage}/>
            <Route path="/user/profile" component={AccountPage}/>
            <Route path="/user/orders" component={AccountPage}/>
            <Route path="/user/ratings" component={AccountPage}/>
            <Route path="/user/addresses" component={AccountPage}/>
            <Route path="/user/e-points" component={AccountPage}/>
            <Route path="/user/chat/:id" component={ConversationMobile}/>
            <Route path="/user/chat" component={AccountPage}/>
            <Route path="/user/notifications/:type" component={AccountPage}/>
            <Route path="/user/notifications" exact render={() => <Redirect to="/user/notifications/all"/>}/>
          </Switch>
        </div>
      </div>
    )
  }
}

export default withRouter(UserPageMobile);
