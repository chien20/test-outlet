import React, {Fragment} from 'react';
import ViewedProducts from './ViewedProducts';
import {Helmet} from 'react-helmet';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import connect from 'react-redux/es/connect/connect';
import NoData from '../../../components/NoData/NoData';
import {Link} from 'react-router-dom';

class ViewedProductsContainer extends React.PureComponent {
  render() {
    const {user} = this.props;
    if (!user.auth.isAuthenticated) {
      return (
        <Fragment>
          <Helmet>
            <title>Sản phẩm đã xem</title>
          </Helmet>
          <Breadcrumb>
            <BreadcrumbItem text="Sản phẩm đã xem" isActive={true}/>
          </Breadcrumb>
          <NoData
            title="Không có đơn hàng"
            description="Bạn chưa đặt mua sản phẩm nào."
          >
            <Link className="btn btn-primary" to="/login">Đăng nhập</Link>
          </NoData>
        </Fragment>
      );
    }
    return (
      <ViewedProducts/>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(ViewedProductsContainer);
