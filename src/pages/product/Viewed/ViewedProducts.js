import React, {Fragment} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {history} from '../../../common/utils/router/history';
import {getProductListAPI} from '../../../api/products';
import ProductItem from '../../../components/ProductItem/ProductItem';
import Pagination from '../../../components/Pagination/Pagination';
import {Helmet} from 'react-helmet';
import {isMobile} from '../../../common/helpers/browser';
import InfiniteScroll from '../../../components/InfiniteScroll/InfiniteScroll';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import CartIconMobile from '../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import _chunk from 'lodash/chunk';
import Adv from '../../../components/Adv/Adv';
import NoData from '../../../components/NoData/NoData';

const PAGE_SIZE = 36;

class ViewedProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isLoaded: false,
      productList: [],
      totalItems: 0,
      sortParams: {
        by: '',
        order: ''
      },
      hasMore: false,
      nextPage: 0,
    };
  }

  componentDidMount() {
    if (!isMobile) {
      this.getProductList(this.props);
    } else {
      this.getProductListMore();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getProductList(nextProps);
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  getProductList(props) {
    const {match: {params: {page}}} = props;
    const {sortParams} = this.state;
    const params = {
      current_page: page || 1,
      page_size: PAGE_SIZE,
      type: 'viewed'
    };
    params.current_page -= 1;
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    getProductListAPI(params, {__auth: true}).then(res => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isLoading: false,
        isLoaded: true,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isLoading: false,
        isLoaded: true,
      });
    });
  }

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/products/history/${page}`);
    } else {
      history.push(`/products/history`);
    }
  };

  onSort = (by, order) => () => {
    this.setState({
      sortParams: {
        by,
        order
      }
    }, () => {
      this.getProductList(this.props);
    });
  };

  getProductListMore = () => {
    const {nextPage, isLoading, sortParams} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: PAGE_SIZE,
      type: 'viewed'
    };
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    this.setState({
      isLoading: true
    });
    getProductListAPI(params, {__auth: true}).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info || {};
          return {
            isLoading: false,
            isLoaded: true,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1,
            totalItems: pageInfo.total_items || 0
          };
        });
      } else {
        this.setState({
          isLoading: false,
          isLoaded: true,
        });
      }
    }).catch(error => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isLoading: false,
        isLoaded: true,
      });
    });
  };

  onLoadMore = () => {
    this.getProductListMore();
  };

  render() {
    const {productList, totalItems, hasMore, isLoaded} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    const chunks = _chunk(productList, 6);
    return (
      <Fragment>
        <Helmet>
          <title>Sản phẩm đã xem</title>
        </Helmet>
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile>Sản phẩm đã xem</TitleMobile>
            <CartIconMobile/>
          </HeaderMobile>
        }
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Sản phẩm đã xem" isActive={true}/>
          </Breadcrumb>
        }
        <section className="section product-category-page common-page">
          <div className="container">
            {
              !isMobile &&
              <div className="section-heading">
                <h2>
                  <span className="text-uppercase">Sản phẩm đã xem</span>
                </h2>
              </div>
            }
            {
              isLoaded && !productList.length &&
              <NoData title="Không có sản phẩm"/>
            }
            <div className="product-grid">
              {
                chunks.map((chunk, chunkIndex) => (
                  <Fragment key={chunkIndex}>
                    {
                      !!chunkIndex && chunkIndex < chunks.length && chunkIndex % 3 === 0 &&
                      <Adv position="inside_list_product"/>
                    }
                    <div className="row product-items chunk" key={chunkIndex}>
                      {
                        chunk.map((item, index) =>
                          <div className="product-item-wrapper" key={index}>
                            <ProductItem product={item}/>
                          </div>
                        )
                      }
                    </div>
                  </Fragment>
                ))
              }
              {
                hasMore && isMobile && <InfiniteScroll loadMore={this.onLoadMore}/>
              }
              <div className="item-pagination hidden-mobile">
                {
                  totalPage > 1 &&
                  <Pagination totalPages={totalPage} currentPage={page || 1} onPageChanged={this.onPageChanged}/>
                }
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default ViewedProducts;
