import React, {Fragment, useState} from 'react';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import ShareIconMobile from '../../../components/HeaderMobile/ShareIconMobile/ShareIconMobile';
import CartIconMobile from '../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import BarIconMobile from '../../../components/HeaderMobile/BarIconMobile/BarIconMobile';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import ProductImagesViewer from '../_components/ProductImagesViewer/ProductImagesViewer';
import ProductMainInfo from '../_components/ProductMainInfo/ProductMainInfo';
import ProductContactInfo from '../_components/ProductContactInfo/ProductContactInfo';
import CommentSection from '../_components/CommentSection/CommentSection';
import OtherProducts from '../_components/OtherProducts/OtherProducts';
import ProductFeaturesMobile from '../_components/ProductFeatures/ProductFeaturesMobile';
import ViewedProducts from '../../home/_components/ViewedProducts/ViewedProducts';

const ProductDetailMobile = ({product}) => {
  const [showDetail, setShowDetail] = useState(false);
  return (
    <Fragment>
    <div className="product-detail">
      <HeaderMobile className="product-detail-header-mb">
        <IconBackMobile className="icon-left"/>
        <div className="icon-right">
          <ShareIconMobile/>
          <CartIconMobile/>
          <BarIconMobile/>
        </div>
      </HeaderMobile>
      <div className="product-detail-page common-page">
        <ProductImagesViewer images={product.images}/>
        <div className={`product-main-info-wrapper ${showDetail ? '--show-detail' : ''}`}>
          <ProductMainInfo product={product}/>
          {
            !showDetail &&
            <div className="show-detail" onClick={() => setShowDetail(true)}>
              Xem mô tả chi tiết 
            </div>
          }
        </div>
        { 
          showDetail &&
          <Fragment>
            <div className="product-description product-section">
              <div className="section-title">
                <h2>Mô tả sản phẩm</h2>
              </div>
              <div className="paper section-content" dangerouslySetInnerHTML={{__html: product.full_description}}/>
            </div>
          </Fragment>
        }
        <ProductContactInfo product={product}/>
        {
          !!product.features.length &&
          <ProductFeaturesMobile features={product.features}/>
        }

        <CommentSection rating={product.rating || 0} productId={product.id}/>
      </div>
      <ViewedProducts/>
      <OtherProducts type="shop" storeId={product.store_id}/>
      <OtherProducts type="relative" categoryId={product.category_id}/>
    </div>
    </Fragment>
  );
};

export default React.memo(ProductDetailMobile);
