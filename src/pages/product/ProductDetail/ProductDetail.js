import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import './ProductDetail.scss';
import {getProductDetailAPI} from '../../../api/products';
import {showAlert} from '../../../common/helpers';
import {Helmet} from 'react-helmet';
import {isMobile} from '../../../common/helpers/browser'; 
import Error404 from '../../error/Error404';
import ProductDetailPC from './ProductDetailPC';
import ProductDetailMobile from './ProductDetailMobile';

const sanitizeHTML = window.sanitizeHtml;

class ProductDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      product: null,
    } 
  }

  componentDidMount() {
    this.getProduct(this.props);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.match.params.id !== nextProps.match.params.id) {
      this.setState({
        isLoading: true,
        product: null
      });
      this.getProduct(nextProps);
    }
  }

  getProduct = (props) => {
    const {match: {params: {id}}} = props;
    getProductDetailAPI(id).then(res => {
      if (res && res.data) {
        const product = res.data;
        if (product.status !== 10) {
          this.setState({
            product: null,
            isLoading: false
          });
          return;
        }
        if (product.short_description && typeof product.short_description === 'string') {
          product.short_description = product.short_description.replace(/(?:\r\n|\r|\n)/g, '<br>');
          product.short_description = sanitizeHTML(product.short_description, {
            allowedTags: ['br'],
            selfClosing: ['br'],
            allowedAttributes: []
          });
        }
        if (product.full_description) {
          product.full_description = sanitizeHTML(product.full_description, {
            allowedTags: ['img', 'br', 'p', 'span', 'ul', 'li', 'ol', 'div'],
            selfClosing: ['img', 'br'],
            allowedAttributes: {
              'img': ['src', 'alt']
            }
          });
        }
        if (product.features) {
          product.features = product.features.filter(item => `${item.value}`.trim());
        }
        if (product.options && product.options.length) {
          product.options.forEach(option => {
            const values = [];
            option.groups_index.forEach((valueIndex, groupIndex) => {
              const group = product.option_groups[groupIndex];
              values.push(group.values[valueIndex]);
            });
            option.name = values.join(', ');
          });
        }
        this.setState({
          product: product,
          isLoading: false
        });
      } else {
        this.setState({
          isLoading: false
        });
        showAlert({
          type: 'error',
          message: `Không tải được thông tin sản phẩm!`
        });
      }
    }).catch(() => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được thông tin sản phẩm!`
      });
    })
  };

  render() {
    const {isLoading, product} = this.state;
    const {categoryMap} = this.props;
    if (isLoading || !categoryMap) {
      return null;
    }
    if (!product) {
      return <Error404/>
    }
    product.category = categoryMap[product.category_id];
    return (
      <Fragment>
        <Helmet>
          <title>{product.name}</title>
        </Helmet>
        {
          !isMobile &&
          <ProductDetailPC product={product} categoryMap={categoryMap}/>
        }
        {
          isMobile &&
          <ProductDetailMobile product={product}/>
        }
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  categoryMap: state.common.category.map
});

export default connect(mapStateToProps)(ProductDetail);
