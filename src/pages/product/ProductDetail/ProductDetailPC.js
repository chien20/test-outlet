import React, {Fragment, useState} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import ProductImagesViewer from '../_components/ProductImagesViewer/ProductImagesViewer';
import ProductSocials from '../_components/ProductSocials/ProductSocials';
import ProductMainInfo from '../_components/ProductMainInfo/ProductMainInfo';
import ProductContactInfo from '../_components/ProductContactInfo/ProductContactInfo';
import CommentSection from '../_components/CommentSection/CommentSection';
import OtherProducts from '../_components/OtherProducts/OtherProducts';
import ProductFeaturesPc from '../_components/ProductFeatures/ProductFeaturesPc';
import Benefits from "../../../components/Benefits/Benefits";
import Adv from '../../../components/Adv/Adv';


const ProductDetailPC = ({product, categoryMap}) => {
  const [showDetail, setShowDetail] = useState(false);
  return (
    <Fragment>
      <Breadcrumb>
        {
          product.category && product.category.parents.map(id => (
            <Fragment key={id}>
              {
                categoryMap[id] &&
                <BreadcrumbItem
                  text={categoryMap[id].name}
                  path={`/${categoryMap[id].slug}/c${id}`}
                  isActive={false}/>
              }
            </Fragment>
          ))
        }
        <BreadcrumbItem
          text={product.category.name}
          path={`/${product.category.slug}/c${product.category.id}`}
          isActive={false}/>
        <BreadcrumbItem text={product.name} isActive={true}/>
      </Breadcrumb>
      <div className="product-detail-page common-page">
        <div className="product-main">
          <div className="product-overview-section">
            <div className="container">
              <div className="product-row">
                <div className="col-images" style={{zIndex: 10}}>
                  <Fragment> 
                    <ProductImagesViewer images={product.images}/>
                    <ProductSocials product={product}/>
                  </Fragment>
                </div>
                <div className="col-info">
                  <ProductMainInfo product={product}/>
                </div>
              </div>
            </div>
          </div>
          <div className="product-shop product-section bg-gray">
            <div className="container">
              <ProductContactInfo product={product}/>
            </div>
          </div>
          <div className="other-products bg-gray">
            <div className="container">
              <OtherProducts type="shop" storeId={product.store_id}/>
            </div>
          </div>
          <div className="product-description-detail bg-gray">
            <div className="container">
              <div className="row">
                <div className="col-9">
                  {
                    !!product.features.length &&
                    <ProductFeaturesPc features={product.features}/>
                  }
                  <div className="product-description product-section">
                    <div className="section-title">
                      <h2>Mô tả chi tiết</h2>
                    </div>
                    <div className={`section-content ${showDetail ? '--show-detail' : ''}`} dangerouslySetInnerHTML={{__html: product.full_description}}/>
                    {
                      !showDetail &&
                      <div className="show-detail">
                        <span onClick={() => setShowDetail(true)}>Xem mô tả chi tiết</span>
                      </div>
                    }
                  </div>
                  <CommentSection rating={product.rating || 0} productId={product.id}/>
                </div>
                <div className="col-3">
                  <Benefits storeId={product.store_id}/>
                  <Adv position="left-sidebar"/>
                </div>
              </div>
            </div>
          </div>
          <div className="other-products bg-gray">
            <div className="container">
              <OtherProducts type="relative" categoryId={product.category_id}/>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
};

export default React.memo(ProductDetailPC);
