import React, {Fragment} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {getProductListAPI} from '../../../api/products';
import ProductItem from '../../../components/ProductItem/ProductItem';
import {Helmet} from 'react-helmet';
import InfiniteScroll from '../../../components/InfiniteScroll/InfiniteScroll';
import {getRunningEventsAPI} from '../../../api/events';
import CountDown from '../../../components/CountDown/CountDown';
import './FlashSale.scss';
import {isMobile} from "../../../common/helpers/browser";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import CartIconMobile from "../../../components/HeaderMobile/CartIconMobile/CartIconMobile";
import NoData from '../../../components/NoData/NoData';
import HTMLView from "../../../components/View/HTMLView";

class FlashSale extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      event: null,
      isLoading: false,
      productList: [],
      totalItems: 0,
      sortParams: {
        by: '',
        order: ''
      },
      hasMore: false,
      nextPage: 0
    };
  }

  componentDidMount() {
    this.getRunningEvents().catch(error => {
      // TODO
    });
  }

  getRunningEvents = async () => {
    this.setState({
      isLoading: true
    });
    const {data: {list: events}} = await getRunningEventsAPI({
      sort_by: 'from',
      sort_order: 'asc'
    });
    const event = events.find(item => item.is_flash_sale) || null;
    if (!event) {
      return;
    }
    event.url = `/flash-sale/${event.slug}/e${event.id}`;
    this.setState({
      event,
      isLoading: false
    }, this.getProductListMore);
  };

  getProductListMore = () => {
    const {nextPage, isLoading, sortParams, event} = this.state;
    if (isLoading || !event) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: 20,
      event_id: event.id
    };
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    this.setState({
      isLoading: true
    });
    getProductListAPI(params).then(res => {
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info || {};
          return {
            isLoading: false,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1,
            totalItems: pageInfo.total_items || 0
          };
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  };

  onLoadMore = () => {
    this.getProductListMore();
  };

  render() {
    const {productList, event, hasMore} = this.state;
    if (!event) {
      return (
        <Fragment>
          <Helmet>
            <title>Flash sale</title>
          </Helmet>
          <section className="section flash-sale-page common-page">
            <NoData title="Không có sản phẩm"/>
          </section>
        </Fragment>
      );
    }
    const toDate = new Date();
    toDate.setTime(event.to);
    return (
      <Fragment>
        <Helmet>
          <title>Flash sale</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text={event.is_flash_sale ? 'Flash sale' : event.name} isActive={true}/>
          </Breadcrumb>
        }
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile>Flash Sale</TitleMobile>
            <CartIconMobile/>
          </HeaderMobile>
        }
        <section className="section flash-sale-page common-page">
          <div className="container">
            <div className="section-heading">
              <h2>
                <span className="d-inline-block">
                  <span>{event.name}</span>
                </span>
              </h2>
            </div>
            <div className="count-down-container">
              <h3>Flash sale</h3>
              <CountDown endTime={toDate}/>
            </div>
            <div className="event-description">
              <HTMLView html={event.content}></HTMLView>
            </div>
            <div className="product-grid">
              <div className="row product-items">
                {
                  productList.map((item, index) => (
                    <div className="product-item-wrapper" key={index}>
                      <ProductItem product={item}/>
                    </div>
                  ))
                }
              </div>
              {
                hasMore && <InfiniteScroll loadMore={this.onLoadMore}/>
              }
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default FlashSale;
