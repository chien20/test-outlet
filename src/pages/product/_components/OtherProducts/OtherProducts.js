import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {getProductListAPI} from '../../../../api/products';
import {isMobile} from '../../../../common/helpers/browser';
import {Link} from 'react-router-dom';
import './OtherProducts.scss';
import Carousel from "nuka-carousel";
import SliderProps from "../../../../components/NukaSlider/SliderProps";

const sliderProps = SliderProps;

class OtherProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Sản phẩm khác',
      productList: [],
      isLoading: false
    };
  }

  componentWillMount() {
    const {type} = this.props;
    const state = {
      title: '',
      productList: [],
      isLoading: true
    };
    switch (type) {
      case 'shop':
        state.title = 'Sản phẩm khác của shop';
        break;
      case 'relative':
        state.title = 'Sản phẩm tương tự';
        break;
      default:
        break;
    }
    this.setState(state, this.getListProduct);
  }

  getListProduct() {
    const {type} = this.props;
    const params = {
      current_page: 0,
      page_size: isMobile ? 6 : 18
    };
    switch (type) {
      case 'shop':
        params.store_id = this.props.storeId;
        break;
      case 'relative':
        params.category_id = this.props.categoryId;
        break;
      default:
        break;
    }
    getProductListAPI(params).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : []
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  }

  getViewMoreLink = () => {
    const {type, storeId, categoryId} = this.props;
    switch (type) {
      case 'shop': 
        return `/shop/${storeId}`;
      case 'relative':
        return `/c/c${categoryId}`;
      default:
        return null;
    }
  };

  render() {
    const {title, productList, isLoading} = this.state;
    if (isLoading || !productList.length) {
      return null;
    }
    const viewMoreLink = this.getViewMoreLink();

    return (
      <div className="section other-products bg-transparent">
        <div className="section-heading">
          <h2>{title}</h2>
          {
            viewMoreLink &&
            <Link className="products-read-more" to={viewMoreLink}>Xem thêm</Link>
          }
        </div>
        <div className="section-inner">
          {
            isMobile &&
            <Carousel
              className="product-items --slider"
              slidesToShow={2}
              slidesToScroll={2}
              {...sliderProps}
            >
              {
                productList.map((item, index) => (
                  <div className="product-item-wrapper" key={index}>
                    <ProductItem product={item}/>
                  </div>
                ))
              }
            </Carousel>
          }
          {
            !isMobile &&
            <Carousel
              className="product-items --slider"
              slidesToShow={6}
              slidesToScroll={3}
              {...sliderProps}
            >
              {
                productList.map((item, index) => (
                  <div className="product-item-wrapper" key={index}>
                    <ProductItem product={item}/>
                  </div>
                ))
              }
            </Carousel>
          }
        </div>
      </div>
    )
  }
}

OtherProducts.propTypes = {
  type: PropTypes.oneOf(['shop', 'relative']),
  storeId: PropTypes.any,
  categoryId: PropTypes.any
};

export default OtherProducts;
