import React, {Fragment} from 'react';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {getProductLikesAPI, likeProductAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import {connect} from 'react-redux';
import './ProductFavorite.scss';

class ProductFavorite extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      likes: null,
      isSubmitting: false
    }
  }

  componentDidMount() {
    this.getProductLikes();
  }

  getProductLikes = () => {
    const {product} = this.props;
    getProductLikesAPI(product.id).then(res => {
      this.setState({
        likes: res.data
      });
    }).catch(() => {
      this.setState({
        likes: {
          count: 0,
          liked: false
        }
      });
    });
  };

  likeProduct = (liked) => () => {
    const {product} = this.props;
    if (this.state.isSubmitting) {
      return;
    }
    this.setState({
      isSubmitting: true
    });
    likeProductAPI(product.id, liked).then(() => {
      this.getProductLikes();
      this.setState({
        isSubmitting: false
      });
    }).catch(error => {
      this.setState({
        isSubmitting: false
      });
      showAlert({
        type: 'error',
        message: error.message
      });
    })
  };

  render() {
    const {user} = this.props;
    const {likes} = this.state;
    return (
      <div className="product-favorite">
        {
          likes &&
          <Fragment>
            {
              (user && user.auth && user.auth.isAuthenticated) ?
                <HyperLink onClick={this.likeProduct(!likes.liked)} title={`Yêu thích`}>
                  {
                    likes.liked ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>
                  }
                </HyperLink>
                :
                <i style={{color: '#DADADA'}} className="far fa-heart"/>
            }
            {
              likes.count === 0 ? null
              :<span className="likes-count">{likes.count}</span> 
            }
          </Fragment>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(ProductFavorite);
