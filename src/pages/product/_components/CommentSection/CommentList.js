import React, {Fragment} from 'react';
import {deleteProductRatingAPI, getProductRatingsAPI} from '../../../../api/products';
import {dateFormat, imageUrl, publicUrl, showAlert} from '../../../../common/helpers';
import StarRating from '../../../../components/StarRating/StarRating';
import './CommentList.scss';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {connect} from 'react-redux';
import Broadcaster from '../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../common/constants/broadcastEvents';
import Pagination from '../../../../components/Pagination/Pagination';
import NoData from '../../../../components/NoData/NoData';

const sanitizeHTML = window.sanitizeHtml;

class CommentList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      totalItems: 0,
      page: 1,
      isLoading: false
    };
  }

  componentDidMount() {
    Broadcaster.on(BROADCAST_EVENT.RATE_PRODUCT_SUCCESS, this.getComments);
    this.getComments();
  }

  componentWillUnmount() {
    Broadcaster.off(BROADCAST_EVENT.RATE_PRODUCT_SUCCESS, this.getComments);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.productId !== this.props.productId) {
      this.getComments();
    }
  }

  getComments = () => {
    const {productId} = this.props;
    const {page} = this.state;
    const params = {
      current_page: page || 1,
      page_size: 10,
      sort_by: 'id',
      sort_order: 'desc'
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getProductRatingsAPI(productId, params).then(res => {
      const state = {
        isLoading: false,
        comments: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      };
      if (state.comments.length && Array.isArray(state.comments[0])) {
        state.comments = state.comments[0];
      }
      if (state.comments.length) {
        state.comments.forEach(item => {
          if (item.comment) {
            item.comment = item.comment.replace(/(?:\r\n|\r|\n)/g, '<br>');
          }
          if (item.body) {
            item.comment = item.body.replace(/(?:\r\n|\r|\n)/g, '<br>');
          }
          if (item.comment) {
            item.comment = sanitizeHTML(item.comment, {
              allowedTags: ['br'],
              selfClosing: ['br']
            });
          }
          if (item.user_info) {
            if (!item.user_info.avatar) {
              item.user_info.avatar = publicUrl('/assets/images/no-image/avatar.png');
            }
          }
        });
      }
      this.setState(state);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
      this.setState({
        isLoading: false
      });
    });
  };

  deleteComment = (id) => () => {
    deleteProductRatingAPI([id]).then(() => {
      this.getComments();
      showAlert({
        type: 'success',
        message: `Đã xóa!`
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handlePageChange = (page) => {
    this.setState({
      page
    }, this.getComments);
  };

  render() {
    const {user} = this.props;
    const {comments, page, totalItems} = this.state;
    if (!comments) {
      return null;
    }
    if (!comments.length) {
      return <NoData title="Chưa có nhận xét" description="Hãy mua hàng và trở thành người đầu tiên nhận xét về sản phẩm này."/>
    }
    const totalPage = Math.ceil(totalItems / 10);
    return (
      <Fragment>
        {/* <div className="comment-top d-none">
          <ul className="sort">
            <li className="sort-by-start">
              <img src={publicUrl(`/assets/images/icons/sort-all.svg`)} alt="icon"/>
              Xem theo: Tất cả các sao
            </li>
            <li className="sort-by-image">
              <input type="checkbox" name="checkboxImageComment"/> Có hình ảnh
            </li>
          </ul>
        </div> */}

        <div className="comment-mid">
          <ul className="sort">
            <li className="total-comments">
              {comments.length} Bình luận
            </li>
            <li className="sort-by-image">
              <input type="checkbox" id="check-img" name="checkboxImageComment"/>
              <label for="check-img" data-content="Có hình ảnh">Có hình ảnh</label>
          </li>
          </ul>
        </div>
        <ul className="comment-list">
          {
            comments.map((item, index) => (
              <li key={index} className="comment-item">
                <div className="user-avatar">
                  <img
                    src={item.user_info.avatar ? imageUrl(item.user_info.avatar) : publicUrl('/assets/images/no-image/avatar.png')}
                    alt={item.user_info.full_name}/>
                </div>
                <div className="user-info">
                  <h4 className="user-name">{item.user_info.full_name}</h4>
                  <div className="rating-value">
                    <StarRating value={item.rate || item.point || 0} isDisabled={true}/>
                    <span className="time">
                       {dateFormat('H:i:s - d/m/Y', Math.floor(Date.parse(item.created_at) / 1000))}
                      </span>
                  </div>
                  <div className="comment" dangerouslySetInnerHTML={{__html: item.comment}}/>
                  {
                    user && user.id === item.user_id &&
                    <div className="action">
                      <HyperLink className="like">
                        <span>Cảm ơn</span>
                         <img
                              src={publicUrl('/assets/images/icons/icon-like.svg')}
                              alt="like"/>
                      </HyperLink>
                      <HyperLink onClick={this.deleteComment(item.id)}>Xóa</HyperLink>
                    </div>
                  }
                </div>
              </li>
            ))
          }
        </ul>
        {/* <Pagination currentPage={page} totalPages={totalPage} totalItems={totalItems} pageSize={10}
                    onPageChanged={this.handlePageChange}/> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info
});

export default connect(mapStateToProps)(CommentList);
