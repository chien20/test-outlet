import React from 'react';
import StarRating from '../../../../components/StarRating/StarRating';
import {ProgressBar} from 'react-bootstrap';
import './RatingStats.scss';

const stars = [5, 4, 3, 2, 1];

class RatingStats extends React.PureComponent {
  render() {
    const {rating, showForm, toggleForm} = this.props;
    return (
      <div className="rating-stats">
        <div className="col-rating-avg">
          <p className="title">Đánh giá trung bình</p>
          <p className="value">{rating}/5</p>
          <StarRating value={rating} isDisabled={true}/>
          <p>(3 nhận xét)</p>
        </div>
        <div className="col-rating-stats">
          <ul>
            {
              stars.map((item, index) => (
                <li key={index}>
                  <StarRating value={item} isDisabled={true}/>
                  <ProgressBar className="rating-progress-bar" now={60}/>
                  <span>50%</span>
                </li>
              ))
            }
          </ul>
        </div>
        <div className="col-write-review">
          <p>Chia sẻ nhận xét về sản phẩm</p>
          <button className="btn btn-orange" onClick={toggleForm}>
            {showForm ? 'Đóng' : 'Viết nhận xét của bạn'}
          </button>
        </div>
      </div>
    )
  }
}

export default RatingStats;