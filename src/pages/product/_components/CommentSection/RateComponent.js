import React from 'react';
import StarRating from '../../../../components/StarRating/StarRating';
import {angryIcon, desperateIcon, happyIcon, inLoveIcon, scepticIcon} from '../../../../assets/images';

const smileys = [angryIcon, desperateIcon, scepticIcon, happyIcon, inLoveIcon];
const comments = [
  'Tệ, quá tệ >.<',
  'Không ổn cho lắm!',
  'Dùng được, không đến nỗi!',
  'Sản phẩm tương đối tốt!',
  'Sản phẩm rất tốt!'
];

class RateComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 4
    };
  }

  onHoverStar = (index) => {
    this.setState({
      currentIndex: index
    });
  };

  render() {
    const {value, onChange} = this.props;
    const {currentIndex} = this.state;
    const currentImage = smileys[currentIndex % smileys.length];
    const currentComment = comments[currentIndex % comments.length];
    return (
      <div className="rate">
        <div>Đánh giá của bạn về sản phẩm này</div>
        <img src={currentImage} alt={``}/>
        <div className="rating-wrapper"
             onMouseLeave={this.handleMouseOut}>
          <StarRating
            value={value}
            onChange={onChange}
            onHoverStar={this.onHoverStar}/>
        </div>
        <div className="description">
          "{currentComment}"
        </div>
        <div style={{display: 'none'}}>
          {
            smileys.map((item, index) => (
              <img src={item} alt={``} key={index}/>
            ))
          }
        </div>
      </div>
    )
  }
}

export default RateComponent;
