import React from 'react';
import {connect} from 'react-redux';
import RatingForm from './RatingForm';
import './CommentSection.scss';
import CommentList from './CommentList';
import RatingStats from './RatingStats';
import {
  checkUserBoughtProductAPI,
  getProductRatingStatsAPI,
  getProductRatingStatsDetailAPI
} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import RatingDetail from "./RatingDetail";

class CommentSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      bought: false,
      ratings: null
    };
  }

  componentWillMount() {
    if (!this.props.rating) {
      this.setState({
        showForm: true
      });
    }
  }

  componentDidMount() {
    this.checkUserBoughtProduct();
    this.getRatingStats();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.productId !== this.props.productId) {
      this.checkUserBoughtProduct();
      this.getRatingStats();
    }
  }

  getRatingStats = () => {
    this.getRatingStatsAsync().then(ratings => {
      this.setState({
        ratings,
      });
    }).catch(() => {
    });
  };

  getRatingStatsAsync = async () => {
    const {productId} = this.props;
    const ratings = {
      count: 0,
      avg: 0,
      detail: {},
    };
    try {
      const {data: {count, avg}} = await getProductRatingStatsAPI(productId);
      if (count > 0) {
        const {data: detail} = await getProductRatingStatsDetailAPI(productId);
        const max = Math.max(...Object.values(detail));
        Object.keys(detail).forEach(key => {
          const value = detail[key] * 1;
          const barPercentage = Math.round(value * 100 / max);
          ratings.detail[key] = {
            value,
            barPercentage: barPercentage > 5 ? barPercentage : 5,
          };
        });
      }
      ratings.count = count;
      ratings.avg = avg;
    } catch (e) {

    }
    return ratings;
  };

  toggleForm = () => {
    this.setState(prevState => ({
      showForm: !prevState.showForm
    }));
  };

  checkUserBoughtProduct = () => {
    const {productId, user} = this.props;
    if (user && user.auth && user.auth.isAuthenticated) {
      checkUserBoughtProductAPI(productId, user.info.id).then(res => {
        this.setState({
          bought: res.data
        });
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    }
  }; 

  render() {
    const {productId, rating} = this.props;
    const {showForm, bought, ratings} = this.state;
    return (
      <div className="comment-section">
        <div className="product-section">
          <div className="section-title">
            <h2>Đánh giá sản phẩm</h2>
          </div>
          <div className="section-content">
            {
              !!ratings?.count &&
              <RatingDetail ratings={ratings}/>
            }
            {
              !!rating &&
              <RatingStats
                rating={rating}
                productId={productId} 
                showForm={showForm}
                toggleForm={this.toggleForm}/>
            }
            {
              bought &&
              <RatingForm
                productId={productId}
                show={showForm}
                toggleForm={this.toggleForm}/>
            }
            <CommentList productId={productId}/>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(CommentSection);
