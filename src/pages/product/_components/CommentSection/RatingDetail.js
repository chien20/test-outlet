import React from "react";
import "./RatingDetail.scss";

const RatingItem = ({numberStar, detail}) => {
  const stars = [];
  for (let i = 0; i < numberStar; i++) {
    stars.push(<i className="fas fa-star active" key={i}/>);
  }
  return (
    <li>
      <div className="star-rating ">
        {stars}
      </div>
      <div className="star-rating-bar">
        <div className="line-rating">
          <span style={{width: `${detail?.barPercentage || 5}%`}}/>
        </div>
        <div className="count-rating">{detail?.value || 0}</div>
      </div>
    </li>
  );
};

const RatingDetail = ({ratings}) => { 
  return (
    <div className="rating-detail">
      <div className="rating-warp">
        <div className="rating-avg">
          <p className="larger-size">{ratings.avg}</p>
          <p>{ratings.count} đánh giá</p>
        </div>
        <ul className="rating-list">
          <RatingItem numberStar={5} detail={ratings.detail?.['5.0']}/>
          <RatingItem numberStar={4} detail={ratings.detail?.['4.0']}/>
          <RatingItem numberStar={3} detail={ratings.detail?.['3.0']}/>
          <RatingItem numberStar={2} detail={ratings.detail?.['2.0']}/>
          <RatingItem numberStar={1} detail={ratings.detail?.['1.0']}/>
        </ul>
      </div>
    </div>
  );
};

export default RatingDetail;
