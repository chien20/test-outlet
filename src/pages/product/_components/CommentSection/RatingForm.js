import React from 'react';
import RateComponent from './RateComponent';
import {handleInputTextChanged, showAlert} from '../../../../common/helpers';
import {rateProductAPI} from '../../../../api/products';
import './RatingForm.scss';
import Broadcaster from '../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../common/constants/broadcastEvents';

class RatingForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      comment: '',
      rate: 5,
      tags: []
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  onRatingChange = (value) => {
    this.setData({rate: value});
  };

  onSubmit = (event) => {
    event.preventDefault();
    const {productId} = this.props;
    const data = this.state;
    rateProductAPI(productId, data).then(() => {
      showAlert({
        type: 'success',
        message: 'Đã gửi! Nhận xét của bạn sẽ được hiển thị sau khi phê duyệt!'
      });
      Broadcaster.broadcast(BROADCAST_EVENT.RATE_PRODUCT_SUCCESS, {
        productId,
        data
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  render() {
    const {show} = this.props;
    const {title, comment, rate} = this.state;
    return (
      <form className={`rating-form ${show ? '' : 'hide'}`} onSubmit={this.onSubmit}>
        <div className="rating-form-wrapper">
          <RateComponent value={rate} onChange={this.onRatingChange}/>
          <div className="comment-form">
            <div className="form-group">
              <label>Tiêu đề nhận xét</label>
              <input
                name="title"
                className="form-control"
                maxLength={128}
                value={title}
                onChange={handleInputTextChanged('title', this.setData)}/>
            </div>
            <div className="form-group">
              <label>Nội dung nhận xét</label>
              <textarea
                name="comment"
                className="form-control"
                required={true}
                rows={3}
                maxLength={1000}
                value={comment}
                onChange={handleInputTextChanged('comment', this.setData)}/>
            </div>
            <div className="button-group">
              <button className="btn btn-warning" type="submit">Gửi nhận xét</button>
            </div>
          </div>
        </div>
      </form>
    )
  }
}

export default RatingForm;
