import React from "react";
import {Link} from "react-router-dom";
import {imageUrl, publicUrl} from "../../../../common/helpers";

class SubCategory extends React.PureComponent {
  render() {
    const {item} = this.props;
    const  avatarUrl = item.avatar_url ? imageUrl(item.avatar_url) : publicUrl(`/assets/images/no-image.png`);

    return (
        <Link className="item" to={`/${item.slug}/c${item.id}`}>
          <div className="img">
            <img src={avatarUrl} alt="img"/>
          </div>
          <div className="name">{item.name}</div>
        </Link>
    )
  }
}

export default SubCategory;