import React from 'react';
import {withRouter} from 'react-router-dom';
import qs from 'qs';
import './ProductFilter.scss';
import InputNumber from '../../../../components/Form/InputNumber/InputNumber';
import FeatureFilter from './FeatureFilter';
import history from '../../../../common/utils/router/history';
import {getBrandsAPI} from '../../../../api/products';
import BrandFilter from './BandFilter';
import Slider from '@material-ui/core/Slider';

class ProductFilter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      brands: [],
      selectedBrands: [],
      features: [],
      min_price: '',
      max_price: '',
      error: null,
      marksPrice: [
        {
          value: 0,
          label: 0,
        },
        {
          value: 50000000,
        },
      ],
    };
  }

  componentDidMount() {
    this.initData();
    this.getBrands();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {category} = this.props;
    if (prevProps.location !== this.props.location) {
      this.initData();
    }
    if (prevProps.category !== category) {
      this.getBrands();
    }
  }

  initData = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    const values = {
      min_price: '',
      max_price: '',
    };
    if (params.min_price) {
      values.min_price = params.min_price;
    }
    if (params.max_price) {
      values.max_price = params.max_price;
    }
    this.setState(values);
  };

  handlePriceChange = (event, newValue) => {
    this.setState({
      error: null,
      min_price: newValue[0],
      max_price: newValue[1],
    });
  };

  valuePrice = (value) => {
    return `${value/1000000} triệu`
  };

  doFilter = (event) => {
    event.preventDefault();
    const {min_price, max_price} = this.state;
    const {location: {search, pathname}} = this.props;
    if (min_price && max_price && min_price * 1 > max_price) {
      this.setState({
        error: 'Giá trị không hợp lệ!',
      });
      return;
    }

    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    if (min_price) {
      params.min_price = min_price;
    } else {
      delete params.min_price;
    }
    if (max_price) {
      params.max_price = max_price;
    } else {
      delete  params.max_price;
    }
    const queryParams = qs.stringify(params, {addQueryPrefix: true});
    history.push({
      pathname,
      search: queryParams,
    });
  };

  getBrands = () => {
    const {category} = this.props;
    getBrandsAPI({category_id: category.id}).then(res => {
      this.setState({
        brands: res.data
      });
    }).catch(error => {
      // TODO
    });
  };

  onSelectBrand = (id) => {
    this.setState(prevState => {
      const selectedBrands = [...prevState.selectedBrands];
      const index = selectedBrands.findIndex(item => item.id === id);
      if (index >= 0) {
        selectedBrands.splice(index, 1);
      } else {
        selectedBrands.push(id);
      }
      return {
        selectedBrands
      };
    });
  };

  handleReset = () => {
    this.setState({
      min_price: '',
      max_price: '',
    });
  };

  render() {
    const {category} = this.props;
    const {min_price, max_price, error, features, brands, selectedBrands, marksPrice} = this.state;
    return (
      <div className="product-filter-drawer">
        <form className="form-group" onSubmit={this.doFilter}>
          <h4 className="filter-name">Khoảng giá</h4>
          <div className="price-filter product-filter-group">
            <Slider
            value={[min_price, max_price]}
            onChange={this.handlePriceChange}
            valueLabelDisplay="on"
            aria-labelledby="range-slider"
            marks={marksPrice}
            getAriaValueText={this.valuePrice}
            valueLabelFormat={this.valuePrice}
            max={50000000}
            step={500000}
            />
          </div>
          <FeatureFilter/>
          <BrandFilter/>
          {/* {
            features.map((item, index) => (
              <FeatureFilter
                key={index}
                featureName={item.name}
                options={item.values}/>
            ))
          }
          {
            brands.length > 0 &&
            <BrandFilter
              options={brands}
              selectedOptions={selectedBrands}
              onSelect={this.onSelectBrand}/>
          } */}
          <div className="submited">
            <button
              className="btn-filter reset"
              onClick={this.handleReset}>Thiết lập lại
            </button>
            <button
              className="btn-filter submit"
              type="submit">Áp dụng
            </button>
          </div>
        </form>
      </div>
    )
  }
}

export default withRouter(ProductFilter);