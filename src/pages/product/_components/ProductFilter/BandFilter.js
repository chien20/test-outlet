import React from 'react';
import PropTypes from 'prop-types';

class BrandFilter extends React.PureComponent {
  onInputChange = (event) => {
    const el = event.target;
    this.props.onSelect(el.dataset.id);
  };

  render() {
    // const {options, selectedOptions} = this.props;
    const options = [
      {
        id:1,
        name: 'SAMSUNG',
      },{
        id:2,
        name: 'PANASONIC',
      },{
        id:3,
        name: 'BENKO',
      },{
        id:4,
        name: 'Midea',
      },{
        id:5,
        name: 'ELECTROLUX',
      },{
        id:6,
        name: 'AQUA',
      },{
        id:7,
        name: 'Daikin',
      },{
        id:8,
        name: 'Agree',
      },{
        id:9,
        name: 'ICTLaw',
      },{
        id:10,
        name: 'SmileTech',
      },
    ]

    return (
      <>
      <h4 className="filter-name">Thương hiệu</h4>
      <div className="product-filter-group feature-filter">
        <ul className="feature-list">
          {
            options.map((item, index) =>
              <li key={index} className="feature-list-item">
                <input type="checkbox" id={`feature-filter-${item.id}`}/>
                <label for={`feature-filter-${item.id}`}>{item.name}</label>
              </li>
            )
          }
        </ul>
      </div>
      </>
    )
  }
}

BrandFilter.propTypes = {
  options: PropTypes.array,
  selectedOptions: PropTypes.array,
  onSelect: PropTypes.func
};

BrandFilter.defaultProps = {
  options: [],
  selectedOptions: []
};

export default BrandFilter;