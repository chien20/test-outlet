import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import th from 'date-fns/esm/locale/th/index.js';

class FeatureFilter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }
  activeItem = (index) => {
    this.setState({ active: index })
  };

  render() {
    // const {featureName, options} = this.props;
    const featureName = 'Dung tích';
    const options = [
      {
        id: 1,
        name: 'Dưới 150 lít',
      },{
        id: 2,
        name: 'Từ 150 - 300 lít',
      },{
        id: 3,
        name: 'Từ 300 - 400 lít',
      },{
        id: 4,
        name: 'Từ 400 - 550 lít',
      },{
        id: 5,
        name: 'Trên 550 lít',
      },
    ];
    const keywords = [
      {
        id: 1,
        name: 'Ngăn đá trên'
      },{
        id: 2,
        name: 'Ngăn đá dưới'
      },{
        id: 3,
        name: 'Mini'
      },{
        id: 4,
        name: 'Tủ lớn, Side by side'
      },{
        id: 5,
        name: 'Tủ nhiều cửa'
      },
    ]
    
    return (
      <>
      <h4 className="filter-name">Kiểu tủ</h4>
      <div className="product-filter-group feature-filter feature-keyword">
        <ul className="feature-list">
          {
            keywords.map((item, index) =>
              <li className={this.state.active === index ? 'active' : ''}
                 onClick={() => this.activeItem(index)}
                 key={index}>
                 <span>{item.name}</span>
              </li>
            )
          }
        </ul>
      </div>
      <h4 className="filter-name">{featureName}</h4>
      <div className="product-filter-group feature-filter">
        <ul className="feature-list">
          {
            options.map((item, index) =>
              <li key={index} className="feature-list-item">
                <input type="checkbox" id={`feature-filter-${item.id}`}/>
                <label for={`feature-filter-${item.id}`}>{item.name}</label>
              </li>
            )
          }
        </ul>
      </div>
      </>
    )
  }
}

FeatureFilter.propTypes = {
  featureName: PropTypes.string,
  options: PropTypes.array,
  selectedOptions: PropTypes.array,
  onSelect: PropTypes.func
};

FeatureFilter.defaultProps = {
  options: [],
  selectedOptions: []
};

export default FeatureFilter;