import React from 'react';
import qs from 'qs';
import {withRouter} from 'react-router-dom';
import history from '../../../../common/utils/router/history';
import InputNumber from '../../../../components/Form/InputNumber/InputNumber';

class PriceFilter extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      min_price: '',
      max_price: '',
      error: null,
    };
  }

  componentDidMount() {
    this.initData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.location !== this.props.location) {
      this.initData();
    }
  }

  initData = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    const values = {
      min_price: '',
      max_price: '',
    };
    if (params.min_price) {
      values.min_price = params.min_price;
    }
    if (params.max_price) {
      values.max_price = params.max_price;
    }
    this.setState(values);
  };

  onMinChange = (value) => {
    this.setState({
      error: null,
      min_price: value,
    });
  };

  onMaxChange = (value) => {
    this.setState({
      error: null,
      max_price: value
    });
  };

  doFilter = (event) => {
    event.preventDefault();

    const {min_price, max_price} = this.state;
    const {location: {search, pathname}} = this.props;

    if (min_price && max_price && min_price * 1 > max_price) {
      this.setState({
        error: 'Giá trị không hợp lệ!',
      });
      return;
    }

    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    if (min_price) {
      params.min_price = min_price;
    } else {
      delete params.min_price;
    }
    if (max_price) {
      params.max_price = max_price;
    } else {
      delete  params.max_price;
    }
    const queryParams = qs.stringify(params, {addQueryPrefix: true});
    history.push({
      pathname,
      search: queryParams,
    });
  };

  render() {
    const {min_price, max_price, error} = this.state;
    return (
      <div className="price-filter product-filter-group">
        <h4 className="widget-title filter-name">Giá tiền (đ)</h4>
        <form className="form-group input-price" onSubmit={this.doFilter}>
          <div className="row">
            <div className="col-6">
              <InputNumber
                className="form-control"
                onChange={this.onMinChange}
                value={min_price}
              />
            </div>
            <div className="col-6">
              <InputNumber
                className="form-control"
                onChange={this.onMaxChange}
                value={max_price}/>
            </div>
          </div>
          {
            error &&
            <div className="text-red">{error}</div>
          }
          <div className="actions text-center">
            <button
              className="btn btn-choice"
              type="submit">OK
            </button>
          </div>
        </form>
      </div>
    )
  }
}

export default withRouter(PriceFilter);
