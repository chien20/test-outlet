import React from 'react';
import {Link} from 'react-router-dom';
import './CategoryList.scss';
import HyperLink from '../../../../components/HyperLink/HyperLink';

class CategoryList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      limit: 30
    };
  }

  showMore = () => {
    this.setState(prevState => ({
      limit: prevState.limit + 20
    }));
  };

  render() {
    const {categories} = this.props;
    const {limit} = this.state;
    let displayCategories = categories;
    let hasMore = false;
    if (categories.length > limit) {
      hasMore = true;
      displayCategories = [];
      for (let i = 0; i < limit; i++) {
        displayCategories.push(categories[i]);
      }
    }
    return (
      /*<div className="widget category-list">
        <h3 className="widget-title">Danh mục sản phẩm</h3>
        <ul>
          {
            displayCategories.map((item, index) =>
              <li key={index}>
                <Link to={item.url ? item.url : `/${item.slug}/c${item.id}`}>{item.name}</Link>
              </li>
            )
          }
          {
            hasMore && <li onClick={this.showMore} className="show-more text-center">
              <HyperLink>+ Xem thêm +</HyperLink>
            </li>
          }
        </ul>
      </div>*/
      <ul className="categories">
        {
          displayCategories.map((item, index) =>
            <li key={index}>
              <Link to={item.url ? item.url : `/${item.slug}/c${item.id}`}>{item.name}</Link>
            </li>
          )
        }
        {
          hasMore && <li onClick={this.showMore} className="show-more text-center">
            <HyperLink>+ Xem thêm +</HyperLink>
          </li>
        }
      </ul>
    )
  }
}

export default CategoryList;