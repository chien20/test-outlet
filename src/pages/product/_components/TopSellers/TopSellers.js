import React from 'react';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {getProductListAPI} from '../../../../api/products';

class TopSellers extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      productList: []
    };
  }

  componentDidMount() {
    this.getListProduct();
  }

  getListProduct() {
    const {categoryId} = this.props;
    const params = {
      current_page: 0,
      page_size: 3,
      type: 'best-sell',
      category_id: categoryId
    };
    getProductListAPI(params).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : []
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  }

  render() {
    const {productList} = this.state;
    if (!productList || !productList.length) {
      return null;
    }
    return (
      <div className="widget top-sellers">
        <h3 className="widget-title">Bán chạy nhất</h3>
        {
          productList.map((item, index) => (
            <ProductItem key={index} product={item}/>
          ))
        }
      </div>
    )
  }
}

export default TopSellers;