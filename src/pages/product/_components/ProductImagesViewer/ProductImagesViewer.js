import React from 'react';
import PropTypes from 'prop-types';
import './ProductImagesViewer.scss';
import $ from 'jquery';
import 'slick-carousel';
import {isMobile} from '../../../../common/helpers/browser';

class ProductImagesViewer extends React.PureComponent {
  state = {
    currentSlide: 0,
  };
  componentDidMount() {
    const el = $('#product-main-slider');
    const mainSliderConfig = {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: isMobile,
      infinite: false,
    };
    if (!isMobile) {
      mainSliderConfig.asNavFor = '#product-thumb-slider';
    }

    $('#product-main-slider').slick(mainSliderConfig);

    if (!isMobile) {
      $('#product-thumb-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '#product-main-slider',
        focusOnSelect: true,
        // centerMode: true,
        arrows: true,
        infinite: false,
        vertical: true,
        verticalSwiping: true

      });
    }
    el.on('afterChange', (event, slick, currentSlide) => {
      this.setState({
        currentSlide,
      });
    });
  }


  render() {
    const {images} = this.props;
    const {currentSlide} = this.state;
    if (images.length === 0) {
      return null;
    }
    return (
      <div className={isMobile ? 'product-images-viewer' : 'product-images-viewer-desktop'}>
        {
          !isMobile &&
          <div className="small-img">
            <div className="slider" id="product-thumb-slider">
              {
                images.map((item, index) => (
                  <div className="item" key={index} style={{backgroundImage: `url(${item.thumbnail_url})`}}/>
                ))
              }
            </div>
          </div>
        }
        <div className="main-image">
          <div className="show" id="product-main-slider">
            {
              images.map((item, index) => (
                <div className="item" key={index} style={{backgroundImage: `url(${item.thumbnail_url})`}}/>
              ))
            }
          </div>
          {
            isMobile && 
            <div className="item-current">
              <span>{currentSlide + 1}</span>
              <span>/</span>
              <span>{images.length}</span>
            </div>
          }
        </div>
      </div>
    );
  }
}

ProductImagesViewer.propTypes = {
  images: PropTypes.array,
};

ProductImagesViewer.defaultProps = {
  images: [],
};

export default ProductImagesViewer;
