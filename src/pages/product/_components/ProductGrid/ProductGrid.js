import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import Pagination from '../../../../components/Pagination/Pagination';
import {getProductListAPI} from '../../../../api/products';
import {isMobile} from '../../../../common/helpers/browser';
import InfiniteScroll from '../../../../components/InfiniteScroll/InfiniteScroll';
import './ProductGrid.scss';
import {publicUrl, showAlert} from '../../../../common/helpers';
import qs from 'qs';
import PriceFilter from '../ProductFilter/PriceFilter';
import ProductFilter from '../ProductFilter/ProductFilter';
import ProductListFilter from '../../ProductListFilter/ProductListFilter';
import _chunk from 'lodash/chunk';
import Adv from '../../../../components/Adv/Adv';
import category from '../../../../redux/reducers/common/category';

const filterKeys = ['min_price', 'max_price', 'sort_by', 'sort_order'];

class ProductGrid extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      productList: [],
      totalItems: 0,
      sortByName: 'Sắp xếp',
      sortParams: {
        by: '',
        order: ''
      },
      hasMore: false,
      nextPage: 0,
      showFilter: false,
    };
  }

  componentDidMount() {
    if (!isMobile) {
      this.getProductList();
    } else {
      this.getProductListMore(true);
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.match !== this.props.match
      || prevProps.location !== this.props.location
      || prevProps.queryParams !== this.props.queryParams
      || prevProps.requestConfig !== this.props.requestConfig
    ) {
      if (!isMobile) {
        this.getProductList();
      } else {
        this.getProductListMore(true);
      }
    }
  }

  getFilterParams = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    const filterParams = {};
    Object.keys(params).forEach(key => {
      if (filterKeys.includes(key)) {
        filterParams[key] = params[key];
      }
    });
    return filterParams;
  };

  getProductList() {
    const {match: {params: {page}}, queryParams, requestConfig = {}, pageSize} = this.props;
    const {sortParams} = this.state;
    const params = {
      current_page: page || 1,
      page_size: pageSize,
      ...queryParams,
      ...this.getFilterParams(),
    };
    params.current_page -= 1;
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    this.setState({
      isLoading: true,
    });
    getProductListAPI(params, requestConfig).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0,
        showFilter: false,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
      this.setState({
        isLoading: false
      });
    });
  }

  getProductListMore = (isSearch = false) => {
    const {queryParams, pageSize} = this.props;
    const {nextPage, isLoading, sortParams} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: isSearch ? 0 : nextPage,
      page_size: pageSize,
      ...queryParams,
      ...this.getFilterParams(),
    };
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    this.setState({
      isLoading: true
    });
    getProductListAPI(params).then(res => {
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info || {};
          return {
            isLoading: false,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1,
            totalItems: pageInfo.total_items || 0,
            showFilter: false,
          };
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  };

  onSort = (by, order, byName = 'Sắp xếp') => () => {
    this.setState({
      sortByName: byName,
      sortParams: {
        by,
        order
      }
    }, () => {
      this.getProductList(this.props);
    });
  };

  onLoadMore = () => {
    this.getProductListMore();
  };

  toggleFilter = () => {
    this.setState(prevState => ({
      showFilter: !prevState.showFilter,
    }));
  };

  handleShowFilter = () => {
    this.setState({
      showFilter: false
    });
  };

  render() {
    const {productList, totalItems, sortParams, sortByName, hasMore, isLoading, showFilter} = this.state;
    const {match: {params: {page}}, onPageChanged, toolbar, pageSize, itemsPerRow, category} = this.props;
    const totalPage = Math.ceil(totalItems / pageSize);
    const chunks = _chunk(productList, itemsPerRow);
    const itemWrapperClassName = itemsPerRow === 4 ? 'four-items-per-row' : '';
    return (
      <div className="product-grid">
        {
          !isMobile && !!toolbar &&
          <div className="item-sort">
            <ul className="sort-list">
              {/*<li className={`${sortParams.by === 'id' && sortParams.order === 'desc' ? 'active' : ''}`}>*/}
              {/*  <HyperLink onClick={this.onSort('id', 'desc')}>Hàng mới</HyperLink>*/}
              {/*</li>*/}
              {/*<li className={`${sortParams.by === 'sell' && sortParams.order === 'desc' ? 'active' : ''}`}>*/}
              {/*  <HyperLink onClick={this.onSort('sell', 'desc')}>Bán chạy</HyperLink>*/}
              {/*</li>*/}
              <li className="sort-by-price">
                <div>
                  <img className="sort-icon m-r-5" src={publicUrl(`/assets/images/icons/sort.svg`)} alt="icon"/>
                  <span>{sortByName}</span>
                </div>
                <ul className="sort-by-dropdown">
                  <li className={`${sortParams.by === 'price' && sortParams.order === 'asc' ? 'active' : ''}`}>
                    <HyperLink onClick={this.onSort('price', 'asc', 'Giá từ thấp đến cao')}>Giá từ thấp đến
                      cao</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'price' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('price', 'desc', 'Giá từ cao đến thấp')}>Giá từ cao đến
                      thấp</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'id' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('id', 'desc', 'Sản phẩm mới nhất')}>Sản phẩm mới
                      nhất</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'id' && sortParams.order === 'asc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('id', 'asc', 'Sản phẩm cũ nhất')}>Sản phẩm cũ
                      nhất</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'sell' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('sell', 'desc', 'Bán chạy nhất')}>Bán chạy
                      nhất</HyperLink>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        }
        {
          isMobile &&
          <Fragment>
            <div className="product-filter">
              <div className="sort-by-price">
                {/* <div className="text-right">
                  <span>{sortByName}</span>
                  <img className="sort-icon m-l-5" src={publicUrl(`/assets/images/icons/sort.svg`)} alt="icon"/>
                </div> */}
                <ul className="sort-by-dropdown">
                  <li className="filters" onClick={this.toggleFilter}>
                    {/* <img className="sort-icon" src={publicUrl(`/assets/images/icons/filter-loc.svg`)} alt="icon"/> */}
                    <HyperLink><i class="fas fa-filter"></i> Lọc</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'id' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('id', 'desc', 'Sản phẩm mới nhất')}>Mới nhất</HyperLink>
                  </li>
                  {/* <li className={`${sortParams.by === 'id' && sortParams.order === 'asc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('id', 'asc', 'Sản phẩm cũ nhất')}>Sản phẩm cũ
                      nhất</HyperLink>
                  </li> */}
                  <li className={`${sortParams.by === 'sell' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('sell', 'desc', 'Bán chạy nhất')}>Bán chạy</HyperLink>
                  </li>
                  <li className={`${sortParams.by === 'price' && sortParams.order === 'asc' ? 'active' : ''}`}>
                    <HyperLink onClick={this.onSort('price', 'asc', 'Giá từ thấp đến cao')}>Giá <i class="fas fa-sort"></i></HyperLink>
                  </li>
                  {/* <li className={`${sortParams.by === 'price' && sortParams.order === 'desc' ? 'active' : ''}`}>
                    <HyperLink className="" onClick={this.onSort('price', 'desc', 'Giá từ cao đến thấp')}>Giá cao</HyperLink>
                  </li> */}
                </ul>
              </div>
            </div>
            {
              showFilter &&
              <div className="filter-options">
                <div className="filter-header">
                  <img src={publicUrl('/assets/images/icons/icon-back.svg')} alt="Back" onClick={this.handleShowFilter}/>
                  <h3 className="title">{category.name}</h3>
                </div>
                <ProductFilter category={category}/>
              </div>
            }
          </Fragment>
        }
        {
          !productList.length && !isLoading &&
          <p className="no-items">Không có sản phẩm nào!</p>
        }
        {
          chunks.map((chunk, chunkIndex) => (
            <Fragment key={chunkIndex}>
              {
                !!chunkIndex && chunkIndex < chunks.length && chunkIndex % 3 === 0 &&
                <Adv position="inside_list_product"/>
              }
              <div className="row product-items">
                {
                  chunk.map((item, index) => (
                    <div className={`product-item-wrapper ${itemWrapperClassName}`} key={index}>
                      <ProductItem product={item}/>
                    </div>
                  ))
                }
              </div>
            </Fragment>
          ))
        }
        {
          hasMore && isMobile && <InfiniteScroll loadMore={this.onLoadMore}/>
        }
        <div className="item-pagination hidden-mobile">
          {
            totalPage > 1 &&
            <Pagination totalPages={totalPage} currentPage={page || 1} onPageChanged={onPageChanged}/>
          }
        </div>
      </div>
    );
  }
}

ProductGrid.propTypes = {
  queryParams: PropTypes.object.isRequired,
  onPageChanged: PropTypes.func.isRequired,
  toolbar: PropTypes.bool,
  pageSize: PropTypes.number,
  itemsPerRow: PropTypes.number,
};

ProductGrid.defaultProps = {
  toolbar: true,
  pageSize: 24,
  itemsPerRow: 4,
};

export default withRouter(ProductGrid);
