import React, {Fragment} from 'react';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import './ProductSocials.scss';
import {getProductLikesAPI, likeProductAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import {connect} from 'react-redux';

class ProductSocials extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      likes: null,
      isSubmitting: false
    }
  }

  componentDidMount() {
    this.getProductLikes();
  }

  getProductLikes = () => {
    const {product} = this.props;
    getProductLikesAPI(product.id).then(res => {
      this.setState({
        likes: res.data
      });
    }).catch(() => {
      this.setState({
        likes: {
          count: 0,
          liked: false
        }
      });
    });
  };

  likeProduct = (liked) => () => {
    const {product} = this.props;
    if (this.state.isSubmitting) {
      return;
    }
    this.setState({
      isSubmitting: true
    });
    likeProductAPI(product.id, liked).then(() => {
      this.getProductLikes();
      this.setState({
        isSubmitting: false
      });
    }).catch(error => {
      this.setState({
        isSubmitting: false
      });
      showAlert({
        type: 'error',
        message: error.message
      });
    })
  };

  render() {
    const {user, product} = this.props;
    const {likes} = this.state;
    const encodedUrl = encodeURI(window.location.href);
    const description = `${product.name} tại Outlet.vn`;
    return (
      <div className="product-socials">
        <div className="product-share">
          <label>Chia sẻ:</label>
          {/*<div className="share-item">*/}
          {/*<HyperLink><i className="fab fa-facebook-messenger"/></HyperLink>*/}
          {/*</div>*/}
          <div className="share-item">
            <a
              href={`https://www.facebook.com/sharer/sharer.php?u=${encodedUrl}`}
              target="_blank"
              rel="noopener noreferrer">
              <i className="fab fa-facebook-f"/>
            </a>
          </div>
          <div className="share-item">
            <a
              href={`https://twitter.com/intent/tweet?url=${encodedUrl}&text=${description}`}
              target="_blank"
              rel="noopener noreferrer">
              <i className="fab fa-twitter"/>
            </a>
          </div>
          <div className="share-item">
            <a
              href={`https://pinterest.com/pin/create/button/?url=${encodedUrl}&media=${product.avatar_url}&description=${description}`}
              target="_blank"
              rel="noopener noreferrer">
              <i className="fab fa-pinterest-p"/>
            </a>
          </div>
          <div className="share-item">
            <a
              href={`https://www.linkedin.com/shareArticle?mini=true&url=${encodedUrl}&summary=${description}`}
              target="_blank"
              rel="noopener noreferrer">
              <i className="fab fa-linkedin-in"/>
            </a>
          </div>
        </div>
        <div className="product-favorite">
          {
            likes &&
            <Fragment>
              {
                (user && user.auth && user.auth.isAuthenticated) ?
                  <HyperLink onClick={this.likeProduct(!likes.liked)}>
                    {
                      likes.liked ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>
                    }
                  </HyperLink>
                  :
                  <>
                    {
                      likes.liked ? <i className="fas fa-heart"/> : <i className="far fa-heart"/>
                    }
                  </>
              }
              Đã thích ( {likes.count} )
            </Fragment>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(ProductSocials);
