import React from 'react';

const ProductFeatures = ({features}) => (
  <div className="product-features product-section">
    <div className="section-title">
      <h2>Thông tin sản phẩm</h2>
    </div>
    <div className="section-content">
      <table className="product-features-table">
        <tbody>
        {
          features.map((item, index) => (
            <tr key={index}>
              <th>
                {item.name}
              </th>
              <td>
                {item.value}
              </td>
            </tr>
          ))
        }
        </tbody>
      </table>
    </div>
  </div>
);

export default React.memo(ProductFeatures);