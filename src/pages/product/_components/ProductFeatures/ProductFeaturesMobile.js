import React from 'react';
import "./ProductFeatures.scss"

const ProductFeatures = ({features}) => (
  <div className="product-features product-section">
    <div className="section-title">
      <h2>Thông tin sản phẩm</h2>
    </div> 
    <div className="entry-description">
    {
          features.map((item, index) => (
            <div className="item">
            <strong className="item-1">{item.name}</strong>
            <div className="item-2">{item.value}</div>
            </div>
          ))
        }
    </div>
  </div>
);

export default React.memo(ProductFeatures);
