import React from 'react';
import PropTypes from 'prop-types';
import Slider from '../../../../components/Slider/Slider';
import {imageUrl} from '../../../../common/helpers';
import {getObjectMediaAPI, getObjectsMediaAPI} from '../../../../api/media';
import {getBannersAPI} from '../../../../api/banners';
import {OBJECT_TYPES} from '../../../../common/constants/objectTypes';

const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: false,
  speed: 20000,
  autoplaySpeed: 20000,
  cssEase: "linear",
};

class CategorySlider extends React.PureComponent {
  state = {
    items: [],
  };

  componentDidMount() {
    this.getBanners().then(items => {
      this.setState({items});
    }).catch(() => {
      this.setState({
        items: [],
      });
    });
  }

  getBanners = async () => {
    const {objectType, objectId} = this.props;
    const {data: {list}} = await getBannersAPI();
    let banners = [];
    if (list.length) {
      const ids = list.map(item => item.id);
      const {data: {list: listImage}} = await getObjectsMediaAPI(ids, OBJECT_TYPES.Banner);
      const map = {};
      listImage.forEach(item => {
        map[item.object_id] = item;
      });
      list.forEach(item => {
        if (map[item.id]) {
          item.url = map[item.id].url;
        }
      });
    }
    if (objectType === OBJECT_TYPES.Category) {
      const commonBanners = (list || []).filter(item => item.position === 'product_categories');
      banners = [...commonBanners];
    }
    if (objectType === OBJECT_TYPES.Group) {
      const commonBanners = (list || []).filter(item => item.position === 'groups');
      banners = [...commonBanners];
    }
    if (objectType && objectId) {
      const {data: {list: list2}} = await getObjectMediaAPI(objectId, objectType, 'banner');
      banners = [...banners, ...list2];
    }
    banners = banners.map(item => ({
      ...item,
      imageUrl: imageUrl(item.url),
    }));
    return banners;
  };

  render() {
    const {items} = this.state;
    if (!items.length) {
      return null;
    }
    return <Slider className="category-slider" {...settings} items={items}/>;
  }
}

CategorySlider.propTypes = {
  objectType: PropTypes.any,
  objectId: PropTypes.any,
};

export default CategorySlider;
