import React from 'react';
import {Link} from 'react-router-dom';
import './CategoryMenu.scss';

class CategoryMenu extends React.PureComponent {
  render() {
    const {childrenList, category} = this.props;
    return (
      <div className="widget category-menu">
        <h3 className="widget-title">Danh mục sản phẩm</h3>
        <div className="category-name">
          {category.name}
        </div>
        <ul className="nav-left">
          {
            childrenList.map((item, index) =>
              <li key={index}>
                <Link to={`/${item.slug}/c${item.id}`}>{item.name}</Link>
              </li>
            )
          }
        </ul>
      </div>
    )
  }
}

export default CategoryMenu;