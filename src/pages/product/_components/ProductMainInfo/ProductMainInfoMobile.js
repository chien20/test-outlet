import React, {Fragment, useState, useEffect} from 'react';
import FlashSaleSection from './FlashSaleSection';
import StarRating from '../../../../components/StarRating/StarRating';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import ProductFavorite from '../ProductFavorite/ProductFavorite';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import GroupPoints from './GroupPoints';
import InputSpin from '../../../../components/Form/InputSpin/InputSpin';
import QRCode from '../../../../components/QRCode/QRCode';
import {Link} from "react-router-dom";
import {publicUrl} from "../../../../common/helpers";
import {getAddressesAPI} from '../../../../api/users';

const ProductMainInfoMobile = ({product, discount, ratings, originalPrice, price, qty, maxQty, selectedOptionId, groupPointsEl, selectedOption, addToCart, handleQtyChange, handleSelectOption, user, store}) => {
  const [showCartPanel, setShowCartPanel] = useState(false);
  const [showSelectOption, setShowSelectOption] = useState(false);
  const [showQRCode, setShowQRCode] = useState(false);
  const [addressDefaultDetail, setAddressDefaultDetail] = useState('');
  const [addressDefaultGeocoding, setAddressDefaultGeocoding] = useState('');
  const handleShowCartPanel = () => {
    setShowCartPanel(true);
  }; 
  const handleHideCartPanel = () => {
    setShowCartPanel(false);
  };
  const handleShowSelectOption = () => {
    setShowSelectOption(true);
  };
  const handleHideSelectOption = () => {
    setShowSelectOption(false);
  };

  const getAddress = async () => {
    const dataAddress = await getAddressesAPI();
    if(dataAddress){
      setAddressDefaultDetail(dataAddress.data.list[0].address);
      setAddressDefaultGeocoding(dataAddress.data.list[0].geocoding.address);
    }
  }
  getAddress().catch(() => {
    setAddressDefaultDetail('');
    setAddressDefaultGeocoding('');
  });

  return (
    <Fragment>
      <div className="product-overview">
        <div className="entry-price">
          {
            !product.prices.isFlashSale &&
            <>
            <div className="product-name flash-sale-off m-t-12">
            <h1 className="mt-4 mt-lg-0 mgt-5">{product.name}</h1>
            {
              discount > 0 &&
              <div className="sale"><span>-{discount}%</span></div>
              }
            </div>
            <div className="entry-price-left">
              <div className="item-price d-flex justify-content-between flash-sale-off">
                <div className="price-left">
                  <div className="new-price mgt-5">{numberAsCurrentcy(price || 0)} đ</div>
                  {
                    originalPrice !== price && !!originalPrice &&
                    <div className="old-price-wrapper">
                      <div className="old-price">{numberAsCurrentcy(originalPrice || 0)} đ</div>
                      <div className="saving-price">Tiết kiệm: {numberAsCurrentcy(originalPrice - price || 0)} đ
                      </div>
                    </div>
                  }
                </div>
              </div>
            </div>
            </>
          }
          {
            product.prices.isFlashSale &&
            <>
            <FlashSaleSection eventId={product.event.event_id}/>
            <div className="entry-price-left">
            <div className="item-price d-flex justify-content-between flash-sale-off">
              <div className="price-left">
                <div className="new-price mgt-5">{numberAsCurrentcy(price || 0)} đ</div>
                {
                  originalPrice !== price && !!originalPrice &&
                  <div className="old-price-wrapper">
                    <div className="old-price">{numberAsCurrentcy(originalPrice || 0)} đ</div>
                    <div className="saving-price">Tiết kiệm: {numberAsCurrentcy(originalPrice - price || 0)} đ
                    </div>
                  </div>
                }
              </div>
              {
              discount > 0 &&
              <div className="sale"><span>-{discount}%</span></div>
              }
              </div>
            </div>
            <div className="product-name flash-sale-off">
              <h1 className="mt-4 mt-lg-0 mgt-5">{product.name}</h1>
            </div>
            </>
          }
          
          <div className="entry-price-rate">
            <div className="review-star">
              <div className="star-color float-left">
                <StarRating value={ratings ? ratings.avg : 0} isDisabled={true}/>
                {
                  ratings &&
                  <HyperLink className="review-text view-rating">
                    {ratings.count > 0 ? `(${ratings.count})` : '(0)'}
                  </HyperLink>
                }
              </div> 
            </div>
            <span className="color-gray"> | </span>
            <span className="sold">Đã bán: 12</span>
            <ProductFavorite product={product}/>
          </div>
        </div>
        <GroupPoints
          ref={groupPointsEl}
          productId={product.id}
          selectedOptionId={selectedOptionId}/>
        <div className="entry-notice">
          {
            maxQty > 0 &&
            <div className="status">
              <img src={publicUrl(`/assets/images/icons/Cart.svg`)} alt="cart-icon"/>
              <span>Chỉ còn {maxQty} sản phẩm. Chọn mua ngay!</span>
            </div>
          }
          {
            maxQty === 0 &&
            <div className="status status-error">
              <span>Hết hàng!</span>
            </div>
          }
        </div>
        <div className="entry-description">
          <div className="item">
            <strong className="item-1">SKU</strong>
            <div className="item-2">{selectedOption ? selectedOption.sku : product.sku}</div>
            <div onClick={() => setShowQRCode(true)}>QR Code <i className="fas fa-chevron-right"/></div>
          </div>
          {
            selectedOption &&
            <div className="item select-option">
              <strong className="item-1">Loại hàng</strong>
              <div className="item-2">{selectedOption ? selectedOption.name : product.name}</div>
              <div onClick={handleShowSelectOption}>Thay đổi <i className="fas fa-chevron-right"/></div>
            </div>
          }
          <div className="item shipping">
          <strong className="item-1">Vận chuyển</strong>
          <div className="item-2">
            {
              addressDefaultDetail !=='' ?
              <span className="address">{addressDefaultDetail}, {addressDefaultGeocoding}</span>
              : <Link to={'/user/addresses'}>
                <span className="address">Thêm địa chỉ</span>
              </Link>
            }
            <span className="shipping-fee">Phí vận chuyển: <span>0 đ</span></span>
          </div>
          <div className="item-3">
            <i className="fas fa-chevron-right"/>
          </div>
            <img className="free-ship" src={publicUrl(`/assets/images/icons/icon-freeship.svg`)} alt="freeship"/>
          </div>
          <div className="item">
            <strong className="item-1-desc">Mô tả</strong>
            <div className="item-2" dangerouslySetInnerHTML={{__html: product.short_description}}/>
          </div>
        </div>
      </div>
      <div className="product-cart-actions cart-button-bottom">
        <button className="btn btn-message" >
          {/* <Link to={`/user/chat/?new=true&user_id=${store.user_id}`} className="btn-chat-mobile"> */}
            <img src={publicUrl(`/assets/images/icons/icon-chat-shop.svg`)} alt="chat-icon"/>
          {/* </Link> */}
          <span>Gửi tin nhắn</span>
        </button>
        <button className="btn btn-cart" onClick={handleShowCartPanel}>
          <img src={publicUrl(`/assets/images/icons/Cart.svg`)} alt="cart-icon"/>
          <span>Thêm vào giỏ</span>
        </button>
        <button className="btn btn-buy" onClick={handleShowCartPanel}>
          <span>Mua ngay</span>
        </button>
      </div>
      {
        showQRCode &&
        <Fragment>
          <div className="cart-panel-backdrop" onClick={() => setShowQRCode(false)}/>
          <div className="cart-panel">
            <div className="entry-description">
              <div className="item">
                <strong>SKU</strong>
                <div className="item-2">
                  {selectedOption ? selectedOption.sku : product.sku}
                </div>
              </div>
              <div className="item">
                <strong>QR Code</strong>
                <div className="item-2">
                  <QRCode
                    width={148}
                    height={148}
                    text={`${window.location.protocol}//${window.location.host}/product/p${product.id}`}
                  />
                </div>
              </div>
            </div>
            <div className="product-cart-actions">
              <button className="btn btn-danger-mobile btn-xl" onClick={() => setShowQRCode(false)}>
                Done
              </button>
            </div>
          </div>
        </Fragment>
      }
      {
        showSelectOption &&
        <Fragment>
          <div className="cart-panel-backdrop" onClick={handleHideSelectOption}/>
          <div className="cart-panel">
            <div className="add-to-cart-options">
              {
                !!product.option_groups &&
                <div className="product-options">
                  {
                    product.option_groups.map((item, index) => (
                      <div className="option-group" key={index}>
                        <label>Chọn {item.name}:</label>
                        <ul>
                          {
                            item.values.map((v, i) => (
                              <li
                                key={i}
                                className={selectedOption && selectedOption.groups_index[index] === i ? 'active' : ''}
                                onClick={handleSelectOption(index, i)}>{v}</li>
                            ))
                          }
                        </ul>
                      </div>
                    ))
                  }
                </div>
              }
            </div>
            <div className="product-cart-actions">
              <button className="btn btn-danger-mobile btn-xl" onClick={handleHideSelectOption}>
                OK
              </button>
            </div>
          </div>
        </Fragment>
      }
      {
        showCartPanel &&
        <Fragment>
          <div className="cart-panel-backdrop" onClick={handleHideCartPanel}/>
          <div className="cart-panel">
            <div className="add-to-cart-options">
              {
                !!product.option_groups &&
                <div className="product-options">
                  {
                    product.option_groups.map((item, index) => (
                      <div className="option-group" key={index}>
                        <label>Chọn {item.name}:</label>
                        <ul>
                          {
                            item.values.map((v, i) => (
                              <li
                                key={i}
                                className={selectedOption && selectedOption.groups_index[index] === i ? 'active' : ''}
                                onClick={handleSelectOption(index, i)}>{v}</li>
                            ))
                          }
                        </ul>
                      </div>
                    ))
                  }
                </div>
              }
              <div className="add-to-cart">
                <div className="select-qty">
                  <label>Số lượng:</label>
                  <InputSpin value={qty} onChange={handleQtyChange} max={maxQty} isDisabled={!qty}/>
                  {
                    qty > 100 && <p className="text-red" style={{marginTop: 10}}>Bạn được mua tối đa 100 sản phẩm</p>
                  }
                </div>
              </div>
            </div>
            <div className="product-cart-actions">
              <button className="btn btn-danger-mobile btn-xl" onClick={addToCart(false)}>
                Thêm vào giỏ hàng
              </button>
            </div>
          </div>
        </Fragment>
      }
    </Fragment>
  )
};

export default ProductMainInfoMobile;
