import React from 'react';
import {getProductPointByGroupAPI} from '../../../../api/products';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {isMobile} from "../../../../common/helpers/browser";

const GroupItem = ({item, selectedOptionId = null, showAll = false, itemsLeft = 0, toggleShowAll}) => {
  let groupAvatar = item.group.avatar.thumbnail_url ? imageUrl(item.group.avatar.thumbnail_url) : publicUrl('/assets/images/no-image/256x256.png');
  let point = 0;
  if (item.point_options && item.point_options.length) {
    const index = item.option_ids.findIndex(i => i === selectedOptionId);
    if (index >= 0 && item.point_options[index]) {
      point = item.point_options[index];
    }
  } else if (item.point) {
    point = item.point;
  }
  if(isMobile) {
    return (
      <div className="group">
        <div className="group-info d-flex align-items-center">
          <div className="group-avatar">
            <img src={groupAvatar} alt={item.group.name}/>
          </div>
          <div className="text">
            <div>Hội viên nhóm</div>
            <div className="group-names">
              <div className={`group-name ${!showAll && itemsLeft > 0 ? 'overflow' : ''}`}>{item.group.name}</div>
              {
                !showAll && itemsLeft > 0 &&
                <HyperLink className="other-names" onClick={toggleShowAll}>
                  và {itemsLeft} hội nhóm khác
                </HyperLink>
              }
            </div>
          </div>
        </div>
        <div className="preferred">
          <span className="preferred-point">+{point.toLocaleString()} điểm</span>
        </div>
      </div>
    )
  }
  return (
    <div className="group">
      <div className="group-info d-flex align-items-center">
        <div className="group-avatar">
          <img src={groupAvatar} alt={item.group.name}/>
        </div>
        <div className="text">
          <div>Hội viên nhóm</div>
          <div className="group-names">
            <div className={`group-name ${!showAll && itemsLeft > 0 ? 'overflow' : ''}`}>{item.group.name}</div>
            {
              !showAll && itemsLeft > 0 &&
              <HyperLink className="other-names" onClick={toggleShowAll}>
                và {itemsLeft} hội nhóm khác
              </HyperLink>
            }
          </div>
        </div>
      </div>
      <div className="preferred">
        <span className="preferred-point">+{point.toLocaleString()} điểm</span>
      </div>
    </div>
  )
};

class GroupPoints extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      groups: [],
      showAll: false
    };
  }

  componentDidMount() {
    this.getPoints();
  }

  getPoints = () => {
    const {productId} = this.props;
    getProductPointByGroupAPI(productId).then(res => {
      this.setState({
        groups: res.data
      });
    }).catch(error => {

    });
  };

  toggleShowAll = () => {
    this.setState({
      showAll: true
    });
  };

  getGroups = () => {
    return this.state.groups;
  };

  render() {
    const {selectedOptionId} = this.props;
    const {groups, showAll} = this.state;
    if (!groups || !groups.length) {
      return null;
    }
    const displayGroups = showAll ? groups : [groups[0]];
    const itemsLeft = groups.length - displayGroups.length;
    return (
      <div className="entry-info-buyer">
        {
          displayGroups.map((item, index) => (
            <GroupItem
              item={item}
              selectedOptionId={selectedOptionId}
              key={index}
              itemsLeft={itemsLeft}
              showAll={showAll}
              toggleShowAll={this.toggleShowAll}/>
          ))
        }
      </div>
    );
  }
}

export default GroupPoints;
