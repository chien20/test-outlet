import React, {Fragment} from 'react';
import FlashSaleSection from './FlashSaleSection';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import StarRating from '../../../../components/StarRating/StarRating';
import GroupPoints from './GroupPoints';
import InputSpin from '../../../../components/Form/InputSpin/InputSpin';
import QRCode from '../../../../components/QRCode/QRCode';
import {publicUrl} from "../../../../common/helpers";

const ProductMainInfoPC = ({product, originalPrice, price, discount, ratings, selectedOption, selectedOptionId, qty, maxQty, groupPointsEl, handleSelectOption, handleQtyChange, addToCart}) => {
  return (
    <Fragment>
      <div className="product-name">
        <h1 className="mt-4 mt-lg-0">{product.prices.isFlashSale &&
        <i className="fas fa-fire-alt flash-sale-icon"/>}{product.name}</h1>
        {
          product.prices.isFlashSale &&
          <FlashSaleSection eventId={product.event.event_id}/>
        }
        <div className="brand-sku">
          <p>Nhãn hiệu: <HyperLink to="/">No brand</HyperLink></p>
          <p>SKU: <span>{product.sku}</span></p>
        </div>
      </div>

      <div className="product-overview">
        <div className="entry-price">
          <div>
            <div className="star-color">
              <StarRating value={ratings ? ratings.avg : 0} isDisabled={true}/>
              {
                ratings &&
                <HyperLink className="review-text view-rating">
                  {ratings.count > 0 ? `(${ratings.count})` : '(0)'}
                </HyperLink>
              }
            </div>
            {
              !product.price.isFlashSale &&
              <div className="product-price">
                <div className="price-left">
                  <div className="new-price">{numberAsCurrentcy(price || 0)} đ</div>
                  {
                    originalPrice !== price && !!originalPrice &&
                    <div className="old">
                      <span className="old-price">{numberAsCurrentcy(originalPrice || 0)} đ</span>
                      <span className="saving-price">
                        Tiết kiệm: {numberAsCurrentcy(originalPrice - price || 0)} đ
                      </span>
                      {
                        discount > 0 &&
                        <span className="sale">-{discount}%</span>
                      }
                    </div>
                  }
                </div>
              </div>
            }
          </div>
          <div className="qr-code">
            <QRCode
              text={`${window.location.protocol}//${window.location.host}/product/p${product.id}`}
            />
          </div>
        </div>
        <GroupPoints
            ref={groupPointsEl}
            productId={product.id}
            selectedOptionId={selectedOptionId}/>
        <div className="entry-notice">
          {
            maxQty > 0 &&
            <div className="status">
              <img src={publicUrl(`/assets/images/icons/Cart.svg`)} alt="cart-icon"/>
              <span>Chỉ còn {maxQty} sản phẩm. Chọn mua ngay!</span>
            </div>
          }
          {
            maxQty === 0 &&
            <div className="status status-error">
              <span>Hết hàng!</span>
            </div>
          }
        </div>
        <div className="entry-description">
          <div className="d-flex border-bottom">
            <label>Mô tả</label> <p dangerouslySetInnerHTML={{__html: product.short_description}}/>
          </div>
          {
            !!product.option_groups &&
            <div className="product-options">
              {
                product.option_groups.map((item, index) => (
                    <div className="option-group d-flex" key={index}>
                      <label>{item.name}</label>
                      <ul>
                        {
                          item.values.map((v, i) => (
                              <li
                                  key={i}
                                  className={selectedOption && selectedOption.groups_index[index] === i ? 'active' : ''}
                                  onClick={handleSelectOption(index, i)}>{v}</li>
                          ))
                        }
                      </ul>
                    </div>
                ))
              }
            </div>
          }
          <div className="d-flex qty">
            <label>Số lượng</label>
            <InputSpin value={qty} onChange={handleQtyChange} max={maxQty} isDisabled={!qty}/>
            {
              qty <= 100 && <span>{maxQty} sản phẩm có sẵn</span>
            }
            {
              qty > 100 && <p className="text-red" style={{marginTop: 10}}>Bạn được mua tối đa 100 sản phẩm</p>
            }
          </div>
        </div>
      </div>
      <div className="product-cart-actions">
        <button className="btn btn-add-cart bg-primary" disabled={!qty || qty > 100} onClick={addToCart(false)}>
          <span>Thêm vào giỏ hàng</span>
          <img src={publicUrl(`/assets/images/icons/cart-white.svg`)} alt="cart-icon"/>
        </button>
        <button className="btn btn-checkout bg-red" disabled={!qty || qty > 100} onClick={addToCart(true)}>
          <span>Mua ngay</span>
          {/* <img src={publicUrl(`/assets/images/icons/arrow.svg`)} alt='arrow-icon'/> */}
        </button>
      </div>
    </Fragment>
  )
};

export default ProductMainInfoPC;
