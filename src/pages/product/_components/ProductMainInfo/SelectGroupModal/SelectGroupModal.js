import React from 'react';
import {Modal} from 'react-bootstrap';
import {imageUrl, publicUrl} from '../../../../../common/helpers';
import './SelectGroupModal.scss';

const GroupItem = ({item, selectedOptionId = null, isSelected = false, handleSelect}) => {
  let groupAvatar = item.group.avatar.thumbnail_url ? imageUrl(item.group.avatar.thumbnail_url) : publicUrl('/assets/images/no-image/256x256.png');
  let point = 0;
  if (item.point_options && item.point_options.length) {
    const index = item.option_ids.findIndex(i => i === selectedOptionId);
    if (index >= 0 && item.point_options[index]) {
      point = item.point_options[index];
    }
  } else if (item.point) {
    point = item.point;
  }
  return (
    <div
      className={`group-item ${isSelected ? 'selected' : ''}`}
      onClick={handleSelect(item.group.id)}>
      <div className="group-item-wrapper">
        <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}>
          {isSelected && <i className="fas fa-check-circle"/>}
        </div>
        <div className="group-name">{item.group.name}</div>
        <div className="preferred-2">
          <div className="preferred-point">+{point.toLocaleString()} điểm</div>
        </div>
      </div>
    </div>
  )
};

class SelectGroupModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      groups: [],
      selectedProductOptionId: null,
      selectedGroupId: null,
      resolver: null
    };
    this.defaultState = {...this.state};
  }

  handleOpen = (groups, selectedProductOptionId, resolver) => {
    this.setState({
      isOpen: true,
      groups,
      selectedProductOptionId,
      resolver
    });
  };

  handleClose = () => {
    this.setState({
      ...this.defaultState
    });
  };

  handSelect = (groupId) => () => {
    this.setState({
      selectedGroupId: groupId
    });
  };

  handleSubmit = () => {
    const {resolver, selectedGroupId} = this.state;
    if (resolver) {
      resolver(selectedGroupId);
    }
    this.handleClose();
  };

  render() {
    const {isOpen, groups, selectedGroupId, selectedProductOptionId} = this.state;
    return (
      <Modal size="md"
             backdrop="static"
             show={isOpen}
             onHide={this.handleClose}
             className="select-groups-modal"
             centered>
        <Modal.Header closeButton>
          <Modal.Title>Chọn hội nhóm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Sản phẩm mà bạn mua được bày bán trong một số hội nhóm mà bạn đã tham gia. Hãy click chọn 1 hội nhóm để nhận được
            điểm thưởng khi mua sản phẩm này.</p>
          <div className="list-group">
            {
              groups.map((item, index) => (
                <GroupItem
                  isSelected={item.group.id === selectedGroupId}
                  key={index}
                  item={item}
                  selectedOptionId={selectedProductOptionId}
                  handleSelect={this.handSelect}/>
              ))
            }
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-default"
            onClick={this.handleClose}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}>Xong
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default SelectGroupModal;
