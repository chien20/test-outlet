import React from 'react';
import {connect} from 'react-redux';
import {addToCartAC} from '../../../order/_redux/actions';
import Broadcaster from '../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../common/constants/broadcastEvents';
import {getProductRatingStatsAPI} from '../../../../api/products';
import SelectGroupModal from './SelectGroupModal/SelectGroupModal';
import {isMobile} from '../../../../common/helpers/browser';
import './ProductMainInfo.scss';
import ProductMainInfoPC from './ProductMainInfoPC';
import ProductMainInfoMobile from './ProductMainInfoMobile';

class ProductMainInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      qty: 0,
      selectedOptionId: null,
      ratings: null
    };
    this.groupPointsEl = React.createRef();
    this.selectGroupModalEl = React.createRef();
  }

  componentDidMount() {
    const {product} = this.props;
    const state = {
      selectedOptionId: null,
      qty: 0
    };
    if (product.qty) {
      state.qty = 1;
    }
    if (product.options && product.options.length) {
      for (let i = 0; i < product.options.length; i++) {
        if (product.options[i].qty > 0) {
          state.qty = 1;
          state.selectedOptionId = product.options[i].id;
          break;
        }
      }
    }
    this.setState(state);
    this.getRatingStats();
    Broadcaster.on(BROADCAST_EVENT.CLICK_ADD_TO_CART, this.onAddToCart);
  }

  componentWillUnmount() {
    Broadcaster.off(BROADCAST_EVENT.CLICK_ADD_TO_CART, this.onAddToCart);
  }

  getRatingStats = () => {
    const {product} = this.props;
    getProductRatingStatsAPI(product.id).then(res => {
      this.setState({
        ratings: res.data
      });
    }).catch(() => {
      this.setState({
        ratings: {
          count: 0,
          avg: 0
        }
      });
    });
  };

  onAddToCart = (data) => {
    this.handleAddToCart(data && data.buy_now);
  };

  handleQtyChange = (value) => {
    this.setState({qty: value});
  };

  addToCart = (buy_now = false) => () => {
    const {selectedOptionId} = this.state;
    const groups = this.groupPointsEl.current.getGroups();
    if (groups.length > 1) {
      const resolver = (groupId = null) => {
        this.handleAddToCart(buy_now, groupId);
      };
      this.selectGroupModalEl.current.handleOpen(groups, selectedOptionId, resolver);
    } else {
      this.handleAddToCart(buy_now, groups.length === 1 ? groups[0].group.id : null);
    }
  };

  handleAddToCart = (buy_now = false, groupId = null) => {
    const {product} = this.props;
    const {selectedOptionId, qty} = this.state;
    const data = {
      product_id: product.id,
      qty,
      buy_now,
      group_id: groupId
    };
    if (selectedOptionId) {
      data.product_option_id = selectedOptionId;
    }
    this.props.dispatch(addToCartAC(data));
  };

  handleSelectOption = (groupIndex, optionIndex) => () => {
    const {product} = this.props;
    const {selectedOptionId} = this.state;
    const selectedOption = product.options.find(item => item.id === selectedOptionId);
    if (!selectedOption || !selectedOption.groups_index) {
      return;
    }
    const groupsIndex = [...selectedOption.groups_index];
    groupsIndex[groupIndex] = optionIndex;
    if (product.options && product.options.length) {
      for (let i = 0; i < product.options.length; i++) {
        if (JSON.stringify(product.options[i].groups_index) === JSON.stringify(groupsIndex)) {
          this.setState({
            selectedOptionId: product.options[i].id
          });
          break;
        }
      }
    }
  };

  render() {
    const {user} = this.props;
    const {product} = this.props;
    const {qty, selectedOptionId, ratings} = this.state;
    let price = product.price;
    let originalPrice = product.original_price;
    let selectedOption = null;
    if (selectedOptionId) {
      selectedOption = product.options.find(item => item.id === selectedOptionId);
    }
    if (selectedOption) {
      price = selectedOption.price;
      originalPrice = selectedOption.original_price;
    }
    let discount_price = originalPrice - price;
    let discount = 0;
    if (discount_price > 0) {
      discount = Math.floor(discount_price * 100 / originalPrice);
    }
    const maxQty = selectedOption ? selectedOption.qty : product.qty;
    return (
      <div className="product-main-info">
        {
          !isMobile &&
          <ProductMainInfoPC
            discount={discount}
            maxQty={maxQty}
            originalPrice={originalPrice}
            price={price}
            product={product}
            user={user}
            qty={qty}
            ratings={ratings}
            selectedOption={selectedOption}
            selectedOptionId={selectedOptionId}
            groupPointsEl={this.groupPointsEl}
            addToCart={this.addToCart}
            handleQtyChange={this.handleQtyChange}
            handleSelectOption={this.handleSelectOption}
          />
        }
        {
          isMobile &&
          <ProductMainInfoMobile
            discount={discount}
            maxQty={maxQty}
            originalPrice={originalPrice}
            price={price}
            product={product}
            qty={qty}
            ratings={ratings}
            selectedOption={selectedOption}
            selectedOptionId={selectedOptionId}
            groupPointsEl={this.groupPointsEl}
            addToCart={this.addToCart}
            handleQtyChange={this.handleQtyChange}
            handleSelectOption={this.handleSelectOption}
          />
        }
        <SelectGroupModal ref={this.selectGroupModalEl}/>
      </div>
    )
  }
}

export default connect()(ProductMainInfo);
