import React from 'react';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {OverlayTrigger, Popover} from 'react-bootstrap';

const VoucherCondition = ({discountType, condition}) => {
  const discountUnit = discountType === 0 ? 'đ' : '%';
  const discount = discountType === 0 ? condition.discount.toLocaleString() : condition.discount;
  return (
    <li>
      Giảm <strong>{discount}{discountUnit}</strong> cho đơn hàng
      từ <strong>{condition.min.toLocaleString()}đ</strong>
    </li>
  )
};

const ShowMore = ({voucher}) => (
  <OverlayTrigger
    trigger="click"
    placement="top"
    item={voucher}
    overlay={
      <Popover
        className="voucher-popover">
        <Popover.Title as="h3">{voucher.name}</Popover.Title>
        <Popover.Content>
          {voucher.description}
          <h3>Mức giảm:</h3>
          <ul>
            {
              voucher.conditions.map((item, index) => (
                <VoucherCondition discountType={voucher.discount_type} condition={item} key={index}/>
              ))
            }
          </ul>
        </Popover.Content>
      </Popover>
    }>
    <HyperLink>(Xem chi tiết)</HyperLink>
  </OverlayTrigger>
);

class VoucherItem extends React.PureComponent {
  render() {
    const {voucher} = this.props;
    const discountUnit = voucher.discount_type === 0 ? 'đ' : '%';
    let discount = 0;
    let minValue = 0;
    voucher.conditions.forEach(item => {
      if (item.discount > discount) {
        discount = item.discount;
        minValue = item.min;
      }
    });
    if (voucher.discount_type === 0) {
      discount = discount.toLocaleString();
    }
    return (
      <li>
        <h4>Nhập mã <span className="text-red">{voucher.voucher}</span></h4>
        <p>Giảm ngay <strong>{discount}{discountUnit}</strong> cho đơn hàng
          từ <strong>{minValue.toLocaleString()}đ</strong>. <ShowMore voucher={voucher}/>
        </p>
      </li>
    );
  }
}

export default VoucherItem;
