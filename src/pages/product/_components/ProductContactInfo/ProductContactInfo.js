import React from 'react';
import {showAlert} from '../../../../common/helpers';
import {getCountActiveProductAPI, getStoreAvgRatingAPI, getStoreDetailAPI} from '../../../../api/stores';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {Link} from 'react-router-dom';
import './ProductContactInfo.scss';
import {getProductVouchersAPI} from '../../../../api/vouchers';
import {isMobile} from '../../../../common/helpers/browser';
import ProductInfoMobile from './ProductContactInfoMobile';
import {getBasicUserInfoAPI} from '../../../../api/users';
import Avatar from "../../../../components/Avatar/Avatar";
import ViewMapModal from '../../../../components/modals/ViewMapModal/ViewMapModal';

class ProductContactInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      store: null,
      vouchers: [],
    };
    this.viewMapRef = React.createRef();
  }

  componentDidMount() {
    this.getStore();
    this.getVouchers();
  }

  getStoreAsync = async () => {
    const {product: {store_id}} = this.props;
    const {data: store} = await getStoreDetailAPI(store_id);
    if (store) {
      const {data: owner} = await getBasicUserInfoAPI(store.user_id);
      const {data: product_count} = await getCountActiveProductAPI(store.id);
      const {data: rating_avg} = await getStoreAvgRatingAPI(store.id);
      store.owner = owner;
      store.product_count = product_count;
      store.rating = parseFloat(rating_avg).toFixed(1);
      return store;
    }
  };

  getStore = () => {
    this.getStoreAsync().then(store => {
      this.setState({
        isLoading: false,
        store: store,
      });
    }).catch((e) => {
      console.error(e);
      this.setState({
        isLoading: false,
      });
      showAlert({
        type: 'error',
        message: 'Không tải được thông tin cửa hàng',
      })
    });
  };

  getVouchers = () => {
    const {product: {id}} = this.props;
    const params = {
      current_page: 0,
      page_size: 3,
    };
    getProductVouchersAPI(id, params).then(res => {
      this.setState({
        vouchers: res.data.list,
      });
    }).catch(error => {

    });
  };

  handleOpenMap = () => {
    this.viewMapRef.current.handleOpen();
  };

  render() {
    const {store} = this.state;
    let joinTime = '';
    if (store && store.created_at) {
      const created = Date.parse(store.created_at);
      const now = new Date().getTime();
      const diff = Math.floor((now - created) / 1000);
      const months = Math.floor(diff / (60 * 60 * 24 * 30));
      if (months > 0) {
        joinTime = `${months} tháng`;
      } else {
        const day = Math.floor(diff / (60 * 60 * 24));
        if (day > 0) {
          joinTime = `${day} ngày`;
        } else {
          joinTime = '';
        }
      }
    }
    if (isMobile) {
      return <ProductInfoMobile store={store} joinTime={joinTime}/>
    }
    return (
      <div className="product-contact-info">
        {
          store &&
          <div className="store-info">
            <div className="avatar-store">
              <Link to={`/shop/${store.id}`}>
                <Avatar src={store.owner.avatar} size={80}/>
              </Link>
            </div>
            <div className="info-right">
              <h3 className="store-name"><Link to={`/shop/${store.id}`}>{store.name}</Link></h3>
              <div className="store-table">
                <div className="store-col">
                  <div className="store-row">
                    <span>Vận chuyển từ: {store.address}</span>
                  </div>
                  <div className="store-row">
                    <HyperLink onClick={this.handleOpenMap}>
                      <i className="fas fa-map-marker-alt red-color"/> Xem bản đồ
                    </HyperLink>
                  </div>
                </div>
                <div className="store-col">
                  <div className="store-row">
                    <span>Sản phẩm: <strong>{store.product_count}</strong></span>
                  </div>
                  <div className="store-row">
                    <span>Tham gia: <strong>{joinTime}</strong></span>
                  </div>
                </div>
                <div className="store-col">
                  <div className="store-row">
                    <span>Đánh giá: <strong>{store.rating}</strong></span>
                  </div>
                  <div className="store-row">
                    <span>Người theo dõi: <strong>120K</strong></span>
                  </div>
                </div>
                <div className="store-col">
                  <div className="store-row">
                    <span>Hội nhóm: <strong>3</strong></span>
                  </div>
                  <div className="store-row">
                    <HyperLink className="red-color">Xem danh sách</HyperLink>
                  </div>
                </div>
                <div className="store-col">
                  <div className="shop-chat-icon">
                    <Link to={`/user/chat/?new=true&user_id=${store.user_id}`} className="btn-chat-mobile"><i
                      className="fa fa-comments"/></Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }

        {
          store &&
          <ViewMapModal
            ref={this.viewMapRef}
            address={store.address}
            geocoding={store.geocoding}
            title={store.name}
          />
        }

        {/*{*/}
        {/*  !!vouchers.length &&*/}
        {/*  <div className="contact-box">*/}
        {/*    <div className="box-header">*/}
        {/*      <h6>Dịch vụ & Khuyến mãi liên quan</h6>*/}
        {/*    </div>*/}
        {/*    <div className="box-body">*/}
        {/*      <ul className="discount-code">*/}
        {/*        {*/}
        {/*          vouchers.map((item, index) => (*/}
        {/*            <VoucherItem key={index} voucher={item}/>*/}
        {/*          ))*/}
        {/*        }*/}
        {/*      </ul>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*}*/}
      </div>
    )
  }
}

export default ProductContactInfo;
