import React, {Fragment} from 'react';
import Avatar from '../../../../components/Avatar/Avatar';
import './ProductContactInfoMobile.scss'
import {Link} from "react-router-dom";
import {publicUrl} from "../../../../common/helpers";

const ProductInfoMobile = ({store, joinTime}) => {
  if (!store) {
    return null;
  }
  return (
    <Fragment>
      <div className="product-section">
        <div className="section-title">
          <h2>Cung cấp bởi</h2>
        </div>
        <div className="section-content">
          <div className="shop-info">
            <Link to={`/shop/${store.id}`}>
              <div className="shop-avatar">
                <Avatar src={store.owner.avatar} size={64}/>
              </div>
            </Link>
            <div className="shop-info-content">
              <Link to={`/shop/${store.id}`}>
                <div className="shop-name">{store.name}</div>
              </Link>
              <div className="shop-info-item"><span>Sản phẩm: </span> {store.product_count}</div>
              <div className="shop-info-item"><span>Tham gia: </span> {joinTime}</div>
              <div className="shop-chat-icon">
                <Link to={`/shop/${store.id}`}>
                  <button className="btn-chat-mobile">Xem shop</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
};

export default ProductInfoMobile;
