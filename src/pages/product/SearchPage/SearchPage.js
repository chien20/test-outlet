import React, {Fragment} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {history} from '../../../common/utils/router/history';
import {getProductListAPI} from '../../../api/products';
import HyperLink from '../../../components/HyperLink/HyperLink';
import ProductItem from '../../../components/ProductItem/ProductItem';
import Pagination from '../../../components/Pagination/Pagination';
import {Helmet} from 'react-helmet';
import NoData from '../../../components/NoData/NoData';
import {isMobile} from '../../../common/helpers/browser';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import CartIconMobile from '../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import './SearchPage.scss';

class SearchPage extends React.PureComponent {
  state = {
    isLoading: true,
    isLoaded: false,
    productList: [],
    totalItems: 0,
    sortParams: {
      by: '',
      order: ''
    }
  };

  componentDidMount() {
    this.getListProduct(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.getListProduct(nextProps);
  }

  getListProduct(props) {
    const {match: {params: {page, q, id}}} = props;
    const {sortParams} = this.state;
    const params = {
      current_page: page || 1,
      page_size: 30,
      q
    };
    if (id) {
      params.category_id = id;
    }
    params.current_page -= 1;
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    getProductListAPI(params).then(res => {
      this.setState({
        isLoading: false,
        isLoaded: true,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0,
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
        isLoaded: true,
      });
    });
  }

  onPageChanged = (page) => {
    const {match: {params: {q, id}}} = this.props;
    let url = '/tim-kiem';
    if (id) {
      url += `/c${id}`;
    }
    if (q) {
      url += `/${q}`;
    }
    if (page) {
      url += `/${page}`;
    }
    history.push(url);
  };

  onSort = (by, order) => () => {
    this.setState({
      sortParams: {
        by,
        order
      }
    }, () => {
      this.getListProduct(this.props);
    });
  };

  render() {
    const {productList, totalItems, sortParams, isLoaded} = this.state;
    const {match: {params: {page, q}}} = this.props;
    const totalPage = Math.ceil(totalItems / 30);
    return (
      <Fragment>
        <Helmet>
          <title>Tìm kiếm "{q}"</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text={`Tìm kiếm "${q}": Có ${totalItems} kết quả được tìm thấy`} isActive={true}/>
          </Breadcrumb>
        }
        {
          isMobile &&
          <HeaderMobile className="fixed">
            <IconBackMobile/>
            <TitleMobile>Tìm kiếm "{q}"</TitleMobile>
            <CartIconMobile/>
          </HeaderMobile>
        }
        {
          !productList.length && isLoaded &&
          <NoData
            title="Không có sản phẩm"
            description="Không có sản phẩm nào phù hợp với tìm kiếm của bạn"
          />
        }
        {
          !!productList.length &&
          <section className="section product-category-page common-page search-page">
            <div className="container">
              <div className="product-grid">
                <div className="item-sort m-t-0">
                  <span className="sort-title">Sắp xếp theo:</span>
                  <ul className="sort-list">
                    <li className={`${sortParams.by === 'id' && sortParams.order === 'desc' ? 'active' : ''}`}>
                      <HyperLink onClick={this.onSort('id', 'desc')}>Hàng mới</HyperLink>
                    </li>
                    <li className={`${sortParams.by === 'sell' && sortParams.order === 'desc' ? 'active' : ''}`}>
                      <HyperLink onClick={this.onSort('sell', 'desc')}>Bán chạy</HyperLink>
                    </li>
                    <li className={`${sortParams.by === 'price' && sortParams.order === 'asc' ? 'active' : ''}`}>
                      <HyperLink onClick={this.onSort('price', 'asc')}>Giá thấp</HyperLink>
                    </li>
                    <li className={`${sortParams.by === 'price' && sortParams.order === 'desc' ? 'active' : ''}`}>
                      <HyperLink onClick={this.onSort('price', 'desc')}>Giá cao</HyperLink>
                    </li>
                  </ul>
                </div>
                <div className="row product-items">
                  {
                    productList.map((item, index) => (
                      <div className="product-item-wrapper" key={index}>
                        <ProductItem product={item}/>
                      </div>
                    ))
                  }
                </div>
                <div className="item-pagination hidden-mobile">
                  {
                    totalPage > 1 &&
                    <Pagination totalPages={totalPage} currentPage={page || 1} onPageChanged={this.onPageChanged}/>
                  }
                </div>
              </div>
            </div>
          </section>
        }
      </Fragment>
    );
  }
}

export default SearchPage;
