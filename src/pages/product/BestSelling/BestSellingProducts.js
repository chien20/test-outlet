import React, {Fragment} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {history} from '../../../common/utils/router/history';
import {getProductListAPI} from '../../../api/products';
import ProductItem from '../../../components/ProductItem/ProductItem';
import Pagination from '../../../components/Pagination/Pagination';
import {Helmet} from 'react-helmet';
import {isMobile} from '../../../common/helpers/browser';
import InfiniteScroll from '../../../components/InfiniteScroll/InfiniteScroll';

class BestSellingProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      productList: [],
      totalItems: 0,
      sortParams: {
        by: '',
        order: ''
      },
      hasMore: false,
      nextPage: 0
    };
  }

  componentDidMount() {
    if (!isMobile) {
      this.getProductList(this.props);
    } else {
      this.getProductListMore();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getProductList(nextProps);
  }

  getProductList(props) {
    const {match: {params: {page}}} = props;
    const {sortParams} = this.state;
    const params = {
      current_page: page || 1,
      page_size: 20,
      type: 'best-sell'
    };
    params.current_page -= 1;
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    getProductListAPI(params, {__auth: true}).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  }

  onPageChanged = (page) => {
    if (page > 1) {
      history.push(`/products/best-selling/${page}`);
    } else {
      history.push(`/products/best-selling`);
    }
  };

  onSort = (by, order) => () => {
    this.setState({
      sortParams: {
        by,
        order
      }
    }, () => {
      this.getProductList(this.props);
    });
  };

  getProductListMore = () => {
    const {nextPage, isLoading, sortParams} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: 20,
      type: 'best-sell'
    };
    if (sortParams.by && sortParams.order) {
      params.sort_by = sortParams.by;
      params.sort_order = sortParams.order;
    }
    this.setState({
      isLoading: true
    });
    getProductListAPI(params).then(res => {
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info || {};
          return {
            isLoading: false,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1,
            totalItems: pageInfo.total_items || 0
          }
        });
      } else {
        this.setState({
          isLoading: false
        });
      }
    }).catch(error => {
      this.setState({
        isLoading: false
      });
    });
  };

  onLoadMore = () => {
    this.getProductListMore();
  };

  render() {
    const {productList, totalItems, hasMore} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / 20);
    return (
      <Fragment>
        <Helmet>
          <title>Sản phẩm bán chạy</title>
        </Helmet>
        <Breadcrumb>
          <BreadcrumbItem text="Sản phẩm bán chạy" isActive={true}/>
        </Breadcrumb>
        <section className="section product-category-page common-page">
          <div className="container">
            <div className="section-heading">
              <h2>
                <span className="text-uppercase">Sản phẩm bán chạy</span>
              </h2>
            </div>
            <div className="product-grid">
              <div className="row product-items">
                {
                  productList.map((item, index) => (
                    <div className="product-item-wrapper" key={index}>
                      <ProductItem product={item}/>
                    </div>
                  ))
                }
              </div>
              {
                hasMore && isMobile && <InfiniteScroll loadMore={this.onLoadMore}/>
              }
              <div className="item-pagination hidden-mobile">
                {
                  totalPage > 1 &&
                  <Pagination totalPages={totalPage} currentPage={page || 1} onPageChanged={this.onPageChanged}/>
                }
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    )
  }
}

export default BestSellingProducts;
