import React, {Fragment} from 'react';
import ProductGrid from '../_components/ProductGrid/ProductGrid';
import {history} from '../../../common/utils/router/history';
import CategoryName from './ShopInfo/CategoryName';
import "./ShopProducts.scss";

class ShopProducts extends React.PureComponent {
  onPageChanged = (page) => {
    const {match: {params: {id, categoryId}}} = this.props;
    if (!categoryId) {
      if (page > 1) {
        history.push(`/shop/${id}/products/${page}`);
      } else {
        history.push(`/shop/${id}`);
      }
    } else {
      if (page > 1) {
        history.push(`/shop/${id}/category/${categoryId}/${page}`);
      } else {
        history.push(`/shop/${id}/category/${categoryId}`);
      }
    }
  };

  render() {
    const {match: {params: {id, categoryId}}} = this.props;
    const queryParams = {store_id: id};
    if (categoryId) {
      queryParams.category_id = categoryId;
    }
    return (
      <Fragment>
        <div className="shop-section__title">
          <CategoryName id={categoryId} storeId={id}/>
        </div>
        <div className="shop-section__content">
          <ProductGrid
            onPageChanged={this.onPageChanged}
            queryParams={queryParams}
            toolbar={true}
            itemsPerRow={6}
            pageSize={36}
          />
        </div>
      </Fragment>
    );
  }
}

export default ShopProducts;
