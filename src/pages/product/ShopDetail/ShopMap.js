import React from 'react';
import {StoreContext} from './ShopDetail';
import LocationPicker from '../../../components/GoogleMap/LocationPicker';
import LoadScript from '../../../components/GoogleMap/LoadScript';

class ShopMapComponent extends React.PureComponent {
  render() {
    const {store} = this.props;
    if (!store) {
      return null;
    }
    if (!store.geocoding) {
      return (
        <p>{store.address}</p>
      )
    }
    return (
      <div className="shop-map">
        <p>{store.geocoding.address}</p>
        <LoadScript>
          <LocationPicker
            address={store.address}
            geocoding={store.geocoding}
          />
        </LoadScript>
      </div>
    );
  }
}

const ShopMap = (ownProps) => (
  <StoreContext.Consumer>
    {
      (props) => (
        <ShopMapComponent {...props} {...ownProps}/>
      )
    }
  </StoreContext.Consumer>
);

export default ShopMap;
