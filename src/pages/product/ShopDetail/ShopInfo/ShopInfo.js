import React from 'react';
import {StoreContext} from '../ShopDetail';
import './ShopInfo.scss';
import {publicUrl, timeDiff} from '../../../../common/helpers';
import Avatar from '../../../../components/Avatar/Avatar';
import StarRating from '../../../../components/StarRating/StarRating';
import {getCountActiveProductAPI, getStoreAvgRatingAPI, getStoreCountRatingAPI} from '../../../../api/stores';
import {Link} from "react-router-dom";
import ESwiper from '../../../../components/ESwiper/ESwiper';

const defaultSliders = [
  {
    url: `https://images.wallpaperscraft.com/image/night_spruce_starfall_185284_1366x768.jpg`,
    title: 'banner 1'
  },
  {
    url: `https://images.wallpaperscraft.com/image/night_starfall_trees_151145_1366x768.jpg`,
    title: 'banner 2'
  },
  {
    url: `https://images.wallpaperscraft.com/image/spruce_trees_fog_185301_1400x1050.jpg`,
    title: 'banner 3'
  }
];

class ShopInfoComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetchingProduct: true,
      productCount: 0,
      rating: 0,
      ratingCount: 0,
    };
  }

  componentDidMount() {
    this.getCountActiveProducts();
    this.getRatingAvg();
    this.getRatingCount();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.store !== this.props.store) {
      this.getCountActiveProducts();
      this.getRatingAvg();
      this.getRatingCount();
    }
  }

  getRatingCount = () => {
    const {store} = this.props;
    if (!store) {
      return;
    }
    getStoreCountRatingAPI(store.id).then(res => {
      this.setState({
        ratingCount: res.data || 0,
      });
    }).catch(error => {

    });
  };

  getRatingAvg = () => {
    const {store} = this.props;
    if (!store) {
      return;
    }
    getStoreAvgRatingAPI(store.id).then(res => {
      this.setState({
        rating: res.data || 0,
      });
    }).catch(error => {

    });
  };

  getCountActiveProducts = () => {
    const {store} = this.props;
    if (!store) {
      return;
    }
    getCountActiveProductAPI(store.id).then(res => {
      this.setState({
        isFetchingProduct: false,
        productCount: res.data || 0,
      });
    }).catch(error => {
    });
  };

  render() {
    const {store} = this.props;

    const reviewed = store.reviewed;
    const authentic = store.other_info && store.other_info.authentic;
    // const refund = store.other_info && store.other_info.refund;
    // const refunds = store.other_info && store.other_info.refunds;

    const {isFetchingProduct, productCount, rating, ratingCount} = this.state;
    const joinTime = timeDiff(store.created_at);

    const sliders = store?.covers?.length ? store.covers : defaultSliders;

    return (
      <div className="shop-info paper">
        <div className="shop-banners">
          <ESwiper sliders={sliders} className="shop-banners__slider"/>
        </div>
        <div className="shop-row">
          <div className="shop-col">
            <Avatar className="shop-avatar" src={store.owner.avatar} size={82}/>
            <div className="shop-summary-info">
              <div className="shop-name">
                <h2>{store.name}</h2>
              </div>
              <div className="d-flex justify-content-between align-items-center">
                <div>
                  {joinTime && <p className="join-time"><i className="far fa-clock"/> Đã tham gia {joinTime}</p>}
                  <p className="shop-product-total">
                    <i className="fas fa-box"/>
                    {
                      isFetchingProduct &&
                      <span>-</span>
                    }
                    {
                      !isFetchingProduct &&
                      <span>&nbsp;{productCount} sản phẩm</span>
                    }
                  </p>
                  <div className="shop-rating">
                    <StarRating isDisabled={true} value={rating}/>
                    <span>(&nbsp;{ratingCount} đánh giá&nbsp;)</span>
                  </div>
                </div>
                <div className="actions">
                  <Link to={`/user/chat/?new=true&user_id=${store.user_id}`} title="Trò chuyện">
                    <img src={publicUrl('/assets/images/icons/icon-chat.svg')} alt="chat-icon"/>
                  </Link>
                </div>
              </div>

            </div>
          </div>
          {
            reviewed &&
            <div className="shop-col">
              <div className="shop-property">
                <div className="d-flex align-items-center">
                  <i className="ico-16 m-r-5"/>
                  <span>Chứng nhận bởi Outlet</span>
                </div>
              </div>
            </div>
          }

          {
            authentic &&
            <div className="shop-col">
              <div className="shop-property">
                <div className="d-flex align-items-center">
                  <i className="ico-17 m-r-5"/>
                  <span>Cam kết chính hãng 100%</span>
                </div>
              </div>
            </div>
          }

          {
            authentic &&
            <div className="shop-col">
              <div className="shop-property">
                <div className="d-flex align-items-center">
                  <i className="ico-18 m-r-5"/>
                  <span>Hoàn tiền 100% nếu hàng giả</span>
                </div>
              </div>
            </div>
          }
        </div>

      </div>
    );
  }
}

const ShopInfo = (ownProps) => (
  <StoreContext.Consumer>
    {
      (props) => (
        <ShopInfoComponent {...props} {...ownProps}/>
      )
    }
  </StoreContext.Consumer>
);

export default ShopInfo;
