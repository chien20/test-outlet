import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

const CategoryName = ({id, categoryList = [], storeId}) => {
  if (!categoryList || !categoryList.length) {
    return '';
  }
  const category = categoryList.find(item => `${item.id}` === `${id}`);


  return (
    <>
      {
        category &&
          <>
            <h3>{category.name}</h3>
            <Link to={`/shop/${storeId}`}><i className="fas fa-long-arrow-alt-left"></i> Trở về</Link>
          </>
      }
      {
        !category &&
          <h3>Tất cả sản phẩm</h3>
      }

    </>
  );
};

const mapStateToProps = (state) => ({
  categoryList: state.common.category.list
});

export default connect(mapStateToProps)(CategoryName);