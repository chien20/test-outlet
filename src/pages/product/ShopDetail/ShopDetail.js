import React from 'react';
import {connect} from 'react-redux';
import {imageUrl, showAlert} from '../../../common/helpers';
import {getStoreCategoriesAPI, getStoreDetailAPI} from '../../../api/stores';
import {Helmet} from 'react-helmet';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {Link, Route, Switch} from 'react-router-dom';
import ShopProducts from './ShopProducts';
import {getBasicUserInfoAPI} from '../../../api/users';
import './ShopDetail.scss';
import {getObjectMediaAPI} from '../../../api/media';
import {OBJECT_TYPES} from '../../../common/constants/objectTypes';
import ShopInfoContainer from './ShopInfo/ShopInfo';
import CategoryList from '../_components/CategoryList/CategoryList';
import ShopProfile from './ShopProfile';
import ShopMap from './ShopMap';

export const StoreContext = React.createContext({store: null});

class ShopDetail extends React.PureComponent {
  state = {
    isLoading: true,
    store: null,
    categories: [],
  };

  componentDidMount() {
    this.getStore();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps?.match?.params?.id !== this.props?.match?.params?.id) {
      this.getStore();
    }
  }

  getStore = () => {
    this.getStoreAsync().then(store => {
      this.setState({
        isLoading: false,
        store,
      });
      this.getCategories();
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: 'Không tải được thông tin cửa hàng'
      });
    });
  };

  getStoreAsync = async () => {
    const {match: {params: {id}}} = this.props;
    const {data: store} = await getStoreDetailAPI(id);
    const {data: owner} = await getBasicUserInfoAPI(store.user_id);
    const {data: {list: media}} = await getObjectMediaAPI(id, OBJECT_TYPES.Store, null, {
      page_size: 100,
    });
    store.owner = owner;
    store.media = media || [];
    store.media.forEach(item => {
      if (!item.type || `${item.type}`.includes('image')) {
        item.use_type = 'cover';
      } else {
        item.use_type = item.type;
      }
      item.preview_url = imageUrl(item.url);
    });
    store.covers = store.media.filter(item => item.use_type === 'cover').map(item => ({
      title: store.name,
      url: item.preview_url,
    }));
    return store;
  };

  getCategories = () => {
    const {match: {params: {id}}} = this.props;
    getStoreCategoriesAPI(id).then(res => {
      if (res.data.list) {
        res.data.list.forEach(item => {
          if (item.name === undefined) {
            item.name = item.category_name;
          }
          item.url = `/shop/${id}/category/${item.id}`;
        });
      }
      this.setState({
        categories: this.filterDisplayCategories(res.data.list),
      });
    }).catch(error => {
      console.log(error);
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  filterDisplayCategories = (categories) => {
    const {match: {params: {id, categoryId}}, categoryMap} = this.props;
    const result = {};
    if (!categoryId) {
      categories.forEach(c => {
        let category = categoryMap[c.id];
        if (!category) {
          return;
        }
        if (category.parents.length) {
          category = categoryMap[category.parents[0]];
        }
        result[category.id] = {
          ...category,
          url: `/shop/${id}/category/${category.id}`,
        };
      });
    } else {
      const parentId = categoryId * 1;
      categories.forEach(c => {
        let category = categoryMap[c.id];
        if (!category || !category.parents || !category.parents.includes(parentId)) {
          return;
        }
        const parentIdIndex = category.parents.findIndex(id => id === parentId);
        if (category.parents[parentIdIndex + 1]) {
          category = categoryMap[category.parents[parentIdIndex + 1]];
        }
        result[category.id] = {
          ...category,
          url: `/shop/${id}/category/${category.id}`,
        };
      });
    }
    return Object.values(result);
  };

  render() {
    const {match: {params: {id, categoryId}}, location: {pathname}} = this.props;
    const {store, categories} = this.state;
    if (!store) {
      return null;
    }
    return (
      <StoreContext.Provider value={{store}}>
        <Helmet>
          <title>{store.name}</title>
        </Helmet>
        <Breadcrumb>
          <BreadcrumbItem text={store.name} isActive={true}/>
        </Breadcrumb>
        <section className="section shop-detail-page common-page">
          <div className="container">
            <div className="shop-products">
              {
                <ShopInfoContainer/>
              }
              <nav className="nav-shop">
                <ul>
                  <li className="fixed"><span>Danh mục</span>
                    <CategoryList categories={categories}/>
                  </li>
                  <li>
                    <Link
                      className={`${pathname === `/shop/${id}` ? 'active' : ''}`}
                      to={`/shop/${id}`}
                    >
                      Sản phẩm
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={`${pathname === `/shop/${id}/profile` ? 'active' : ''}`}
                      to={`/shop/${id}/profile`}
                    >
                      Giới thiệu
                    </Link>
                  </li>
                  <li>
                    <Link
                      className={`${pathname === `/shop/${id}/map` ? 'active' : ''}`}
                      to={`/shop/${id}/map`}
                    >
                      Bản đồ
                    </Link>
                  </li>
                </ul>
              </nav>
              <div className="shop-section">
                <Switch>
                  <Route path="/shop/:id/profile" exact component={ShopProfile} store={store}/>
                  <Route path="/shop/:id/map" exact component={ShopMap} store={store}/>
                  <Route path="/shop/:id/category/:categoryId/:page?" exact component={ShopProducts} store={store}/>
                  <Route path="/shop/:id" exact component={ShopProducts} store={store}/>
                </Switch>
              </div>
            </div>
          </div>
        </section>
      </StoreContext.Provider>
    );
  }
}

const mapStateToProps = (state) => ({
  categoryMap: state.common.category.map,
});

export default connect(mapStateToProps)(ShopDetail);
