import React from 'react';
import {StoreContext} from './ShopDetail';
import HTMLView from '../../../components/View/HTMLView';

class ShopProfileComponent extends React.PureComponent {
  render() {
    const {store} = this.props;
    if (!store) {
      return null;
    }
    return (
      <div className="shop-description">
        <HTMLView html={store.description}/>
      </div>
    );
  }
}

const ShopProfile = (ownProps) => (
  <StoreContext.Consumer>
    {
      (props) => (
        <ShopProfileComponent {...props} {...ownProps}/>
      )
    }
  </StoreContext.Consumer>
);

export default ShopProfile;
