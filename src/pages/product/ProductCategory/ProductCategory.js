import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import CategoryMenu from '../_components/CategoryMenu/CategoryMenu';
import CategorySlider from '../_components/CategorySlider/CategorySlider';
import ProductGrid from '../_components/ProductGrid/ProductGrid';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {Helmet} from 'react-helmet';
import {Link} from "react-router-dom";
import PriceFilter from '../_components/ProductFilter/PriceFilter';
import {history} from '../../../common/utils/router/history';
import {isMobile} from '../../../common/helpers/browser';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import {OBJECT_TYPES} from '../../../common/constants/objectTypes';
import searchIconMobileBlack from '../../../assets/images/icons/search-icon-black.svg';
import './ProductCategory.scss';
import {imageUrl, publicUrl} from '../../../common/helpers';
import Adv from '../../../components/Adv/Adv';
import {Tab, Nav} from "react-bootstrap";

class ProductCategory extends React.PureComponent {
  onPageChanged = (page) => {
    const {match: {params: {id}}, categoryList} = this.props;
    if (!categoryList || !categoryList.length) {
      return null;
    }
    const category = categoryList.find(item => `${item.id}` === `${id}`);
    if (page > 1) {
      history.push(`/${category.slug}/c${category.id}/${page}`);
    } else {
      history.push(`/${category.slug}/c${category.id}`);
    }
  };

  render() {
    const {match: {params: {id}}, categoryList, categoryMap} = this.props;
    if (!categoryList || !categoryList.length) {
      return null;
    }
    const category = categoryMap[id];
    const children = categoryList.filter(item => `${item.parent_id}` === `${id}`);
    if (!category) {
      return null;
    }
    return (
      <>
        <Helmet>
          <title>{category.name}</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            {
              category.parents && category.parents.map(id => (
                <Fragment key={id}>
                  {
                    categoryMap[id] &&
                    <BreadcrumbItem
                      text={categoryMap[id].name}
                      path={`/${categoryMap[id].slug}/c${id}`}
                      isActive={false}/>
                  }
                </Fragment>
              ))
            }
            <BreadcrumbItem text={category.name} isActive={true}/>
          </Breadcrumb>
        }
        { 
          isMobile &&
          <HeaderMobile className="header-mobile-category">
            <IconBackMobile/>
            <TitleMobile>{category.name}</TitleMobile>
            <div className="category-search">
              <img src={searchIconMobileBlack} alt=""/>
            </div>
            {/* <div className="filters" onClick={this.toggleFilter}>
                <img className="sort-icon" src={publicUrl(`/assets/images/icons/filter-loc.svg`)} alt="icon"/>
            </div> */}
          </HeaderMobile>
        }
        {
          !isMobile &&
          <section className="section product-category-page common-page">
          <div className="container">
            <div className="e-row">
              <div className="col-left hidden-mobile">
                {
                  !!children && !!children.length &&
                  <CategoryMenu childrenList={children} category={category}/>
                }
                <PriceFilter category={category}/>
                <Adv position="left_sidebar"/>
              </div>
              <div className="col-main">
                <CategorySlider
                  objectId={category.id}
                  objectType={OBJECT_TYPES.Category}
                  key={`${category.id}`}
                />
                <ProductGrid onPageChanged={this.onPageChanged} queryParams={{category_id: category.id}}/>
              </div>
            </div>
          </div>
        </section>
        }
        {
          isMobile &&
          <section className="section product-category-page common-page">
            <div className="e-row">
              <div className="col-left hidden-mobile">
                {
                  !!children && !!children.length &&
                  <CategoryMenu childrenList={children} category={category}/>
                }
                <Adv position="left_sidebar"/>
              </div>
              <div className="col-main"> 
                <CategorySlider
                  objectId={category.id}
                  objectType={OBJECT_TYPES.Category}
                  key={`${category.id}`}
                />
              <Tab.Container defaultActiveKey={0}>
                <Nav>
                  {
                    isMobile &&
                    <Fragment>
                    {
                      !!children && !!children.length &&
                      <div className="sub-category-list">
                        {
                          children.map( (item, index) => (
                          <Nav.Item className="item" key={index}>
                            <Nav.Link eventKey={index}>
                              <img src={imageUrl(item.icon_url)} alt="Icon"/>
                              <label>{item.name}</label>
                            </Nav.Link>
                          </Nav.Item>
                      ))
                        }
                      </div>
                    }
                  </Fragment>
                  }
                </Nav>
                <Tab.Content className="product-category-content">
                {
                    children.map((item,index) => (
                        <Tab.Pane eventKey={index} key={index}>
                          {/* <Link className="read-more" to={`/${item.slug}/c${item.id}`}>Xem thêm</Link> */}
                          <ProductGrid category={category} onPageChanged={this.onPageChanged} queryParams={{category_id: item.id}}/>
                        </Tab.Pane>
                    ))
                  }
                
                </Tab.Content>
              </Tab.Container>
                
              </div>
            </div>
        </section>
        }
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  categoryList: state.common.category.list,
  categoryMap: state.common.category.map,
});

export default connect(mapStateToProps)(ProductCategory);
