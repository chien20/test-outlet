import React, {Fragment} from "react";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import './ProductListFilter.scss'

class ProductListFilter extends React.PureComponent {

  handleRadio = () => {
    return true;
  }
  render() { 
    return (
        <div className="product-list-filter">
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile>Filter</TitleMobile>
          </HeaderMobile>
          <div className="list-filter-content">
            <div className="filter-options">
              <div className="filter-box">
                <h4 className="filter-box__title">Kiểu tủ</h4>
                <div className="filter-box__content">
                  <ul className="list-keyword">
                    <li className="active"><span>Ngăn đá trên</span></li>
                    <li><span>Ngăn đá dưới</span></li>
                    <li><span>Mini</span></li>
                    <li><span>Tủ lớn, Side by side</span></li>
                    <li><span>Tủ nhiều cửa</span></li>
                  </ul>
                </div>
              </div>

              <div className="filter-box">
                <h4 className="filter-box__title">Dung tích</h4>
                <div className="filter-box__content">
                  <div className="checkbox-list">
                    <div className="checkbox-item custom-radio-2">
                      <input type="radio" id="test1" name="radio-group" onChange={this.handleRadio} />
                      <label htmlFor="test1">Dưới 150 lít</label>
                    </div>
                    <div className="checkbox-item custom-radio-2">
                      <input type="radio"  id="test2" name="radio-group" onChange={this.handleRadio}/>
                      <label htmlFor="test2">Từ 150 - 300 lít</label>
                    </div>
                    <div className="checkbox-item custom-radio-2">
                      <input type="radio" id="test3" name="radio-group" onChange={this.handleRadio}/>
                      <label htmlFor="test3">Từ 300 - 400 lít</label>
                    </div>
                    <div className="checkbox-item custom-radio-2">
                      <input type="radio" id="test4" name="radio-group" onChange={this.handleRadio}/>
                      <label htmlFor="test4">Từ 400 - 500 lít</label>
                    </div>
                    <div className="checkbox-item custom-radio-2">
                      <input type="radio" id="test5" name="radio-group" onChange={this.handleRadio} />
                      <label htmlFor="test5">Trên 500 lít</label>
                    </div>
                  </div>
                </div>
              </div>

              <div className="filter-box">
                <h4 className="filter-box__title">Thương hiệu</h4>
                <div className="filter-box__content">
                  <div className="checkbox-list">
                    <div className="checkbox-item custom-checkbox-2">
                      <input type="checkbox" id="test15" name="filter"/>
                      <label htmlFor="test15">SAMSUNG</label>
                    </div>
                    <div className="checkbox-item custom-checkbox-2">
                      <input type="checkbox" id="test25" name="filter"/>
                      <label htmlFor="test25">BENKO</label>
                    </div>
                    <div className="checkbox-item custom-checkbox-2">
                      <input type="checkbox" id="test35" name="filter"/>
                      <label htmlFor="test35">AQUA</label>
                    </div>
                    <div className="checkbox-item custom-checkbox-2">
                      <input type="checkbox" id="test45" name="filter"/>
                      <label htmlFor="test45">LG</label>
                    </div>
                    <div className="checkbox-item custom-checkbox-2">
                      <input type="checkbox" id="test55"name="filter"/>
                      <label htmlFor="test55">Panasonic</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="actions-bottom">
              <button className="btn cancel-btn">Xóa</button>
              <button className="btn btn-danger-mobile apply-btn">Áp dụng</button>
            </div>
          </div>
        </div>
    )
  }
}

export default ProductListFilter;