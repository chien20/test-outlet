import React, {Fragment} from 'react';
import {Redirect} from 'react-router-dom';
import {isMobile} from '../../common/helpers/browser';
import {Helmet} from 'react-helmet';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import {getStaticPageDetailAPI} from '../../api/static-pages';
import {getCertDetailAPI} from '../../api/certs';

class CertDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: null,
      isFetching: true
    };
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProp, prevState, snapshot) {
    if (this.props.match.params.id !== prevProp.match.params.id) {
      this.getData();
    }
  }

  getData = () => {
    const {match: {params: {id}}} = this.props;
    getCertDetailAPI(id).then(res => {
      this.setState({
        isFetching: false,
        page: res.data,
      });
    }).catch(() => {
      this.setState({
        isFetching: false,
        page: null
      });
    })
  };

  render() {
    const {page, isFetching} = this.state;
    if (isFetching) {
      return null;
    }
    if (!page) {
      return <Redirect to="/error/404/"/>
    }
    return (
      <Fragment>
        <Helmet>
          <title>{page.name}</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Chứng nhận"/>
            <BreadcrumbItem text={page.name} isActive={true}/>
          </Breadcrumb>
        }
        <div className="common-page">
          <div className="container">
            <div className="paper">
              <h1>{page.name}</h1>
              <h2>Mô tả:</h2>
              <div className="description" dangerouslySetInnerHTML={{__html: page.description}}/>
              <h2>Hướng dẫn đăng ký:</h2>
              <div className="description" dangerouslySetInnerHTML={{__html: page.tutorial}}/>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default CertDetail;
