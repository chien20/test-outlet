import React, {Fragment} from 'react';
import {Redirect} from 'react-router-dom';
import {isMobile} from '../../common/helpers/browser';
import {Helmet} from 'react-helmet';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import {getStaticPageDetailAPI} from '../../api/static-pages';

class StaticPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: null,
      isFetching: true
    };
  }

  componentDidMount() {
    this.getPage();
  }

  componentDidUpdate(prevProp, prevState, snapshot) {
    if (this.props.match.params.slug !== prevProp.match.params.slug) {
      this.getPage();
    }
  }

  getPage = () => {
    const {match: {params: {slug}}} = this.props;
    getStaticPageDetailAPI(slug).then(res => {
      this.setState({
        isFetching: false,
        page: res.data
      });
    }).catch(() => {
      this.setState({
        isFetching: false,
        page: null
      });
    })
  };

  render() {
    const {page, isFetching} = this.state;
    if (isFetching) {
      return null;
    }
    if (!page) {
      return <Redirect to="/error/404/"/>
    }
    return (
      <Fragment>
        <Helmet>
          <title>{page.title}</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text={page.name} isActive={true}/>
          </Breadcrumb>
        }
        <div className="common-page">
          <div className="container">
            <div className="paper" dangerouslySetInnerHTML={{__html: page.content}}/>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default StaticPage;