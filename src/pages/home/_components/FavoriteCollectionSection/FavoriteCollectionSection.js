import React from 'react';
import './FavoriteCollectionSection.scss';
import {Link} from 'react-router-dom';


const FavoriteCollectionSection = () => {
  return (
    <section className="section favorite-colection-section">
      <div className="container">
        <div className="section-heading no-line-through">
          <h2>
            Bộ Sưu tập Yêu thích
          </h2>
          <div className="link-read-more">
            <Link to="/">Xem thêm</Link>
          </div>
        </div>
        <div className="section-inner">
          <div className="colection-items">
            <div className="colection-item">
                <img src="assets/images/colection/colection-my-pham.png" alt="Mỹ phẩm"/>
                <h3 className="colection-title">Mỹ phẩm - làm đẹp</h3>
                <p className="colection-des">Đã bán 2k+</p>
            </div>
            <div className="colection-item">
                <img src="assets/images/colection/colection-thoi-trang.png" alt="Mỹ phẩm"/>
                <h3 className="colection-title">Thời trang</h3>
                <p className="colection-des">Đã bán 2k+</p>
            </div>
            <div className="colection-item">
                <img src="assets/images/colection/colection-my-pham.png" alt="Mỹ phẩm"/>
                <h3 className="colection-title">Mỹ phẩm - làm đẹp</h3>
                <p className="colection-des">Đã bán 2k+</p>
            </div>
            <div className="colection-item">
                <img src="assets/images/colection/colection-thoi-trang.png" alt="Mỹ phẩm"/>
                <h3 className="colection-title">Thời trang</h3>
                <p className="colection-des">Đã bán 2k+</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};


export default React.memo(FavoriteCollectionSection);
