import React from 'react';
import './PopularSearchSection.scss';
import {Link} from 'react-router-dom';


const FavoriteCollectionSection = () => {
  return ( 
    <section className="section popular-search-section">
      <div className="container">
        <div className="section-heading no-line-through">
          <h2>
            Tìm kiếm phổ biến
          </h2>
          <div className="link-read-more">
            <Link to="/">
              <span>
                <img src="assets/images/icons/icon-reload.svg" alt=""/>
              </span> Làm mới
            </Link>
          </div>
        </div>
        <div className="section-inner">
          <div className="popular-search-items">
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Đầm dự tiệc</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Máy  tính xách tay</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Bỉm sữa</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Xe đạp cho bé trai</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Dép sandal cho bé trai</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Máy lọc nước</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Đồ gia dụng</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Kem dưỡng da</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Bột ăn dặm</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Giày snacker</p>
              </Link>
            </div>
            <div className="search-item">
              <Link to="/">
                <p className="search-title">Áo chống nắng</p>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};


export default React.memo(FavoriteCollectionSection);
