import React from 'react';
import {publicUrl} from '../../../../common/helpers';
import './MapSection.scss';

class MapSection extends React.PureComponent {
  render() {
    return (
      <section className="section shop-near-by-you section-gradient">
        <div className="container">
          <div className="section-heading">
            <h2>
              <span className="d-inline-block">
                <span className="text-uppercase">Tìm Shop gần bạn được chứng nhận bởi Outlet</span>
              </span>
            </h2>
          </div>

          <div className="filter-shop">
            <div className="row">
              <div className="col-lg-3 col-sm-6 form-group">
                <select className="form-control">
                  <option>Sản phẩm</option>
                </select>
              </div>

              <div className="col-lg-3 col-sm-6 form-group">
                <select className="form-control">
                  <option>Địa điểm</option>
                </select>
              </div>

              <div className="col-lg-3 col-sm-6 form-group">
                <select className="form-control">
                  <option>Xếp hạng</option>
                </select>
              </div>

              <div className="col-lg-3 col-sm-6 form-group">
                <select className="form-control">
                  <option>Giờ mở cửa</option>
                </select>
              </div>
            </div>
          </div>

          <div className="map-wrapper">
            <img src={publicUrl('/assets/images/v2/upload/map-xl.jpg')} alt={`map`}/>
          </div>

          {/*<div className="advs">*/}
          {/*<Link to="/" href="#"><img src={publicUrl('/assets/images/v2/upload/aside-banner-10.jpg')} alt={``}/></Link>*/}
          {/*</div>*/}

        </div>
      </section>
    )
  }
}

export default MapSection;

