import React, {Fragment} from 'react';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import ProductGrid from '../../../product/_components/ProductGrid/ProductGrid';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {getProductListAPI} from '../../../../api/products';
import InfiniteScroll from '../../../../components/InfiniteScroll/InfiniteScroll';
import {isMobile} from '../../../../common/helpers/browser';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import _chunk from 'lodash/chunk';
import {Tab, Nav} from "react-bootstrap";
import "./SuggestProductSection.scss"

class SuggestSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      hasMore: false,
      nextPage: 0,
      isLoading: false
    };
  }

  componentDidMount() {
    this.getProductList();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  getProductList = () => {
    const {nextPage, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: 24,
      type: 'recommend',
      sort_by: 'id',
      sort_order: 'desc',
    };
    this.setState({
      isLoading: true
    });
    getProductListAPI(params).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info;
          return {
            isLoading: false,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1
          }
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    }).catch(error => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isLoading: false,
      });
    });
  };

  onClickLoadMore = () => {
    this.getProductList();
  };

  render() {
    const {categories} = this.props;
    const {productList, hasMore} = this.state;
    if (!productList || !productList.length) {
      return null;
    }
    const chunks = _chunk(productList, 6);
    return (
      <section className="section products-section">
        <Tab.Container defaultActiveKey={0} className="container">
          <div className="section-heading no-line-through">
            <h2>
              Có thể bạn sẽ thích
            </h2>
            <Nav className="products-heading-category">
              <Nav.Item key={0} className="categorys-item">
                  <Nav.Link eventKey={0} className="categorys-item-title">
                    <span>
                      <img style={{width:'12px'}} src="assets/images/icons/icon-hot.svg" alt="Khuyến mãi hot"/>
                    </span> Khuyến mãi Hot
                  </Nav.Link> 
              </Nav.Item>
              {
                categories.map( (item, index) => (
                  <Nav.Item key={index+1} className="categorys-item">
                    {
                      item.children &&
                        <Nav.Link eventKey={index+1} className="categorys-item-title">
                          <span>
                          <img src={imageUrl(item.icon_url)} alt="Icon"/>
                          </span> {item.name}
                        </Nav.Link>
                    }
                  </Nav.Item>
                ))
              }
            </Nav> 
          </div>
          <Tab.Content className="section-inner">
            {
              chunks.map((chunk, chunkIndex) => (
                <Tab.Pane key={chunkIndex} eventKey={0}>
                  <div className="row product-items" key={chunkIndex}>
                    {
                      chunk.map((item, index) =>
                        <div className="product-item-wrapper" key={index}>
                          <ProductItem product={item}/>
                          {
                            isMobile && hasMore && productList.length < 60 &&
                              <InfiniteScroll loadMore={this.onClickLoadMore} hasMore={hasMore}/>
                          }
                        </div>
                      )
                    }
                  </div>
                </Tab.Pane>
              ))
            }
            {
              categories.map((item, index) => (
                <Tab.Pane key={index+1} eventKey={index+1}>
                  {
                    item.children &&
                    <ProductGrid key={index} onPageChanged={this.onPageChanged} queryParams={{category_id: item.id}}/>
                  }
                </Tab.Pane>
              ))
            }
            {/* {
              hasMore && productList.length < 60 &&
              <Fragment>
                <div className="btn-read-more d-mobile-none">
                  <HyperLink to="/products/flash-sale/" onClick={this.onClickLoadMore}>Xem Thêm</HyperLink>
                </div>
                {
                  isMobile && <InfiniteScroll loadMore={this.onClickLoadMore} hasMore={hasMore}/>
                }
              </Fragment>
            } */}
          </Tab.Content>
        </Tab.Container>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  categories: state.common.category.tree,
});

export default connect(mapStateToProps)(SuggestSection);
