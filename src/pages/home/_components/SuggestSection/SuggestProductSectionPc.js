import React, {Fragment} from 'react';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {getProductListAPI} from '../../../../api/products';
import InfiniteScroll from '../../../../components/InfiniteScroll/InfiniteScroll';
import {isMobile} from '../../../../common/helpers/browser';
import _chunk from 'lodash/chunk';

class SuggestProductSectionPc extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      hasMore: false,
      nextPage: 0,
      isLoading: false
    };
  }

  componentDidMount() {
    this.getProductList();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  getProductList = () => {
    const {nextPage, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: 24,
      type: 'recommend',
      sort_by: 'id',
      sort_order: 'desc',
    };
    this.setState({
      isLoading: true
    });
    getProductListAPI(params).then(res => {
      if (this.unmounted) {
        return;
      }
      if (res && res.data && res.data.list && res.data.page_info) {
        this.setState(prevState => {
          const pageInfo = res.data.page_info;
          return {
            isLoading: false,
            productList: [...prevState.productList, ...res.data.list],
            hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
            nextPage: pageInfo.current_page + 1
          }
        });
      } else {
        this.setState({
          isLoading: false,
        });
      }
    }).catch(error => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isLoading: false,
      });
    });
  };

  onClickLoadMore = () => {
    this.getProductList();
  };

  render() {
    const {productList, hasMore} = this.state;
    if (!productList || !productList.length) {
      return null;
    }
    const chunks = _chunk(productList, 6);
    return (
      <section className="section products-section">
        <div className="container">
          <div className="section-heading no-line-through">
            <h2>
              Có thể bạn sẽ thích
            </h2>
          </div>
          <div className="section-inner bg-white p-5">
            {
              chunks.map((chunk, chunkIndex) => (
                <Fragment key={chunkIndex}>
                  {/*{*/}
                  {/*chunkIndex < chunks.length &&*/}
                  {/*<Adv position="inside_list_product"/>*/}
                  {/*}*/}
                  <div className="row product-items" key={chunkIndex}>
                    {
                      chunk.map((item, index) =>
                        <div className="product-item-wrapper" key={index}>
                          <ProductItem product={item}/>
                        </div>
                      )
                    }
                  </div>
                </Fragment>
              ))
            }
            {
              hasMore && productList.length < 60 &&
              <Fragment>
                <div className="btn-read-more d-mobile-none">
                  <HyperLink to="/products/flash-sale/" onClick={this.onClickLoadMore}>Xem Thêm</HyperLink>
                </div>
                {
                  isMobile && <InfiniteScroll loadMore={this.onClickLoadMore} hasMore={hasMore}/>
                }
              </Fragment>
            }
          </div>
        </div>
      </section>
    )
  }
}

export default SuggestProductSectionPc;