import React from 'react';
import './SuggestCategorySection.scss';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

const Items = [
  {
    primary: {
      textColor: '#014294',
      bgColor: '#D1E5FD',
      title: 'Deal công nghệ',
      description: 'Giảm đến 50% ++',
      images: {
        primary: publicUrl('/assets/images/suggest/primary/big.png'),
        secondaries: [
          publicUrl('/assets/images/suggest/primary/small.png'),
          publicUrl('/assets/images/suggest/primary/small-1.png'),
        ]
      }
    },
    secondaries: [
      {
        title: 'Áo sơ mi công sở',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small.png'),
      },
      {
        title: 'Giày thể thao',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small-1.png'),
      },
    ],
  },
  {
    primary: {
      textColor: '#48B765',
      bgColor: '#D7F2DE',
      title: 'Nhà cửa & Đời sống',
      description: 'Giảm đến 50% ++',
      images: {
        primary: publicUrl('/assets/images/suggest/primary/big-1.png'),
        secondaries: [
          publicUrl('/assets/images/suggest/primary/small-2.png'),
          publicUrl('/assets/images/suggest/primary/small-3.png'),
        ]
      }
    },
    secondaries: [ 
      {
        title: 'Đồ chơi nhựa trẻ em',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small-2.png'),
      },
      {
        title: 'Bách hóa',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small-3.png'),
      },
    ],
  },
  {
    primary: {
      textColor: '#F2994A',
      bgColor: '#FFF2EB',
      title: 'Trang sức & Nước hoa',
      description: 'Giảm đến 50% ++',
      images: {
        primary: publicUrl('/assets/images/suggest/primary/big-2.png'),
        secondaries: [
          publicUrl('/assets/images/suggest/primary/small-4.png'),
          publicUrl('/assets/images/suggest/primary/small-5.png'),
        ]
      }
    },
    secondaries: [
      {
        title: 'Mũ bảo hiểm',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small-4.png'),
      },
      {
        title: 'Giày búp bê',
        description: '112 sản phẩm',
        image: publicUrl('/assets/images/suggest/secondary/small-5.png'),
      },
    ],
  }
];

const SecondaryProduct = ({banners}) => {
  const item = banners && banners.length ? banners[0] : {};
  return (
    <Link to={item.link || '/'} className="secondary-product d-flex-column d-flex-center f-w-50">
      <div className="secondary-image p-10">
        {
          item.image && item.image.url &&
          <img src={imageUrl(item.image.url)} alt={``}/>
        }
      </div>
      <div className="title">{item.name || 'N/A'}</div>
      <div className="description">{item.title || 'N/A'}</div>
    </Link>
  );
};

const PrimaryProduct = ({banners}) => {
  const item = banners && banners.length ? banners[0] : {};
  const wrapperStyle = {}, textStyle = {};
  if (item.background_color) {
    const colors = item.background_color.split('|');
    if (colors.length >= 1) {
      wrapperStyle.backgroundColor = colors[0];
    }
    if (colors >= 2) {
      textStyle.color = colors[1];
    }
  }
  return (
    <Link to={item.link || '/'} className="primary-product d-flex-row" style={wrapperStyle}>
      <div className="titles d-flex-column d-flex-center f-w-50">
        <div className="titles-wrap">
          <div className="title text-uppercase fw-500" style={textStyle}>{item.name || 'N/A'}</div>
          <div className="description">{item.title || 'N/A'}</div>
        </div>
      </div>
      <div className="images d-flex-row">
        {
          item.image && item.image.url &&
          <img src={imageUrl(item.image.url)} alt={``}/>
        }
      </div>
    </Link>
  );
};

const SuggestItem = ({groupIndex, bannerByPositions}) => {
  if (!bannerByPositions) {
    return null;
  }
  return (
    <div className="suggest-item">
      <PrimaryProduct banners={bannerByPositions[`suggest_search_${groupIndex * 3 + 1}`]}/>
      <div className="secondary-products d-flex-row">
        <SecondaryProduct banners={bannerByPositions[`suggest_search_${groupIndex * 3 + 2}`]}/>
        <SecondaryProduct banners={bannerByPositions[`suggest_search_${groupIndex * 3 + 3}`]}/>
      </div>
    </div>
  );
}; 

const SuggestCategorySection = ({bannerByPositions}) => {
  return (
    <section className="section suggest-category-section">
      <div className="container">
        <div className="section-heading no-line-through">
          <h2>
            Gợi ý cho bạn
          </h2>
          <div className="link-read-more">
            <Link to="/products/flash-sale">Xem Thêm</Link>
          </div> 
        </div>
        <div className="section-inner">
          <div className="suggest-items">
            <SuggestItem groupIndex={0} bannerByPositions={bannerByPositions}/>
            <SuggestItem groupIndex={1} bannerByPositions={bannerByPositions}/>
            <SuggestItem groupIndex={2} bannerByPositions={bannerByPositions}/>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  bannerByPositions: state.common.banners.byPositions
});

export default connect(mapStateToProps)(React.memo(SuggestCategorySection));
