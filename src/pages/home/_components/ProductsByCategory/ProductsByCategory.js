import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {Link} from 'react-router-dom';
import {getProductListAPI} from '../../../../api/products';

class ProductsByCategory extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { 
      productList: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  getProducts = () => {
    this.getProductsAsync().catch(error => {
      // TODO
    });
  };

  getProductsAsync = async () => {
    const {category} = this.props;
    this.setState({
      isLoading: true
    });
    const {data: {list: products}} = await getProductListAPI({
      current_page: 0,
      page_size: 6,
      category_id: category.id,
    }, {__auth: true});
    this.setState({
      productList: products,
      isLoading: false,
    });
  };

  render() {
    const {category} = this.props;
    const {productList} = this.state;
    if (!category || !productList.length || productList.length < 6) {
      return null;
    }
    return (
      <section className={`section viewed-section`}>
        <div className="container">
          <div className="section-heading no-line-through">
            <h2>
              {category.name}
            </h2>
            <div className="link-read-more">
              <Link to={`/${category.slug}/c${category.id}`}>Xem Thêm</Link>
            </div>
          </div>
          <div className="section-inner bg-white rounded p-5">
            <div className="row product-items">
              {
                productList.map((item, index) =>
                  <div className="product-item-wrapper" key={index}>
                    <ProductItem product={item}/>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </section>
    )
  }
}

ProductsByCategory.propTypes = {
  category: PropTypes.object,
};

export default ProductsByCategory;
