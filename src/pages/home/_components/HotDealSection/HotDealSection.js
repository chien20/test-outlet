import React from 'react';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {Link} from 'react-router-dom';
import CountDown from '../../../../components/CountDown/CountDown';
import {getProductListAPI} from '../../../../api/products';
import {isMobile} from '../../../../common/helpers/browser';
import {getRunningEventsAPI} from '../../../../api/events';
import Carousel from "nuka-carousel";
import SliderProps from "../../../../components/NukaSlider/SliderProps";

const sliderProps = SliderProps;

class HotDealSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      event: null,
      productList: [],
      isLoading: false
    };
  }

  componentDidMount() {
    this.getRunningEvents().catch(error => {
      // TODO
    });
  }

  getRunningEvents = async () => {
    this.setState({
      isLoading: true
    });
    const {data: {list: events}} = await getRunningEventsAPI({
      sort_by: 'from',
      sort_order: 'asc'
    });
    const event = events.find(item => item.is_flash_sale) || null;
    if (!event) {
      return;
    }
    event.url = `/flash-sale/${event.slug}/e${event.id}`;
    const {data: {list: products}} = await getProductListAPI({
      event_id: event.id,
      current_page: 0,
      page_size: isMobile ? 16 : 24
    });
    this.setState({
      event,
      productList: products,
      isLoading: false
    });
  };

  render() {
    const {productList, event} = this.state;
    if (!event) {
      return null;
    }
    const toDate = new Date();
    toDate.setTime(event.to);

    return (
      <section className={`section flash-sale-section ${!isMobile ? 'bg-white p-b-50' : ''}`}>
        <div className="container">
          <div className="section-heading">
            <h2>
              <span className="d-inline-block">
                {
                  !isMobile &&
                  <span>Flash Sale Up To 80% Off</span>
                }
                {
                  isMobile &&
                  <span>Flash sale</span>
                }
              </span>
              <CountDown endTime={toDate}/>
            </h2>
            <Link to="/products/flash-sale">Xem Thêm</Link>
          </div>
          <div className="section-inner">
            {
              isMobile &&
              <Carousel
                className="product-items --slider"
                slidesToShow={2}
                slidesToScroll={2}
                {...sliderProps}
              >
                {
                  productList.map((item, index) => (
                      <div className="product-item-wrapper" key={index}>
                        <ProductItem product={item} showQtyLeft={true}/>
                      </div>
                  ))
                }
              </Carousel>
            }
            {
              !isMobile &&
              <Carousel
                className="product-items --slider"
                slidesToShow={6}
                slidesToScroll={3}
                speed={1200}
                cellSpacing={10}
                {...sliderProps}
              >
                {
                  productList.map((item, index) => (
                      <div className="product-item-wrapper" key={index}>
                        <ProductItem product={item} showQtyLeft={true}/>
                      </div>
                  ))
                }
              </Carousel>
            }
          </div>
        </div>
      </section>
    )
  }
}

export default HotDealSection;
