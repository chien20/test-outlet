import React from 'react';
import './SaleBannerSection.scss';
import {Link} from 'react-router-dom';


const SaleBannerSection = () => {
  return (
    <section className="section favorite-colection-section">
      <div className="container">
        <div className="section-inner">
            <div className="sale-banners">
                <Link to="/">
                <div className="sale-banner">
                    <img src="assets/images/sale-banners/sale-banner-1.png" alt="big-sale-50%"/>
                    <h3 className="sale-banner-title">Big Sale</h3>
                    <p className="sale-banner-des">50% off</p>
                </div>
                </Link>
                <Link to="/">
                    <div className="sale-banner">
                        <img src="assets/images/sale-banners/sale-banner-2.png" alt="big-sale-50%"/>
                        <h3 className="sale-banner-title">Big Sale</h3>
                        <p className="sale-banner-des">50% off</p>
                    </div>
                </Link>
            </div>
        </div>
      </div>
    </section>
  );
};


export default React.memo(SaleBannerSection);
