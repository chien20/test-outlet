import React from "react";
import "./BannerMiddle.scss"
import {publicUrl} from "../../../../common/helpers";

class BannerMiddle extends React.PureComponent{
  render() {
    return (
      <section className="section banner-section">
       <div className="container">
         <div className="banner-mid">
           <img src={publicUrl(`/assets/images/banners/banner-home-mid-7-7.png`)} alt="banner-mid"/>
         </div>
       </div>
      </section>
    )
  }
}

export default BannerMiddle;