import React from 'react';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {Link} from 'react-router-dom';
import {getProductListAPI} from '../../../../api/products';
import "./ViewedProducts.scss";

class ViewedProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.isAuthenticated !== this.props.isAuthenticated) {
      this.getProducts();
    }
  }

  getProducts = () => {
    this.getProductsAsync().catch(error => {
      // TODO
    });
  };

  getProductsAsync = async () => {
    this.setState({
      isLoading: true
    });
    const {data: {list: products}} = await getProductListAPI({
      current_page: 0,
      page_size: 6,
      type: 'viewed',
    }, {__auth: true});
    this.setState({
      productList: products,
      isLoading: false,
    });
  };

  render() {
    const {productList} = this.state;
    if (!productList.length) {
      return null;
    }
    return (
      <section className={`section viewed-section`}>
        <div className="container">
          <div className="section-heading no-line-through">
            <h2>
              Sản phẩm đã xem
            </h2>
            <div className="link-read-more">
              <Link to="/products/history">Xem thêm</Link>
            </div>
          </div>
          <div className="section-inner rounded">
            <div className="row product-items">
              {
                productList.map((item, index) =>
                  <div className="product-item-wrapper" key={index}>
                    <ProductItem product={item}/>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </section> 
    )
  }
}

export default ViewedProducts;
