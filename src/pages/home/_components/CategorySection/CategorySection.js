import React from 'react';
import {Link} from 'react-router-dom';
import './CategorySection.scss';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import {connect} from 'react-redux';

const MenuItem = ({item}) => {
  const itemUrl = `/${item.slug}/c${item.id}`;
  const itemImage = item.avatar_url ? imageUrl(item.avatar_url) : publicUrl('/assets/images/no-image/256x256.png');
  return (
    <li>
      <Link to={itemUrl}>
        <div className="img-figure">
          <img src={itemImage} alt={item.name}/>
        </div>
        <span>{item.name}</span>
      </Link>
    </li>
  );
};

class CategorySection extends React.PureComponent {
  render() {
    const {category} = this.props;
    const width = Math.ceil(category.length / 2) * 112 + 2;
    return (
      <section className="section category-section">
        <div className="container">
          <div className="section-heading">
            <h2 className="text-uppercase">Danh mục</h2>
          </div>
          <div className="section-inner">
            <ul style={{width}}>
              {
                category.map((item, index) => (
                  <MenuItem item={item} key={index}/>
                ))
              }
            </ul>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  category: state.common.category.tree
});

export default connect(mapStateToProps)(CategorySection);