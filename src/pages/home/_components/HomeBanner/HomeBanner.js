import React from 'react';
import {connect} from 'react-redux';
import {publicUrl} from '../../../../common/helpers';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import './HomeBanner.scss';
import Adv from '../../../../components/Adv/Adv';

class HomeBanner extends React.PureComponent {

  render() {
    // const {bannerByPositions} = this.props;
    return (
      <section className="home-banner bg-white">
        <div className="container">
          <div className="e-row">
            <div className="col-main offset-left">
              <div className="d-flex-row">
                <div className="primary-banners">
                  <Adv position="main_slider" className="--primary"/>
                  <div className="--secondaries d-flex-row">
                    <Adv position="main_banner_1" className="--secondary f-w-50"/>
                    <Adv position="main_banner_2" className="--secondary f-w-50"/>
                  </div>
                </div>
                <div className="secondary-banners">
                  <Adv position="main_banner_3" className="--primary"/>
                  <Adv position="main_banner_4" className="--secondary"/>
                </div>
                <div className="other-services">
                  <HyperLink underConstruction={true} className="service-item">
                    <img src={publicUrl('/assets/images/icons/icon-uu-dai.svg')} alt={``}/>
                    <span>Ưu đãi đối tác</span>
                  </HyperLink>
                  <HyperLink underConstruction={true} className="service-item">
                    <img src={publicUrl('/assets/images/icons/icon-dat-phong.svg')} alt={``}/>
                    <span>Đặt phòng khách sạn</span>
                  </HyperLink>
                  <HyperLink underConstruction={true} className="service-item">
                    <img src={publicUrl('/assets/images/icons/icon-may-bay.svg')} alt={``}/>
                    <span>Đặt vé máy bay</span>
                  </HyperLink>
                  <HyperLink underConstruction={true} className="service-item">
                    <img src={publicUrl('/assets/images/icons/icon-dat-hang.svg')} alt={``}/>
                    <span>Đặt hàng sản phẩm</span>
                  </HyperLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = (state) => ({
  bannerByPositions: state.common.banners.byPositions
});

export default connect(mapStateToProps)(HomeBanner);
