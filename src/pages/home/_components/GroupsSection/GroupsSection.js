import React from 'react';
import {Link} from 'react-router-dom';
import {isMobile} from '../../../../common/helpers/browser';
import {getGroupMemberCountAPI, getSuggestGroupsAPI} from '../../../../api/groups';
import GroupItem from '../../../../components/Group/GroupItem/GroupItem';
import {publicUrl, showAlert} from '../../../../common/helpers';
import './GroupsSection.scss';

class GroupsSection extends React.PureComponent {
  constructor(props) {
    super(props); 
    this.state = {
      groups: [],
      isLoading: false,
      groupMemberCount : {}
    };
  } 

  componentDidMount() {
    this.getGroups().catch(error => {
      // TODO
    });
  }

  getGroups = async () => {
    this.setState({
      isLoading: true
    });
    const {data: {list: groups}} = await getSuggestGroupsAPI({
      current_page: 0,
      page_size: 8,
    });
    this.getGroupMemberCount(groups);
    this.setState({
      groups,
      isLoading: false,
    });
  };

  getGroupMemberCount = (groups) => {
    const ids = groups.map(group => group.id);
    getGroupMemberCountAPI(ids).then(res => {
      this.setState(prevState => ({
        groupMemberCount: {
          ...prevState.groupMemberCount,
          ...res.data,
        }
      }));
    }).catch(error => {
      console.log(error);
      showAlert({
        type: 'error',
        message: `Không tải được thành viên hội nhóm!`
      })
    });
  }

  render() {
    const {groups, groupMemberCount} = this.state;
    console.log(groupMemberCount);
    if (!groups.length) {
      return null;
    }
    return (
      <section className="section groups-section">
        <div className="container">
          <div className="section-heading no-line-through">
            <h2>
              Hội nhóm
            </h2>

            <div className="flex-wrap">
              {
                !isMobile &&
                <div className="sub-title">
                  <div className="feature">
                    <i className="far fa-check-square"/> Ưu đãi giảm giá
                  </div>
                  <div className="feature">
                    <i className="far fa-check-square"/> Giao hàng miễn phí
                  </div>
                  <div className="feature">
                    <i className="far fa-check-square"/> Chia sẻ bạn bè
                  </div>
                </div>
              }
              <div className="link-read-more">
                {
                  isMobile ? <Link to="/groups/register">Tạo hội nhóm</Link> 
                  :<Link to="/groups">Xem thêm</Link>
                }
              </div>
            </div>
          </div>
          <div className="section-inner">
            {
              isMobile &&
              <div className="sub-title">
                <div className="feature">
                  <img src={publicUrl('/assets/images/icons/icon-tick.svg')} alt=""/>
                  <p>Ưu đãi giảm giá</p>
                </div>
                <div className="feature">
                  <img src={publicUrl('/assets/images/icons/icon-tick.svg')} alt=""/>
                  <p>Giao hàng miễn phí</p>
                </div>
                <div className="feature">
                  <img src={publicUrl('/assets/images/icons/icon-tick.svg')} alt=""/>
                  <p>Chia sẻ bạn bè</p>
                </div>
              </div>
            }
            <div className="groups">
              {
                !isMobile &&
                <div className="groups-image">
                  <img src={publicUrl('/assets/images/backgrounds/bg-groups-home-pc.svg')} alt="Home"/>
                  <div className="caption">
                    <div className="title">Khám phá</div>
                    <div className="title size-xl">Hội nhóm<br/> của bạn</div>
                    <div className="sub-title">Hàng ngàn ưu đãi</div>
                  </div>
                </div>
              }
              {
                  isMobile &&
                  <div className="groups-image">
                    <img src={publicUrl('/assets/images/backgrounds/bg-groups-home-mb.png')} alt="groups"/>
                    <div className="groups-image-info">
                      <h4>Tham gia hội nhóm cùng bạn</h4>
                      <span>Nhận ngay ưu đãi</span>
                      <Link to="/groups">
                        <button>Tham gia</button>
                      </Link>
                    </div>
                  </div>
              }
              <div className="group-items">
                {
                  groups.map((item, index) => (
                    <GroupItem
                      key={index}
                      group={item}
                      showMemberCount={true}
                      memberCount={groupMemberCount[item.id]}
                    />
                  ))
                }
              </div>
            </div>
            {
              isMobile &&
              <div className="group-read-more">
                <Link to="/groups">Xem thêm</Link>
              </div>
            }
          </div>
        </div>
      </section>
    )
  }
}

export default GroupsSection;
