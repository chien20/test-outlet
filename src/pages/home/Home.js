import React from 'react';
import {LazyLoadModule} from '../../layouts/LazyLoadModule';
import {isMobile} from '../../common/helpers/browser';
import './Home.scss';

const Home = () => {
  if (isMobile) {
    return (
      <LazyLoadModule resolve={() => (
        import('./HomeMobile/HomeMobile')
      )}/>
    )
  }
  return (
    <LazyLoadModule resolve={() => (
      import('./HomePC')
    )}/>
  )
};

export default Home;