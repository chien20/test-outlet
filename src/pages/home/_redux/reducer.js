import createReducer from '../../../common/utils/redux/createReducer';

const initialState = {};

const handlers = {};

export default createReducer(initialState, handlers);
