import React from 'react';
import {Helmet} from 'react-helmet';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import SearchBoxMobile from '../../../components/HeaderMobile/SearchBoxMobile/SearchBoxMobile';
import CartIconMobile from '../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import BarDrawerIconMobile from '../../../components/HeaderMobile/BarDrawerIconMobile/BarDrawerIconMobile';
// import HotDealSection from '../_components/HotDealSection/HotDealSection';
// import SuggestCategorySection from '../_components/SuggestSection/SuggestCategorySection';
import FavoriteCollectionSection from '../_components/FavoriteCollectionSection/FavoriteCollectionSection';
import PopularSearchSection from '../_components/PopularSearchSection/PopularSearchSection';
import SaleBannerSection from '../_components/SaleBannerSection/SaleBannerSection';
import SliderMobile from './SliderMobile/SliderMobile';
import WalletMobile from './Wallet/WalletMobile';
import CategoryMobile from './CategoryMobile/CategoryMobile';
import FlashSaleSection from './FlashSaleSection/FlashSaleSection';
import ViewedProducts from '../_components/ViewedProducts/ViewedProducts';
import GroupsSection from '../_components/GroupsSection/GroupsSection';
import SuggestProductSectionMb from '../_components/SuggestSection/SuggestProductSectionMb';

class HomeMobile extends React.PureComponent {
  render() {
    return (
      <div className="home-page">
        <Helmet>
          <title>Outlet</title>
        </Helmet>
        <div className="header-mobile-home pd-t-16">
          <HeaderMobile className="home-header">
            <BarDrawerIconMobile/>
            <img src="/assets/images/logo/logo-mobile.svg" className="header-mobile-logo" alt=""/>
            <CartIconMobile/>
          </HeaderMobile>
          <SearchBoxMobile/>
        </div>
        <SliderMobile/>
        <WalletMobile/>
        <CategoryMobile/>
        {/* <HotDealSection/> */}
        <FlashSaleSection/>
        <ViewedProducts/>
        {/* <SuggestCategorySection/> */}
        <FavoriteCollectionSection/>
        <PopularSearchSection/>
        <SaleBannerSection/>
        <GroupsSection/>
        <SuggestProductSectionMb/>
      </div>
    )
  }
}

export default HomeMobile;
