import React from 'react';
import {publicUrl} from '../../../../common/helpers';
import './WalletMobile.scss';

class WalletMobile extends React.PureComponent {
  render() {
    return (
      <div className="wallet-mobile">
        <div className="wallets">
            <div className="wallet-item pay">
            <div className="wallet-title">
                <img 
                src={publicUrl('assets/images/icons/icon-airpay.svg')}
                alt="airpay">
                </img>
                <h5>Ưu đãi ví AirPay</h5>
            </div>
            <p className="wallet-des">Voucher giảm 10%</p>
            </div>
            <div className="wallet-item coin">
            <div className="wallet-title">
                <img 
                src={publicUrl('assets/images/icons/icon-coin.svg')}
                alt="airpay">
                </img>
                <h5>Xu</h5>
            </div>
            <p className="wallet-des">Đổi xu lấy mã giảm giá</p>
            </div>
        </div>
      </div>
    );
  }
}

export default WalletMobile;
