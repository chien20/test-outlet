import React from 'react';
import Slider from 'react-slick';
import connect from 'react-redux/es/connect/connect';
import Banner from '../../../../components/Adv/Banner';
import HeaderMobile from '../../../../components/HeaderMobile/HeaderMobile';
import SearchBoxMobile from '../../../../components/HeaderMobile/SearchBoxMobile/SearchBoxMobile';
import CartIconMobile from '../../../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import BarDrawerIconMobile from '../../../../components/HeaderMobile/BarDrawerIconMobile/BarDrawerIconMobile';
import './SliderMobile.scss';

class SliderMobile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0
    };
  }


  slickSettings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    infinite: true,
    autoplay: true
  };

  afterChange = (event) => {
    this.setState({
      currentIndex: event
    });
  };

  render() {
    const {bannerByPositions} = this.props;
    const {currentIndex} = this.state;
    const sliderImages = bannerByPositions['slider_mobile'] || [];
    if (!sliderImages || !sliderImages.length) {
      return null;
    }
    let background = 'rgba(255, 255, 255, 0)';
    if (sliderImages[currentIndex] && sliderImages[currentIndex].background_color) {
      background = sliderImages[currentIndex].background_color;
    }
    return (
      <div className="slider-mobile-wrapper">
        <Slider className="slider slider-bl" {...this.slickSettings} afterChange={this.afterChange}>
          {
            sliderImages.map((item, index) => (
              <Banner banner={item} key={index}/>
            ))
          }
        </Slider>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  bannerByPositions: state.common.banners.byPositions
});

export default connect(mapStateToProps)(SliderMobile);