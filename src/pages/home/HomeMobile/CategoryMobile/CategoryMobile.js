import React from 'react';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import './CategoryMobile.scss';
import {Nav} from "react-bootstrap";

class CategoryMobile extends React.PureComponent {
  underConstruction = (event) => {
    event.preventDefault();
    alert('Tính năng đang được phát triển');
  };  

  render() {
    const {categories} = this.props;
    return (
      <div className="category-mobile">
        <div className="categories">
           <Link className="categories-item" to="/categories">
              <img
                src={publicUrl('assets/images/icons/icon-danh-muc.svg')}
                alt="Danh mục"
              />
              <span>Danh mục sản phẩm</span>
           </Link>
           {
              categories.map( (item, index) => (
                    <Link className="categories-item" key={index} to={`/${item.slug}/c${item.id}`} eventKey={index}>
                      <img src={imageUrl(item.icon_url)} alt="Icon"/>
                      <span>{item.name}</span>
                    </Link> 
              ))
           }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  categories: state.common.category.tree,
});

export default connect(mapStateToProps)(CategoryMobile);
