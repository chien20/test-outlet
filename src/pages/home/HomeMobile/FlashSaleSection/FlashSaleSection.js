import React, {Fragment} from 'react';
import {getProductListAPI} from '../../../../api/products';
import ProductItem from '../../../../components/ProductItem/ProductItem';
import {getRunningEventsAPI} from '../../../../api/events';
import CountDown from '../../../../components/CountDown/CountDown';
import './FlashSaleSection.scss';
import {isMobile} from "../../../../common/helpers/browser";
import {Link} from 'react-router-dom';
import {publicUrl} from '../../../../common/helpers';
import NoData from '../../../../components/NoData/NoData';
import HTMLView from "../../../../components/View/HTMLView";

class FlashSaleSection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      event: null,
      isLoading: false,
      productList: [],
      totalItems: 0,
      sortParams: {
        by: '',
        order: ''
      },
      nextPage: 0
    };
  }

  // componentDidMount() {
  //   this.getRunningEvents().catch(error => {
  //     // TODO
  //   });
  // }

  // getRunningEvents = async () => {
  //   this.setState({
  //     isLoading: true
  //   });
  //   const {data: {list: events}} = await getRunningEventsAPI({
  //     sort_by: 'from',
  //     sort_order: 'asc'
  //   });
  //   const event = events.find(item => item.is_flash_sale) || null;
  //   if (!event) {
  //     return;
  //   }
  //   event.url = `/flash-sale/${event.slug}/e${event.id}`;
  //   this.setState({
  //     event,
  //     isLoading: false
  //   }, this.getProductListMore);
  // };

  // getProductListMore = () => {
  //   const {nextPage, isLoading, sortParams, event} = this.state;
  //   if (isLoading || !event) {
  //     return;
  //   }
  //   const params = {
  //     current_page: nextPage,
  //     page_size: 20,
  //     event_id: event.id
  //   };
  //   if (sortParams.by && sortParams.order) {
  //     params.sort_by = sortParams.by;
  //     params.sort_order = sortParams.order;
  //   }
  //   this.setState({
  //     isLoading: true
  //   });
  //   getProductListAPI(params).then(res => {
  //     if (res && res.data && res.data.list && res.data.page_info) {
  //       this.setState(prevState => {
  //         const pageInfo = res.data.page_info || {};
  //         return {
  //           isLoading: false,
  //           productList: [...prevState.productList, ...res.data.list],
  //           hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
  //           nextPage: pageInfo.current_page + 1,
  //           totalItems: pageInfo.total_items || 0
  //         };
  //       });
  //     } else {
  //       this.setState({
  //         isLoading: false
  //       });
  //     }
  //   }).catch(error => {
  //     this.setState({
  //       isLoading: false
  //     });
  //   });
  // };


  componentDidMount() {
    this.getProducts();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isAuthenticated !== this.props.isAuthenticated) {
      this.getProducts();
    }
  }

  getProducts = () => {
    this.getProductsAsync().catch(error => {
    });
  };

  getProductsAsync = async () => {
    this.setState({
      isLoading: true
    });
    const {data: {list: products}} = await getProductListAPI({
      current_page: 0,
      page_size: 20,
      type: 'recommend',
    }, {__auth: true});
    this.setState({
      productList: products,
      isLoading: false,
    });
  };

  render() {
    const {productList, event} = this.state;
    // if (!event) {
    //   return null
    // }
    const toDate = new Date();
    // toDate.setTime(event.to);
    return (
      <Fragment> 
        <section className="section flash-sale-section-mb">
          <div className="container">
            <div className="section-heading">
              {
                isMobile && 
                <>
                <h2>F<span>
                  <img 
                  src={publicUrl('assets/images/icons/icon-flash.svg')}
                  alt="flash">
                  </img>
                </span>ash <span>Sale</span>
                <CountDown endTime={toDate}/>
                </h2>
                <div className="link-read-more">
                  <Link to="/products/flash-sale">Xem thêm</Link>
                </div>
                </>
              }
            </div>
            <div className="section-inner">
              <div className="row product-items">
                {
                  productList.map((item, index) => (
                    <div className="product-item-wrapper" key={index}>
                      <ProductItem product={item}/>
                      <div className="bar-flash-sale">
                        <div className="bar-sold"></div>
                        <span className="quantity-sold">Đã bán 20</span>
                      </div>
                    </div>
                  )) 
                }
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default FlashSaleSection;
