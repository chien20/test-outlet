import React from 'react';
import {connect} from 'react-redux';
import HotDealSection from './_components/HotDealSection/HotDealSection';
import HomeBanner from './_components/HomeBanner/HomeBanner';
import SuggestCategorySection from './_components/SuggestSection/SuggestCategorySection';
import {Helmet} from 'react-helmet';
import ViewedProducts from "./_components/ViewedProducts/ViewedProducts";
import GroupsSection from "./_components/GroupsSection/GroupsSection";
import BannerMiddle from "./_components/BannerMiddle/BannerMiddle";
import SuggestProductSectionPc from './_components/SuggestSection/SuggestProductSectionPc';
import ProductsByCategory from './_components/ProductsByCategory/ProductsByCategory';

class HomePC extends React.PureComponent {
  render() {
    const {isAuthenticated, categories} = this.props;
    return (
      <div className="home-page">
        <Helmet>
          <title>Outlet</title>
        </Helmet>
        <HomeBanner/>
        <HotDealSection/>
        <SuggestCategorySection/>
        <GroupsSection/>
        <BannerMiddle/>
        <SuggestProductSectionPc/>
        <ViewedProducts isAuthenticated={isAuthenticated}/>
        {
          categories && categories.length &&
          categories.map((category, index) => (
            <ProductsByCategory category={category} key={index}/>
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.user.auth.isAuthenticated,
  categories: state.common.category.tree
});

export default connect(mapStateToProps)(HomePC);
