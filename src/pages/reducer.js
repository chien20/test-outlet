import {combineReducers} from 'redux';
import home from './home/_redux/reducer';
import order from './order/_redux/reducer';
import user from './user/_redux/reducer';

export default combineReducers({
  home,
  order,
  user
});
