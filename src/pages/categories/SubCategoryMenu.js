import React, {Fragment} from 'react';

import {Link} from "react-router-dom";
import ProductGrid from '../product/_components/ProductGrid/ProductGrid';
import {history} from '../../common/utils/router/history';

class SubCategoryMenu  extends React.PureComponent {
  onPageChanged = (page) => {
    const {match: {params: {id}}, categoryList} = this.props;
    if (!categoryList || !categoryList.length) {
      return null;
    }
    const category = categoryList.find(item => `${item.id}` === `${id}`);
    if (page > 1) {
      history.push(`/${category.slug}/c${category.id}/${page}`);
    } else {
      history.push(`/${category.slug}/c${category.id}`);
    }
  };


  render() {
    const {subCategory} = this.props;
    return (
      <div className="sub-category">
        <div className="title">
          <h3>{subCategory.name}</h3>
          <Link to={`/${subCategory.slug}/c${subCategory.id}`}>Xem thêm</Link>
        </div>
        <div className="content">
          <div className="sub-categories">
                  <ProductGrid key={`category-${subCategory.id}`} pageSize={4} onPageChanged={this.onPageChanged} queryParams={{category_id: subCategory.id}}/>
          </div>
        </div>
      </div>
    );
  }

};

export default SubCategoryMenu;