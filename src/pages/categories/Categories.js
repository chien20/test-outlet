import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import HeaderMobile from '../../components/HeaderMobile/HeaderMobile';
import SearchBoxMobile from '../../components/HeaderMobile/SearchBoxMobile/SearchBoxMobile';
import CartIconMobile from '../../components/HeaderMobile/CartIconMobile/CartIconMobile';
import SubCategoryMenu from './SubCategoryMenu';
import {connect} from 'react-redux';
import {imageUrl, publicUrl} from '../../common/helpers';
import './Categories.scss';
import {Tab, Nav} from "react-bootstrap";

class Categories extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isActive : false
    }
  } 

  handleClick = (e) => {
    e.preventDefault();
    this.setState({
      isActive : true
    })
  }
  render() {
    
    const {categories} = this.props;
    return ( 
        <Fragment>
          <Helmet>
            <title>Danh mục sản phẩm</title>
          </Helmet>
          <HeaderMobile className="header-mobile-categories">
            <SearchBoxMobile/>
            <CartIconMobile/>
          </HeaderMobile>

          <div className="categories-page common-page"> 
            <div className="wrapper">
              <Tab.Container defaultActiveKey={0}>
                <Nav className="categories-menu-sidebar">
                    {
                      categories.map( (item, index) => (
                          <Nav.Item className="category-item" key={index}>
                            <Nav.Link eventKey={index}>
                              <img src={imageUrl(item.icon_url)} alt="Icon"/>
                              <label>{item.name}</label>
                            </Nav.Link>
                          </Nav.Item>
                      ))
                    }
                </Nav>
                <Tab.Content className="categories-content">
                
                  {
                    categories.map((item,index) => ( 
                        <Tab.Pane eventKey={index} key={index}>
                          {
                            item.children &&
                              <Fragment>
                                {
                                  item.children.map((item,index) =>
                                    <SubCategoryMenu subCategory={item} key={index}/>
                                  )
                                }
                              </Fragment>
                          }
                          {/* <div className="link-more">
                            <Link to={`/${item.slug}/c${item.id}`}>Xem thêm</Link>
                          </div> */}
                        </Tab.Pane> 
                    ))
                  }
                </Tab.Content>

              </Tab.Container>
            </div>
          </div>
        </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  categories: state.common.category.tree,
});

export default connect(mapStateToProps)(Categories);