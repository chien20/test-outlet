import orderWatchers from './order/_redux/sagas';
import userWatchers from './user/_redux/sagas';

export default [
  orderWatchers(),
  ...userWatchers
];
