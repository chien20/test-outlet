import React, {Fragment} from 'react';
import {getFAQQuestionAPI} from '../../api/faq';
import {showAlert} from '../../common/helpers';
import {isMobile} from '../../common/helpers/browser';
import HeaderMobile from '../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../components/HeaderMobile/TitleMobile/TitleMobile';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import HTMLView from '../../components/View/HTMLView';
import './FAQQuestion.scss';

class FAQQuestion extends React.PureComponent {
  state = {
    isLoaded: false,
    question: null,
  };

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.match?.params?.id !== this.props.match?.params?.id) {
      this.getData();
    }
  }

  getData = () => {
    const {match: {params: {id}}} = this.props;
    getFAQQuestionAPI(id).then(res => {
      this.setState({
        question: res.data,
        isLoaded: true,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: 'Không tải được dữ liệu',
      });
    });
  };

  render() {
    const {isLoaded, question} = this.state;
    if (!isLoaded) {
      return null;
    }
    return (
      <Fragment>
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30 m-l-5">{question.name}</TitleMobile>
          </HeaderMobile>
        }
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Trung tâm trợ giúp"/>
            <BreadcrumbItem text={question.name} isActive={true}/>
          </Breadcrumb>
        }
        <div className="common-page faq-question">
          <div className="container">
            <div className="paper">
              {
                !isMobile &&
                <h1 className="title">{question.name}</h1>
              }
              <div className="page-content">
                <HTMLView html={question.content}/>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default FAQQuestion;
