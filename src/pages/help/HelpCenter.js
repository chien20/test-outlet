import React, {Fragment} from 'react';
import {isMobile} from '../../common/helpers/browser';
import HeaderMobile from '../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../components/HeaderMobile/TitleMobile/TitleMobile';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import {getFAQCategoriesAPI, getFAQQuestionsAPI} from '../../api/faq';
import _chunk from 'lodash/chunk';
import FAQTopic from '../../components/FAQ/FAQTopic';
import {showAlert} from '../../common/helpers';
import './HelpCenter.scss';

class HelpCenter extends React.PureComponent {
  state = {
    isLoaded: false,
    categories: [],
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    this.getDataAsync().then(state => {
      this.setState({
        ...state,
        isLoaded: true,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: 'Không tải được dữ liệu',
      });
    });
  };

  getDataAsync = async () => {
    const {data: {list: categories}} = await getFAQCategoriesAPI({page_size: 100, sort_by: 'id', sort_order: 'asc'});
    const questionRes = await this.getQuestions(categories);
    const categoryQuestions = questionRes.map(item => item?.data?.list || []).reduce((result, questions) => {
      if (questions?.length) {
        result[questions[0].category_id] = questions;
      }
      return result;
    }, {});
    for (let i = categories.length - 1; i >= 0; i--) {
      const category = categories[i];
      if (!categoryQuestions[category.id]?.length) {
        categories.splice(i, 1);
      } else {
        category.questions = categoryQuestions[category.id];
      }
    }
    return {
      categories,
    };
  };

  getQuestions = async (categories) => {
    return Promise.all(categories.map(category => getFAQQuestionsAPI({
      category_id: category.id,
      page_size: 10,
      sort_by: 'id',
      sort_order: 'asc',
    })));
  };

  render() {
    return (
      <Fragment>
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30 m-l-5">Trung tâm trợ giúp</TitleMobile>
          </HeaderMobile>
        }
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Trung tâm trợ giúp" isActive={true}/>
          </Breadcrumb>
        }
        <div className="common-page help-center">
          <div className="container">
            <div className="paper">
              {
                !isMobile &&
                <h1 className="title">Trung tâm trợ giúp</h1>
              }
              <div className="page-content">
                <div className="search-section">
                  <div className="search-input">
                    <input type="text" placeholder="Tìm kiếm..."/>
                    <i className="fas fa-search"/>
                    <div className="example">Ví dụ: tài khoản, đặt hàng, epoint, hội nhóm...</div>
                  </div>
                </div>
                {
                  this.renderContent()
                }
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }

  renderContent = () => {
    const {isLoaded, categories} = this.state;
    if (!isLoaded) {
      return null;
    }
    const chunks = _chunk(categories, 2);
    return (
      <div className="topics">
        {
          chunks.map((chunk, chunkIndex) => (
            <div
              className="topic-row"
              key={chunkIndex}
            >
              {
                chunk.map((category, index) => (
                  <FAQTopic
                    key={index}
                    category={category}
                  />
                ))
              }
            </div>
          ))
        }
      </div>
    );
  };
}

export default HelpCenter;
