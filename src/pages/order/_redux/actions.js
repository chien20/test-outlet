export const GET_CART = 'GET_CART';
export const GET_CART_SILENT = 'GET_CART_SILENT';
export const GET_CART_SUCCESS = 'GET_CART_SUCCESS';
export const GET_CART_FAILED = 'GET_CART_FAILED';
export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_TO_CART_OFFLINE = 'ADD_TO_CART_OFFLINE';
export const UPDATE_CART_PRODUCT = 'UPDATE_CART_PRODUCT';
export const UPDATE_CART_PRODUCT_OFFLINE = 'UPDATE_CART_PRODUCT_OFFLINE';
export const DELETE_CART_PRODUCT = 'DELETE_CART_PRODUCT';
export const DELETE_CART_PRODUCT_OFFLINE = 'DELETE_CART_PRODUCT_OFFLINE';
export const CLEAN_CART = 'CLEAN_CART';
export const CHECKOUT_SUCCESS = 'CHECKOUT_SUCCESS';

export const TOGGLE_SELECT_CART_ITEM = 'TOGGLE_SELECT_CART_ITEM';
export const TOGGLE_SELECT_CART_ALL_ITEM = 'TOGGLE_SELECT_CART_ALL_ITEM';
export const TOGGLE_SELECT_CART_STORE = 'TOGGLE_SELECT_CART_STORE';

export const getCartAC = () => ({
  type: GET_CART
});

export const getCartSilentAC = () => ({
  type: GET_CART_SILENT
});

export const getCartSuccessAC = (cart) => ({
  type: GET_CART_SUCCESS,
  cart
});

export const getCartFailedAC = () => ({
  type: GET_CART_FAILED
});

export const addToCartAC = (data) => ({
  type: ADD_TO_CART,
  data
});

export const updateCartProductAC = (data) => ({
  type: UPDATE_CART_PRODUCT,
  data
});

export const deleteCartProductAC = (data) => ({
  type: DELETE_CART_PRODUCT,
  data
});

export const addToCartOfflineAC = (data) => ({
  type: ADD_TO_CART_OFFLINE,
  data
});

export const updateCartProductOfflineAC = (data) => ({
  type: UPDATE_CART_PRODUCT_OFFLINE,
  data
});

export const deleteCartProductOfflineAC = (data) => ({
  type: DELETE_CART_PRODUCT_OFFLINE,
  data
});

export const cleanCartAC = () => ({
  type: CLEAN_CART
});

export const checkoutSuccessAC = () => ({
  type: CHECKOUT_SUCCESS
});

export const toggleSelectCartItemAC = (id) => ({
  type: TOGGLE_SELECT_CART_ITEM,
  id,
});

export const toggleSelectCartStoreAC = (storeId, isSelected) => ({
  type: TOGGLE_SELECT_CART_STORE,
  storeId,
  isSelected,
});

export const toggleSelectCartAllItemAC = (isSelected) => ({
  type: TOGGLE_SELECT_CART_ALL_ITEM,
  isSelected,
});
