import createReducer from '../../../common/utils/redux/createReducer';
import {CLEAN_CART, GET_CART, GET_CART_FAILED, GET_CART_SUCCESS} from './actions';
import {LOGOUT} from '../../../redux/actions/user';

const initialState = {
  cart: {
    data: [],
    sub_total: 0,
    total_qty: 0,
    isLoading: true,
    hasError: false
  }
};

const handlers = {
  [GET_CART]: (state) => {
    return {
      ...state,
      cart: {
        ...state.cart,
        isLoading: true
      }
    }
  },
  [GET_CART_SUCCESS]: (state, action) => {
    const data = action.cart || [];
    let sub_total = 0;
    let qty = 0;
    if (Array.isArray(data) && data) {
      data.forEach(item => {
        const {product} = item;
        if (!item.group_id) {
          delete item.group_id;
        }
        item.display_price = product.price;
        item.display_original_price = product.original_price;
        item.display_name = product.name;
        if (item.product_option_id && product.options) {
          const option = product.options.find(option => option.id === item.product_option_id);
          if (option) {
            item.display_price = option.price;
            item.display_original_price = option.original_price;
            if (option.groups_index && option.groups_index.length) {
              if (option.groups_index.length === 1 && product.option_groups[0].values) {
                const variantName = product.option_groups[0].values[option.groups_index[0]];
                item.display_name = `${product.name} (${variantName})`;
              } else if (option.groups_index.length === 2
                && product.option_groups[0].values
                && product.option_groups[1].values) {
                const variantName = `${product.option_groups[0].values[option.groups_index[0]]} - ${product.option_groups[1].values[option.groups_index[1]]}`;
                item.display_name = `${product.name} (${variantName})`;
              }
            }
          }
        }
        item.sub_total = item.display_price * item.qty;
        sub_total += item.sub_total;
        qty += item.qty;
      });
    }
    return {
      ...state,
      cart: {
        data,
        sub_total,
        qty,
        isLoading: false,
        hasError: false
      }
    }
  },
  [GET_CART_FAILED]: (state) => {
    return {
      ...state,
      cart: {
        ...state.cart,
        isLoading: false,
        hasError: true
      }
    }
  },
  [CLEAN_CART]: (state) => {
    return {
      ...state,
      cart: {
        data: [],
        sub_total: 0,
        total_qty: 0,
        isLoading: false,
        hasError: false
      }
    }
  },
  [LOGOUT]: (state) => {
    return {
      ...state,
      cart: {
        data: [],
        sub_total: 0,
        total_qty: 0,
        isLoading: true,
        hasError: false
      }
    }
  }
};

export default createReducer(initialState, handlers);
