import {call, put, takeEvery} from 'redux-saga/effects';
import {
  addToCartAC,
  checkoutSuccessAC,
  cleanCartAC,
  deleteCartProductAC,
  getCartAC,
  getCartFailedAC,
  getCartSilentAC,
  getCartSuccessAC,
  TOGGLE_SELECT_CART_ALL_ITEM,
  TOGGLE_SELECT_CART_ITEM,
  TOGGLE_SELECT_CART_STORE,
  updateCartProductAC
} from './actions';
import {addToCartAPI, deleteCartAPI, getCartAPI, updateCartAPI} from '../../../api/cart';
import {showAlert} from '../../../common/helpers';
import Store from '../../../redux/store/Store';
import {openModal} from '../../../redux/actions/modals/modals';
import history from '../../../common/utils/router/history';
import {authenticationSuccessAC} from '../../../redux/actions/user';
import {isMobile} from '../../../common/helpers/browser';

function* getCartFlow() {
  try {
    const response = yield call(getCartAPI);
    if (response && response.data) {
      yield put(getCartSuccessAC(response.data || []));
    }
  } catch (error) {
    showAlert({
      type: 'error',
      message: `Lỗi: ${error.message}`
    });
    yield put(getCartFailedAC());
  }
}

function* toggleCartItemFlow(action) {
  try {
    const cart = Store.getState().pages.order.cart;
    const {id} = action;
    const item = (cart.data || []).find(item => item.id === id);
    if (item) {
      yield call(updateCartAPI, {
        id,
        product_id: item.product_id,
        qty: item.qty,
        is_selected: !item.is_selected,
      });
      yield put(getCartAC());
    }
  } catch (e) {

  }
}

function* toggleCartStoreFlow(action) {
  try {
    const {isSelected, storeId} = action;
    const data = Store.getState().pages.order.cart.data;
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if (item.store_id === storeId && item.is_selected !== isSelected) {
        yield call(updateCartAPI, {
          id: item.id,
          product_id: item.product_id,
          qty: item.qty,
          is_selected: isSelected,
        });
      }
    }
    yield put(getCartAC());
  } catch (e) {

  }
}

function* toggleCartAllFlow(action) {
  try {
    const {isSelected} = action;
    const data = Store.getState().pages.order.cart.data;
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if (item.is_selected !== isSelected) {
        yield call(updateCartAPI, {
          id: item.id,
          product_id: item.product_id,
          qty: item.qty,
          is_selected: isSelected,
        });
      }
    }
    yield put(getCartAC());
  } catch (e) {

  }
}

function* getCartSilentFlow() {
  try {
    const response = yield call(getCartAPI);
    if (response && response.data) {
      yield put(getCartSuccessAC(response.data || []));
    }
  } catch (error) {
    yield put(getCartFailedAC());
  }
}

function* addToCartFlow(action) {
  const user = Store.getState().user;
  if (user.auth.isAuthenticated) {
    try {
      const response = yield call(addToCartAPI, action.data);
      if (response && response.data) {
        yield put(getCartAC());
        if (action.data.buy_now) {
          history.push('/cart');
        } else {
          if (isMobile) {
            history.push('/cart');
          }
          showAlert({
            type: 'success',
            message: 'Đã thêm vào giỏ hàng'
          });
        }
      } else {
        showAlert({
          type: 'error',
          message: 'Lỗi!'
        });
      }
    } catch (error) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    }
  } else {
    // yield put(addToCartOfflineAC(action));
    yield put(openModal('login'));
  }
}

function* updateCartProductFlow(action) {
  const user = Store.getState().user;
  if (user.auth.isAuthenticated) {
    try {
      const response = yield call(updateCartAPI, action.data);
      if (response && response.data) {
        yield put(getCartAC());
      } else {
        showAlert({
          type: 'error',
          message: 'Lỗi!'
        });
      }
    } catch (error) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    }
  } else {
    yield put(openModal('login'));
  }
}

function* deleteCartProductFlow(action) {
  const user = Store.getState().user;
  if (user.auth.isAuthenticated) {
    try {
      const response = yield call(deleteCartAPI, action.data);
      if (response && response.data) {
        yield put(getCartAC());
      } else {
        showAlert({
          type: 'error',
          message: 'Lỗi!'
        });
      }
    } catch (error) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    }
  } else {
    yield put(openModal('login'));
  }
}

function* checkoutSuccessFlow() {
  try {
    const cart = Store.getState().pages.order.cart;
    const {data} = cart;
    const productIds = data.map(item => item.product_id);
    for (let i = 0; i < productIds.length; i++) {
      yield call(deleteCartAPI, {product_id: productIds[i]});
    }
    yield put(cleanCartAC());
  } catch (error) {
    console.error(error);
    showAlert({
      type: 'error',
      message: `Lỗi: ${error.message}`
    });
  }
}

export default function* pageWatcher() {
  yield takeEvery(getCartAC().type, getCartFlow);
  yield takeEvery(getCartSilentAC().type, getCartSilentFlow);
  yield takeEvery(authenticationSuccessAC().type, getCartSilentFlow);
  yield takeEvery(addToCartAC().type, addToCartFlow);
  yield takeEvery(updateCartProductAC().type, updateCartProductFlow);
  yield takeEvery(deleteCartProductAC().type, deleteCartProductFlow);
  yield takeEvery(checkoutSuccessAC().type, checkoutSuccessFlow);
  yield takeEvery(TOGGLE_SELECT_CART_ITEM, toggleCartItemFlow);
  yield takeEvery(TOGGLE_SELECT_CART_STORE, toggleCartStoreFlow);
  yield takeEvery(TOGGLE_SELECT_CART_ALL_ITEM, toggleCartAllFlow);
}
