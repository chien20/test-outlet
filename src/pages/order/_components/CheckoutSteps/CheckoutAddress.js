import React from 'react';
import {handleInputTextChanged, handleSelectChange, showAlert} from '../../../../common/helpers';
import {getListDistrictAPI, getListProvinceAPI, getListWardAPI} from '../../../../api/locations';
import Select from '../../../../components/Form/Select/Select';

class CheckoutAddress extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      provinces: [{
        id: null,
        name: 'Chọn Tỉnh/Thành phố'
      }],
      districts: [{
        id: null,
        name: 'Chọn Quận/Huyện'
      }],
      wards: [{
        id: null,
        name: 'Chọn Phường/Xã/Thị trấn'
      }]
    };
  }

  componentDidMount() {
    this.getProvinces();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.formData.province_id !== this.props.formData.province_id) {
      this.getDistricts();
    }
    if (prevProps.formData.district_id !== this.props.formData.district_id) {
      this.getWards();
    }
  }

  getProvinces = () => {
    getListProvinceAPI().then(res => {
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Tỉnh/Thành phố'
        });
        this.setState({
          provinces: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getDistricts = () => {
    const {formData} = this.props;
    getListDistrictAPI(formData.province_id).then(res => {
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Quận/Huyện'
        });
        this.setState({
          districts: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getWards = () => {
    const {formData} = this.props;
    getListWardAPI(formData.district_id).then(res => {
      if (res && res.data && res.data.list) {
        let list = [];
        if (res.data.list[0] && Array.isArray(res.data.list[0])) {
          list = res.data.list[0];
        } else {
          list = res.data.list;
        }
        list.splice(0, 0, {
          id: null,
          name: 'Chọn Phường/Xã/Thị trấn'
        });
        this.setState({
          wards: list
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  setData = (data) => {
    const {formData, setData} = this.props;
    if (data && data.province_id !== undefined) {
      data.district_id = null;
      data.ward_id = null;
    }
    if (data && data.district_id !== undefined) {
      data.ward_id = null;
    }
    setData({
      receiver: {...formData, ...data}
    });
  };

  render() {
    const {formData} = this.props;
    const {provinces, districts, wards} = this.state;
    const selectedProvince = provinces.find(item => item.id === formData.province_id);
    const selectedDistrict = districts.find(item => item.id === formData.district_id);
    const selectedWard = wards.find(item => item.id === formData.ward_id);
    return (
      <div className="step">
        <label className="step-title">Thông tin người nhận</label>
        <div className="step-content">
          <div className="row">
            <div className="col-6 full-width-mobile">
              <div className="form-group">
                <label>Họ tên</label>
                <input
                  className="form-control"
                  required={true}
                  maxLength={128}
                  value={formData.full_name}
                  onChange={handleInputTextChanged('full_name', this.setData)}/>
              </div>
            </div>
            <div className="col-6 full-width-mobile">
              <div className="form-group">
                <label>Điện thoại</label>
                <input
                  className="form-control"
                  required={true}
                  maxLength={15}
                  value={formData.phone}
                  onChange={handleInputTextChanged('phone', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Tỉnh/Thành phố</label>
                <Select
                  options={provinces}
                  value={selectedProvince}
                  placeholder="Chọn tỉnh thành phố"
                  onChange={handleSelectChange('province_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Quận/Huyện</label>
                <Select
                  options={districts}
                  value={selectedDistrict}
                  placeholder="Chọn quận huyện"
                  onChange={handleSelectChange('district_id', 'id', this.setData)}/>
              </div>
            </div>
            <div className="col-4 full-width-mobile">
              <div className="form-group">
                <label>Phường/Xã/Thị trấn</label>
                <Select
                  options={wards}
                  value={selectedWard}
                  placeholder="Chọn xã"
                  onChange={handleSelectChange('ward_id', 'id', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="form-group">
                <label>Địa chỉ</label>
                <input
                  className="form-control"
                  required={true}
                  maxLength={256}
                  value={formData.address}
                  onChange={handleInputTextChanged('address', this.setData)}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CheckoutAddress;
