import React from 'react';
import {handleCheckBoxChanged} from '../../../../common/helpers';

class CheckoutPaymentMethod extends React.PureComponent {
  setData = (data) => {
    const {formData, formKey, setData} = this.props;
    setData({
      [formKey]: {...formData, ...data}
    });
  };

  render() {
    const {formData, setData} = this.props;
    return (
      <div className="step">
        <label className="step-title">03 - Hình thức thanh toán</label>
        <div className="step-content">
          <div className="custom-control custom-radio">
            <input
              type="radio"
              className="custom-control-input"
              value={0}
              checked={formData.payment_method_id === 0}
              onChange={handleCheckBoxChanged('payment_method_id', setData, 0, 'int')}
              id="cod"/>
            <label className="custom-control-label" htmlFor="cod">Thanh toán bằng tiền mặt khi nhận hàng</label>
          </div>
          <div className="custom-control custom-radio">
            <input
              type="radio"
              className="custom-control-input"
              value={10}
              checked={formData.payment_method_id === 10}
              onChange={handleCheckBoxChanged('payment_method_id', setData, 10, 'int')}
              id="credit"/>
            <label className="custom-control-label" htmlFor="credit">Thanh toán bằng thẻ quốc tế (Visa, Master,
              JCB)</label>
          </div>
          <div className="custom-control custom-radio">
            <input
              type="radio"
              className="custom-control-input"
              value={20}
              checked={formData.payment_method_id === 20}
              onChange={handleCheckBoxChanged('payment_method_id', setData, 20, 'int')}
              id="atm"/>
            <label className="custom-control-label" htmlFor="atm">Thanh toán bằng thẻ ATM nội địa/Internet
              banking</label>
          </div>
        </div>
      </div>
    )
  }
}

export default CheckoutPaymentMethod;
