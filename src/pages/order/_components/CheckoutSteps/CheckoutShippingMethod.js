import React from 'react';
import {handleCheckBoxChanged, publicUrl} from '../../../../common/helpers';
import "./CheckoutSteps.scss"
import {numberAsCurrentcy} from "../../../../common/helpers/format";

class CheckoutShippingMethod extends React.PureComponent {

  setData = (data) => {
    const {formData, formKey, setData} = this.props;
    setData({
      [formKey]: {...formData, ...data}
    });
  };

  render() {
    const {formData, setData} = this.props;
    return (
      <div className="step">
        <label className="step-title">02 - Hình thức giao hàng</label>
        <div className="step-content checkout-shipping-methods">
          <ul className="shipping-brands" style={{display: 'none'}}>
            <li className="brand-item selected">
              <img src={publicUrl(`./assets/images/shipping-brands/ghtk.png`)} alt="brand"/>
              <div className="ship-time">1 ngày</div>
              <div className="price">{numberAsCurrentcy(20000)} đ</div>
            </li>
            <li className="brand-item">
              <img src={publicUrl(`./assets/images/shipping-brands/ghn.png`)} alt="brand"/>
              <div className="ship-time">1 ngày</div>
              <div className="price">{numberAsCurrentcy(30000)} đ</div>
            </li>
          </ul>
          <div className="custom-control custom-radio">
            <input
              type="radio"
              className="custom-control-input"
              value={0}
              checked={formData.shipping_method_id === 0}
              onChange={handleCheckBoxChanged('shipping_method_id', setData, 0, 'int')}
              id="ghtk"/>
            <label className="custom-control-label" htmlFor="ghtk">Giao hàng tiết kiệm (dự kiến nhận được sau 3 ngày, không kể ngày nghỉ)</label>
          </div>
          <div className="custom-control custom-radio">
            <input
              type="radio"
              className="custom-control-input"
              disabled={true}
              value={1}
              checked={formData.shipping_method_id === 1}
              onChange={handleCheckBoxChanged('shipping_method_id', setData, 1, 'int')}
              id="ghn"/>
            <label className="custom-control-label" htmlFor="ghn">Giao hàng nhanh (nhận hàng trong 24h)</label>
          </div>
        </div>
      </div>
    )
  }
}

export default CheckoutShippingMethod;
