import React from 'react';
import {handleInputTextChanged} from '../../../../common/helpers';
import {Collapse} from 'react-bootstrap';

class CheckoutReceiver extends React.PureComponent {
  setData = (key) => (data) => {
    const {formData, setData} = this.props;
    setData({
      [key]: {...formData[key], ...data}
    });
  };

  handleToggleCheckbox = (key) => (event) => {
    const checked = event.target.checked;
    this.props.setData({[key]: checked});
  };

  render() {
    const {formData} = this.props;
    return (
      <div className="step">
        <label className="step-title">04 - Thông tin người mua</label>
        <div className="step-content">
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              value={true}
              checked={formData.use_receiver_info === true}
              onChange={this.handleToggleCheckbox('use_receiver_info')}
              id="use_receiver_info"/>
            <label className="custom-control-label" htmlFor="use_receiver_info">Sử dụng thông tin người nhận</label>
          </div>
          <Collapse in={!formData.use_receiver_info}>
            <div className="m-t-10">
              <div className="row">
                <div className="col-6 full-width-mobile">
                  <div className="form-group">
                    <label>Họ tên</label>
                    <input
                      className="form-control"
                      maxLength={128}
                      value={formData.buyer.full_name}
                      onChange={handleInputTextChanged('full_name', this.setData('buyer'))}/>
                  </div>
                </div>
                <div className="col-6 full-width-mobile">
                  <div className="form-group">
                    <label>Điện thoại</label>
                    <input
                      className="form-control"
                      maxLength={15}
                      value={formData.buyer.phone}
                      onChange={handleInputTextChanged('phone', this.setData('buyer'))}/>
                  </div>
                </div>
              </div>
            </div>
          </Collapse>

          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              value={true}
              checked={formData.use_invoice_info === true}
              onChange={this.handleToggleCheckbox('use_invoice_info')}
              id="use_invoice_info"/>
            <label className="custom-control-label" htmlFor="use_invoice_info">Thông tin xuất hóa đơn</label>
          </div>
          <Collapse in={formData.use_invoice_info}>
            <div className="m-t-10">
              <div className="row">
                <div className="col-8 full-width-mobile">
                  <div className="form-group">
                    <label>Tên công ty</label>
                    <input
                      className="form-control"
                      maxLength={256}
                      value={formData.invoice_info.company_name}
                      onChange={handleInputTextChanged('company_name', this.setData('invoice_info'))}/>
                  </div>
                </div>
                <div className="col-4 full-width-mobile">
                  <div className="form-group">
                    <label>Mã số thuế</label>
                    <input
                      className="form-control"
                      maxLength={15}
                      value={formData.invoice_info.tax_code}
                      onChange={handleInputTextChanged('tax_code', this.setData('invoice_info'))}/>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <label>Địa chỉ</label>
                <textarea
                  className="form-control"
                  placeholder="Nhập địa chỉ đầy đủ"
                  maxLength={256}
                  value={formData.invoice_info.address}
                  onChange={handleInputTextChanged('address', this.setData('invoice_info'))}/>
              </div>
            </div>
          </Collapse>

        </div>
      </div>
    )
  }
}

export default CheckoutReceiver;
