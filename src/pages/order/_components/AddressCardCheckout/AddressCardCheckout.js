import React, {Fragment} from "react";
import HyperLink from "../../../../components/HyperLink/HyperLink";
import "./AddressCardCheckout.scss"
import {deleteAddressesAPI} from "../../../../api/users";
import {showAlert, uuidv4} from "../../../../common/helpers";
import PropTypes from "prop-types";

class AddressCardCheckout extends React.PureComponent {
  state = {
    id: uuidv4(),
  }

  editAddress = (event) => {
    event.stopPropagation();
    this.props.onEdit(this.props.address);
  };

  deleteAddress = (event) => {
    event.stopPropagation();
    const r = window.confirm('Địa chỉ này sẽ bị xóa. Bạn có chắc chắn?');
    if (r) {
      deleteAddressesAPI(this.props.address.id).then(() => {
        this.props.onDeleteSuccess();
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `${error.message}`
        });
      });
    }
  };


 render() {
   const {address, onClick, isSelected} = this.props;
   const {id} = this.state;
   if (!address) {
     return null;
   }
   return (
     <Fragment>
       <div className="address-card-checkout">
         <div className="address-type">{address.is_company_address ? "Công ty" : "Nhà riêng"}</div>
         <div className="name-phone"><span className="name">{address.full_name}</span> | {address.phone}</div>
         <div className="address">{address.address}</div>
         <div className="choose-address">
           <input type="checkbox" className={isSelected ? `selected` : ``} onClick={onClick} id={id}/>
           <label htmlFor={id}>Giao tới địa chỉ này</label>
         </div>
         <HyperLink className="action" title="Chỉnh sửa" onClick={this.editAddress}><i className="fas fa-pencil-alt"/></HyperLink>
       </div>

     </Fragment>

   );
 }
}

AddressCardCheckout.propTypes = {
  address: PropTypes.object,
  onEdit: PropTypes.func,
  onDeleteSuccess: PropTypes.func,
  onClick: PropTypes.func,
  isSelected: PropTypes.bool
}

export default AddressCardCheckout;
