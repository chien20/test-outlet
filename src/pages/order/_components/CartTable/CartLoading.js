import React from 'react';
import DivLoading from '../../../../components/DivLoading/DivLoading';

const CartRow = () => (
  <div className="t-row">
    <div className="c-check">
      <DivLoading width="20px" height="20px"/>
    </div>
    <div className="c-name">
      <div className="p-avatar">
        <DivLoading width="80px" height="80px"/>
      </div>
      <div className="p-name">
        <DivLoading height="20px"/>
        <DivLoading height="20px"/>
      </div>
    </div>
    <div className="c-price">
      <DivLoading/>
    </div>
    <div className="c-qty">
      <DivLoading/>
    </div>
    <div className="c-total">
      <DivLoading/>
    </div>
    <div className="c-action">
      <DivLoading/>
    </div>
  </div>
);

const CartLoading = () => (
  <div className="cart-page bg-gray common-page">
    <div className="container">
      <div className="cart-table">
        <div className="t-body">
          <div className="t-row">
            <DivLoading wrapperStyle={{marginBottom: '10px'}}/>
          </div>
          <CartRow/>
          <CartRow/>
        </div>
      </div>
    </div>
  </div>
);

export default CartLoading;
