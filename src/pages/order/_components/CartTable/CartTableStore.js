import React from 'react';
import CartTableProduct from './CartTableProduct';
import {Link} from 'react-router-dom';
import {publicUrl} from "../../../../common/helpers";

class CartTableStore extends React.PureComponent {
  isSelectAll = () => {
    const {data: {products}} = this.props;
    const isUnSelect = (products || []).find(item => !item.is_selected);
    return !isUnSelect;
  };

  render() {
    const {data: {info, products}, toggleItem, toggleStore} = this.props;
    return (
      <div className="t-body shadow ">
        <div className="t-row shop-header">
          <div className="c-check">
            <input
              className="check-box"
              type="checkbox"
              name="selectStore"
              checked={this.isSelectAll()}
              onChange={toggleStore(info.id)}
            />
          </div>
          <div className="c-shop">
            <Link to={`/shop/${info.id}`}>{info.name}</Link>
            <Link to={`/user/chat/?new=true&user_id=${info.user_id}`}>
              <img src={publicUrl(`/assets/images/icons/Chat.svg`)} alt="chat-icon"/>
            </Link>
          </div>
        </div>
        {
          products.map((item, index) => (
            <CartTableProduct
              key={index}
              data={item}
              toggleItem={toggleItem}
            />
          ))
        }
      </div>
    );
  }
}

export default CartTableStore;
