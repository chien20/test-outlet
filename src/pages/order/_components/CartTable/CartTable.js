import React from 'react';
import CartTableStore from './CartTableStore';
import './CartTable.scss';


class CartTable extends React.PureComponent {
  isSelectAll = () => {
    const {data} = this.props;
    const isUnSelect = (data || []).find(item => !item.is_selected);
    return !isUnSelect;
  };

  render() {
    const {data, qty, toggleAllItem, toggleItem, toggleStore} = this.props;
    const displayData = [];
    const stores = {};
    data.forEach(item => {
      if (!stores[item.store_id]) {
        stores[item.store_id] = {
          info: item.store,
          products: []
        };
      }
      stores[item.store_id].products.push(item);
    });
    Object.keys(stores).forEach((item) => {
      displayData.push(stores[item]);
    });
    return (
      <div className="cart-table">
        <div className="t-head">
          <div className="t-row">
            <div className="c-check">
              <input
                className="check-box"
                type="checkbox"
                name="selectAll"
                checked={this.isSelectAll()}
                onChange={toggleAllItem}
              />
            </div>
            <div className="c-name">
              Chọn tất cả ({qty} sản phẩm)
            </div>
          </div>
        </div>
        {
          displayData.map((item, index) => (
            <CartTableStore
              key={index}
              data={item}
              toggleStore={toggleStore}
              toggleItem={toggleItem}
            />
          ))
        }
      </div>
    );
  }
}

export default CartTable;
