import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {convertToSlug, numberAsCurrentcy} from '../../../../common/helpers/format';
import {imageUrl} from '../../../../common/helpers';
import InputSpin from '../../../../components/Form/InputSpin/InputSpin';
import {deleteCartProductAC, updateCartProductAC} from '../../_redux/actions';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import ProductFavorite from "../../../product/_components/ProductFavorite/ProductFavorite";

class CartTableProduct extends React.PureComponent {
  handleQtyChange = (qty) => {
    const {data: {id, product_id, product_option_id}} = this.props;
    this.props.dispatch(updateCartProductAC({
      id,
      product_id,
      product_option_id,
      qty
    }));
  };

  handleDelete = () => {
    const {data: {id, product_id, product_option_id}} = this.props;
    this.props.dispatch(deleteCartProductAC({
      id,
      product_id,
      product_option_id
    }));
  };

  render() {
    const {data, toggleItem} = this.props;
    const {product} = data;
    let imgUrl = null;
    if (product.images && product.images.length) {
      imgUrl = imageUrl(product.images[0].thumbnail_url);
    }
    const imageBg = {};
    if (imgUrl) {
      imageBg.backgroundImage = `url(${imgUrl})`;
    }
    const slug = product.slug ? product.slug : convertToSlug(product.name);
    const productUrl = `/${slug}/p${product.id}`;
    return (
      <div className="t-row shop-products">
        <div className="c-check">
          <input
            className="check-box"
            type="checkbox"
            name="select"
            checked={data.is_selected}
            onChange={toggleItem(data.id)}
          />
        </div>
        <div className="c-name">
          <Link to={productUrl}><span className="p-image" style={imageBg}/></Link>
          <Link to={productUrl}>{data.display_name}</Link>
          <div className="d-mobile-block price-color">{numberAsCurrentcy(data.display_price)} đ</div>
          <div className="d-mobile-block price-color">
            <InputSpin value={data.qty} onChange={this.handleQtyChange} min={1}/>
          </div>
        </div>
        <div className="c-price">
          <div className="price">{numberAsCurrentcy(data.display_price)} đ</div>
          {
            data.product.original_price > 0 &&
            <div className="old-price">{numberAsCurrentcy(data.product.original_price)} đ</div>
          }
        </div>
        <div className="c-qty">
          <InputSpin value={data.qty} onChange={this.handleQtyChange} min={1} max={100}/>
          <div className="actions">
            <div className="action favorite">
              <ProductFavorite product={data.product}/>
            </div>
            <div className="action delete">
              <HyperLink onClick={this.handleDelete} title={`Xóa`}>
                <i className="far fa-trash-alt"/>
              </HyperLink>
            </div>
          </div>

        </div>
        <div className="delete-btn d-mobile-block" onClick={this.handleDelete}>x</div>
      </div>
    );
  }
}

export default connect()(CartTableProduct);
