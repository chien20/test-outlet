import React, {Fragment} from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import {Link} from 'react-router-dom';
import {getUserPointsAPI} from '../../../../api/users';

class CheckoutCartInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      point: 0
    };
  }

  componentDidMount() {
    this.getPoints();
  }

  getPoints = () => {
    getUserPointsAPI().then(res => {
      this.setState({
        point: res.data.total
      });
    }).catch(error => {
    });
  };

  handleToggleCheckbox = (key) => (event) => {
    const checked = event.target.checked;
    this.props.setData({[key]: checked});
  };

  render() {
    const {cart, cartTotal, formData} = this.props;
    const {point} = this.state;
    return (
      <div className="paper cart-info">
        <div className="step cart-list">
          <label className="step-title">Đơn hàng ({cart.qty} sản phẩm)</label>
          <ul>
            {
              cart.data.map((item, index) => {
                const productUrl = `/${item.product.slug}/p${item.product.id}`;
                return (
                  <li key={index}>
                    <Link to={productUrl} className="item-name"><span>{item.qty} x</span> {item.display_name}</Link>
                    <span className="item-price">{numberAsCurrentcy(item.sub_total)} đ</span>
                  </li>
                );
              })
            }
          </ul>
          <ul>
            <li className="new-section">
              <span>Tạm tính</span>
              <strong className="price-color">{numberAsCurrentcy(cartTotal.total)} đ</strong>
            </li>
            <li>
              <span>Giảm giá</span>
              <strong className="price-color">{numberAsCurrentcy(cartTotal.discount)} đ</strong>
            </li>
            {
              !!cartTotal.shipping_discount &&
              <Fragment>
                <li>
                  <span>Phí vận chuyển</span>
                  <strong className="price-color">{numberAsCurrentcy(cartTotal.total_ship_fee)} đ</strong>
                </li>
                <li>
                  <span>Giảm giá vận chuyển</span>
                  <strong className="price-color">{numberAsCurrentcy(cartTotal.shipping_discount)} đ</strong>
                </li>
              </Fragment>
            }
            {
              !cartTotal.shipping_discount && !!cartTotal.ship_fee &&
              <li>
                <span>Phí vận chuyển</span>
                <strong className="price-color">{numberAsCurrentcy(cartTotal.ship_fee)} đ</strong>
              </li>
            }
            {
              !cartTotal.shipping_discount && !cartTotal.ship_fee &&
              <li>
                <span>Phí vận chuyển</span>
                <span className="color-gray">Chưa tính</span>
              </li>
            }
          </ul>
          <div className="use-points">
            <div className="custom-control custom-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                value={true}
                disabled={!point}
                checked={formData.use_point === true}
                onChange={this.handleToggleCheckbox('use_point')}
                id="use_point"/>
              <label className="custom-control-label" htmlFor="use_point">Sử dụng ePoint ({point} điểm)</label>
            </div>
          </div>
          <div className="cart-list-footer">
            <ul className="total">
              <li className="new-section">
                <span>Thành tiền</span>
                <strong className="price-color">{numberAsCurrentcy(cartTotal.total - cartTotal.discount)} đ</strong>
              </li>
              <li><small>Đã bao gồm thuế VAT nếu có</small></li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckoutCartInfo;
