import React from 'react';
import PropTypes from 'prop-types';
import {getVoucherInfoAPI} from '../../../../api/vouchers';
import HyperLink from '../../../../components/HyperLink/HyperLink';

const VoucherCondition = ({discountType, condition}) => {
  const discountUnit = discountType === 0 ? 'đ' : '%';
  const discount = discountType === 0 ? condition.discount.toLocaleString() : condition.discount;
  return (
    <li>
      Giảm <strong>{discount}{discountUnit}</strong> cho đơn hàng
      từ <strong>{condition.min.toLocaleString()}đ</strong>
    </li>
  )
};

class AddVoucher extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      isLoading: false,
      error: null,
      voucherDetail: null
    };
  }

  componentDidMount() {
    this.getVoucherInfo();
  }

  getVoucherInfo = () => {
    const {voucher} = this.props;
    if (!voucher) {
      return;
    }
    this.setState({
      isLoading: true
    });
    getVoucherInfoAPI({
      voucher
    }).then(res => {
      this.setState({
        voucherDetail: res.data,
        isLoading: false
      });
    }).catch(error => {
      this.setState({
        voucherDetail: null,
        isLoading: false
      })
    });
  };

  setInputValue = (event) => {
    this.setState({
      inputValue: event.target.value
    });
  };

  removeVoucher = () => {
    const {handleAddVoucher} = this.props;
    this.setState({
      inputValue: '',
      error: null,
      voucherDetail: null
    });
    handleAddVoucher('');
  };

  submit = (event) => {
    event.preventDefault();
    const {handleAddVoucher} = this.props;
    const {inputValue} = this.state;
    this.setState({
      isLoading: true
    });
    getVoucherInfoAPI({
      voucher: inputValue
    }).then(res => {
      this.setState({
        voucherDetail: res.data,
        isLoading: false,
        error: null
      });
      handleAddVoucher(inputValue);
    }).catch(error => {
      this.setState({
        voucherDetail: null,
        isLoading: false,
        error: error.message
      })
    });
  };

  render() {
    const {inputValue, voucherDetail, isLoading, error} = this.state;
    return (
      <div className="paper add-voucher">
        {
          !voucherDetail &&
          <form onSubmit={this.submit}>
            <p>Bạn có mã giảm giá?</p>
            {
              error &&
              <div className="alert alert-danger">
                {
                  error
                }
              </div>
            }
            <div className="form-group">
              <input
                value={inputValue}
                onChange={this.setInputValue}
                type="text"
                className="form-control"
                placeholder="Nhập mã giảm giá"/>
                <button
                  disabled={!inputValue || isLoading}
                  type="submit">
                  <i className="fas fa-arrow-right"/>
                </button>
            </div>
          </form>
        }
        {
          voucherDetail &&
          <div className="voucher-detail">
            <p>Mã giảm giá</p>
            <p><strong className="discount-code">{voucherDetail.voucher}</strong></p>
            <ul>
              {
                voucherDetail.conditions.map((item, index) => (
                  <VoucherCondition discountType={voucherDetail.discount_type} condition={item} key={index}/>
                ))
              }
            </ul>
            <p><HyperLink onClick={this.removeVoucher}>Sử dụng mã giảm giá khác?</HyperLink></p>
          </div>
        }
      </div>
    );
  }
}

AddVoucher.propTypes = {
  voucher: PropTypes.string,
  handleAddVoucher: PropTypes.func
};

export default AddVoucher;
