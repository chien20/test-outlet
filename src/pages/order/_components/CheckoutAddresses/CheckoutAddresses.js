import React from 'react';
import PropTypes from 'prop-types';
import {getAddressesAPI} from '../../../../api/users';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {withRouter} from "react-router-dom";
import './CheckoutAddresses.scss';
import qs from 'qs';
import history from '../../../../common/utils/router/history';
import {buildQueryString} from '../../../../common/helpers';
import SelectAddressModal from './SelectAddressModal';
import UpsertAddressModal from './UpsertAddressModal';

class CheckoutAddresses extends React.PureComponent {
  state = {
    addresses: [],
    isFetching: false,
    selectedAddress: null,
    selectedId: null,
  };

  unmounted = false;

  selectAddressModal = React.createRef();
  upsertAddressModal = React.createRef();

  componentDidMount() {
    this.getSelectedId();
    this.getAddresses();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.selectedId !== this.state.selectedId || prevState.addresses !== this.state.addresses) {
      this.setData();
    }
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  getSelectedId = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    if (!params.address_id) {
      return;
    }
    this.setState({
      selectedId: params.address_id * 1,
    });
  };

  getAddresses = () => {
    this.setState({
      isFetching: true
    });
    getAddressesAPI().then(res => {
      if (this.unmounted) {
        return;
      }
      const data = res.data.list || [];
      this.setState(prevState => {
        let {selectedId} = prevState;
        if (!selectedId) {
          const defaultAddress = data.find(item => item.is_default);
          selectedId = defaultAddress ? defaultAddress.id : (data.length ? data[0].id : null);
        }
        return {
          addresses: data,
          isFetching: false,
          selectedId,
        };
      });
    }).catch(() => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isFetching: false
      });
    });
  };

  setData = () => {
    let {addresses, selectedId} = this.state;
    let selectedAddress = addresses.find(item => item.id === selectedId);
    if (!selectedAddress) {
      selectedAddress = addresses.find(item => item.is_default);
    }
    if (!selectedAddress && addresses.length) {
      selectedAddress = addresses[0];
    }
    this.props.setData({
      receiver: selectedAddress,
    });
  };

  handleAddAddressMobile = () => {
    history.push('/user/addresses?' + buildQueryString({
      action: 'new',
      returnUrl: '/checkout',
    }));
  };

  onUpsertSuccess = (address) => {
    this.setState({
      selectedId: address?.id,
    });
    this.getAddresses();
  };

  handleSelectAddress = (selectedId) => {
    this.setState({
      selectedId,
    });
  };

  handleOpenSelectAddress = () => {
    const {selectedId} = this.state;
    this.selectAddressModal.current.handleOpen(selectedId);
  };

  handleOpenAddAddress = () => {
    this.upsertAddressModal.current.handleOpen(null);
    this.selectAddressModal.current.handleClose();
  };

  handleOpenEditAddress = (address) => {
    this.upsertAddressModal.current.handleOpen(address);
    this.selectAddressModal.current.handleClose();
  };

  render() {
    const {isFetching, addresses} = this.state;
    const {formData} = this.props;
    const receiver = formData.receiver;
    // if (isMobile) {
    //   if (!addresses.length) {
    //     return (
    //       <div className="address-box empty">
    //         <p>Bạn chưa có địa chỉ nhận hàng, click vào nút dưới đây để thêm mới.</p>
    //         <div className="btn-add-container">
    //           <HyperLink onClick={this.handleAddAddressMobile}>
    //             <i className="fas fa-plus-circle"/>
    //             <span> Thêm mới</span>
    //           </HyperLink>
    //         </div>
    //       </div>
    //     );
    //   }
    //   const selectedAddress = addresses.find(item => item.id === selectedId);
    //   return (
    //     <div className="address-box">
    //       {
    //         selectedAddress &&
    //         <Fragment>
    //           <div className="type">
    //             <span>{selectedAddress.is_company_address ? 'Công ty' : 'Nhà riêng'}</span>
    //             <Link to={`/checkout-address?address_id=${selectedId}`} className="address-edit">Thay đổi</Link>
    //           </div>
    //           <div className="name-phone">{selectedAddress.full_name} | {selectedAddress.phone}</div>
    //           <div className="address"><span> {selectedAddress.address}</span></div>
    //         </Fragment>
    //       }
    //     </div>
    //   );
    // }
    return (
      <div className="step">
        <label className="step-title">01 - Địa chỉ giao hàng</label>
        <div className="step-content">
          {
            !isFetching && !addresses.length &&
            <div className="address-card-checkout empty">
              <p>Bạn chưa có địa chỉ nhận hàng, click vào nút dưới đây để thêm mới.</p>
              <div className="btn-add-container">
                <HyperLink onClick={this.handleOpenAddAddress}>
                  <i className="fas fa-plus-circle"/>
                  <span> Thêm mới</span>
                </HyperLink>
              </div>
            </div>
          }
          {
            receiver &&
            <div className="address-info">
              <div className="address-type">{receiver.is_company_address ? "Công ty" : "Nhà riêng"}</div>
              <div className="name-phone"><span className="name">{receiver.full_name}</span> | {receiver.phone}</div>
              <div className="address">{receiver.address}</div>
              <HyperLink className="action" onClick={this.handleOpenSelectAddress}>Thay đổi</HyperLink>
            </div>
          }
        </div>
        <SelectAddressModal
          ref={this.selectAddressModal}
          addresses={addresses}
          getAddresses={this.getAddresses}
          handleEditAddress={this.handleOpenEditAddress}
          handleAddAddress={this.handleOpenAddAddress}
          handleSelectAddress={this.handleSelectAddress}
        />
        <UpsertAddressModal
          ref={this.upsertAddressModal}
          onUpsertSuccess={this.onUpsertSuccess}
        />
      </div>
    );
  }
}

CheckoutAddresses.propTypes = {
  setData: PropTypes.func
};

export default withRouter(CheckoutAddresses);
