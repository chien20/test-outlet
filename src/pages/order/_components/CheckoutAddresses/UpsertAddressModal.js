import React from 'react';
import {Modal} from 'react-bootstrap';
import UpsertAddress from '../../../user/account/Addresses/UpsertAddress';

class UpsertAddressModal extends React.PureComponent {
  state = {
    isOpen: false,
    selectedAddress: null,
  };

  handleOpen = (selectedAddress) => {
    this.setState({
      isOpen: true,
      selectedAddress,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  onUpsertSuccess = (item) => {
    const {onUpsertSuccess} = this.props;
    onUpsertSuccess(item);
    this.handleClose();
  };

  render() {
    const {isOpen, selectedAddress} = this.state;
    return (
      <Modal
        dialogClassName="e-modal edit-address-modal"
        size="lg"
        centered show={isOpen}
        onHide={this.handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>{!selectedAddress ? `Thêm mới địa chỉ` : `Chỉnh sửa địa chỉ`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <UpsertAddress
            address={selectedAddress}
            onCancel={this.handleClose}
            onSuccess={this.onUpsertSuccess}
          />
        </Modal.Body>
      </Modal>
    );
  }
}

export default UpsertAddressModal;
