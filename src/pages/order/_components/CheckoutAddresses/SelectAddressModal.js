import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import AddressCardCheckout from '../AddressCardCheckout/AddressCardCheckout';
import HyperLink from '../../../../components/HyperLink/HyperLink';

class SelectAddressModal extends React.PureComponent {
  state = {
    isOpen: false,
    selectedId: null,
  };

  handleOpen = (selectedId) => {
    this.setState({
      isOpen: true,
      selectedId,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  handleSelect = (item) => () => {
    this.setState({
      selectedId: item.id,
    });
  };

  handleSubmit = () => {
    const {handleSelectAddress} = this.props;
    const {selectedId} = this.state;
    handleSelectAddress(selectedId);
    this.handleClose();
  };

  render() {
    const {addresses, getAddresses, handleEditAddress, handleAddAddress} = this.props;
    const {isOpen, selectedId} = this.state;
    return (
      <Modal
        className="e-modal address-list-modal"
        size="lg"
        centered
        show={isOpen}
        onHide={this.handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>Địa chỉ nhận hàng</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row address-cards">
            {
              addresses.map((item) => (
                <div
                  className="col-6"
                  key={item.id}
                >
                  <AddressCardCheckout
                    address={item}
                    onEdit={handleEditAddress}
                    onDeleteSuccess={getAddresses}
                    isSelected={item.id === selectedId}
                    onClick={this.handleSelect(item)}
                  />
                </div>
              ))
            }
            <div className="col-6">
              <div className="address-card-checkout add-address">
                <HyperLink onClick={handleAddAddress}>
                  <i className="fas fa-plus-circle"/>
                  <span>Thêm mới</span>
                </HyperLink>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button className="btn" onClick={this.handleSubmit}>Hoàn thành</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default SelectAddressModal;
