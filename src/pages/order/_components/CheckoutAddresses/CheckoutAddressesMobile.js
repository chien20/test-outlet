import React, {Fragment} from 'react';
import {getAddressesAPI} from '../../../../api/users';
import AddressCard from '../../../user/account/Addresses/AddressCard';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import HeaderMobile from "../../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../../components/HeaderMobile/TitleMobile/TitleMobile";
import IconAddMobile from "../../../../components/HeaderMobile/IconAddMobile/IconAddMobile";
import DivLoading from "../../../../components/DivLoading/DivLoading";
import NoData from "../../../../components/NoData/NoData";
import history from "../../../../common/utils/router/history";
import './CheckoutAddresses.scss';
import qs from 'qs';

class CheckoutAddressesMobile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      addresses: [],
      isFetching: false,
      selectedAddress: null,
      selectedId: null,
      addNew: false
    };
    this.unmounted = false;
  }

  componentDidMount() {
    this.getSelectedId();
    this.getAddresses();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  getSelectedId = () => {
    const {location: {search}} = this.props;
    const params = qs.parse(search || '', {ignoreQueryPrefix: true});
    if (!params.address_id) {
      return;
    }
    this.setState({
      selectedId: params.address_id * 1,
    });
  };

  getAddresses = () => {
    this.setState({
      isFetching: true
    });
    getAddressesAPI().then(res => {
      const data = res.data.list || [];
      const defaultAddress = data.find(item => item.is_default);
      this.setState(prevState => {
        let {selectedId} = prevState;
        if (!selectedId) {
          selectedId = defaultAddress ? defaultAddress.id : (data.length ? data[0].id : null);
        }
        return {
          addresses: data,
          isFetching: false,
          selectedId,
        }
      });
    }).catch(() => {
      if (this.unmounted) {
        return;
      }
      this.setState({
        isFetching: false
      });
    });
  };

  selectCard = (address) => () => {
    history.push(`/checkout?address_id=${address.id}`);
  };

  onEdit = (item) => {
    console.log('Edit', item);
    this.setState({
      selectedAddress: item
    });
  };

  render() {
    const {isFetching, addresses, selectedId} = this.state;
    return (
      <Fragment>
        <HeaderMobile>
          <IconBackMobile/>
          <TitleMobile>Sổ địa chỉ</TitleMobile>
          <IconAddMobile linkTo={`checkout-address/add`}/>
        </HeaderMobile>
        <div className="container">
          <div className="page-content">
            {
              isFetching && <DivLoading/>
            }
            {
              !isFetching &&
              <Fragment>
                {
                  !addresses.length &&
                  <NoData
                    title="Không có địa chỉ"
                    description="Bạn chưa có địa chỉ nào, click vào nút dưới đây để thêm địa chỉ mới.">
                    <HyperLink className="btn btn-primary" onClick={this.toggleAddAddress}>Thêm địa chỉ mới</HyperLink>
                  </NoData>
                }
                {
                  addresses.map((item, index) => (
                    <AddressCard
                      key={index}
                      address={item}
                      onEdit={this.onEdit}
                      onDeleteSuccess={this.getAddresses}
                      isSelected={item.id === selectedId}
                      onClick={this.selectCard(item)}
                    />
                  ))
                }
              </Fragment>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

export default CheckoutAddressesMobile;
