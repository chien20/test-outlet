import React, {Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import successIcon from '../../../assets/images/icons/icon_success.png';
import errorIcon from '../../../assets/images/icons/icon-error.svg';
import loadingIcon from '../../../assets/images/icons/icon-loading.svg';
import {getInvoiceDetailAPI} from '../../../api/invoices';
import './Checkout.scss';
import {checkoutSuccessAC} from '../_redux/actions';
import {updateOnePayStatusAPI} from '../../../api/orders';

class CheckoutComplete extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      status: 0,
      isFetching: true,
      hasError: false
    };
  }

  componentDidMount() {
    this.getInvoiceStatus().then((data) => {
      this.setState({
        ...data,
        isFetching: false,
        hasError: false,
      });
    }).catch(error => {
      this.setState({
        isFetching: false,
        hasError: true,
      });
    });
  }

  getInvoiceStatus = async () => {
    const {location: {search}, match: {params: {invoiceId}}} = this.props;
    if (search && search.includes('vpc_SecureHash')) {
      await updateOnePayStatusAPI(search);
    }
    const {data} = await getInvoiceDetailAPI(invoiceId);
    if (data && data.status === 10) {
      this.props.dispatch(checkoutSuccessAC());
    }
    return data;
  };

  render() {
    const {status, isFetching, hasError} = this.state;
    return (
      <Fragment>
        <Helmet>
          <title>Đặt hàng</title>
        </Helmet>
        <Breadcrumb>
          <BreadcrumbItem text="Đặt hàng" isActive={true}/>
        </Breadcrumb>
        <div className="checkout-page common-page" onSubmit={this.handleSave}>
          <div className="container">
            {
              isFetching &&
              <div className="no-data">
                <img src={loadingIcon} alt={`Loading...`}/>
              </div>
            }
            {
              !isFetching && hasError &&
              <div className="no-data">
                <img src={errorIcon} alt={`Lỗi`}/>
                <p>Lỗi!</p>
                <p>Không kiểm tra được thông tin thanh toán! Vui lòng xem lại trạng thái thanh toán trong mục "Tài khoản
                  của tôi" -> "Hóa đơn"! Xin cảm ơn!</p>
              </div>
            }
            {
              !isFetching && !hasError && status === 0 &&
              <div className="no-data">
                <img src={errorIcon} alt={`Chưa thanh toán`}/>
                <p>Chưa thanh toán!</p>
                <p>Bạn chưa thanh toán sản phẩm!</p>
              </div>
            }
            {
              !isFetching && !hasError && status === 10 &&
              <div className="no-data">
                <img src={successIcon} alt={`Thành công`}/>
                <p>Thanh toán thành công!</p>
                <p>Đã thanh toán thành công! Đơn hàng của bạn đã được tiếp nhận và đang chờ xử lý! Xin cảm ơn!</p>
              </div>
            }
            {
              !isFetching && !hasError && (status === 20 || status === 30) &&
              <div className="no-data">
                <img src={errorIcon} alt={`Đã hủy`}/>
                <p>Thanh toán không thành công!</p>
                <p>Đơn hàng chưa được thanh toán. Vui lòng đặt hàng lại và thanh toán lại hoặc lựa chọn phương thức
                  thanh toán khác!</p>
              </div>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect()(CheckoutComplete);
