import React, {Fragment} from 'react';
import {Breadcrumb, BreadcrumbItem} from '../../../components/Breadcrumb';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';
import CheckoutShippingMethod from '../_components/CheckoutSteps/CheckoutShippingMethod';
import CheckoutPaymentMethod from '../_components/CheckoutSteps/CheckoutPaymentMethod';
import CheckoutReceiver from '../_components/CheckoutSteps/CheckoutReceiver';
import {submitOrderAPI} from '../../../api/orders';
import {showAlert} from '../../../common/helpers';
import CheckoutCartInfo from '../_components/Widgets/CheckoutCartInfo';
import {Link} from 'react-router-dom';
import successIcon from '../../../assets/images/icons/icon_success.png';
import './Checkout.scss';
import {checkoutSuccessAC} from '../_redux/actions';
import history from '../../../common/utils/router/history';
import CheckoutAddresses from '../_components/CheckoutAddresses/CheckoutAddresses';
import {addAddressesAPI} from '../../../api/users';
import AddVoucher from '../_components/Widgets/AddVoucher';
import {getCartTotalAPI} from '../../../api/cart';
import {isMobile} from '../../../common/helpers/browser';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';

class Checkout extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isSuccess: false,
      formData: {
        receiver: {
          full_name: '',
          phone: '',
          province_id: '',
          district_id: '',
          ward_id: '',
          address: '',
          is_default: false, // true or false
          is_company_address: false, // true or false
          save_this_address: true
        },
        buyer: {
          full_name: '',
          address: '',
          phone: ''
        },
        payment_method_id: 0, // 0: COD, 10: credit card, 20: ATM
        shipping_method_id: 0, // 0: Giao hàng tiết kiệm
        invoice_info: {
          company_name: '',
          tax_code: '',
          address: ''
        },
        cart_info: [],
        use_receiver_info: true,
        use_invoice_info: false,
        voucher: '',
        vouchers: [],
        use_point: false
      },
      cartTotal: null
    };
  }

  componentDidMount() {
    const {user, cart} = this.props;
    const {formData} = this.state;
    const cartInfo = [];
    if (!cart.data.length) {
      history.replace('/cart');
      return;
    }
    cart.data.forEach(item => {
      cartInfo.push({
        product_option_id: item.product_option_id,
        product_id: item.product_id,
        qty: item.qty,
        group_id: item.group_id,
      });
    });
    this.setData({
      receiver: {
        ...formData.receiver,
        full_name: user.full_name,
        phone: user.phone
      },
      cart_info: cartInfo
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.formData !== this.state.formData) {
      if (prevState.formData.voucher !== this.state.formData.voucher
        || prevState.formData.cart_info !== this.state.formData.cart_info
        || prevState.formData.use_point !== this.state.formData.use_point
        || prevState.formData.receiver !== this.state.formData.receiver) {
        this.getCartTotal();
      }
    }
  }

  setData = (data) => {
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  getCartTotal = () => {
    const {formData: {cart_info, voucher, use_point, receiver}} = this.state;
    this.setState({
      isLoading: true
    });
    const postData = {
      cart_info: cart_info,
      vouchers: [],
      use_point: use_point,
      receiver,
    };
    if (!receiver?.province_id) {
      delete postData.receiver;
    }
    if (voucher) {
      postData.vouchers.push(voucher);
    }
    getCartTotalAPI(postData).then(res => {
      const cartTotal = res.data;
      cartTotal.total_ship_fee = cartTotal.total_ship_fee || 0;
      cartTotal.shipping_discount = cartTotal.shipping_discount || 0;
      cartTotal.ship_fee = cartTotal.total_ship_fee - cartTotal.shipping_discount;
      this.setState({
        cartTotal,
        isLoading: false
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không kiểm tra được thông tin đơn hàng`
      });
    });
  };

  handleAddVoucher = (voucher) => {
    this.setData({
      voucher
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const {formData} = this.state;
    if (!formData.receiver.province_id) {
      showAlert({
        type: 'error',
        message: `Lỗi: Vui lòng chọn Tỉnh/Thành phố`
      });
      return;
    }
    if (!formData.receiver.district_id) {
      showAlert({
        type: 'error',
        message: `Lỗi: Vui lòng chọn Quận/Huyện`
      });
      return;
    }
    if (!formData.receiver.ward_id) {
      showAlert({
        type: 'error',
        message: `Lỗi: Vui lòng chọn Phường/Xã/Thị Trấn`
      });
      return;
    }
    this.setState({
      isLoading: true
    });
    if (formData.receiver.save_this_address) {
      addAddressesAPI(formData.receiver).then(() => {
        this.handleSave();
      }).catch(() => {
        this.handleSave();
      });
    } else {
      this.handleSave();
    }
  };

  handleSave = () => {
    const {formData} = this.state;
    const postData = JSON.parse(JSON.stringify(formData));
    postData.vouchers = [];
    postData.store_ids = [];
    submitOrderAPI(postData).then((res) => {
      this.setState({
        isLoading: false,
        isSuccess: true
      });
      if (postData.payment_method_id === 20 || postData.payment_method_id === 10) {
        if (res && res.data && res.data.payment_url) {
          window.location.href = res.data.payment_url;
        }
      } else if (res.data.invoice_id) {
        this.props.dispatch(checkoutSuccessAC());
      }
    }).catch(error => {
      this.setState({
        isLoading: false,
        isSuccess: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  render() {
    const {cart} = this.props;
    const {formData, cartTotal, isLoading, isSuccess} = this.state;
    const isBtnSubmitDisabled = !formData?.receiver?.id || isLoading;
    return (
      <Fragment>
        <Helmet>
          <title>Đặt hàng</title>
        </Helmet>
        {
          !isMobile &&
          <Breadcrumb>
            <BreadcrumbItem text="Đặt hàng" isActive={true}/>
          </Breadcrumb>
        }
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30">Đặt hàng</TitleMobile>
          </HeaderMobile>
        }
        <div className="checkout-page common-page">
          {/*{*/}
          {/*  !isMobile &&*/}
          <div className="container">
            {
              isSuccess &&
              <div className="no-data">
                <img src={successIcon} alt={`Success`}/>
                <p>Đặt hàng thành công!</p>
                <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
              </div>
            }
            {
              !isSuccess &&
              <div className="e-row">
                <div className="col-main">
                  <div className="content">
                    <CheckoutAddresses
                      formData={formData}
                      setData={this.setData}
                    />
                    <CheckoutShippingMethod
                      formData={formData}
                      setData={this.setData}/>
                    <CheckoutPaymentMethod
                      formData={formData}
                      setData={this.setData}/>
                    <CheckoutReceiver
                      formData={formData}
                      setData={this.setData}/>
                    {
                      isMobile &&
                      <div className="step">
                        <label className="step-title">05 - Kiểm tra thông tin</label>
                        <div className="step-content">
                          {
                            cartTotal &&
                            <CheckoutCartInfo
                              cart={cart}
                              cartTotal={cartTotal}
                              formData={formData}
                              setData={this.setData}
                            />
                          }
                          <AddVoucher voucher={formData.voucher} handleAddVoucher={this.handleAddVoucher}/>
                        </div>
                      </div>
                    }
                    {
                      !isMobile &&
                      <div className="btn-checkout-container">
                        <button
                          className="btn btn-primary btn-checkout"
                          type="submit"
                          disabled={isBtnSubmitDisabled}
                          onClick={this.handleSubmit}
                        >
                          Thanh toán
                        </button>
                      </div>
                    }
                  </div>
                </div>
                {
                  !isMobile &&
                  <div className="col-left">
                    {
                      cartTotal &&
                      <CheckoutCartInfo
                        cart={cart}
                        cartTotal={cartTotal}
                        formData={formData}
                        setData={this.setData}/>
                    }
                    <AddVoucher voucher={formData.voucher} handleAddVoucher={this.handleAddVoucher}/>
                  </div>
                }
              </div>
            }
          </div>
          {
            isMobile && !isSuccess &&
            <div className="checkout-btn-bottom btn-bottom-mobile">
              <button
                className="btn btn-danger-mobile btn-xl"
                disabled={isBtnSubmitDisabled}
                onClick={this.handleSubmit}
              >
                Thanh toán
              </button>
            </div>
          }
          {/* }*/}
          {/*{*/}
          {/*  isMobile &&*/}
          {/*    <Fragment>*/}
          {/*      <div className="container">*/}
          {/*        {*/}
          {/*          isSuccess &&*/}
          {/*          <div className="no-data">*/}
          {/*            <img src={successIcon} alt={`Success`}/>*/}
          {/*            <p>Đặt hàng thành công!</p>*/}
          {/*            <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>*/}
          {/*          </div>*/}
          {/*        }*/}
          {/*        {*/}
          {/*          !isSuccess &&*/}
          {/*          <Fragment>*/}
          {/*            <CheckoutMobile*/}
          {/*              formData={formData}*/}
          {/*              setData={this.setData}*/}
          {/*            />*/}
          {/*          </Fragment>*/}
          {/*        }*/}
          {/*      </div>*/}
          {/*      <div className="checkout-btn-bottom btn-bottom-mobile">*/}
          {/*        <button className="btn btn-danger-mobile btn-xl">Thanh toán</button>*/}
          {/*      </div>*/}
          {/*    </Fragment>*/}
          {/*}*/}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
  cart: state.pages.order.cart
});

export default connect(mapStateToProps)(Checkout);
