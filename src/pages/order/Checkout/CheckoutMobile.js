import React, {Fragment} from "react";
import CheckoutAddresses from "../_components/CheckoutAddresses/CheckoutAddresses";
import "./CheckoutMobile.scss"

class CheckoutMobile extends React.PureComponent {

  render() {
    const {setData, formData} = this.props;
    return (
      <Fragment>
        <div className="section-mobile">
          <div className="section-title">
            <h2>Địa chỉ nhận hàng</h2>
          </div>
          <div className="section-content">
            <CheckoutAddresses
              formData={formData}
              setData={setData}
            />
          </div>
        </div>
        <div className="checkout-btn-bottom btn-bottom-mobile">
          <div className="sub-total">
            <span>Thành tiền</span>
            <div className="price"> đ</div>
          </div>
          <button className="btn btn-danger-mobile btn-xl">Thanh toán</button>
        </div>
      </Fragment>
    )
  }
}

export default CheckoutMobile;