import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {getCartAC, toggleSelectCartAllItemAC, toggleSelectCartItemAC, toggleSelectCartStoreAC} from '../_redux/actions';
import CartLoading from '../_components/CartTable/CartLoading';
import CartTable from '../_components/CartTable/CartTable';
import {Link} from 'react-router-dom';
import {isMobile} from '../../../common/helpers/browser';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import CartMobile from "./CartMobile";
import "./Cart.scss";
import CheckoutCart from "./CheckoutCart";
import NoData from '../../../components/NoData/NoData';

class Cart extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(getCartAC());
  }

  toggleItem = (id) => () => {
    this.props.dispatch(toggleSelectCartItemAC(id));
  };

  toggleAllItem = (event) => {
    this.props.dispatch(toggleSelectCartAllItemAC(event.target.checked));
  };

  toggleStore = (storeId) => (event) => {
    this.props.dispatch(toggleSelectCartStoreAC(storeId, event.target.checked));
  };

  render() {
    const {cart: {data, isLoading, qty, sub_total}} = this.props;
    if (isLoading && (!data || !data.length)) {
      return <CartLoading/>;
    }
    const hasData = data && !!data.length;
    return (
      <Fragment>
        <Helmet>
          <title>Giỏ hàng</title>
        </Helmet>
        {
          !isMobile &&
          <div className="breadcrumb-cart">
            <div className="container">
              <div className="d-flex justify-content-between align-items-center">
                <h3>Giỏ hàng của tôi</h3>
                <Link to={`/`}>Tiếp tục mua sắm <i className="fas fa-chevron-right"/></Link>
              </div>
            </div>
          </div>
        }
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile>Giỏ hàng ({qty})</TitleMobile>
          </HeaderMobile>
        }
        <div className="cart-page common-page">
          {
            isMobile &&
            <div className="container">
              {
                hasData && <CartMobile data={data} qty={qty} subTotal={sub_total}/>
              }
              {
                !hasData &&
                <NoData
                  title="Không có sản phẩm"
                  description="Không có sản phẩm nào trong giỏ hàng của bạn."
                >
                  <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
                </NoData>
              }
            </div>

          }
          {
            !isMobile &&
            <div className="container">
              <div className="d-flex">
                <Fragment>
                  {
                    hasData &&
                    <CartTable
                      data={data}
                      qty={qty}
                      subTotal={sub_total}
                      toggleItem={this.toggleItem}
                      toggleAllItem={this.toggleAllItem}
                      toggleStore={this.toggleStore}
                    />
                  }
                  {
                    !hasData &&
                    <NoData
                      title="Không có sản phẩm"
                      description="Không có sản phẩm nào trong giỏ hàng của bạn."
                    >
                      <Link className="btn btn-primary" to="/">Tiếp tục mua sắm</Link>
                    </NoData>
                  }
                </Fragment>
                <CheckoutCart subTotal={sub_total}/>
              </div>

            </div>
          }
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  cart: state.pages.order.cart
});

export default connect(mapStateToProps)(Cart);
