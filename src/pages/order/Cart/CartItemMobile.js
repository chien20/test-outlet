import React from "react";
import {imageUrl} from "../../../common/helpers";
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {convertToSlug, numberAsCurrentcy} from "../../../common/helpers/format";
import InputSpin from "../../../components/Form/InputSpin/InputSpin";
import {deleteCartProductAC, updateCartProductAC} from "../_redux/actions";
import {Dropdown} from "react-bootstrap";


const CustomToggle = React.forwardRef(({children, onClick}, ref) => (
  <div
    className="bar-icon'"
    ref={ref}
    onClick={onClick}
  >
    <i className="fa fa-ellipsis-v"/>
  </div>
));

class CartItemMobile extends React.PureComponent {
  handleQtyChange = (qty) => {
    const {data: {id, product_id, product_option_id}} = this.props;
    this.props.dispatch(updateCartProductAC({
      id,
      product_id,
      product_option_id,
      qty
    }));
  };
  handleDelete = () => {
    const {data: {id, product_id, product_option_id}} = this.props;
    this.props.dispatch(deleteCartProductAC({
      id,
      product_id,
      product_option_id
    }));
  };
  render() {
    const {data} = this.props;
    const shopName = data.store.name;
    const product = data.product;
    let imgUrl = null;
    if (product.images && product.images.length) {
      imgUrl = imageUrl(product.images[0].thumbnail_url);
    }
    const bgImage = {};
    if (imgUrl) {
      bgImage.backgroundImage = `url(${imgUrl})`;
    }
    const slug = product.slug ? product.slug : convertToSlug(product.name);
    const productUrl = `/${slug}/p${product.id}`;
    return (
      <div className="cart-item">
        <div className="cart-item-header">
          <Link to={`/shop/${data.store_id}`}>
            <h2 className="c-shop">{shopName}</h2>
          </Link>
          <Dropdown className="cart-item-actions">
            <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components"/>
            <Dropdown.Menu className="dropdown-bar-icon">
              <Dropdown.Item onClick={this.handleDelete}>
                Xóa sản phẩm
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <div className="cart-item-content">
          <Link to={productUrl}>
            <div className="p-image" style={bgImage}/>
          </Link>
          <div className="p-content">
            <Link to={productUrl} className="p-name">{data.display_name}</Link>
            <div className="p-price">
              <span className="old-price">{numberAsCurrentcy(data.display_original_price)} đ</span>
              <span className="new-price">{numberAsCurrentcy(data.display_price)} đ</span>
            </div>
            <div className="p-qty">
              <InputSpin value={data.qty} onChange={this.handleQtyChange} min={1} max={100}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect()(CartItemMobile);

