import React, {Fragment} from "react";
import {publicUrl} from "../../../common/helpers";
import {numberAsCurrentcy} from "../../../common/helpers/format";
import {Link} from "react-router-dom";

class CheckoutCart extends React.PureComponent {

  render() {
    const {subTotal} = this.props;
    return (
        <div className="checkout">
          <div className="overview">
            <h4 className="title">Tóm tắt đơn hàng</h4>
            <ul>
              <li><Fragment>Thành tiền</Fragment><span>{numberAsCurrentcy(subTotal)} đ</span></li>
              <li><Fragment>Phí vẩn chuyển</Fragment><span>0 đ</span>
              </li>
              <li><Fragment>Được giảm giá</Fragment><span>0 đ</span></li>
            </ul>
            <div className="total">Tổng cộng <span className="price">{numberAsCurrentcy(subTotal)} đ</span></div>
            <p className="vat">Đã bao gồm VAT nếu có</p>

          </div>
          <Link to={`/checkout`}><button className="btn btn-xl btn-cart-checkout">Tiến hành đặt hàng</button></Link>
          <div className="payment-methods">
            <p>Chúng tôi chấp nhận</p>
            <ul className="methods">
              <li><img src={publicUrl(`/assets/images/icons/cart-page/COD.svg`)} alt="Cod"/></li>
              <li><img src={publicUrl(`/assets/images/icons/cart-page/Visa.svg`)} alt="Visa"/></li>
              <li><img src={publicUrl(`/assets/images/icons/cart-page/Master.svg`)} alt="Master"/></li>
              <li><img src={publicUrl(`/assets/images/icons/cart-page/ATM.svg`)} alt="ATM"/></li>
            </ul>
          </div>
        </div>
    );
  }
}

export default CheckoutCart;