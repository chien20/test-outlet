import React, {Fragment} from 'react';
import "./CartMobile.scss"
import CartItemMobile from "./CartItemMobile";
import {numberAsCurrentcy} from "../../../common/helpers/format";
import {Link} from "react-router-dom";

class CartMobile extends React.PureComponent {
  render() {
    const {data, subTotal} = this.props;
    return (
      <Fragment>
        <div className="cart-mobile">
          <div className="cart-list-product">
            {
              data.map((item, index) => (
                <CartItemMobile key={index} data={item}/>
              ))
            }
          </div>
        </div>
        <div className="cart-action cart-btn-bottom">
          <div className="sub-total">
            <span>Tạm tính</span>
            <span className="price-total">{numberAsCurrentcy(subTotal)} đ</span>
          </div>
          <button className="btn btn-xl btn-danger-mobile">
            <Link to={`/checkout`}>Tiến hành đặt hàng</Link>
          </button>
        </div>
      </Fragment>
    )
  }
}

export default CartMobile;

