import React from 'react';
import PropTypes from 'prop-types';
import HTMLView from '../../../../../components/View/HTMLView';
import {withGroupDetail} from '../../GroupDetailContext';

class GroupPolicy extends React.PureComponent {
  render() {
    const {group} = this.props;
    if (!group.policy) {
      return null;
    }
    return (
      <div className="group-info group-policy">
        <div className="group-property">
          <div className="property-value">
            <HTMLView html={group.policy}/>
          </div>
        </div>
      </div>
    )
  }
}

GroupPolicy.propTypes = {
  group: PropTypes.object
};

export default withGroupDetail(GroupPolicy);
