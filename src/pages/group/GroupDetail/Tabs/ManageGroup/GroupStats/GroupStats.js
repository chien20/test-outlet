import React from 'react';
import {Tab, Tabs} from 'react-bootstrap';
import StatsMembers from './Tabs/StatsMembers';
import './GroupStats.scss';
import StatsProducts from './Tabs/StatsProducts';
import StatsRevenue from './Tabs/StatsRevenue';
import StatsCommission from './Tabs/StatsCommission';
import StatsOrders from './Tabs/StatsOrders';

class GroupStats extends React.PureComponent {
  render() {
    const {match: {params: {groupId}}} = this.props;
    return (
      <div className="group-stats">
        <div className="page-title">
          <h3>Thống kê</h3>
        </div>
        <div className="page-content">
          <Tabs defaultActiveKey="members" unmountOnExit={true}>
            <Tab eventKey="members" title="Thành viên">
              <StatsMembers groupId={groupId}/>
            </Tab>
            <Tab eventKey="products" title="Sản phẩm">
              <StatsProducts groupId={groupId}/>
            </Tab>
            {/*<Tab eventKey="revenue" title="Doanh thu">*/}
            {/*  <StatsRevenue groupId={groupId}/>*/}
            {/*</Tab>*/}
            {/*<Tab eventKey="commission" title="Hoa hồng">*/}
            {/*  <StatsCommission groupId={groupId}/>*/}
            {/*</Tab>*/}
            <Tab eventKey="orders" title="Đơn hàng">
              <StatsOrders groupId={groupId}/>
            </Tab>
          </Tabs>
        </div>
      </div>
    )
  }
}

export default GroupStats;
