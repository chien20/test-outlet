import React from 'react';
import StatsChart from '../../../../../../../components/Chart/StatsChart/StatsChart';
import {
  getGroupStatsMembersByLevelAPI,
  getGroupStatsMembersByRoleAPI,
  getGroupStatsMembersByTimeAPI
} from '../../../../../../../api/groups';

const transformLevel = (res) => {
  if (res) {
    res.forEach(item => {
      item.levelName = `Level ${item.level}`;
    });
  }
  return res;
};

class StatsMembers extends React.PureComponent {
  render() {
    const {groupId} = this.props;
    return (
      <section className="group-stats-section">
        <StatsChart
          label="Thành viên tham gia"
          type="line"
          selectView={true}
          handleRequest={getGroupStatsMembersByTimeAPI(groupId)}/>
        <div className="e-row">
          <div className="col-half">
            <StatsChart
              label="Theo vai trò"
              type="bar"
              handleRequest={getGroupStatsMembersByRoleAPI(groupId)}
              optionLabelKey="role_name"/>
          </div>
          <div className="col-half">
            <StatsChart
              label="Theo level"
              type="bar"
              handleRequest={getGroupStatsMembersByLevelAPI(groupId)}
              transformResponse={transformLevel}
              optionLabelKey="levelName"/>
          </div>
        </div>
      </section>
    )
  }
}

export default StatsMembers;
