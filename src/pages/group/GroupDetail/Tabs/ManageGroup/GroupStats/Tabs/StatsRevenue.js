import React from 'react';
import StatsChart from '../../../../../../../components/Chart/StatsChart/StatsChart';
import {
  getGroupStatsRevenueByMembersAPI,
  getGroupStatsRevenueByProductsAPI,
  getGroupStatsRevenueByTimeAPI
} from '../../../../../../../api/groups';

const transformProductName = (res) => {
  if (res) {
    res.forEach(item => {
      item.product_name = '...' + `${item.product_name}`.slice(-14);
    });
  }
  return res;
};

class StatsRevenue extends React.PureComponent {
  render() {
    const {groupId} = this.props;
    return (
      <section className="group-stats-section">
        <StatsChart
          label="Doanh thu"
          type="line"
          selectView={true}
          handleRequest={getGroupStatsRevenueByTimeAPI(groupId)}/>
        <div className="e-row">
          <div className="col-half">
            <StatsChart
              label="Top thành viên"
              type="bar"
              handleRequest={getGroupStatsRevenueByMembersAPI(groupId)}
              optionLabelKey="user_name"/>
          </div>
          <div className="col-half">
            <StatsChart
              label="Top sản phẩm"
              type="bar"
              handleRequest={getGroupStatsRevenueByProductsAPI(groupId)}
              transformResponse={transformProductName}
              optionLabelKey="product_name"/>
          </div>
        </div>
      </section>
    )
  }
}

export default StatsRevenue;
