import React from 'react';
import StatsChart from '../../../../../../../components/Chart/StatsChart/StatsChart';
import {
  getGroupStatsProductsActiveAPI,
  getGroupStatsProductsDeleteAPI,
  getGroupStatsProductsNewAPI,
  getGroupStatsProductsUpdateAPI
} from '../../../../../../../api/groups';

class StatsProducts extends React.PureComponent {
  render() {
    const {groupId} = this.props;
    return (
      <section className="group-stats-section">
        <div className="e-row">
          <div className="col-half">
            <StatsChart
              label="Sản phẩm bày bán"
              type="line"
              selectView={true}
              handleRequest={getGroupStatsProductsActiveAPI(groupId)}/>
          </div>
          <div className="col-half">
            <StatsChart
              label="Sản phẩm mới"
              type="line"
              selectView={true}
              handleRequest={getGroupStatsProductsNewAPI(groupId)}/>
          </div>
        </div>
        <div className="e-row">
          <div className="col-half">
            <StatsChart
              label="Sản phẩm cập nhật"
              type="line"
              selectView={true}
              handleRequest={getGroupStatsProductsUpdateAPI(groupId)}/>
          </div>
          <div className="col-half">
            <StatsChart
              label="Sản phẩm bị xóa"
              type="line"
              selectView={true}
              handleRequest={getGroupStatsProductsDeleteAPI(groupId)}/>
          </div>
        </div>
      </section>
    )
  }
}

export default StatsProducts;
