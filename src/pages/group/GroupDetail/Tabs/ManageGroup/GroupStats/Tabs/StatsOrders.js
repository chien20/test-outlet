import React from 'react';
import StatsChart from '../../../../../../../components/Chart/StatsChart/StatsChart';
import {
  getGroupStatsOrdersByMembersAPI,
  getGroupStatsOrdersByProductsAPI,
  getGroupStatsOrdersByTimeAPI
} from '../../../../../../../api/groups';

const transformProductName = (res) => {
  if (res) {
    res.forEach(item => {
      item.product_name = '...' + `${item.product_name}`.slice(-14);
    });
  }
  return res;
};

class StatsOrders extends React.PureComponent {
  render() {
    const {groupId} = this.props;
    return (
      <section className="group-stats-section">
        <StatsChart
          label="Đơn hàng bán được"
          type="line"
          selectView={true}
          handleRequest={getGroupStatsOrdersByTimeAPI(groupId)}/>
        <div className="e-row">
          <div className="col-half">
            <StatsChart
              label="Top thành viên"
              type="bar"
              handleRequest={getGroupStatsOrdersByMembersAPI(groupId)}
              optionLabelKey="user_name"/>
          </div>
          <div className="col-half">
            <StatsChart
              label="Top sản phẩm"
              type="bar"
              handleRequest={getGroupStatsOrdersByProductsAPI(groupId)}
              transformResponse={transformProductName}
              optionLabelKey="product_name"/>
          </div>
        </div>
      </section>
    )
  }
}

export default StatsOrders;
