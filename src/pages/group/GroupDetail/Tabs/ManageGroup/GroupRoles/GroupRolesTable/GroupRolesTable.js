import React from 'react';
import {Link} from 'react-router-dom';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';
import './GroupRolesTable.scss';

class GroupRolesTable extends React.PureComponent {
  render() {
    const {roles, groupId, handleDelete} = this.props;
    return (
      <div className="group-roles-table table-responsive">
        <table className="table table-bordered">
          <thead>
          <tr>
            <th>Tên vai trò</th>
            <th>Mô tả</th>
            <th>Thành viên</th>
            <th>Thao tác</th>
          </tr>
          </thead>
          <tbody>
          {
            roles.map((item, index) => (
              <tr key={index}>
                <td><Link to={`/groups/${groupId}/manage/roles/${item.id}`}>{item.name}</Link></td>
                <td>{item.description}</td>
                <td>{item.users_count || 0}</td>
                <td className="actions">
                  <Link to={`/groups/${groupId}/manage/roles/${item.id}`} className="text-green">
                    <i className="fas fa-edit"/> Sửa
                  </Link>
                  <HyperLink onClick={handleDelete(item.id)} className="text-red">
                    <i className="fas fa-trash"/> Xóa
                  </HyperLink>
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default GroupRolesTable;
