import React, {Fragment} from 'react';
import {deleteGroupRolesAPI, getListGroupRolesAPI} from '../../../../../../api/groups';
import {showAlert} from '../../../../../../common/helpers/index';
import {history} from '../../../../../../common/utils/router/history';
import {Link} from 'react-router-dom';
import Pagination from '../../../../../../components/Pagination/Pagination';
import CheckboxTable from '../../../../../../components/Table/CheckboxTable';
import HyperLink from '../../../../../../components/HyperLink/HyperLink';

const PAGE_SIZE = 10;

class GroupRoles extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      roleList: [],
      totalItems: 0,
      isLoading: false,
      keyword: '',
      selection: [],
      selectAll: false
    };
  }

  componentDidMount() {
    this.getListGroupRoles(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match !== this.props.match) {
      this.getListGroupRoles(nextProps);
    }
  }

  getListGroupRoles(props) {
    const {match: {params: {page, groupId}}} = props;
    const {keyword} = this.state;
    const params = {
      current_page: page || 1,
      page_size: PAGE_SIZE,
      q: keyword || ''
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getListGroupRolesAPI(groupId, params).then(res => {
      this.setState({
        isLoading: false,
        roleList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được dữ liệu!`
      });
    });
  }

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListProduct(this.props);
  };

  handleDelete = (roleId) => () => {
    const {match: {params: {groupId}}} = this.props;
    deleteGroupRolesAPI(groupId, [roleId]).then(res => {
      showAlert({
        type: 'success',
        message: `Đã xóa`
      });
      this.getListGroupRoles(this.props);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    const {match: {params: {groupId}}} = this.props;
    if (page > 1) {
      history.push(`/groups/${groupId}/manage/roles/p/${page}`);
    } else {
      history.push(`/groups/${groupId}/manage/roles`);
    }
  };

  onSelectChange = (selection, selectAll) => {
    this.setState({
      selection,
      selectAll
    });
  };

  getDetailPageLink = (id) => {
    const {match: {params: {groupId}}} = this.props;
    return `/groups/${groupId}/manage/roles/${id}`;
  };

  columns = [
    {
      Header: 'Tên vai trò',
      accessor: 'name',
      width: 200
    },
    {
      Header: 'Mô tả',
      accessor: 'description'
    },
    {
      Header: 'Thành viên',
      accessor: 'users_count',
      width: 100,
      className: 'text-center'
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 120,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <Fragment>
          <Link to={this.getDetailPageLink(item.id)} className="text-green">
            <i className="fas fa-edit"/> Sửa
          </Link>
          <HyperLink onClick={this.handleDelete(item.id)} className="text-red">
            <i className="fas fa-trash"/> Xóa
          </HyperLink>
        </Fragment>
      )
    }
  ];

  render() {
    const {roleList, totalItems, selection, selectAll} = this.state;
    const {match: {params: {page, groupId}}} = this.props;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <div className="roles-page">
        <div className="page-title">
          <h3>Quản lý vai trò</h3>
          <div className="action-group">
            {/*<form className="search-box" onSubmit={this.handleSearch}>*/}
            {/*<input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>*/}
            {/*<button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>*/}
            {/*</form>*/}
            <Link to={`/groups/${groupId}/manage/roles/add`} className="btn btn-secondary">
              <i className="fa fa-plus"/> Thêm vai trò
            </Link>
          </div>
        </div>
        <div className="page-content">
          <CheckboxTable data={roleList}
                         columns={this.columns}
                         selection={selection}
                         selectAll={selectAll}
                         onSelectChange={this.onSelectChange}
                         showPagination={false}
                         defaultPageSize={PAGE_SIZE}/>
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default GroupRoles;
