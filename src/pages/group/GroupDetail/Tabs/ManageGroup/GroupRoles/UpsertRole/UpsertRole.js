import React from 'react';
import PermissionCheckbox from '../../_components/PermissionCheckbox/PermissionCheckbox';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../../../../common/constants/app';
import {handleInputTextChanged, showAlert} from '../../../../../../../common/helpers/index';
import {createGroupRoleAPI, getRoleDetailAPI, updateGroupRoleAPI} from '../../../../../../../api/groups';
import history from '../../../../../../../common/utils/router/history';
import {Link} from 'react-router-dom';

class UpsertRole extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: '',
        description: '',
        permission_ids: []
      }
    };
  }

  componentDidMount() {
    const {match: {params: {groupId, roleId}}} = this.props;
    if (groupId && roleId) {
      this.getRoleDetail();
    }
  }

  getRoleDetail = () => {
    const {match: {params: {groupId, roleId}}} = this.props;
    getRoleDetailAPI(groupId, roleId).then(res => {
      this.setData(res.data);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    })
  };

  onChange = (permissionId) => (event) => {
    const checked = event.target.checked;
    this.setState(prevState => {
      const permission_ids = [...prevState.formData.permission_ids];
      if (!checked) {
        for (let i = permission_ids.length - 1; i >= 0; i--) {
          if (permission_ids[i] === permissionId) {
            permission_ids.splice(i, 1);
          }
        }
      } else {
        permission_ids.push(permissionId);
      }
      return {
        formData: {
          ...prevState.formData,
          permission_ids
        }
      }
    });
  };

  setData = (data) => {
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  handleSave = () => {
    const {match: {params: {groupId, roleId}}} = this.props;
    const {formData} = this.state;
    if (!roleId) {
      createGroupRoleAPI(groupId, formData).then(res => {
        showAlert({
          type: 'success',
          message: `Đã lưu!`
        });
        history.replace(`/groups/${groupId}/manage/roles`);
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    } else {
      updateGroupRoleAPI(groupId, roleId, formData).then(res => {
        showAlert({
          type: 'success',
          message: `Đã lưu!`
        });
        history.replace(`/groups/${groupId}/manage/roles`);
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    }
  };

  render() {
    const {match: {params: {groupId}}} = this.props;
    const {formData} = this.state;
    return (
      <div className="roles-page">
        <div className="page-title">
          <h1>Thêm vai trò</h1>
        </div>
        <div className="page-content">
          <div className="row">
            <div className="col-md-9">
              <div className="form-group">
                <label>Tên vai trò (*)</label>
                <input
                  type="text"
                  className="form-control"
                  value={formData.name}
                  name="name"
                  required={true}
                  minLength={3}
                  maxLength={100}
                  onChange={handleInputTextChanged('name', this.setData)}/>
                <div className="invalid-feedback">
                  Vui lòng nhập tên vai trò, tối thiểu 3 ký tự, tối đa 100 ký tự.
                </div>
              </div>
              <div className="form-group">
                <label>Mô tả</label>
                <textarea
                  className="form-control"
                  value={formData.description}
                  name="description"
                  onChange={handleInputTextChanged('description', this.setData)}/>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-section">
                <label className="form-section-label">Bài viết</label>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_BAI_VIET}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.DANG_BAI_VIET}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BAI_VIET}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
              </div>
              <div className="form-section">
                <label className="form-section-label">Bình luận</label>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.BINH_LUAN}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BINH_LUAN}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
              </div>
              <div className="form-section">
                <label className="form-section-label">Sản phẩm</label>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_SAN_PHAM}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.DANG_SAN_PHAM}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_SAN_PHAM}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-section">
                <label className="form-section-label">Hội nhóm</label>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_GIOI_THIEU}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.SUA_THONG_TIN_GIOI_THIEU}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                {/*<PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_HOA_HONG}*/}
                {/*                    permission_ids={formData.permission_ids}*/}
                {/*                    onChange={this.onChange}/>*/}
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_THANH_VIEN}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.MOI_THANH_VIEN}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_THANH_VIEN}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
                <PermissionCheckbox permission={GROUP_PERMISSIONS_BY_NAME.XEM_BAO_CAO_THONG_KE}
                                    permission_ids={formData.permission_ids}
                                    onChange={this.onChange}/>
              </div>
            </div>
          </div>
          <div className="form-submit text-right">
            <Link to={`/groups/${groupId}/manage/roles`} className="btn btn-default">Hủy bỏ</Link>
            <button className="btn btn-primary" type="submit" onClick={this.handleSave}>Lưu lại</button>
          </div>
        </div>
      </div>
    )
  }
}

export default UpsertRole;
