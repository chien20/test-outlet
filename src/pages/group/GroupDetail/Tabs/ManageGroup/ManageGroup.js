import React, {Fragment} from 'react';
import StatsCard from './_components/StatsCard/StatsCard';
import ActionMenuItem from './_components/ActionMenu/ActionMenuItem';
import GroupActivity from './_components/GroupActivity/GroupActivity';
import AlertModal from '../../../../../components/modals/AlertModal/AlertModal';
import Broadcaster from '../../../../../common/helpers/broadcaster';
import {
  deleteGroupAPI,
  getGroupStatsMembersCountAPI,
  getGroupStatsOrdersCountAPI,
  getGroupStatsProductsCountAPI,
  getGroupStatsRevenueCountAPI,
  getListGroupActivitiesAPI,
  getListGroupMembersAPI
} from '../../../../../api/groups';
import history from '../../../../../common/utils/router/history';
import {diffTimeToString, showAlert} from '../../../../../common/helpers/index';
import {numberAsCurrentcy} from '../../../../../common/helpers/format';
import './ManageGroup.scss';
import {withGroupDetail} from '../../GroupDetailContext';
import NoData from '../../../../../components/NoData/NoData';

class ManageGroup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      memberCount: 0,
      productCount: 0,
      orderCount: 0,
      revenueCount: 0,
      members: [],
      activities: []
    };
  }

  componentDidMount() {
    this.getStats();
    this.getListGroupMembers();
    this.getListGroupActivities();
  }

  getStats = async () => {
    const {group} = this.props;
    const groupId = group.id;
    try {
      const {data: {total: memberCount}} = await getGroupStatsMembersCountAPI(groupId)();
      this.setState({
        memberCount
      });
    } catch (e) {
      showAlert({
        type: 'error',
        message: `${e.message}`
      });
    }
    try {
      const {data: {total: productCount}} = await getGroupStatsProductsCountAPI(groupId)();
      this.setState({
        productCount
      });
    } catch (e) {
      showAlert({
        type: 'error',
        message: `${e.message}`
      });
    }
    try {
      const {data: {total: orderCount}} = await getGroupStatsOrdersCountAPI(groupId)();
      this.setState({
        orderCount
      });
    } catch (e) {
      showAlert({
        type: 'error',
        message: `${e.message}`
      });
    }
    try {
      const {data: {total: revenueCount}} = await getGroupStatsRevenueCountAPI(groupId)();
      this.setState({
        revenueCount
      });
    } catch (e) {
      showAlert({
        type: 'error',
        message: `${e.message}`
      });
    }
  };

  getListGroupMembers() {
    const {group} = this.props;
    const groupId = group.id;
    const params = {
      current_page: 0,
      page_size: 6,
      sort_by: 'id',
      sort_order: 'desc'
    };
    getListGroupMembersAPI(groupId, params).then(res => {
      const list = res.data && res.data.list ? res.data.list : [];
      const diff = params.page_size - list.length;
      if (diff > 0) {
        for (let i = 0; i < diff; i++) {
          list.push(null);
        }
      }
      this.setState({
        members: list
      });
    }).catch((error) => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  }

  getListGroupActivities() {
    const {group} = this.props;
    const groupId = group.id;
    const params = {
      current_page: 0,
      page_size: 5,
      sort_by: 'id',
      sort_order: 'desc'
    };
    getListGroupActivitiesAPI(groupId, params).then(res => {
      const list = res.data && res.data.list ? res.data.list : [];
      const diff = params.page_size - list.length;
      if (diff > 0) {
        for (let i = 0; i < diff; i++) {
          list.push(null);
        }
      }
      this.setState({
        activities: list
      });
    }).catch((error) => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  }

  handleDelete = async () => {
    const {group} = this.props;
    const groupId = group.id;
    Broadcaster.broadcast('CONFIRM_DELETE_GROUP', {
      isLoading: true
    });
    try {
      const {data, message} = await deleteGroupAPI([groupId]);
      if (data) {
        showAlert({
          type: 'success',
          message: 'Đã xóa!'
        });
        Broadcaster.broadcast('CONFIRM_DELETE_GROUP', {
          isLoading: false,
          isOpen: false
        });
        history.replace('/groups');
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi: ${message}`
        });
      }
    } catch (e) {
      Broadcaster.broadcast('CONFIRM_DELETE_GROUP', {
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  onDelete = (event) => {
    event.preventDefault();
    Broadcaster.broadcast('CONFIRM_DELETE_GROUP', {
      title: 'Xóa hội nhóm',
      message: 'Bạn có chắc muốn xóa hội nhóm này? Toàn bộ dữ liệu liên quan sẽ bị xóa và không thể khôi phục.',
      isLoading: false,
      buttons: [
        {
          name: 'Đồng ý',
          className: 'btn btn-danger',
          onClick: this.handleDelete
        }
      ]
    });
  };

  render() {
    const {group} = this.props;
    const groupId = group.id;
    const {memberCount, productCount, orderCount, revenueCount, members, activities} = this.state;
    return (
      <div className="manage-group">
        <div className="page-content">
          <div className="group-status">
            {
              group.approved_status === 0 &&
              <div className="alert alert-secondary">Hội nhóm đang được chờ phê duyệt</div>
            }
            {
              group.approved_status === 10 &&
              <div className="alert alert-danger">Hội nhóm bị từ chối</div>
            }
            {
              (group.status === 20 || group.status === 30) &&
              <div className="alert alert-danger">Hội nhóm đang bị khóa bởi admin</div>
            }
          </div>
          <div className="group-stats clearfix">
            <div className="stats-card-wrap">
              <StatsCard
                icon="fas fa-users"
                name="Thành viên"
                value={numberAsCurrentcy(memberCount)}/>
            </div>
            <div className="stats-card-wrap">
              <StatsCard
                icon="fas fa-cube"
                name="Sản phẩm"
                value={numberAsCurrentcy(productCount)}
                className="green"/>
            </div>
            <div className="stats-card-wrap">
              <StatsCard
                icon="fas fa-shopping-cart"
                name="Đơn hàng"
                value={numberAsCurrentcy(orderCount)}
                className="orange"/>
            </div>
            {/*<div className="stats-card-wrap">*/}
            {/*  <StatsCard*/}
            {/*    icon="fas fa-coins"*/}
            {/*    name="Doanh thu (VND)"*/}
            {/*    value={numberAsCurrentcy(revenueCount)}*/}
            {/*    className="red"/>*/}
            {/*</div>*/}
          </div>
          <div className="group-actions">
            <h3>Thao tác</h3>
            <div className="action-menu">
              <ActionMenuItem name="Cập nhật thông tin" link={`/groups/${groupId}/manage/info`}/>
              <ActionMenuItem name="Quản lý thành viên" icon="fas fa-users" link={`/groups/${groupId}/manage/members`}/>
              <ActionMenuItem name="Quản lý vai trò" icon="fas fa-users-cog" link={`/groups/${groupId}/manage/roles`}/>
              <ActionMenuItem name="Quản lý sản phẩm" icon="fas fa-cube" link={`/groups/${groupId}/manage/products`}/>
              <ActionMenuItem name="Quản lý bài viết" icon="fas fa-file-word" link={`/groups/${groupId}/manage/posts`}/>
              {/*<ActionMenuItem name="Quản lý bình luận" icon="fas fa-comments" link={`/user/groups/${groupId}/comments`}/>*/}
              {/*<ActionMenuItem name="Báo cáo vi phạm" icon="fas fa-flag" link={`/user/groups/${groupId}/reports`}/>*/}
              <ActionMenuItem name="Thống kê" icon="fas fa-chart-pie" link={`/groups/${groupId}/manage/stats`}/>
              <ActionMenuItem
                name="Xóa hội nhóm"
                icon="fas fa-trash"
                link={`/groups/${groupId}`}
                onClick={this.onDelete}/>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="group-actions">
                <h3>Hoạt động gần đây</h3>
                <div className="list-activity">
                  {
                    activities.map((item, index) => (
                      <GroupActivity
                        key={index}
                        data={item}/>
                    ))
                  }
                  {
                    !activities.filter(item => item).length &&
                    <NoData/>
                  }
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="group-actions">
                <h3>Thành viên mới</h3>
                <table className="table table-bordered table-new-members">
                  <thead>
                  <tr>
                    <th>Tên thành viên</th>
                    <th>Vai trò</th>
                    <th>Thời gian</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    members.map((member, index) => (
                      <Fragment key={index}>
                        {
                          member &&
                          <tr>
                            <td>{member.full_name}</td>
                            <td>{member.role ? member.role.name : member.parent_user_id === member.user_id ? 'Chủ hội nhóm' : ''}</td>
                            <td>{diffTimeToString(member.join_at)}</td>
                          </tr>
                        }
                        {
                          !member &&
                          <tr>
                            <td>&nbsp;</td>
                            <td/>
                            <td/>
                          </tr>
                        }
                      </Fragment>
                    ))
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <AlertModal eventName="CONFIRM_DELETE_GROUP"/>
        </div>
      </div>
    );
  }
}

export default withGroupDetail(ManageGroup);
