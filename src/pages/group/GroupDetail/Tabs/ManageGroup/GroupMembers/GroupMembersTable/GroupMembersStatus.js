import React, {Fragment} from 'react';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';

const GroupMembersStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chờ phê duyệt</span>;
    case 10:
      return <span className="text-red">Đã bị từ chối</span>;
    case 20:
      return <span className="text-green">Đang hoạt động</span>;
    default:
      return '';
  }
};

export const UpdateGroupMembersStatus = ({item, isGroupOwner, handleUpdateStatus, handleDelete, handleEdit}) => {
  if (item.user_id === item.parent_user_id) {
    return null;
  }
  switch (item.approved_status) {
    case 0:
      return (
        <Fragment>
          <HyperLink
            onClick={handleUpdateStatus(item.user_id, 20)}
            className="text-green">
            <i className="fas fa-check"/> Phê duyệt
          </HyperLink>
          <HyperLink
            onClick={handleUpdateStatus(item.user_id, 10)}
            className="text-red">
            <i className="fas fa-ban"/> Từ chối
          </HyperLink>
        </Fragment>
      );
    default:
      return (
        <Fragment>
          {
            !isGroupOwner &&
            <Fragment>
              <HyperLink
                onClick={handleEdit(item)}
                className="text-secondary">
                <i className="fas fa-edit"/> Sửa
              </HyperLink>
              <HyperLink
                onClick={handleDelete(item.user_id)}
                className="text-red">
                <i className="fas fa-trash"/> Xóa
              </HyperLink>
            </Fragment>
          }
        </Fragment>
      );
  }
};

export default GroupMembersStatus;
