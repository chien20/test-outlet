import React from 'react';
import GroupMembersStatus, {UpdateGroupMembersStatus} from './GroupMembersStatus';
import './GroupMembersTable.scss';

class GroupMembersTable extends React.PureComponent {
  render() {
    const {members, handleUpdateStatus, handleEdit, handleDelete} = this.props;
    return (
      <div className="group-members-table table-responsive">
        <table className="table table-bordered">
          <thead>
          <tr>
            <th>Tên thành viên</th>
            <th>Vai trò</th>
            <th>Ngày gia nhập</th>
            <th>Trạng thái</th>
            <th>Thao tác</th>
          </tr>
          </thead>
          <tbody>
          {
            members.map((item, index) => (
              <tr key={index}>
                <td>{item.full_name}</td>
                <td>{item.role ? item.role.name : item.parent_user_id === item.user_id ? 'Chủ hội nhóm' : ''}</td>
                <td>{item.join_at}</td>
                <td><GroupMembersStatus status={item.approved_status}/></td>
                <td className="actions">
                  <UpdateGroupMembersStatus
                    isGroupOwner={item.parent_user_id === item.user_id}
                    item={item}
                    handleUpdateStatus={handleUpdateStatus}
                    handleEdit={handleEdit}
                    handleDelete={handleDelete}/>
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default GroupMembersTable;
