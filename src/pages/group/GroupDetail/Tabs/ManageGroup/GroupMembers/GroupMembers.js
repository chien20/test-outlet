import React from 'react';
import GroupMembersTable from './GroupMembersTable/GroupMembersTable';
import {deleteGroupMemberAPI, getListGroupMembersAPI, updateGroupMemberStatusAPI} from '../../../../../../api/groups';
import {showAlert} from '../../../../../../common/helpers/index';
import {history} from '../../../../../../common/utils/router/history';
import Pagination from '../../../../../../components/Pagination/Pagination';
import InviteUserModal from '../_components/InviteUserModal/InviteUserModal';
import Broadcaster from '../../../../../../common/helpers/broadcaster';
import AlertModal from '../../../../../../components/modals/AlertModal/AlertModal';
import EditUserModal from '../_components/EditUserModal/EditUserModal';

class GroupMembers extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      memberList: [],
      selectedUserId: null,
      totalItems: 0,
      isLoading: false,
      keyword: ''
    };
  }

  componentDidMount() {
    this.getListGroupMembers(this.props);
    Broadcaster.on('EDIT_GROUP_MEMBER_SUCCESS', this.onEditSuccess);
  }

  componentWillUnmount() {
    Broadcaster.off('EDIT_GROUP_MEMBER_SUCCESS', this.onEditSuccess);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match !== this.props.match) {
      this.getListGroupMembers(nextProps);
    }
  }

  onEditSuccess = () => {
    this.getListGroupMembers();
  };

  getListGroupMembers(props) {
    if (!props) {
      props = this.props;
    }
    const {match: {params: {page, groupId}}} = props;
    const {keyword} = this.state;
    const params = {
      current_page: page || 1,
      page_size: 20,
      q: keyword || '',
      sort_by: 'id',
      sort_order: 'desc'
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getListGroupMembersAPI(groupId, params).then(res => {
      this.setState({
        isLoading: false,
        memberList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách thành viên!`
      });
    });
  }

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListGroupMembers(this.props);
  };

  handleUpdateStatus = (userId, status) => () => {
    const {match: {params: {groupId}}} = this.props;
    updateGroupMemberStatusAPI(groupId, userId, {
      approve_status: status,
      reason: 'OK'
    }).then(res => {
      this.getListGroupMembers(this.props);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    })
  };

  handleDelete = async () => {
    const {match: {params: {groupId}}} = this.props;
    const {selectedUserId} = this.state;
    Broadcaster.broadcast('CONFIRM_DELETE_GROUP_MEMBER', {
      isLoading: true
    });
    try {
      const {data, message} = await deleteGroupMemberAPI(groupId, selectedUserId, {
        reason: 'Thích thì xóa!'
      });
      if (data) {
        showAlert({
          type: 'success',
          message: 'Đã xóa!'
        });
        Broadcaster.broadcast('CONFIRM_DELETE_GROUP_MEMBER', {
          isLoading: false,
          isOpen: false
        });
        this.getListGroupMembers(this.props);
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi: ${message}`
        });
      }
    } catch (e) {
      Broadcaster.broadcast('CONFIRM_DELETE_GROUP_MEMBER', {
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  onEdit = (member) => () => {
    Broadcaster.broadcast('EDIT_GROUP_MEMBER', {
      selectedRoleId: member.role.id,
      userId: member.user_id,
    });
  };

  onDelete = (userId) => () => {
    this.setState({
      selectedUserId: userId
    }, () => {
      Broadcaster.broadcast('CONFIRM_DELETE_GROUP_MEMBER', {
        title: 'Xóa thành viên',
        message: 'Bạn có chắc muốn xóa thành viên này?',
        isLoading: false,
        buttons: [
          {
            name: 'Đồng ý',
            className: 'btn btn-danger',
            onClick: this.handleDelete
          }
        ]
      });
    });
  };

  handleInviteMember = () => {
    Broadcaster.broadcast('INVITE_GROUP_MEMBER');
  };

  onPageChanged = (page) => {
    const {match: {params: {groupId}}} = this.props;
    if (page > 1) {
      history.push(`/groups/${groupId}/manage/members/${page}`);
    } else {
      history.push(`/groups/${groupId}/manage/members`);
    }
  };

  render() {
    const {memberList, totalItems, keyword} = this.state;
    const {match: {params: {page, groupId}}} = this.props;
    const totalPage = Math.ceil(totalItems / 20);
    return (
      <div className="product-list-page">
        <div className="page-title">
          <h3>Quản lý thành viên</h3>
          <div className="action-group">
            <form className="search-box" onSubmit={this.handleSearch}>
              <input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>
              <button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>
            </form>
            <button className="btn btn-secondary" onClick={this.handleInviteMember}>
              <i className="fa fa-plus"/> Mời thành viên mới
            </button>
          </div>
        </div>
        <div className="page-content">
          <GroupMembersTable
            members={memberList}
            handleUpdateStatus={this.handleUpdateStatus}
            handleDelete={this.onDelete}
            handleEdit={this.onEdit}/>
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
        <InviteUserModal eventName="INVITE_GROUP_MEMBER" groupId={groupId}/>
        <EditUserModal eventName={'EDIT_GROUP_MEMBER'} groupId={groupId}/>
        <AlertModal eventName="CONFIRM_DELETE_GROUP_MEMBER"/>
      </div>
    )
  }
}

export default GroupMembers;
