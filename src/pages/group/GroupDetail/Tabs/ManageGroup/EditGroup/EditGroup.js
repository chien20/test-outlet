import React from 'react';
import {Tab, Tabs} from 'react-bootstrap';
import GroupInfo from '../_components/EditGroup/GroupInfo';
// import CommissionInfo from '../_components/EditGroup/CommissionInfo';
import {Link} from 'react-router-dom';
import {getGroupDetailAPI, updateGroupAPI} from '../../../../../../api/groups';
import {imageUrl, showAlert} from '../../../../../../common/helpers/index';
import {deleteMediaAPI, getObjectMediaAPI, uploadMediaAPI} from '../../../../../../api/media';
import {OBJECT_TYPES} from '../../../../../../common/constants/objectTypes';
import CommissionInfo from '../_components/EditGroup/CommissionInfo';
import './EditGroup.scss';

class EditGroup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: '',
        description: '',
        privacy: 10, // 10: public, 20: private
        service_fee: 10,
        manage_fee: 10,
        reseller_commission: 30,
        customer_commission: 30,
        affiliate_commission: 10,
        affiliate_commission_detail: [100],
        reseller_commission_detail: [100],
        agree_condition: true,
        questions_list: [], //optional
        verify_type: 10, //(10: SDT, 20: Email)
        policy: '',
        media: [],
      },
      files: {
        avatar_file: null,
        cover_file: null,
        contract_file: null,
        authority_file: null
      },
      isLoading: false,
      isSuccess: false,
      activeStep: 0,
      toggleTime: '',
    };
  }

  componentDidMount() {
    this.getGroupDetail();
    document.addEventListener('keydown', this.toggleFullMode);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.toggleFullMode);
  }

  getGroupDetail = () => {
    this.getDataAsync()
    .then((group) => {
      this.setState({
        formData: group,
        initialFormData: JSON.parse(JSON.stringify(group)),
      });
    })
    .catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  getDataAsync = async () => {
    const {match: {params: {groupId}}} = this.props;
    const {data: group} = await getGroupDetailAPI(groupId);
    const {data: {list: media}} = await getObjectMediaAPI(groupId, OBJECT_TYPES.Group, null, {page_size: 100});
    group.media = media || [];
    group.media.forEach(item => {
      item.use_type = item.type;
      delete item.type;
      item.preview_url = imageUrl(item.url);
    });
    return group;
  };

  toggleFullMode = (event) => {
    let {location: {pathname}, history} = this.props;
    if (event?.keyCode === 84 && event.altKey) {
      pathname = `${pathname}`.replace(/\/$/, "");
      if (pathname.endsWith('info')) {
        history.replace(pathname.replaceAll('info', 'info-full'));
      } else {
        history.replace(pathname.replaceAll('info-full', 'info'));
      }
      this.setState({
        toggleTime: new Date().toISOString(),
      });
    }
  };

  setData = (data) => {
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  handleFileChange = (name) => (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.setState(prevState => {
        return {
          ...prevState,
          files: {
            ...prevState.files,
            [name]: file
          }
        };
      });
    }
  };

  handleSave = async () => {
    const {match: {params: {groupId}}} = this.props;
    const {formData: {media, ...postData}, files, initialFormData} = this.state;
    try {
      let errorCount = 0;
      await updateGroupAPI(groupId, postData);
      if (files.avatar_file) {
        const data = {
          object_type: OBJECT_TYPES.Group,
          object_id: groupId,
          type: 'avatar'
        };
        await uploadMediaAPI(files.avatar_file, data);
      }
      const initialMediaIds = (initialFormData?.media || []).map(item => item.id);
      const mediaIds = (media || []).map(item => item.id);
      const newMedia = (media || []).filter(item => !item.id);
      const deletedMediaIds = initialMediaIds.filter(id => !mediaIds.includes(id));
      if (newMedia.length) {
        for (let i = 0; i < newMedia.length; i++) {
          try {
            await uploadMediaAPI(newMedia[i], {
              object_type: OBJECT_TYPES.Group,
              object_id: groupId,
              type: newMedia[i].use_type,
            });
          } catch (error) {
            errorCount++;
          }
        }
      }
      if (deletedMediaIds.length) {
        try {
          await deleteMediaAPI(deletedMediaIds);
        } catch (e) {
          errorCount++;
        }
      }
      showAlert({
        type: 'success',
        message: `Đã lưu`
      });
    } catch (error) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    }
  };

  render() {
    const {match: {params: {groupId}, path}} = this.props;
    const {formData, files, toggleTime} = this.state;
    return (
      <div className="register-group-form edit-group-page">
        <div className="page-title">
          <h3>Sửa hội nhóm</h3>
        </div>
        <div className="page-content">
          <Tabs defaultActiveKey="groupInfo" id="edit-group" key={toggleTime}>
            <Tab eventKey="groupInfo" title="Thông tin cơ bản">
              <GroupInfo
                formData={formData}
                files={files}
                setData={this.setData}
                handleFileChange={this.handleFileChange}/>
            </Tab>
            {
              `${path}`.includes('info-full') &&
              <Tab eventKey="commissionInfo" title="Tỷ lệ điểm thưởng">
                <CommissionInfo
                  formData={formData}
                  setData={this.setData}
                />
              </Tab>
            }
          </Tabs>
          <div className="form-submit text-right">
            <Link to={`/groups/${groupId}/manage`} className="btn btn-default">Hủy bỏ</Link>
            <button className="btn btn-primary" type="submit" onClick={this.handleSave}>Cập nhật</button>
          </div>
        </div>
      </div>
    );
  }
}

export default EditGroup;
