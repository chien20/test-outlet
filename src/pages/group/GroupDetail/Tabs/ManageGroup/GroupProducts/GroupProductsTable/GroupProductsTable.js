import React from 'react';
import GroupProductsStatus, {UpdateGroupProductsStatus} from './GroupProductsStatus';
import './GroupProductsTable.scss';

class GroupProductsTable extends React.PureComponent {
  render() {
    const {members, handleUpdateStatus, handleDelete} = this.props;
    return (
      <div className="group-products-table table-responsive">
        <table className="table table-bordered">
          <thead>
          <tr>
            <th>Tên sản phẩm</th>
            {/*<th>Người bán</th>*/}
            <th>Ngày đăng</th>
            <th>Trạng thái</th>
            <th>Thao tác</th>
          </tr>
          </thead>
          <tbody>
          {
            members.map((item, index) => (
              <tr key={index}>
                <td>{item.name}</td>
                {/*<td>{item.store_id ? '' : ''}</td>*/}
                <td>{item.created_at}</td>
                <td><GroupProductsStatus status={item.approved_status}/></td>
                <td className="actions">
                  <UpdateGroupProductsStatus
                    item={item}
                    handleUpdateStatus={handleUpdateStatus}
                    handleDelete={handleDelete}/>
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    )
  }
}

export default GroupProductsTable;
