import React from 'react';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';

const GroupProductsStatus = ({status, reviewed = false}) => {
  switch (status) {
    case 0:
      return <span>Chờ phê duyệt</span>;
    case 10:
      return <span className="text-red">Đã bị từ chối</span>;
    case 20:
      if (!reviewed) {
        return <span className="text-blue">Chưa phê duyệt</span>;
      }
      return <span className="text-green">Đã phê duyệt</span>;
    default:
      return '';
  }
};

export const UpdateGroupProductsStatus = ({item, isGroupOwner, handleUpdateStatus, handleUpdateReview}) => {
  const actions = [];
  if (!item.reviewed) {
    actions.push(
      <HyperLink onClick={handleUpdateReview(item.id, true)} className="text-blue" key="reviewed" title="Phê duyệt">
        <i className="fas fa-check"/>
      </HyperLink>
    );
  }
  if (item.approved_status === 10) {
    actions.push(
      <HyperLink onClick={handleUpdateStatus(item.id, 20)} className="text-green" key="restore" title="Khôi phục">
        <i className="fas fa-check"/>
      </HyperLink>
    )
  }
  if (item.approved_status !== 10) {
    actions.push(
      <HyperLink onClick={handleUpdateStatus(item.id, 10)} className="text-red" key="block" title="Chặn">
        <i className="fas fa-ban"/>
      </HyperLink>
    )
  }

  return actions;
};

export default GroupProductsStatus;
