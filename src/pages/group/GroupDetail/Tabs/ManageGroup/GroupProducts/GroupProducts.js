import React from 'react';
import {approveGroupProductAPI, getListGroupProductAPI, updateReviewFlagAPI} from '../../../../../../api/groups';
import {showAlert} from '../../../../../../common/helpers/index';
import {history} from '../../../../../../common/utils/router/history';
import Pagination from '../../../../../../components/Pagination/Pagination';
import AlertModal from '../../../../../../components/modals/AlertModal/AlertModal';
import {formatDate} from '../../../../../../common/helpers/format';
import GroupProductsStatus, {UpdateGroupProductsStatus} from './GroupProductsTable/GroupProductsStatus';
import CommonTable from '../../../../../../components/Table/CommonTable';

const PAGE_SIZE = 10;

class GroupProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      selectedProductId: null,
      totalItems: 0,
      isLoading: false,
      keyword: '',
    };
  }

  componentDidMount() {
    this.getListGroupProducts(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match !== this.props.match) {
      this.getListGroupProducts(nextProps);
    }
  }

  getListGroupProducts(props) {
    const {match: {params: {page, groupId}}} = props;
    const {keyword} = this.state;
    const params = {
      current_page: page || 1,
      page_size: 20,
      q: keyword || ''
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getListGroupProductAPI(groupId, params).then(res => {
      this.setState({
        isLoading: false,
        products: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách sản phẩm!`
      });
    });
  }

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListGroupProducts(this.props);
  };

  handleUpdateStatus = (productId, status) => async () => {
    const {match: {params: {groupId}}} = this.props;
    try {
      const {data: res, message} = await approveGroupProductAPI(groupId, productId, {
        approved_status: status,
        reason: 'OK'
      });
      if (res) {
        this.getListGroupProducts(this.props);
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi: ${message}`
        });
      }
    } catch (e) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  handleUpdateReview = (productId, reviewed) => async () => {
    const {match: {params: {groupId}}} = this.props;
    try {
      const {data: res, message} = await updateReviewFlagAPI(groupId, productId, reviewed);
      if (res) {
        this.getListGroupProducts(this.props);
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi: ${message}`
        });
      }
    } catch (e) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  onPageChanged = (page) => {
    const {match: {params: {groupId}}} = this.props;
    if (page > 1) {
      history.push(`/groups/${groupId}/manage/products/${page}`);
    } else {
      history.push(`/groups/${groupId}/manage/products`);
    }
  };

  columns = [
    {
      Header: 'Tên sản phẩm',
      accessor: 'name'
    },
    {
      Header: 'Ngày đăng',
      id: 'created_date',
      width: 120,
      accessor: (item) => (
        <div>{formatDate(item.created_at)}</div>
      ),
    },
    {
      Header: 'Trạng thái',
      id: 'status',
      width: 120,
      accessor: (item) => (
        <GroupProductsStatus status={item.approved_status} reviewed={item.reviewed}/>
      ),
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 100,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <UpdateGroupProductsStatus
          item={item}
          handleUpdateStatus={this.handleUpdateStatus}
          handleUpdateReview={this.handleUpdateReview}
        />
      ),
    }
  ];

  render() {
    const {products, totalItems, keyword} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / 20);
    return (
      <div className="product-list-page">
        <div className="page-title">
          <h3>Quản lý sản phẩm</h3>
          <div className="action-group">
            <form className="search-box" onSubmit={this.handleSearch}>
              <input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>
              <button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>
            </form>
          </div>
        </div>
        <div className="page-content">
          <CommonTable
            data={products}
            columns={this.columns}
            showPagination={false}
            defaultPageSize={PAGE_SIZE}
            minRows={0}
          />
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
        <AlertModal eventName="CONFIRM_DELETE_GROUP_PRODUCT"/>
      </div>
    )
  }
}

export default GroupProducts;
