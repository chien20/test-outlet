import React from 'react';
import {Route, Switch} from 'react-router-dom';
import ManageGroup from './ManageGroup';
import EditGroup from './EditGroup/EditGroup';
import GroupPosts from './GroupPosts/GroupPosts';
import GroupMembers from './GroupMembers/GroupMembers';
import GroupProducts from './GroupProducts/GroupProducts';
import GroupRoles from './GroupRoles/GroupRoles';
import UpsertRole from './GroupRoles/UpsertRole/UpsertRole';
import GroupStats from './GroupStats/GroupStats';
import PermissionChecker from '../../../_components/PermissionChecker/PermissionChecker';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../../common/constants/app';

const tabs = [
  {
    path: (path) => [`${path}/info`, `${path}/info-full`],
    component: EditGroup,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.SUA_THONG_TIN_GIOI_THIEU.id],
  },
  {
    path: (path) => [`${path}/products`, `${path}/products/p/:page?`],
    component: GroupProducts,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_SAN_PHAM.id],
  },
  {
    path: (path) => [`${path}/posts`, `${path}/posts/p/:page?`],
    component: GroupPosts,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BAI_VIET.id],
  },
  {
    path: (path) => [`${path}/members`, `${path}/members/p/:page?`],
    component: GroupMembers,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_THANH_VIEN.id],
  },
  {
    path: (path) => [`${path}/roles`, `${path}/roles/p/:page?`],
    component: GroupRoles,
    exact: true,
    permissions: [],
  },
  {
    path: (path) => [`${path}/roles/add`, `${path}/roles/:roleId`],
    component: UpsertRole,
    exact: true,
    permissions: [],
  },
  {
    path: (path) => `${path}/stats`,
    component: GroupStats,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_BAO_CAO_THONG_KE.id],
  },
];

const ManageGroupRoutes = ({match: {path}, permissions, isOwner, onRestrict}) => {
  return (
    <div className="manage-group-pages">
      <Switch>
        <Route
          exact
          component={ManageGroup}
          path={path}
        />
        {
          tabs.map((tab) => {
            const Component = tab.component;
            return (
              <Route
                key={tab.path}
                path={tab.path(path)}
                exact={tab.exact}
                render={(props) => (
                  <PermissionChecker
                    userPermissions={permissions}
                    requiredPermissions={tab.permissions}
                    onRestrict={onRestrict}
                    isOwner={isOwner}
                  >
                    <Component
                      {...props}
                      permissions={permissions}
                      onRestrict={onRestrict}
                      isOwner={isOwner}
                    />
                  </PermissionChecker>
                )}
              />
            );
          })
        }
      </Switch>
    </div>
  );
};

export default ManageGroupRoutes;
