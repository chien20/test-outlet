import React, {Fragment} from 'react';
import {handleInputTextChanged} from '../../../../../../../common/helpers/index';
import CommissionDetail from '../../../../../RegisterGroup/Steps/CommissionDetail';
import CommissionExplain from './CommissionExplain';
import './CommissionInfo.scss';
import {isMobile} from "../../../../../../../common/helpers/browser";

class CommissionInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      focusElement: 'common'
    };
  }

  focusElement = (name, state) => () => {
    if (state) {
      this.setState({
        focusElement: name
      });
    } else {
      this.setState(prevState => {
        if (prevState.focusElement === name) {
          return {
            focusElement: 'common'
          };
        }
        return null;
      });
    }
  };

  isValid = () => {
    const {formData: {manage_fee, service_fee, customer_commission, reseller_commission, affiliate_commission}} = this.props;
    const total = manage_fee + service_fee + customer_commission + reseller_commission + affiliate_commission;
    return total === 100;
  };

  render() {
    const {formData, setData} = this.props;
    const {focusElement} = this.state;
    return (
      <Fragment>
        {
          !isMobile &&
          <div className="commission-info">
            <div className="quote">
              <p>
                Nhằm khuyến khích người dùng mua hàng trong hội nhóm, hoa hồng nhận được mỗi khi bán một sản phẩm sẽ được
                chia cho một số người có vai trò phân phối sản phẩm đó trong hội nhóm. Bằng cách thiết lập tỷ lệ dưới đây,
                bạn có thể điều chỉnh tỷ lệ hoa hồng được hưởng của các vai trò.
              </p>
              <p>
                <strong className="hint">Gợi ý:</strong> Xem ví dụ bên cạnh để biết thêm chi tiết.
              </p>
            </div>
            <div className="row">
              <div className="col-md-5">
                <div className="form-section">
                  {/*<label className="form-section-label">Tỷ lệ chung</label>*/}
                  {
                    !this.isValid() &&
                    <div className="alert alert-danger">
                      <strong>Lỗi</strong>
                      <div>Tổng các giá trị trong mục này phải bằng 100%</div>
                    </div>
                  }
                  <div className="form-group row">
                    <label className="col-sm-8">Tỷ lệ thưởng cho ban quản trị/chủ hội nhóm <sup>*</sup></label>
                    <div className="input-group col-sm-4">
                      <input
                          type="number"
                          className="form-control"
                          value={formData.manage_fee}
                          name="manage_fee"
                          required={true}
                          min={0}
                          max={100}
                          onFocus={this.focusElement('common.manage_fee', true)}
                          onBlur={this.focusElement('common.manage_fee', false)}
                          onChange={handleInputTextChanged('manage_fee', setData, 'float')}/>
                      <div className="input-group-append" style={{display: 'block'}}>
                        <div className="input-group-text">%</div>
                      </div>
                      <div className="invalid-feedback">
                        Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-8">Tỷ lệ thưởng cho Outlet <sup>*</sup></label>
                    <div className="input-group col-sm-4">
                      <input
                          type="number"
                          className="form-control"
                          value={formData.service_fee}
                          name="service_fee"
                          required={true}
                          min={0}
                          max={100}
                          onFocus={this.focusElement('common.service_fee', true)}
                          onBlur={this.focusElement('common.service_fee', false)}
                          onChange={handleInputTextChanged('service_fee', setData, 'float')}/>
                      <div className="input-group-append">
                        <div className="input-group-text">%</div>
                      </div>
                      <div className="invalid-feedback">
                        Vui lòng nhập tỷ lệ thưởng cho Outlet (từ 0-100% hoa hồng).
                      </div>
                    </div>
                  </div>
                  <div className="form-group row">
                    <label className="col-sm-8">Tỷ lệ thưởng cho thành viên <sup>*</sup></label>
                    <div className="input-group col-sm-4">
                      <input
                          type="text"
                          className="form-control"
                          value={formData.customer_commission}
                          name="customer_commission"
                          required={true}
                          min={0}
                          max={100}
                          onFocus={this.focusElement('common.customer_commission', true)}
                          onBlur={this.focusElement('common.customer_commission', false)}
                          onChange={handleInputTextChanged('customer_commission', setData, 'float')}/>
                      <div className="input-group-append">
                        <div className="input-group-text">%</div>
                      </div>
                      <div className="invalid-feedback">
                        Vui lòng nhập tỷ lệ thưởng cho người mua (từ 0-100% hoa hồng).
                      </div>
                    </div>
                  </div>
                  {/*<div className="form-group row">*/}
                  {/*  <label className="col-sm-8">Tỷ lệ thưởng cho đại lý <sup>*</sup></label>*/}
                  {/*  <div className="input-group col-sm-4">*/}
                  {/*    <input*/}
                  {/*        type="number"*/}
                  {/*        className="form-control"*/}
                  {/*        value={formData.reseller_commission}*/}
                  {/*        name="reseller_commission"*/}
                  {/*        required={true}*/}
                  {/*        min={0}*/}
                  {/*        max={100}*/}
                  {/*        onFocus={this.focusElement('common.reseller_commission', true)}*/}
                  {/*        onBlur={this.focusElement('common.reseller_commission', false)}*/}
                  {/*        onChange={handleInputTextChanged('reseller_commission', setData, 'float')}/>*/}
                  {/*    <div className="input-group-append">*/}
                  {/*      <div className="input-group-text">%</div>*/}
                  {/*    </div>*/}
                  {/*    <div className="invalid-feedback">*/}
                  {/*      Vui lòng nhập tỷ lệ thưởng cho đại lý (từ 0-100% hoa hồng).*/}
                  {/*    </div>*/}
                  {/*  </div>*/}
                  {/*</div>*/}
                  {/*<div className="form-group row">*/}
                  {/*  <label className="col-sm-8">Tỷ lệ thưởng cho người giới thiệu <sup>*</sup></label>*/}
                  {/*  <div className="input-group col-sm-4">*/}
                  {/*    <input*/}
                  {/*        type="text"*/}
                  {/*        className="form-control"*/}
                  {/*        value={formData.affiliate_commission}*/}
                  {/*        name="affiliate_commission"*/}
                  {/*        required={true}*/}
                  {/*        min={0}*/}
                  {/*        max={100}*/}
                  {/*        onFocus={this.focusElement('common.affiliate_commission', true)}*/}
                  {/*        onBlur={this.focusElement('common.affiliate_commission', false)}*/}
                  {/*        onChange={handleInputTextChanged('affiliate_commission', setData, 'float')}/>*/}
                  {/*    <div className="input-group-append">*/}
                  {/*      <div className="input-group-text">%</div>*/}
                  {/*    </div>*/}
                  {/*    <div className="invalid-feedback">*/}
                  {/*      Vui lòng nhập tỷ lệ thưởng cho người giới thiệu (từ 0-100% hoa hồng).*/}
                  {/*    </div>*/}
                  {/*  </div>*/}
                  {/*</div>*/}
                </div>
                {/*<CommissionDetail*/}
                {/*    formData={formData}*/}
                {/*    setData={setData}*/}
                {/*    objectName="đại lý"*/}
                {/*    dataKey="reseller_commission_detail"*/}
                {/*    relatedDataKey="reseller_commission"/>*/}
                {/*<CommissionDetail*/}
                {/*    formData={formData}*/}
                {/*    setData={setData}*/}
                {/*    objectName="người giới thiệu"*/}
                {/*    dataKey="affiliate_commission_detail"*/}
                {/*    relatedDataKey="affiliate_commission"/>*/}
              </div>
              <div className="col-md-7">
                <CommissionExplain
                    focusElement={focusElement}
                    formData={formData}
                />
              </div>
            </div>
          </div>
        }
        {
          isMobile &&
          <div className="commission-info">

            <div className="row">
              <div className="col-12">
                <div className="form-section">
                  {
                    !this.isValid() &&
                    <div className="alert alert-danger">
                      <strong>Lỗi</strong>
                      <div>Tổng các giá trị trong mục này phải bằng 100%</div>
                    </div>
                  }
                  <div className="form-group">
                    <div className="wrapper">
                      <label>Tỷ lệ thưởng cho chủ hội nhóm <sup>*</sup></label>
                      <div className="input-group">
                        <input
                            type="number"
                            className="form-control"
                            value={formData.manage_fee}
                            name="manage_fee"
                            required={true}
                            min={0}
                            max={100}
                            onFocus={this.focusElement('common.manage_fee', true)}
                            onBlur={this.focusElement('common.manage_fee', false)}
                            onChange={handleInputTextChanged('manage_fee', setData, 'float')}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                      </div>
                    </div>
                    <div className="invalid-feedback">
                      Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="wrapper">
                      <label>Tỷ lệ thưởng cho Outlet <sup>*</sup></label>
                      <div className="input-group">
                        <input
                            type="number"
                            className="form-control"
                            value={formData.service_fee}
                            name="service_fee"
                            required={true}
                            min={0}
                            max={100}
                            onFocus={this.focusElement('common.service_fee', true)}
                            onBlur={this.focusElement('common.service_fee', false)}
                            onChange={handleInputTextChanged('service_fee', setData, 'float')}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                      </div>
                    </div>
                    <div className="invalid-feedback">
                      Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="wrapper">
                      <label>Tỷ lệ thưởng cho người mua <sup>*</sup></label>
                      <div className="input-group">
                        <input
                            type="text"
                            className="form-control"
                            value={formData.customer_commission}
                            name="customer_commission"
                            required={true}
                            min={0}
                            max={100}
                            onFocus={this.focusElement('common.customer_commission', true)}
                            onBlur={this.focusElement('common.customer_commission', false)}
                            onChange={handleInputTextChanged('customer_commission', setData, 'float')}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                      </div>
                    </div>
                    <div className="invalid-feedback">
                      Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="wrapper">
                      <label>Tỷ lệ thưởng cho đại lý <sup>*</sup></label>
                      <div className="input-group">
                        <input
                            type="number"
                            className="form-control"
                            value={formData.reseller_commission}
                            name="reseller_commission"
                            required={true}
                            min={0}
                            max={100}
                            onFocus={this.focusElement('common.reseller_commission', true)}
                            onBlur={this.focusElement('common.reseller_commission', false)}
                            onChange={handleInputTextChanged('reseller_commission', setData, 'float')}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                      </div>
                    </div>
                    <div className="invalid-feedback">
                      Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="wrapper">
                      <label>Tỷ lệ thưởng cho người giới thiệu <sup>*</sup></label>
                      <div className="input-group">
                        <input
                            type="text"
                            className="form-control"
                            value={formData.affiliate_commission}
                            name="affiliate_commission"
                            required={true}
                            min={0}
                            max={100}
                            onFocus={this.focusElement('common.affiliate_commission', true)}
                            onBlur={this.focusElement('common.affiliate_commission', false)}
                            onChange={handleInputTextChanged('affiliate_commission', setData, 'float')}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                      </div>
                    </div>
                    <div className="invalid-feedback">
                      Vui lòng nhập tỷ lệ thưởng cho chủ hội nhóm (từ 0-100% hoa hồng).
                    </div>
                  </div>
                </div>
                {/*<CommissionDetail*/}
                {/*    formData={formData}*/}
                {/*    setData={setData}*/}
                {/*    objectName="đại lý"*/}
                {/*    dataKey="reseller_commission_detail"*/}
                {/*    relatedDataKey="reseller_commission"/>*/}
                {/*<CommissionDetail*/}
                {/*    formData={formData}*/}
                {/*    setData={setData}*/}
                {/*    objectName="người giới thiệu"*/}
                {/*    dataKey="affiliate_commission_detail"*/}
                {/*    relatedDataKey="affiliate_commission"/>*/}
              </div>
            </div>
          </div>
        }
      </Fragment>


    )
  }
}

export default CommissionInfo;
