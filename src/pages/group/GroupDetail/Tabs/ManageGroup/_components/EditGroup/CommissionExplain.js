import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './CommissionExplain.scss';
import {numberAsCurrentcy} from '../../../../../../../common/helpers/format';

const calValue = (total, percent) => {
  if (!percent) {
    return 0;
  }
  const value = Math.floor(total * percent / 100);
  return numberAsCurrentcy(value);
};

class CommissionExplain extends React.PureComponent {
  render() {
    const {focusElement, formData} = this.props;
    const resellerComission = Math.floor(100000 * formData.reseller_commission / 100);
    const affiliateCommission = Math.floor(100000 * formData.affiliate_commission / 100);
    return (
      <div className="commission-explain">
        <h2>Ví dụ:</h2>
        {
          focusElement.indexOf('common') === 0 &&
          <Fragment>
            <p>
              Giả sử sản phẩm có giá <strong>1.000.000đ</strong> và người bán hàng trích ra <strong>100.000đ</strong> để
              trả thưởng cho hệ thống phân phối sản phẩm của hội nhóm. Khi đó:
            </p>
            <ul>
              <li>
                <label>Chủ hội nhóm nhận được:</label>
                100.000đ * {formData.manage_fee}% = {calValue(100000, formData.manage_fee)}đ
              </li>
              <li>
                <label>Outlet nhận được:</label>
                100.000đ * {formData.service_fee}% = {calValue(100000, formData.service_fee)}đ
              </li>
              <li>
                <label>Thành viên hội nhóm khi mua hàng nhận được:</label>
                100.000đ * {formData.customer_commission}% = {calValue(100000, formData.customer_commission)}đ
              </li>
              {/*<li>*/}
              {/*  <label>Đại lý nhận được:</label>*/}
              {/*  100.000đ * {formData.reseller_commission}% = {numberAsCurrentcy(resellerComission)}đ*/}
              {/*</li>*/}
              {/*<li>*/}
              {/*  <label>Người giới thiệu người mua hàng nhận được:</label>*/}
              {/*  100.000đ * {formData.affiliate_commission}% = {numberAsCurrentcy(affiliateCommission)}đ*/}
              {/*</li>*/}
            </ul>
            {/*{*/}
            {/*  formData.reseller_commission > 0 &&*/}
            {/*  <Fragment>*/}
            {/*    <h3>Tỷ lệ thưởng cho đại lý các cấp</h3>*/}
            {/*    <p>*/}
            {/*      Vì bạn có thiết lập hệ thống đại lý phân phối, nên các cấp đại lý sẽ chia*/}
            {/*      nhau <strong>{numberAsCurrentcy(resellerComission)}đ</strong> ở trên theo <strong>Tỷ lệ*/}
            {/*      thưởng cho đại lý các cấp</strong> ở bên.*/}
            {/*    </p>*/}
            {/*    <p>*/}
            {/*      Tiền thưởng được chia như sau:*/}
            {/*    </p>*/}
            {/*    <ul>*/}
            {/*      {*/}
            {/*        formData.reseller_commission_detail.map((item, index) => (*/}
            {/*          <li key={index}>*/}
            {/*            <label>Đại lý cấp {index + 1} nhận được:</label>*/}
            {/*            {numberAsCurrentcy(resellerComission)}đ * {item}% = {calValue(resellerComission, item)}đ*/}
            {/*          </li>*/}
            {/*        ))*/}
            {/*      }*/}
            {/*    </ul>*/}
            {/*  </Fragment>*/}
            {/*}*/}
            {/*{*/}
            {/*  formData.affiliate_commission > 0 &&*/}
            {/*  <Fragment>*/}
            {/*    <h3>Tỷ lệ thưởng cho người giới thiệu các cấp</h3>*/}
            {/*    <p>*/}
            {/*      Vì bạn có thiết lập điểm thưởng cho người giới thiệu người mua hàng, nên những người giới thiệu sẽ*/}
            {/*      chia nhau <strong>{numberAsCurrentcy(affiliateCommission)}đ</strong> ở trên theo <strong>Tỷ lệ*/}
            {/*      thưởng cho người giới thiệu</strong> ở bên.*/}
            {/*    </p>*/}
            {/*    <p>*/}
            {/*      Tiền thưởng được chia như sau:*/}
            {/*    </p>*/}
            {/*    <ul>*/}
            {/*      {*/}
            {/*        formData.affiliate_commission_detail.map((item, index) => (*/}
            {/*          <li key={index}>*/}
            {/*            <label>Người giới thiệu cấp {index + 1} nhận được:</label>*/}
            {/*            {numberAsCurrentcy(affiliateCommission)}đ * {item}% = {calValue(affiliateCommission, item)}đ*/}
            {/*          </li>*/}
            {/*        ))*/}
            {/*      }*/}
            {/*    </ul>*/}
            {/*  </Fragment>*/}
            {/*}*/}
          </Fragment>
        }
      </div>
    );
  }
}

CommissionExplain.propTypes = {
  focusElement: PropTypes.string,
  formData: PropTypes.object
};

export default CommissionExplain;
