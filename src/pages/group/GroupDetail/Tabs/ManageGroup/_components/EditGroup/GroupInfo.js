import React, {Fragment} from 'react';
import {
  // handleCheckBoxChanged,
  handleEditorChanged,
  handleInputTextChanged,
  handleRadioChanged
} from '../../../../../../../common/helpers/index';
import CKEditor from '../../../../../../../components/Editor/CKEditor/CKEditor';
import {isMobile} from "../../../../../../../common/helpers/browser";
import UploadImages from '../../../../../../../components/images/UploadImages';

class GroupInfo extends React.PureComponent {
  render() {
    const {formData, files, setData, handleFileChange} = this.props;
    return (
      <Fragment>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label>Tên hội nhóm <sup>*</sup></label>
              <input
                type="text"
                className="form-control"
                value={formData.name}
                name="name"
                required={true}
                minLength={3}
                maxLength={100}
                onChange={handleInputTextChanged('name', setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập tên hội nhóm, tối thiểu 3 ký tự, tối đa 100 ký tự.
              </div>
            </div>
            {
              !isMobile &&
              <div className="form-group">
                <label>Quyền riêng tư</label>
                <div className="form-inline-group">
                  <div className="form-check form-check-inline">
                    <input
                        className="form-check-input"
                        name="gender"
                        type="radio"
                        id="privacy-public"
                        value={10}
                        checked={`${formData.privacy}` === '10'}
                        onChange={handleRadioChanged('privacy', setData)}/>
                    <label
                        className="form-check-label"
                        htmlFor="privacy-public">Công khai</label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                        className="form-check-input"
                        name="gender"
                        type="radio"
                        id="privacy-private"
                        value={20}
                        checked={`${formData.privacy}` === '20'}
                        onChange={handleRadioChanged('privacy', setData)}/>
                    <label
                        className="form-check-label"
                        htmlFor="privacy-private">Kín</label>
                  </div>
                </div>
              </div>
            }
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label>Ảnh đại diện</label>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  id="avatar_file"
                  onChange={handleFileChange('avatar_file')}/>
                <label className="custom-file-label" htmlFor="avatar_file">
                  {files.avatar_file ? files.avatar_file.name : 'Chọn file...'}
                </label>
                <div className="invalid-feedback">Vui lòng chọn file ảnh đại diện (.jpg, .jpeg, .png)</div>
              </div>
            </div>
            {
              formData?.media &&
              <div className="form-group">
                <label>Hình ảnh cover</label>
                <UploadImages
                  value={formData.media}
                  onChange={setData}
                  fieldName="media"
                  multiple={true}
                  maxFiles={10}
                  useType="cover"
                />
              </div>
            }
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label>Mô tả hội nhóm <sup>*</sup></label>
              <textarea
                className="form-control"
                value={formData.description}
                name="description"
                required={true}
                minLength={30}
                maxLength={1000}
                rows={6}
                onChange={handleInputTextChanged('description', setData)}/>
              <div className="invalid-feedback">
                Vui lòng nhập mô tả hội nhóm, tối thiểu 30 ký tự, tối đa 1000 ký tự.
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="form-group">
              <label>Nội quy hội nhóm</label>
              <CKEditor
                value={formData.policy}
                onChange={handleEditorChanged('policy', setData)}/>
            </div>
          </div>
        </div>
        {
          isMobile &&
          <div className="row wrapper-radio">
            <div className="col-md-12">
              <div className="form-group">
                <label>Quyền riêng tư</label>
                <div className="form-inline-group">
                  <div className="form-check">
                    <input
                        className="form-check-input"
                        name="gender"
                        type="radio"
                        id="privacy-public"
                        value={10}
                        checked={`${formData.privacy}` === '10'}
                        onChange={handleRadioChanged('privacy', setData)}/>
                    <label
                        className="form-check-label"
                        htmlFor="privacy-public">Công khai</label>
                  </div>
                  <div className="form-check">
                    <input
                        className="form-check-input"
                        name="gender"
                        type="radio"
                        id="privacy-private"
                        value={20}
                        checked={`${formData.privacy}` === '20'}
                        onChange={handleRadioChanged('privacy', setData)}/>
                    <label
                        className="form-check-label"
                        htmlFor="privacy-private">Kín</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        }
      </Fragment>
    )
  }
}

export default GroupInfo;
