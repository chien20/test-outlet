import React from 'react';
import Avatar from '../../../../../../../components/Avatar/Avatar';
import './GroupActivity.scss';
import {convertNotification} from '../../../../../../../common/helpers/format';
import {diffTimeToString} from '../../../../../../../common/helpers';

const GroupActivity = ({data}) => {
  if (!data) {
    return (
      <div className="group-activity empty-row">
      </div>
    )
  }
  return (
    <div className="group-activity">
      <Avatar className="user-avatar" size={40}/>
      <p dangerouslySetInnerHTML={{__html: convertNotification(data.message, data.extra_data)}}/>
      <p className="time">{diffTimeToString(data.created_at)}</p>
    </div>
  )
};

export default GroupActivity;
