import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'react-bootstrap';
import Broadcaster from '../../../../../../../common/helpers/broadcaster';
import {handleSelectChange, showAlert} from '../../../../../../../common/helpers/index';
import {assignRoleForMemberAPI, getListGroupRolesAPI} from '../../../../../../../api/groups';
import Select from '../../../../../../../components/Form/Select/Select';

class EditUserModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isLoading: false,
      roles: [],
      selectedRoleId: null,
      userId: null,
    };
    this.unmounted = false;
  }

  componentDidMount() {
    Broadcaster.on(this.props.eventName, this.handleOpen);
  }

  componentWillUnmount() {
    this.unmounted = true;
    Broadcaster.off(this.props.eventName, this.handleOpen);
  }

  setData = (data) => {
    this.setState(prevState => ({
      ...prevState,
      ...data
    }));
  };

  getRoles = () => {
    const {groupId} = this.props;
    getListGroupRolesAPI(groupId, {
      page_size: 100,
      current_page: 0
    }).then(res => {
      this.setState({
        isLoading: false,
        roles: res.data.list || [],
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
      });
      showAlert(`Lỗi: ${error.message}`);
    });
  };

  handleOpen = (data = {}) => {
    if (this.unmounted) {
      return;
    }
    this.getRoles();
    this.setState({
      isOpen: true,
      ...data
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  handleSubmit = async () => {
    const {groupId} = this.props;
    const {userId, selectedRoleId} = this.state;
    try {
      const data = {
        role_id: selectedRoleId,
      };
      await assignRoleForMemberAPI(groupId, userId, data);
      showAlert({
        type: 'success',
        message: `Đã cập nhật!`
      });
      this.handleClose();
      Broadcaster.broadcast('EDIT_GROUP_MEMBER_SUCCESS', {userId});
    } catch (e) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  render() {
    const {isOpen, isLoading, roles, selectedRoleId} = this.state;
    const selectedRole = roles.find(item => item.id === selectedRoleId) || null;
    return (
      <Modal size="md" show={isOpen} onHide={this.handleClose} className="invite-member-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>Cập nhật vai trò</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-group">
            <label>Vai trò</label>
            <Select
              options={roles}
              value={selectedRole}
              optionLabelKey={'name'}
              optionValueKey={'id'}
              onChange={handleSelectChange('selectedRoleId', 'id', this.setData)}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-default" onClick={this.handleClose} disabled={isLoading}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}
            disabled={isLoading || !selectedRole}
          >
            Xong
          </button>
        </Modal.Footer>
      </Modal>
    )
  }
}

EditUserModal.propTypes = {
  eventName: PropTypes.string.isRequired,
  groupId: PropTypes.any.isRequired
};

export default EditUserModal;
