import React from 'react';
import './StatsCard.scss';

class StatsCard extends React.PureComponent {
  render() {
    const {icon, name, value, className = '', children} = this.props;
    return (
      <div className={`card stats-card ${className}`}>
        <div className="card-body">
          <div className="d-flex">
            <div className="card-icon align-self-center">
              <div className="card-icon-bg">
                <i className={icon}/>
              </div>
            </div>
            <div className="align-self-center">
              <h6 className="text-muted m-t-10 m-b-0 text-ellipsis">{name}</h6>
              <h2 className="m-t-0 text-ellipsis">{value}</h2>
            </div>
          </div>
          {
            children
          }
        </div>
      </div>
    )
  }
}

export default StatsCard;