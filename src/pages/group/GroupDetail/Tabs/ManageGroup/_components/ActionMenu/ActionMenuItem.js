import React from 'react';
import {Link} from 'react-router-dom';
import './ActionMenuItem.scss';

const ActionMenuItem = ({name, onClick, icon = 'fas fa-edit', link = '/'}) => (
  <Link to={link} className="action-menu-item" onClick={onClick}>
    <div className="action-icon">
      <i className={icon}/>
    </div>
    <div className="action-name">{name}</div>
  </Link>
);

export default ActionMenuItem;
