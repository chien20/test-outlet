import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'react-bootstrap';
import Broadcaster from '../../../../../../../common/helpers/broadcaster';
import {handleInputTextChanged, handleSelectChange, showAlert} from '../../../../../../../common/helpers/index';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';
import {connect} from 'react-redux';
import './InviteMemberModal.scss';
import {getListGroupRolesAPI, inviteGroupMembersAPI} from '../../../../../../../api/groups';
import Select from '../../../../../../../components/Form/Select/Select';

class InviteUserModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isLoading: false,
      email: '',
      roleId: null,
      useLink: false,
      roles: [],
      selectedRoleId: null,
    };
    this.unmounted = false;
  }

  componentDidMount() {
    Broadcaster.on(this.props.eventName, this.handleOpen);
  }

  componentWillUnmount() {
    this.unmounted = true;
    Broadcaster.off(this.props.eventName, this.handleOpen);
  }

  setData = (data) => {
    this.setState(prevState => ({
      ...prevState,
      ...data
    }));
  };

  useLink = () => {
    this.setState(prevState => ({
      useLink: !prevState.useLink
    }));
  };

  getRoles = () => {
    const {groupId, showRole} = this.props;
    if (!showRole) {
      return;
    }
    this.setState({
      isLoading: true,
    });
    getListGroupRolesAPI(groupId, {
      page_size: 100,
      current_page: 0
    }).then(res => {
      this.setState({
        isLoading: false,
        roles: res.data.list || [],
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
      });
      showAlert(`Lỗi: ${error.message}`);
    });
  };

  getRefLink = () => {
    const {groupId, user} = this.props;
    const {selectedRoleId} = this.state;
    let refLink = `${window.location.protocol}//${window.location.host}`;
    const uri = `/groups/${groupId}?ref=${user.id}&role=${selectedRoleId}`;
    if (window.location.port !== '80') {
      refLink += `:${window.location.port}`;
    }
    return refLink + uri;
  };

  handleOpen = (data = {}) => {
    if (this.unmounted) {
      return;
    }
    this.getRoles();
    this.setState({
      isOpen: true,
      ...data
    });
  };

  handleCopy = (value) => () => {
    const input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.value = value;
    document.body.appendChild(input);
    input.select();
    document.execCommand('copy');
    input.remove();
    showAlert({
      id: 'copy',
      type: 'success',
      message: `Đã copy vào clipboard!`
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  handleSubmit = async () => {
    const {groupId} = this.props;
    const {email, selectedRoleId} = this.state;
    if (!email) {
      this.handleClose();
      return;
    }
    const users = [];
    if (`${email}`.includes('@')) {
      users.push({
        email,
        type: 1
      });
    } else {
      users.push({
        phone: email,
        type: 2
      });
    }
    try {
      const data = {
        group_id: groupId,
        role_id: selectedRoleId,
        users,
      };
      const {data: res} = await inviteGroupMembersAPI(data);
      const success = !!res.filter(item => item.success).length;
      if (success) {
        showAlert({
          type: 'success',
          message: `Đã mời!`
        });
      } else {
        let error = res?.[0]?.error_message || 'Không rõ';
        if (error.includes('System not yet support invite by provide phone number')) {
          error = 'Hệ thống chưa hỗ trợ mời thành viên bằng số điện thoại';
        }
        showAlert({
          type: 'error',
          message: `Lỗi: ${error}`
        });
      }
      this.handleClose();
    } catch (e) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  };

  render() {
    const {showRole} = this.props;
    const {isOpen, isLoading, roles, selectedRoleId, useLink, email} = this.state;
    const refLink = this.getRefLink();
    const selectedRole = roles.find(item => item.id === selectedRoleId) || null;
    return (
      <Modal size="md" show={isOpen} onHide={this.handleClose} className="invite-member-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>Mời thành viên</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-group">
            <label>Nhập email/số điện thoại</label>
            <input
              type="text"
              className="form-control"
              name="email"
              value={email}
              onChange={handleInputTextChanged('email', this.setData)}/>
          </div>
          {
            showRole &&
            <div className="form-group">
              <label>Vai trò</label>
              <Select
                options={roles}
                value={selectedRole}
                optionLabelKey={'name'}
                optionValueKey={'id'}
                onChange={handleSelectChange('selectedRoleId', 'id', this.setData)}
              />
            </div>
          }
          {
            !useLink &&
            <HyperLink className="share-link" onClick={this.useLink}>Nhận liên kết có thể chia sẻ được</HyperLink>
          }
          {
            useLink &&
            <div className="form-group">
              <label>Liên kết chia sẻ</label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  name="link"
                  defaultValue={refLink}
                  readOnly={true}
                  disabled={true}/>
                <div className="input-group-append">
                  <i className="far fa-copy input-group-text" title="Sao chép" onClick={this.handleCopy(refLink)}/>
                </div>
              </div>
            </div>
          }
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-default" onClick={this.handleClose} disabled={isLoading}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}
            disabled={isLoading || (!selectedRole && showRole) || (!email && !useLink)}
          >
            Xong
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

InviteUserModal.propTypes = {
  eventName: PropTypes.string.isRequired,
  groupId: PropTypes.any.isRequired,
  showRole: PropTypes.bool,
};

InviteUserModal.defaultProps = {
  showRole: true,
};

const mapStateToProps = (state) => ({
  user: state.user.info
});

export default connect(mapStateToProps)(InviteUserModal);
