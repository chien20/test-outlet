import React from 'react';

class PermissionCheckbox extends React.PureComponent {
  render() {
    const {permission, permission_ids, onChange} = this.props;
    return (
      <div className="custom-control custom-checkbox">
        <input
          type="checkbox"
          className="custom-control-input"
          checked={permission_ids.indexOf(permission.id) >= 0}
          id={`group-permission-${permission.id}`}
          onChange={onChange(permission.id)}/>
        <label
          className="custom-control-label"
          htmlFor={`group-permission-${permission.id}`}>
          {permission.name}
        </label>
      </div>
    )
  }
}

export default PermissionCheckbox;