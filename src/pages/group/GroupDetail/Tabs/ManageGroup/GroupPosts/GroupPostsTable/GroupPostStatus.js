import React, {Fragment} from 'react';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';

const GroupPostStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chờ phê duyệt</span>;
    case 10:
      return <span className="text-red">Đã bị từ chối</span>;
    case 20:
      return <span className="text-green">Đang hoạt động</span>;
    default:
      return '';
  }
};

export const UpdateGroupPostStatus = ({item, handleUpdateStatus, handleDelete}) => {
  const {approve_status} = item;
  return (
    <Fragment>
      {
        approve_status !== 20 &&
        <HyperLink onClick={handleUpdateStatus(20)} className="text-green" title="Phê duyệt">
          <i className="fas fa-check"/>
        </HyperLink>
      }
      {
        approve_status !== 10 &&
        <HyperLink onClick={handleUpdateStatus(10)} className="text-red" title="Từ chối">
          <i className="fas fa-ban"/>
        </HyperLink>
      }
      <HyperLink onClick={handleDelete} className="text-red" title="Xóa">
        <i className="fas fa-trash"/>
      </HyperLink>
    </Fragment>
  );
};

export default GroupPostStatus;
