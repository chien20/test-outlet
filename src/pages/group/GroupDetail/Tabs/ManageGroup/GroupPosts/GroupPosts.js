import React from 'react';
import {deleteGroupPostAPI, getGroupPostsForOwnerAPI, updateGroupPostStatusAPI} from '../../../../../../api/groups';
import {showAlert} from '../../../../../../common/helpers/index';
import {history} from '../../../../../../common/utils/router/history';
import Pagination from '../../../../../../components/Pagination/Pagination';
import CheckboxTable from '../../../../../../components/Table/CheckboxTable';
import GroupPostStatus, {UpdateGroupPostStatus} from './GroupPostsTable/GroupPostStatus';
import {Link} from 'react-router-dom';
import './GroupPosts.scss';

const PAGE_SIZE = 10;

const getTextFromHTML = (html) => {
  const el = document.createElement('div');
  el.innerHTML = html;
  return el.innerText;
};

class GroupPosts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      totalItems: 0,
      isLoading: false,
      keyword: '',
      selection: [],
      selectAll: false
    };
  }

  componentDidMount() {
    this.getListGroupPosts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.match !== this.props.match) {
      this.getListGroupPosts();
    }
  }

  getListGroupPosts() {
    const {match: {params: {page, groupId}}} = this.props;
    const {keyword} = this.state;
    const params = {
      current_page: page || 1,
      page_size: PAGE_SIZE,
      q: keyword || ''
    };
    params.current_page -= 1;
    this.setState({
      isLoading: true
    });
    getGroupPostsForOwnerAPI(groupId, params).then(res => {
      this.setState({
        isLoading: false,
        posts: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được dữ liệu!`
      });
    });
  }

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListGroupPosts(this.props);
  };

  handleDelete = (postId) => () => {
    const {match: {params: {groupId}}} = this.props;
    const r = window.confirm('Bài viết này sẽ bị xóa, bạn có chắc?');
    if (!r) {
      return;
    }
    deleteGroupPostAPI(groupId, postId).then(res => {
      if (res) {
        showAlert({
          type: 'success',
          message: `Đã xóa`
        });
        this.getListGroupPosts();
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi: Unknown`
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handleUpdateStatus = (postId) => (status) => () => {
    const {match: {params: {groupId}}} = this.props;
    if (status === 10) {
      const r = window.confirm('Bài viết này sẽ bị từ chối, bạn có chắc?');
      if (!r) {
        return;
      }
    }
    updateGroupPostStatusAPI(groupId, postId, {
      approve_status: status
    }).then(res => {
      showAlert({
        type: 'success',
        message: `Success`
      });
      this.getListGroupPosts();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onPageChanged = (page) => {
    const {match: {params: {groupId}}} = this.props;
    if (page > 1) {
      history.push(`/groups/${groupId}/manage/posts/p/${page}`);
    } else {
      history.push(`/groups/${groupId}/manage/posts`);
    }
  };

  onSelectChange = (selection, selectAll) => {
    this.setState({
      selection,
      selectAll
    });
  };

  columns = [
    {
      Header: 'Nội dung',
      id: 'content',
      accessor: (item) => (
        <Link
          to={`/groups/${this.props.match.params.groupId}/posts/${item.id}`}
          className="post-intro"
          target="_blank">
          {getTextFromHTML(item.content)}
        </Link>
      )
    },
    {
      Header: 'Trạng thái',
      id: 'status',
      width: 130,
      accessor: (item) => (
        <GroupPostStatus status={item.approve_status}/>
      )
    },
    {
      Header: 'Thao tác',
      id: 'action',
      width: 120,
      className: 'text-center action-buttons',
      accessor: (item) => (
        <UpdateGroupPostStatus
          item={item}
          handleDelete={this.handleDelete(item.id)}
          handleUpdateStatus={this.handleUpdateStatus(item.id)}/>
      )
    }
  ];

  render() {
    const {posts, totalItems, selection, selectAll} = this.state;
    const {match: {params: {page}}} = this.props;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <div className="posts-page">
        <div className="page-title">
          <h3>Quản lý bài viết</h3>
          <div className="action-group">
            {/*<form className="search-box" onSubmit={this.handleSearch}>*/}
            {/*<input type="text" className="form-control" value={keyword} onChange={this.handleInputSearchChange}/>*/}
            {/*<button type="submit" className="btn search-icon"><i className="fa fa-search"/></button>*/}
            {/*</form>*/}
          </div>
        </div>
        <div className="page-content">
          <CheckboxTable data={posts}
                         columns={this.columns}
                         selection={selection}
                         selectAll={selectAll}
                         onSelectChange={this.onSelectChange}
                         showPagination={false}
                         defaultPageSize={PAGE_SIZE}/>
          <div className="item-pagination hidden-mobile">
            {
              totalPage > 1 &&
              <Pagination totalPages={totalPage} currentPage={page || 0} onPageChanged={this.onPageChanged}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default GroupPosts;
