import React from 'react';
import './GroupPosts.scss';
import {getGroupPostDetailAPI, getGroupPostsAPI} from '../../../../../api/groups';
import {showAlert} from '../../../../../common/helpers';
import NewPost from './NewPost/NewPost';
import GroupPost from './GroupPost/GroupPost';
import InfiniteScroll from '../../../../../components/InfiniteScroll/InfiniteScroll';
import DivLoading from '../../../../../components/DivLoading/DivLoading';
import AlertModal from '../../../../../components/modals/AlertModal/AlertModal';
import BROADCAST_EVENT from '../../../../../common/constants/broadcastEvents';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../../common/constants/app';
import PermissionChecker from '../../../_components/PermissionChecker/PermissionChecker';
import {withGroupDetail} from '../../GroupDetailContext';
import ReportPostModal from './GroupPost/ReportPostModal/ReportPostModal';
import {OBJECT_TYPES} from '../../../../../common/constants/objectTypes';

class GroupPosts extends React.PureComponent {
  state = {
    posts: [],
    nextPage: 0,
    hasMore: false,
    isFetching: true
  };

  reportPostModal = React.createRef();

  componentDidMount() {
    this.getGroupPosts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.group !== this.props.group) {
      this.getGroupPosts();
    }
  }

  getGroupPosts = () => {
    const {group} = this.props;
    const {nextPage} = this.state;
    this.setState({
      isFetching: true
    });
    const params = {
      current_page: nextPage,
      page_size: 20
    };
    getGroupPostsAPI(group.id, params).then(res => {
      const pageInfo = res.data.page_info;
      this.setState(prevState => {
        return {
          posts: [...prevState.posts, ...res.data.list],
          hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
          nextPage: pageInfo.current_page + 1,
          isFetching: false
        };
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  onClickLoadMore = () => {
    if (!this.state.isFetching) {
      this.getGroupPosts();
    }
  };

  onDeleteSuccess = (postId) => {
    this.setState(prevState => {
      const {posts} = prevState;
      const idx = (posts || []).findIndex(item => item.id === postId);
      if (idx >= 0) {
        const newPosts = [...posts];
        newPosts.splice(idx, 1);
        return {
          posts: newPosts,
        };
      }
      return null;
    });
  };

  onAddSuccess = (postId) => {
    const {group} = this.props;
    getGroupPostDetailAPI(group.id, postId).then(res => {
      this.setState(prevState => {
        return {
          posts: [res.data, ...prevState.posts],
        }
      });
    }).catch(error => {
    });
  };

  handleReportPost = (postId) => {
    const {group} = this.props;
    this.reportPostModal.current.handleOpen(group.id, postId, OBJECT_TYPES.GroupPost);
  };

  render() {
    const {group, ...others} = this.props;
    const {isFetching, hasMore, posts} = this.state;
    return (
      <div className="group-posts">
        {
          group.joined &&
          <PermissionChecker
            requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.DANG_BAI_VIET.id]}
            userPermissions={others.permissions}>
            <NewPost group={group} onAddSuccess={this.onAddSuccess}/>
          </PermissionChecker>
        }
        <div className="post-list">
          {
            posts.map((item) => (
              <GroupPost
                {...others}
                postId={item.id}
                groupId={group.id}
                key={item.id}
                onDeleteSuccess={this.onDeleteSuccess}
                handleReportPost={this.handleReportPost}
              />
            ))
          }
          {
            hasMore &&
            <InfiniteScroll loadMore={this.onClickLoadMore} hasMore={hasMore}/>
          }
          {
            isFetching &&
            <DivLoading style={{marginTop: 10}}/>
          }
        </div>
        <AlertModal eventName={BROADCAST_EVENT.ON_DELETE_COMMENT}/>
        <ReportPostModal ref={this.reportPostModal}/>
      </div>
    );
  }
}

export default withGroupDetail(GroupPosts);
