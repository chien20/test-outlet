import React from 'react';
import './UploadImages.scss';

const MAX_IMAGES = 8;

class UploadImages extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      previewUrls: [],
      selectedIndex: null
    };
    this.inputFileEl = React.createRef();
  }

  componentWillUnmount() {
    this.clear();
  }

  static revokePreview(previewUrl) {
    if (previewUrl) {
      URL.revokeObjectURL(previewUrl);
    }
  }

  handleChange = (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.setState(prevState => {
        const {selectedIndex} = prevState;
        const images = [...prevState.images];
        if (images.length > MAX_IMAGES) {
          return null;
        }
        const item = {
          file: file,
          previewUrl: URL.createObjectURL(file)
        };
        if (selectedIndex === null) {
          images.push(item);
        } else {
          UploadImages.revokePreview(images[selectedIndex].previewUrl);
          images[selectedIndex] = item;
        }
        return {
          images: images
        }
      });
    }
  };

  browseImage = () => {
    const {images} = this.state;
    if (images.length >= MAX_IMAGES) {
      return;
    }
    this.inputFileEl.current.click();
  };

  handleChangeImage = (index) => () => {
    this.setState({
      selectedIndex: index
    }, this.browseImage);
  };

  handleAddImage = () => {
    this.setState({
      selectedIndex: null
    }, this.browseImage);
  };

  handleDeleteImage = (index) => (event) => {
    event.stopPropagation();
    this.setState(prevState => {
      const images = [...prevState.images];
      if (images[index]) {
        UploadImages.revokePreview(images[index].previewUrl);
        images.splice(index, 1);
        return {
          images
        };
      }
      return null;
    });
  };

  getImages = () => {
    return this.state.images.map(item => item.file);
  };

  clear = () => {
    const {images} = this.state;
    images.forEach(item => {
      UploadImages.revokePreview(item.previewUrl);
    });
    this.setState({
      images: []
    });
  };

  render() {
    const {images} = this.state;
    return (
      <div className="upload-images">
        <input
          ref={this.inputFileEl}
          key={images.length}
          onChange={this.handleChange}
          type="file"
          multiple={false}
          style={{display: 'none'}}/>
        {
          images.map((item, index) => (
            <div className="image-item" key={index} onClick={this.handleChangeImage(index)}>
              <img src={item.previewUrl} alt={`Preview`}/>
              <span className="delete-btn" onClick={this.handleDeleteImage(index)}>
                <i className="fas fa-trash-alt"/>
              </span>
            </div>
          ))
        }
        {
          images.length > 0 && images.length < MAX_IMAGES &&
          <div className="add-image" onClick={this.handleAddImage}><span>+</span></div>
        }
      </div>
    )
  }
}

export default UploadImages;
