import React from 'react';
import {showAlert} from '../../../../../../common/helpers/index';
import {addGroupPostAPI} from '../../../../../../api/groups';
import ContentEditable from 'react-contenteditable';
import SelectProductModal from '../../../../../../components/Product/SelectProductModal';
import HyperLink from '../../../../../../components/HyperLink/HyperLink';
import {getProductsByIdsAPI} from '../../../../../../api/products';
import UploadImages from './UploadImages';
import {OBJECT_TYPES} from '../../../../../../common/constants/objectTypes';
import {uploadMediaAPI} from '../../../../../../api/media';

const style = {};

class NewPost extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      productIds: [],
      isSubmitting: false,
      product: null
    };
    this.contentEditable = React.createRef();
    this.selectProductModalEl = React.createRef();
    this.uploadImageEl = React.createRef();
  }

  setData = (data) => {
    this.setState(data)
  };

  onChange = (event) => {
    this.setState({
      content: event.target.value
    });
  };

  handleSelectProducts = () => {
    this.selectProductModalEl.current.handleOpen({
      selectedIds: this.state.productIds
    }, this.onSelectProducts);
  };

  onSelectProducts = async (productIds) => {
    let products = [];
    if (productIds.length) {
      const {data} = await getProductsByIdsAPI(productIds);
      products = data;
    }
    this.setData({
      productIds,
      product: products && products.length ? products[0] : null
    });
  };

  handleAddImage = () => {
    this.uploadImageEl.current.handleAddImage();
  };

  savePost = async () => {
    const {group, onAddSuccess} = this.props;
    const {content, productIds} = this.state;
    this.setState({
      isSubmitting: true
    });
    const successCb = (postId) => {
      this.setState({
        content: '',
        isSubmitting: false
      });
      showAlert({
        type: 'success',
        message: `Đã đăng bài viết!`
      });
      onAddSuccess(postId);
      this.uploadImageEl.current.clear();
    };
    try {
      const postData = {
        content: content,
        product_ids: productIds
      };
      const {data: id} = await addGroupPostAPI(group.id, postData);
      const images = this.uploadImageEl.current.getImages();
      if (images.length === 0) {
        successCb(id);
        return;
      }
      for (let i = 0; i < images.length; i++) {
        const data = {
          object_type: OBJECT_TYPES.GroupPost,
          object_id: id,
          type: 'avatar'
        };
        await uploadMediaAPI(images[i], data);
      }
      successCb(id);
    } catch (error) {
      this.setState({
        isSubmitting: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    }
  };

  render() {
    const {content, productIds, product, isSubmitting} = this.state;
    return (
      <div className="new-post">
        <ContentEditable
          className="editor"
          tagName="div"
          disabled={false}
          style={style}
          html={content}
          innerRef={this.contentEditable}
          onChange={this.onChange}/>
        <UploadImages ref={this.uploadImageEl}/>
        <div className="attachment">
          {
            !!productIds.length &&
            <p>
              {
                !product &&
                <HyperLink onClick={this.handleSelectProducts}>
                  {productIds.length} sản phẩm
                </HyperLink>
              }
              {
                product &&
                <HyperLink onClick={this.handleSelectProducts}>
                  {product.name}{`${productIds.length > 1 ? ` và ${productIds.length - 1} sản phẩm` : ''}`}
                </HyperLink>
              }
            </p>
          }
        </div>
        <div className="bottom-toolbar">
          <div className="addon-buttons">
            <button className="btn btn-default" onClick={this.handleAddImage}>
              <i className="fas fa-image"/> Thêm ảnh
            </button>
            <button className="btn btn-default" onClick={this.handleSelectProducts}>
              <i className="fas fa-tshirt"/> Gắn thẻ sản phẩm
            </button>
          </div>
          <button
            className="btn btn-primary"
            disabled={!content || isSubmitting}
            onClick={this.savePost}>
            <i className="fas fa-save"/> Đăng bài
          </button>
        </div>
        <SelectProductModal ref={this.selectProductModalEl}/>
      </div>
    )
  }
}

export default NewPost;
