import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import './GroupPost.scss';
import {getBasicUserInfoAC} from '../../../../../../redux/actions/common';
import Avatar from '../../../../../../components/Avatar/Avatar';
import HyperLink from '../../../../../../components/HyperLink/HyperLink';
import NewComment from './Comments/NewComment';
import ListComments from './Comments/ListComments/ListComments';
import {diffTimeToString, showAlert} from '../../../../../../common/helpers';
import {
  deleteGroupPostAPI,
  getGroupPostDetailAPI,
  getGroupPostLikesAPI,
  likeGroupPostAPI
} from '../../../../../../api/groups';
import Broadcaster from '../../../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../../../common/constants/broadcastEvents';
import HTMLView from '../../../../../../components/View/HTMLView';
import PostPoducts from './Products/PostPoducts';
import PostImages from './Images/PostImages';
import {Link} from 'react-router-dom';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../../../common/constants/app';
import PermissionChecker from '../../../../_components/PermissionChecker/PermissionChecker';

class GroupPost extends React.PureComponent {
  state = {
    postDetail: null,
    like: {
      count: 0,
      users: [],
      liked: false,
    },
    comment: 0,
    showMenu: false,
  };

  componentDidMount() {
    this.getPostDetail();
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.post !== this.props.post) {
      this.getPostDetail();
    }
    if (prevState.postDetail !== this.state.postDetail) {
      this.getAuthor();
      this.getLikes();
      if (this.props.onLoaded) {
        this.props.onLoaded(this.state.postDetail);
      }
    }
  }

  getPostDetail = () => {
    const {postId, groupId} = this.props;
    getGroupPostDetailAPI(groupId, postId).then(res => {
      this.setState({
        postDetail: res.data
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Không lấy được thông tin bài viết: ${error.message}`,
        id: 'get-group-post-detail'
      });
    });
  };

  getAuthor = () => {
    const {users} = this.props;
    const {postDetail} = this.state;
    if (!postDetail) {
      return;
    }
    if (!users[postDetail.created_by]) {
      this.props.dispatch(getBasicUserInfoAC(postDetail.created_by));
    }
  };

  getLikes = () => {
    const {postDetail} = this.state;
    if (!postDetail) {
      return;
    }
    getGroupPostLikesAPI(postDetail.id).then(res => {
      this.setState(prevState => ({
        like: {...prevState.like, ...res.data}
      }));
    }).catch(error => {

    });
  };

  likeItem = (status) => () => {
    const {groupId} = this.props;
    const {postDetail} = this.state;
    if (!postDetail) {
      return;
    }
    likeGroupPostAPI(groupId, postDetail.id, {like: status}).then(res => {
      if (res.data) {
        Broadcaster.broadcast(BROADCAST_EVENT.LIKE_POST_SUCCESS, {
          post_id: postDetail.id
        });
        this.getLikes();
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  setData = (data) => {
    this.setState(data);
  };

  toggleMenu = () => {
    this.setState(prevState => ({
      showMenu: !prevState.showMenu,
    }), () => {
      document.removeEventListener('click', this.handleClickOutside);
      if (this.state.showMenu) {
        document.addEventListener('click', this.handleClickOutside);
      }
    });
  };

  handleClickOutside = () => {
    if (this.state.showMenu) {
      this.setState({
        showMenu: false,
      });
    }
  };

  handleDelete = () => {
    const {groupId, postId, onDeleteSuccess} = this.props;
    const r = window.confirm('Bài viết này sẽ bị xóa, bạn có chắc muốn xóa không?');
    if (r) {
      deleteGroupPostAPI(groupId, postId).then(res => {
        if (res) {
          showAlert({
            type: 'success',
            message: `Đã xóa`
          });
          onDeleteSuccess(postId);
        } else {
          showAlert({
            type: 'error',
            message: `Lỗi: Unknown`
          });
        }
      }).catch(error => {
        showAlert({
          type: 'error',
          message: `Lỗi: ${error.message}`
        });
      });
    }
  };

  handleReport = () => {
    const {postId, handleReportPost} = this.props;
    handleReportPost(postId);
  };

  handleFocusNewComment = () => {
    const {postId} = this.props;
    const el = document.getElementById(`post-${postId}-new-comment`);
    if (el) {
      el.focus();
    }
  };

  render() {
    const {users, user, groupId, ...others} = this.props;
    const {postDetail, like, comment, showMenu} = this.state;

    if (!postDetail) {
      return null;
    }

    const author = users[postDetail.created_by];

    const createdTime = diffTimeToString(postDetail.created_at);

    return (
      <div className="group-post">
        <div className="post-header">
          <div className="post-author">
            <Avatar src={author ? author.avatar : null} size={40}/>
            <HyperLink className="author-name">{author ? author.full_name : ''}</HyperLink>
            <Link to={`/groups/${groupId}/posts/${postDetail.id}`} className="created-time">{createdTime}</Link>
          </div>
          <div className={`post-menu ${showMenu ? 'open' : ''}`}>
            <HyperLink
              className="toggle-btn"
              onClick={this.toggleMenu}
            >
              <i className="fas fa-ellipsis-v"/>
            </HyperLink>
            <ul className="drop-menu">
              {
                user?.id === author?.id &&
                <Fragment>
                  <li><HyperLink onClick={this.handleDelete}>Xóa bài viết</HyperLink></li>
                </Fragment>
              }
              {
                user?.id !== author?.id &&
                <Fragment>
                  <li><HyperLink onClick={this.handleReport}>Báo cáo vi phạm</HyperLink></li>
                </Fragment>
              }
            </ul>
          </div>
        </div>
        <div className="post-content">
          <HTMLView html={postDetail.content}/>
        </div>
        {
          !!postDetail.images && !!postDetail.images.length &&
          <PostImages images={postDetail.images}/>
        }
        {
          postDetail.product_ids.length > 0 &&
          <PostPoducts productIds={postDetail.product_ids}/>
        }
        <div className="post-stats">
          <div><i className="fas fa-thumbs-up"/> {like.count} thích</div>
          <div>{comment} bình luận</div>
        </div>
        <div className="post-actions">
          <HyperLink onClick={this.likeItem(!like.liked)} className={like.liked ? 'active' : ''}>
            <i className="far fa-thumbs-up"/> Thích
          </HyperLink>
          <HyperLink onClick={this.handleFocusNewComment}><i className="far fa-comments"/> Bình luận</HyperLink>
          <HyperLink><i className="far fa-share-square"/> Chia sẻ</HyperLink>
        </div>
        <div className="post-comments">
          <ListComments
            postId={postDetail.id}
            permissions={others.permissions}
            setData={this.setData}
          />
          {
            user &&
            <PermissionChecker
              requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.BINH_LUAN.id]}
              userPermissions={others.permissions}>
              <NewComment
                postId={postDetail.id}
                groupId={groupId}
                user={user}
              />
            </PermissionChecker>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
  users: state.common.users.basicInfo
});

export default connect(mapStateToProps)(GroupPost);
