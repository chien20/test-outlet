import React from 'react';
import ProductItem from '../../../../../../../components/ProductItem/ProductItem';
import {getProductsByIdsAPI} from '../../../../../../../api/products';

class PostPoducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  getProducts = () => {
    const {productIds} = this.props;
    getProductsByIdsAPI(productIds).then(res => {
      this.setState({
        products: res.data.list || []
      });
    }).catch(error => {

    });
  };

  render() {
    const {products} = this.state;
    if (!products.length) {
      return null;
    }
    return (
      <div className="post-products">
        <h3>Sản phẩm liên quan</h3>
        <div className="product-list">
          {
            products.map(item => (
              <ProductItem product={item} key={item.id}/>
            ))
          }
        </div>
      </div>
    )
  }
}

export default PostPoducts;
