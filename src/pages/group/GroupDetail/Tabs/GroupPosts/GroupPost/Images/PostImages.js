import React from 'react';
import './PostImages.scss';
import {imageUrl} from '../../../../../../../common/helpers';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';
import FsLightbox from 'fslightbox-react';

class PostImages extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      toggler: false,
      photoIndex: 0
    };
  }

  toggleLightbox = (index) => () => {
    this.setState(prevState => ({
      toggler: !prevState.toggler,
      photoIndex: index
    }));
  };

  render() {
    const {images} = this.props;
    const {toggler, photoIndex} = this.state;
    const fullImages = images.map(item => ({
      ...item,
      url: imageUrl(item.url),
      thumbnail_url: imageUrl(item.url)
    }));
    const imagesSrc = fullImages.map(item => item.url);
    const displayImages = fullImages.slice(0, 4);
    const itemsLeft = fullImages.length - displayImages.length;
    return (
      <div className="post-images">
        {
          displayImages.map((item, index) => {
            const imageBg = {};
            if (item.thumbnail_url) {
              imageBg.backgroundImage = `url(${item.thumbnail_url})`;
            }
            return (
              <HyperLink
                className="post-image"
                key={index}
                style={imageBg}
                onClick={this.toggleLightbox(index + 1)}>
                {
                  item.thumbnail_url &&
                  <img src={item.thumbnail_url} alt={``}/>
                }
                {
                  index === displayImages.length - 1 && itemsLeft > 0 &&
                  <div className="has-more">
                    <span>+{itemsLeft}</span>
                  </div>
                }
              </HyperLink>
            )
          })
        }
        <FsLightbox
          toggler={toggler}
          sources={imagesSrc}
          slide={photoIndex}
        />
      </div>
    )
  }
}

export default PostImages;
