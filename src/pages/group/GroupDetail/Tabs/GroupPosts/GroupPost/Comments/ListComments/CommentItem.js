import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import './CommentItem.scss';
import {diffTimeToString, showAlert} from '../../../../../../../../common/helpers';
import Avatar from '../../../../../../../../components/Avatar/Avatar';
import HyperLink from '../../../../../../../../components/HyperLink/HyperLink';
import Broadcaster from '../../../../../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../../../../../common/constants/broadcastEvents';
import {deletePostCommentAPI} from '../../../../../../../../api/groups';
import {getBasicUserInfoAC} from '../../../../../../../../redux/actions/common';
import HTMLView from '../../../../../../../../components/View/HTMLView';
import NewComment from '../NewComment';
import PermissionChecker from '../../../../../../_components/PermissionChecker/PermissionChecker';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../../../../../common/constants/app';
import ListCommentsComponent from './ListCommentsComponent';

class CommentItem extends React.PureComponent {
  componentDidMount() {
    this.getAuthor();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.item !== this.props.item) {
      this.getAuthor();
    }
  }

  getAuthor = () => {
    const {author, item} = this.props;
    if (!author) {
      this.props.dispatch(getBasicUserInfoAC(item.created_by));
    }
  };

  deleteItem = () => {
    Broadcaster.broadcast(BROADCAST_EVENT.ON_DELETE_COMMENT, {
      title: 'Xóa bình luận',
      message: 'Bạn có chắc muốn xóa bình luận này?',
      buttons: [
        {
          name: 'Xóa',
          className: 'btn btn-danger',
          onClick: this.handleDelete
        }
      ]
    });
  };

  editItem = () => {
    const {item} = this.props;
    this.props.handleEdit(item.id);
  };

  replyItem = () => {
    const {item, postId, author} = this.props;
    Broadcaster.broadcast(BROADCAST_EVENT.REPLY_COMMENT, {
      postId,
      commentId: item.id,
      author,
    });
  };

  handleDelete = () => {
    const {item, postId} = this.props;
    deletePostCommentAPI(postId, item.id).then(res => {
      if (res.data) {
        showAlert({
          type: 'success',
          message: 'Đã xóa!'
        });
        Broadcaster.broadcast(BROADCAST_EVENT.DELETE_COMMENT_SUCCESS, {
          ...item,
          post_id: postId
        });
        Broadcaster.broadcast(BROADCAST_EVENT.ON_DELETE_COMMENT, {
          isOpen: false
        });
      } else {
        showAlert({
          type: 'error',
          message: `Lỗi`
        });
      }
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handleCancelEdit = () => {
    this.props.handleEdit(null);
  };

  render() {
    const {item, user, author, postId, isEditing, permissions, editingCommentId, handleEdit, onUpdateSuccess} = this.props;
    if (isEditing) {
      return (
        <li className="comment-item editing">
          <NewComment
            isEdit={true}
            user={user}
            postId={postId}
            commentId={item.id}
            initialValue={item.comment}
            onUpdateSuccess={onUpdateSuccess}
          />
          <HyperLink onClick={this.handleCancelEdit} className="cancel-link">Hủy chỉnh sửa</HyperLink>
        </li>
      );
    }
    return (
      <li className="comment-item-container">
        <div className="comment-item">
          <div className="comment-header">
            <Avatar src={author ? author.avatar : null}/>
          </div>
          <div className="comment-body">
            <div className="comment-content">
              <HyperLink className="author-name">{author ? author.full_name : ''}</HyperLink>
              <HTMLView html={item.comment}/>
            </div>
            <div className="comment-actions">
              {
                author && user && author.id === user.id &&
                <Fragment>
                  <HyperLink onClick={this.editItem}>Sửa</HyperLink>
                  <HyperLink onClick={this.deleteItem}>Xóa</HyperLink>
                </Fragment>
              }
              {
                author && user && author.id !== user.id &&
                <PermissionChecker
                  requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.BINH_LUAN.id]}
                  userPermissions={permissions}
                >
                  <HyperLink onClick={this.replyItem}>Trả lời</HyperLink>
                </PermissionChecker>
              }
              <span className="created-time">{diffTimeToString(item.updated_date || item.created_date)}</span>
            </div>
          </div>
        </div>
        {
          !!item.child?.length &&
          <ListCommentsComponent
            postId={postId}
            permissions={permissions}
            comments={item.child}
            editingCommentId={editingCommentId}
            handleEdit={handleEdit}
            onUpdateSuccess={onUpdateSuccess}
          />
        }
      </li>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  user: state.user.info,
  author: state.common.users.basicInfo[ownProps.item.created_by]
});

export default connect(mapStateToProps)(CommentItem);
