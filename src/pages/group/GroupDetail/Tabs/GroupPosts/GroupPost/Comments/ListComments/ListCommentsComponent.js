import React from 'react';
import CommentItem from './CommentItem';

class ListCommentsComponent extends React.PureComponent {
  render() {
    const {postId, permissions, comments, editingCommentId, handleEdit, onUpdateSuccess} = this.props;
    return (
      <ul className="list-comments">
        {
          comments.map((item) => (
            <CommentItem
              key={item.id}
              item={item}
              postId={postId}
              permissions={permissions}
              editingCommentId={editingCommentId}
              isEditing={editingCommentId === item.id}
              handleEdit={handleEdit}
              onUpdateSuccess={onUpdateSuccess}
            />
          ))
        }
      </ul>
    );
  }
}

export default ListCommentsComponent;
