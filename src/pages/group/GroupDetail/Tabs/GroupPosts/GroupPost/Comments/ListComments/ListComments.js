import React from 'react';
import {getPostCommentsAPI} from '../../../../../../../../api/groups';
import Broadcaster from '../../../../../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../../../../../common/constants/broadcastEvents';
import ListCommentsComponent from './ListCommentsComponent';
import CommentItem from './CommentItem';

class ListComments extends React.PureComponent {
  state = {
    comments: [],
    isFetching: false,
    editingCommentId: null,
  };

  componentDidMount() {
    this.getComments();
    Broadcaster.on(BROADCAST_EVENT.ADD_COMMENT_SUCCESS, this.onCommentsChanged);
    Broadcaster.on(BROADCAST_EVENT.DELETE_COMMENT_SUCCESS, this.onDeleteComment);
  }

  componentWillUnmount() {
    Broadcaster.off(BROADCAST_EVENT.ADD_COMMENT_SUCCESS, this.onCommentsChanged);
    Broadcaster.off(BROADCAST_EVENT.DELETE_COMMENT_SUCCESS, this.onDeleteComment);
  }

  onCommentsChanged = (data) => {
    const {postId} = this.props;
    if (data && data.post_id === postId) {
      this.getComments();
    }
  };

  onDeleteComment = (data) => {
    const {postId} = this.props;
    if (data && data.post_id === postId) {
      this.setState(prevState => {
        const comments = [...prevState.comments];
        let changed = false;
        for (let i = comments.length - 1; i >= 0; i--) {
          if (comments[i].id === data.id) {
            comments.splice(i, 1);
            changed = true;
          }
        }
        return {
          comments: changed ? comments : prevState.comments
        };
      });
    }
  };

  getComments = () => {
    const {postId} = this.props;
    this.setState({
      isFetching: true
    });
    const params = {
      sort_by: 'id',
      sort_order: 'desc'
    };
    getPostCommentsAPI(postId, params).then(res => {
      this.setState(prevState => {

        const comments = (prevState.comments || []).reduce((result, item) => {
          result[item.id] = item;
          return result;
        }, {});

        (res.data.list || []).forEach(item => {
          comments[item.id] = {
            ...item,
            child: [],
          };
        });

        Object.values(comments).forEach(item => {
          if (item.reply_to && comments[item.reply_to]) {
            comments[item.reply_to] = {
              ...comments[item.reply_to],
              child: [
                ...comments[item.reply_to].child,
                item,
              ],
            };
          }
        });

        const newList = Object.values(comments).sort((a, b) => (a.id - b.id)).filter(item => !item.reply_to);

        return {
          isFetching: false,
          comments: newList,
        };
      });
      this.props.setData({
        comment: res.data.page_info.total_items
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    });
  };

  handleEdit = (commentId) => {
    this.setState({
      editingCommentId: commentId,
    });
  };

  onUpdateSuccess = (data) => {
    this.setState(prevState => {
      const comments = [...prevState.comments];
      const idx = comments.findIndex(item => item.id === data.id);
      if (idx < 0) {
        return null;
      }
      comments[idx] = {
        ...comments[idx],
        ...data,
      };
      return {
        comments,
        editingCommentId: null,
      };
    });
  };

  render() {
    const {postId, permissions} = this.props;
    const {comments, editingCommentId} = this.state;
    return (
      <ListCommentsComponent
        postId={postId}
        permissions={permissions}
        comments={comments}
        editingCommentId={editingCommentId}
        handleEdit={this.handleEdit}
        onUpdateSuccess={this.onUpdateSuccess}
      />
    );
  }
}

export default ListComments;
