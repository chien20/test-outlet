import React from 'react';
import PropType from 'prop-types';
import ContentEditable from 'react-contenteditable';
import Avatar from '../../../../../../../components/Avatar/Avatar';
import {KEY_CODE} from '../../../../../../../common/constants/keyCode';
import {addCommentToPostAPI, updatePostCommentAPI} from '../../../../../../../api/groups';
import {showAlert} from '../../../../../../../common/helpers/index';
import Broadcaster from '../../../../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../../../../common/constants/broadcastEvents';
import HyperLink from '../../../../../../../components/HyperLink/HyperLink';

const style = {};

class NewComment extends React.PureComponent {
  state = {
    comment: this.props.initialValue || '',
    replyTo: null,
  };
  contentEditable = React.createRef();

  componentDidMount() {
    Broadcaster.on(BROADCAST_EVENT.REPLY_COMMENT, this.handleReply);
  }

  componentWillUnmount() {
    Broadcaster.off(BROADCAST_EVENT.REPLY_COMMENT, this.handleReply);
  }

  handleReply = (payload) => {
    const {postId} = payload;
    if (postId === this.props.postId) {
      this.setState({
        replyTo: {
          commentId: payload.commentId,
          userName: payload.author?.full_name || '',
        }
      });
      this.contentEditable.current.focus();
    }
  };

  onChange = (event) => {
    this.setState({
      comment: event.target.value
    });
  };

  onKeyDown = (event) => {
    if (event && event.keyCode === KEY_CODE.ENTER && !event.shiftKey) {
      event.preventDefault();
      this.submit();
    }
  };

  submit = () => {
    const {isEdit} = this.props;
    if (!isEdit) {
      this.handleAdd();
    } else {
      this.handleUpdate();
    }
  };

  handleAdd = () => {
    const {postId} = this.props;
    const {comment, replyTo} = this.state;
    const postData = {
      comment,
      reply_to: replyTo?.commentId || 0,
    };
    addCommentToPostAPI(postId, postData).then(res => {
      const commentItem = {
        ...postData,
        post_id: postId,
        id: res.data.id
      };
      Broadcaster.broadcast(BROADCAST_EVENT.ADD_COMMENT_SUCCESS, commentItem);
      this.setState({
        comment: '',
        replyTo: null,
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  handleUpdate = () => {
    const {postId, commentId, onUpdateSuccess} = this.props;
    const {comment} = this.state;
    updatePostCommentAPI(postId, commentId, {comment}).then(() => {
      onUpdateSuccess({
        id: commentId,
        comment,
        updated_date: new Date().toISOString()
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  cancelReply = () => {
    this.setState({
      replyTo: null,
    });
    this.contentEditable.current.blur();
  }

  render() {
    const {user, postId} = this.props;
    const {comment, replyTo} = this.state;
    return (
      <div className="new-comment-container">
        <div className="new-comment">
          <Avatar src={user.avatar || null} size={40}/>
          <div className="editor-wrapper">
            <ContentEditable
              className="editor"
              tagName="div"
              disabled={false}
              style={style}
              html={comment}
              innerRef={this.contentEditable}
              onChange={this.onChange}
              onKeyDown={this.onKeyDown}
              id={`post-${postId}-new-comment`}
            />
          </div>
        </div>
        {
          replyTo &&
          <p className="reply-to">Trả lời: <strong>{replyTo.userName}</strong> <HyperLink onClick={this.cancelReply}>Hủy</HyperLink></p>
        }
      </div>
    );
  }
}

NewComment.propTypes = {
  isEdit: PropType.bool,
  initialValue: PropType.string,
  postId: PropType.number,
  commentId: PropType.number,
  onUpdateSuccess: PropType.func,
};

NewComment.defaultProps = {
  isEdit: false,
  initialValue: '',
  commentId: null,
};

export default NewComment;
