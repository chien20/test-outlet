import React from 'react';
import {Modal} from 'react-bootstrap';
import {handleInputTextChanged, showAlert} from '../../../../../../../common/helpers';
import {reportGroupItemAPI} from '../../../../../../../api/groups';

class ReportPostModal extends React.PureComponent {
  state = {
    isOpen: false,
    isSubmitting: false,
    reason: '',
    objectId: null,
    objectType: null,
    groupId: null,
  };

  setData = (data) => {
    this.setState(data);
  };

  handleOpen = (groupId, objectId, objectType) => {
    this.setState({
      isOpen: objectId && objectType,
      isSubmitting: false,
      reason: '',
      groupId,
      objectId,
      objectType,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  handleSubmit = () => {
    const {groupId, objectId, objectType, reason} = this.state;
    this.setState({isSubmitting: true});
    reportGroupItemAPI(groupId, {
      object_id: objectId,
      object_type: objectType,
      reason,
    }).then(() => {
      showAlert({
        message: 'Đã gửi báo cáo!',
        type: 'success'
      });
      this.handleClose();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: 'Gửi báo cáo thất bại! Vui lòng thử lại sau!'
      });
      this.setState({isSubmitting: false});
    });
  };

  render() {
    const {isOpen, isSubmitting, reason} = this.state;
    return (
      <Modal size="md" show={isOpen} onHide={this.handleClose} className="alert-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>Gửi báo cáo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-group">
            <label>Lý do (*):</label>
            <textarea
              className="form-control"
              value={reason}
              onChange={handleInputTextChanged('reason', this.setData)}
              required={true}
            />
          </div>
          <p>Vui lòng nhập chi tiết lý do bạn báo cáo.</p>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-default" onClick={this.handleClose}>
            Đóng
          </button>
          <button className="btn btn-primary" onClick={this.handleSubmit} disabled={isSubmitting}>
            Gửi
          </button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default ReportPostModal;
