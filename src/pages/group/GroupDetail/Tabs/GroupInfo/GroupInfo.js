import React from 'react';
import {withGroupDetail} from '../../GroupDetailContext';
import HTMLView from '../../../../../components/View/HTMLView';

class GroupInfo extends React.PureComponent {
  render() {
    const {group} = this.props;
    return (
      <div className="group-info">
        <div className="group-property">
          <label>Loại nhóm</label>
          <div className="property-value">{group.privacy === 10 ? 'Công khai' : 'Riêng tư'}</div>
        </div>
        <div className="group-property">
          <label>Giới thiệu</label>
          <div className="property-value">{group.description}</div>
        </div>
        {
          group.policy &&
          <div className="group-property">
            <label>Nội quy</label>
            <div className="property-value">
              <HTMLView html={group.policy}/>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default withGroupDetail(GroupInfo);
