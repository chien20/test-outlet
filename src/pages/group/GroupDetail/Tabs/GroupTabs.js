import React, {Fragment} from 'react';
import GroupInfo from './GroupInfo/GroupInfo';
import GroupProducts from './GroupProducts/GroupProducts';
import Broadcaster from '../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../common/constants/broadcastEvents';
import {getGroupMemberPermissionsAPI} from '../../../../api/groups';
import PermissionChecker from '../../_components/PermissionChecker/PermissionChecker';
import {GROUP_PERMISSIONS_BY_NAME} from '../../../../common/constants/app';
import GroupPosts from './GroupPosts/GroupPosts';
import InviteUserModal from './ManageGroup/_components/InviteUserModal/InviteUserModal';
import connect from 'react-redux/es/connect/connect';
import {Redirect, Route, Switch} from 'react-router-dom';
import ManageGroupRoutes from './ManageGroup/ManageGroupRoutes';
import GroupPostDetail from '../../GroupPostDetail/GroupPostDetail';
import {TabMenuItem, TabMenuNav} from '../../../../components/TabMenu/TabMenu';
import history from '../../../../common/utils/router/history';
import Dropdown from 'react-bootstrap/Dropdown';

const defaultPermissions = [GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_GIOI_THIEU.id];

const tabs = [
  {
    path: '/groups/:groupId/products',
    component: GroupProducts,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_SAN_PHAM.id],
  },
  {
    path: '/groups/:groupId/products/p/:page',
    component: GroupProducts,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_SAN_PHAM.id],
  },
  {
    path: '/groups/:groupId/posts/:postId',
    component: GroupPostDetail,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_BAI_VIET.id],
  },
  {
    path: '/groups/:groupId/posts',
    component: GroupPosts,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_BAI_VIET.id],
  },
  {
    path: '/groups/:groupId/info',
    component: GroupInfo,
    exact: true,
    permissions: [GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_GIOI_THIEU.id],
  },
  {
    path: '/groups/:groupId/manage',
    component: ManageGroupRoutes,
    exact: false,
    permissions: [
      GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BAI_VIET.id,
      GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BINH_LUAN.id,
      GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_SAN_PHAM.id,
      GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_THANH_VIEN.id,
      GROUP_PERMISSIONS_BY_NAME.XEM_BAO_CAO_THONG_KE.id,
      GROUP_PERMISSIONS_BY_NAME.SUA_THONG_TIN_GIOI_THIEU.id,
    ],
  }
];

class GroupTabs extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      permissions: null,
    };
  }

  componentDidMount() {
    this.getPermissions();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.group !== this.props.group || prevProps.isAuthenticated !== this.props.isAuthenticated) {
      this.getPermissions();
    }
  }

  getPermissions = () => {
    const {group, isAuthenticated} = this.props;
    if (!isAuthenticated) {
      this.setState({
        permissions: defaultPermissions,
      });
      return;
    }
    getGroupMemberPermissionsAPI(group.id).then(res => {
      const permissions = res.data || [];
      if (!permissions.includes(GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_GIOI_THIEU.id)) {
        permissions.push(GROUP_PERMISSIONS_BY_NAME.XEM_THONG_TIN_GIOI_THIEU.id);
      }
      this.setState({
        permissions,
      });
    }).catch(() => {
      this.setState({
        permissions: defaultPermissions,
      });
    });
  };

  getDefaultRoute = () => {
    const {group} = this.props;
    const {permissions} = this.state;
    if (!permissions) {
      return null;
    }
    console.log(permissions);
    if (permissions.includes(GROUP_PERMISSIONS_BY_NAME.XEM_BAI_VIET.id)) {
      return `/groups/${group.id}/posts`;
    }
    if (permissions.includes(GROUP_PERMISSIONS_BY_NAME.XEM_SAN_PHAM.id)) {
      return `/groups/${group.id}/products`;
    }
    return `/groups/${group.id}/info`;
  };

  redirectDefaultRoute = () => {
    const route = this.getDefaultRoute();
    if (!route) {
      return;
    }
    history.replace(route);
  };

  renderDefaultRoute = () => {
    const route = this.getDefaultRoute();
    if (!route) {
      return null;
    }
    return (
      <Redirect to={route}/>
    );
  };

  shareProducts = () => {
    const {group} = this.props;
    Broadcaster.broadcast(BROADCAST_EVENT.SHARE_PRODUCT_TO_GROUP, {
      group_id: group.id
    });
  };

  inviteUser = () => {
    Broadcaster.broadcast(BROADCAST_EVENT.INVITE_USER_TO_GROUP);
  };

  render() {
    const {group, user} = this.props;
    const {permissions} = this.state;
    if (!permissions) {
      return null;
    }
    const isOwner = user?.id === group?.owner_id;
    return (
      <Fragment>
        <div className="group-tabs">
          <TabMenuNav className="nav-tabs">
            <PermissionChecker
              requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.XEM_BAI_VIET.id]}
              userPermissions={permissions}
              isOwner={isOwner}
            >
              <TabMenuItem
                href={`/groups/${group.id}/posts`}
              >
                Bài viết
              </TabMenuItem>
            </PermissionChecker>
            <PermissionChecker
              requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.XEM_SAN_PHAM.id]}
              userPermissions={permissions}
              isOwner={isOwner}
            >
              <TabMenuItem
                href={`/groups/${group.id}/products`}
              >
                Sản phẩm
              </TabMenuItem>
            </PermissionChecker>
            <TabMenuItem
              href={`/groups/${group.id}/info`}
            >
              Giới thiệu
            </TabMenuItem>
            <PermissionChecker
              requiredPermissions={[
                GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BAI_VIET.id,
                GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_BINH_LUAN.id,
                GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_SAN_PHAM.id,
                GROUP_PERMISSIONS_BY_NAME.KIEM_DUYET_THANH_VIEN.id,
                GROUP_PERMISSIONS_BY_NAME.XEM_BAO_CAO_THONG_KE.id,
                GROUP_PERMISSIONS_BY_NAME.SUA_THONG_TIN_GIOI_THIEU.id,
              ]}
              userPermissions={permissions}
              isOwner={isOwner}
            >
              <TabMenuItem
                href={`/groups/${group.id}/manage`}
              >
                Quản lý
              </TabMenuItem>
            </PermissionChecker>
            <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                Tiện ích
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="https://meet.eplaza.com.vn/" target="_blank">Họp online</Dropdown.Item>
                <Dropdown.Item
                  href="/p/ho-tro-xu-ly-khieu-nai-tranh-chap-giua-cac-thanh-vien"
                  target="_blank"
                >
                  Giải quyết tranh chấp
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </TabMenuNav>
          {
            group.joined &&
            <div className="group-actions">
              <PermissionChecker
                requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.DANG_SAN_PHAM.id]}
                userPermissions={permissions}>
                <button className="btn btn-secondary" onClick={this.shareProducts}>+ Đăng sản phẩm</button>
              </PermissionChecker>
              <PermissionChecker
                requiredPermissions={[GROUP_PERMISSIONS_BY_NAME.MOI_THANH_VIEN.id]}
                userPermissions={permissions}>
                <button className="btn btn-primary" onClick={this.inviteUser}>+ Mời thành viên</button>
                <InviteUserModal
                  groupId={group.id}
                  showRole={isOwner}
                  eventName={BROADCAST_EVENT.INVITE_USER_TO_GROUP}
                />
              </PermissionChecker>
            </div>
          }
        </div>
        <div className="tab-content">
          <Switch>
            {
              tabs.map((tab) => {
                const Component = tab.component;
                return (
                  <Route
                    key={tab.path}
                    path={tab.path}
                    render={(props) => (
                      <PermissionChecker
                        userPermissions={permissions}
                        requiredPermissions={tab.permissions}
                        onRestrict={this.redirectDefaultRoute}
                        isOwner={isOwner}
                      >
                        <Component
                          {...props}
                          permissions={permissions}
                          isOwner={isOwner}
                          onRestrict={this.redirectDefaultRoute}
                        />
                      </PermissionChecker>
                    )}
                    exact={tab.exact}
                  />
                );
              })
            }
            <Route render={this.renderDefaultRoute}/>
          </Switch>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info,
});

export default connect(mapStateToProps)(GroupTabs);
