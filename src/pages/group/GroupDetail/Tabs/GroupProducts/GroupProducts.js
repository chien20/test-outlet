import React from 'react';
import {withRouter} from 'react-router-dom';
import {history} from '../../../../../common/utils/router/history';
import ProductGrid from '../../../../product/_components/ProductGrid/ProductGrid';

const requestConfig = {
  __auth: true
};

class GroupProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      queryParams: null
    };
  }

  componentDidMount() {
    const {match: {params: {groupId}}} = this.props;
    const queryParams = {group_id: groupId};
    this.setState({
      queryParams: queryParams
    });
  }

  onPageChanged = (page) => {
    const {match: {params: {groupId}}} = this.props;
    if (page > 1) {
      history.push(`/groups/${groupId}/products/p/${page}/`);
    } else {
      history.push(`/groups/${groupId}/`);
    }
  };

  render() {
    const {queryParams} = this.state;
    if (!queryParams) {
      return null;
    }
    return (
      <div className="group-products">
        <ProductGrid
          onPageChanged={this.onPageChanged}
          queryParams={queryParams}
          requestConfig={requestConfig}
          toolbar={false}/>
      </div>
    )
  }
}

export default withRouter(GroupProducts);
