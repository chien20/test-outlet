import React from 'react';

const GroupDetailContext = React.createContext({group: null});

export const withGroupDetail = Component => {
  return React.forwardRef((ownProps, ref) => {
    return (
      <GroupDetailContext.Consumer>
        {props => <Component {...props} {...ownProps} ref={ref}/>}
      </GroupDetailContext.Consumer>
    );
  });
};

export default GroupDetailContext;
