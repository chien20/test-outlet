import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import qs from 'qs';
import GroupHeader from '../_components/GroupHeader/GroupHeader';
import {acceptInviteInfoAPI, getGroupDetailAPI, getInviteInfoAPI, requestJoinGroupAPI} from '../../../api/groups';
import {imageUrl, showAlert} from '../../../common/helpers';
import GroupTabs from './Tabs/GroupTabs';
import ShareProductsModal from '../_components/ShareProductsModal/ShareProductsModal';
import JoinGroup from './JoinGroup';
import './GroupDetail.scss';
import GroupDetailContext from './GroupDetailContext';
import {isMobile} from '../../../common/helpers/browser';
import IconBackMobile from '../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../components/HeaderMobile/TitleMobile/TitleMobile';
import HeaderMobile from '../../../components/HeaderMobile/HeaderMobile';
import {getObjectMediaAPI} from '../../../api/media';
import {OBJECT_TYPES} from '../../../common/constants/objectTypes';

class GroupDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      group: null,
      invite: null
    };
  }

  componentDidMount() {
    this.getGroupDetail();
    this.getInvite();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match && this.props.match) {
      if (prevProps.match.params.groupId !== this.props.match.params.groupId) {
        this.getGroupDetail();
      }
      if (prevProps.match.params.inviteId !== this.props.match.params.inviteId) {
        this.getInvite();
      }
    }
    if (prevProps.isAuthenticated !== this.props.isAuthenticated) {
      this.getGroupDetail();
      this.getInvite();
    }
  }

  getGroupDetail = () => {
    this.getDataAsync().then(group => {
      this.setState({
        isLoading: false,
        group,
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
        group: null
      });
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  getDataAsync = async () => {
    const {match: {params: {groupId}}} = this.props;
    const {data: group} = await getGroupDetailAPI(groupId);
    const {data: {list: media}} = await getObjectMediaAPI(groupId, OBJECT_TYPES.Group, null, {page_size: 100});
    group.media = media || [];
    group.media.forEach(item => {
      item.use_type = item.type;
      delete item.type;
      item.preview_url = imageUrl(item.url);
    });
    group.covers = group.media.filter(item => item.use_type === 'cover').map(item => ({
      title: group.name,
      url: item.preview_url,
    }));
    return group;
  };

  getInvite = () => {
    const {match: {params: {inviteId}}, isAuthenticated} = this.props;
    if (!inviteId || !isAuthenticated) {
      return;
    }
    getInviteInfoAPI(inviteId).then(res => {
      this.setState({
        invite: res.data
      });
    }).catch(error => {
      this.setState({
        invite: null
      });
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  requestJoin = () => {
    const {location: {search}, match: {params: {groupId}}} = this.props;
    const data = {
      answers: []
    };
    if (search && typeof search === 'string') {
      const params = qs.parse(search || '', {ignoreQueryPrefix: true});
      if (params && params.ref) {
        data.referer_user_id = params.ref;
        return;
      }
    }
    requestJoinGroupAPI(groupId, data).then(() => {
      showAlert({
        type: 'success',
        message: `Đã yêu cầu tham gia! Vui lòng chờ phê duyệt!`
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  acceptInvite = (accept) => () => {
    const {match: {params: {inviteId}}} = this.props;
    acceptInviteInfoAPI({
      invite_id: inviteId,
      accept,
    }).then(() => {
      this.setState({
        invite: null
      }, this.getGroupDetail);
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  render() {
    const {isAuthenticated, location: {pathname}} = this.props;
    const {group, invite} = this.state;
    if (!group) {
      return null;
    }
    return (
      <GroupDetailContext.Provider value={{group}}>
        <Helmet>
          <title>{group.name}</title>
        </Helmet>
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30 m-l-5">{group.name}</TitleMobile>
          </HeaderMobile>
        }
        <div className="group-detail group-page-content paper">
          <GroupHeader group={group}/>
          {
            !group.joined && isAuthenticated &&
            <Fragment>
              {
                !invite &&
                <JoinGroup
                  requestJoin={this.requestJoin}
                  groupId={group.id}
                />
              }
              {
                invite &&
                <div className="alert alert-success invite">
                  <div className="message"><strong>{invite.sender.full_name || invite.sender.fullName}</strong> đã mời
                    bạn tham gia nhóm
                  </div>
                  <div className="action">
                    <button className="btn btn-success" onClick={this.acceptInvite(true)}>Đồng ý</button>
                    <button className="btn btn-danger" onClick={this.acceptInvite(false)}>Từ chối</button>
                  </div>
                </div>
              }
            </Fragment>
          }
          <GroupTabs
            group={group}
            isAuthenticated={isAuthenticated}
            pathname={pathname}
          />
        </div>
        <ShareProductsModal groupId={group.id}/>
      </GroupDetailContext.Provider>
    );
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.user.auth.isAuthenticated,
});

export default connect(mapStateToProps)(GroupDetail);
