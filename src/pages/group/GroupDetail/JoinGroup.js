import React from 'react';
import PropTypes from 'prop-types';
import {acceptInviteInfoAPI, getInvitesAPI} from '../../../api/groups';
import {showAlert} from '../../../common/helpers';

class JoinGroup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: true,
      invites: []
    };
  }

  componentDidMount() {
    this.getInvites();
  }

  getInvites = () => {
    const {groupId} = this.props;
    this.setState({
      isFetching: false
    });
    getInvitesAPI(groupId).then(res => {
      this.setState({
        invites: res.data.list || [],
        isFetching: false
      });
    }).catch(error => {
      this.setState({
        isFetching: false
      });
    });
  };

  acceptInvite = (accept, inviteId) => () => {
    acceptInviteInfoAPI({
      invite_id: inviteId,
      accept,
    }).then(() => {
      this.setState({
        isFetching: true
      });
      showAlert({
        type: 'success',
        message: 'Success'
      });
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `${error.message}`
      });
    });
  };

  render() {
    const {requestJoin} = this.props;
    const {isFetching, invites} = this.state;
    if (isFetching) {
      return null;
    }
    if (!invites || !invites.length) {
      return (
        <div className="join-now">
          <button className="btn btn-primary" onClick={requestJoin}> + Tham gia nhóm</button>
          <span>Tham gia nhóm để nhận được nhiều ưu đãi</span>
        </div>
      );
    }
    const invite = invites[0];
    return (
      <div className="alert alert-success invite">
        <div className="message"><strong>{invite.sender.full_name || invite.sender.fullName}</strong> đã mời
          bạn tham gia nhóm
        </div>
        <div className="action">
          <button className="btn btn-success" onClick={this.acceptInvite(true, invite.id)}>Đồng ý</button>
          <button className="btn btn-danger" onClick={this.acceptInvite(false, invite.id)}>Từ chối</button>
        </div>
      </div>
    );
  }
}

JoinGroup.propTypes = {
  requestJoin: PropTypes.func,
  groupId: PropTypes.any
};

export default JoinGroup;
