import React, {Fragment} from "react";
import "./GroupFaq.scss";
import {isMobile} from "../../../common/helpers/browser";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import FaqCard from "./FaqCard";

class GroupFaq extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      questions_list: [
        {
          id: 1,
          question: "Hội nhóm là gì?",
          answer: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus\n" +
            "terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer\n" +
            "labore wes anderson cred nesciunt sapiente ea proident."
        },
        {
          id: 2,
          question: "Tại sao nên tham gia vào hội nhóm trên Outlet ?\n?",
          answer: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus\n" +
            "terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer\n" +
            "labore wes anderson cred nesciunt sapiente ea proident."
        },
        {
          id: 3,
          question: "Những yêu cầu để tạo một hội nhóm trên Outlet ?\n",
          answer: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus\n" +
            "terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer\n" +
            "labore wes anderson cred nesciunt sapiente ea proident."
        },
        {
          id: 4,
          question: "Quy trình tạo một hội nhóm mới trên Outlet mất bao lâu?\n",
          answer: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus\n" +
            "terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer\n" +
            "labore wes anderson cred nesciunt sapiente ea proident."
        },
        {
          id: 5,
          question: "Chính sách chiết khấu giữa các đại lý trong 1 hội nhóm?",
          answer: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus\n" +
            "terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer\n" +
            "labore wes anderson cred nesciunt sapiente ea proident."
        },
      ],
    }
  }

  render() {
    const {questions_list} = this.state;

    return (
      <Fragment>
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30 m-l-5">Những câu hỏi thường gặp</TitleMobile>
          </HeaderMobile>
        }
        <div className="group-faq group-page-content paper">
          {
            !isMobile &&
            <h3 className="title">Những câu hỏi thường gặp</h3>
          }
          <div className="faqs">
            {
              questions_list.map((item) => (
                <FaqCard key={item.id} item={item}/>
              ))
            }
          </div>
        </div>
      </Fragment>
    )
  }
}

export default GroupFaq;