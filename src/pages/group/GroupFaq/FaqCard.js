import React from "react";
import {Button, Collapse} from "react-bootstrap";

class FaqCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = ({
      isOpen: false,
    })
  }

  setOpen = (isOpen) => {
    this.setState({
      isOpen: !isOpen
    })
  }

  render() {
    const { isOpen } = this.state;
    const { item } = this.props
    return (
        <div className="card">
          <Button className="faq"
                  onClick={() => this.setOpen(isOpen)}
                  aria-controls={`faq-`+ item.id}
                  aria-expanded={isOpen}
          >
            <span>{ item.question }</span>
          </Button>
          <Collapse in={isOpen}>
            <div id={`faq-`+ item.id} className="answer">{ item.answer }</div>
          </Collapse>
        </div>
    )
  }

}

export default FaqCard;