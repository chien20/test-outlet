import React, {Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';
import GroupHome from './GroupHome/GroupHome';
import Sidebar from './_components/Sidebar/Sidebar';
import {Breadcrumb, BreadcrumbItem} from '../../components/Breadcrumb';
import GroupDetail from './GroupDetail/GroupDetail';
import {Helmet} from 'react-helmet';
import Explore from './Explore/Explore';
import {isMobile} from '../../common/helpers/browser';
import RegisterGroup from './RegisterGroup/RegisterGroup';
import './GroupPage.scss';
import GroupFaq from "./GroupFaq/GroupFaq";

const PageHeader = () => (
  <Fragment>
    <Helmet>
      <title>Hội nhóm</title>
    </Helmet>
    {
      !isMobile &&
      <Breadcrumb>
        <BreadcrumbItem text="Hội nhóm" isActive={true}/>
      </Breadcrumb>
    }
  </Fragment>
);

class GroupPage extends React.PureComponent {
  render() {
    return (
      <Fragment>
        <PageHeader/>
        <section className="section group-page common-page">
          <div className="container">
            <div className="e-row">
              {
                !isMobile &&
                <div className="col-left">
                  <Sidebar/>
                </div>
              }
              <div className="col-main">
                <Switch>
                  <Route component={GroupHome} path="/groups/" exact/>
                  <Route component={RegisterGroup} path="/groups/register" exact/>
                  <Route component={Explore} path="/groups/explore/" exact/>
                  <Route component={GroupFaq} path="/groups/faq" exact/>
                  <Route component={GroupDetail} path="/groups/:groupId"/>
                </Switch>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    )
  }
}

export default GroupPage;
