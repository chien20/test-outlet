import React, {Fragment} from 'react';
import GroupPost from '../GroupDetail/Tabs/GroupPosts/GroupPost/GroupPost';
import './GroupPostDetail.scss';

class GroupPostDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      postDetail: null
    };
  }

  onLoaded = (postDetail) => {
    this.setState({
      postDetail: postDetail
    });
  };

  render() {
    const {match: {params: {groupId, postId}}} = this.props;
    return (
      <Fragment>
        <div className="group-post-detail group-page-content paper">
          <GroupPost groupId={groupId} postId={postId} onLoaded={this.onLoaded}/>
        </div>
      </Fragment>
    )
  }
}

export default GroupPostDetail;
