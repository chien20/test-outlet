import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import CategorySlider from '../../product/_components/CategorySlider/CategorySlider';
import {Helmet} from 'react-helmet';
import {getSuggestGroupsAPI} from '../../../api/groups';
import {imageUrl, publicUrl, showAlert} from '../../../common/helpers';
import GroupItem from '../../../components/Group/GroupItem/GroupItem';
import DivLoading from '../../../components/DivLoading/DivLoading';
import HyperLink from '../../../components/HyperLink/HyperLink';
import {isMobile} from '../../../common/helpers/browser';
import InfiniteScroll from '../../../components/InfiniteScroll/InfiniteScroll';
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import IconAddMobile from "../../../components/HeaderMobile/IconAddMobile/IconAddMobile";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import {Link} from "react-router-dom";

const ITEMS_PER_PAGE = 32;

class Explore extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      groupList: [],
      hasMore: false,
      nextPage: 0,
      isLoading: false,
      suggestGroups: []
    };
  }

  componentDidMount() {
    this.getListGroups();
  }

  getListGroups = () => {
    const {nextPage, isLoading} = this.state;
    if (isLoading) {
      return;
    }
    const params = {
      current_page: nextPage,
      page_size: ITEMS_PER_PAGE
    };
    this.setState({
      isLoading: true
    });
    getSuggestGroupsAPI(params).then(res => {
      const pageInfo = res.data.page_info;
      this.setState(prevState => ({
        isLoading: false,
        groupList: [...prevState.groupList, ...res.data.list],
        hasMore: (pageInfo.current_page + 1) * pageInfo.page_size < pageInfo.total_items,
        nextPage: pageInfo.current_page + 1
      }));
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách hội nhóm!`
      });
    });
  };

  onClickLoadMore = (event) => {
    if (event) {
      event.preventDefault();
    }
    this.getListGroups();
  };

  render() {
    const {groupList, isLoading, hasMore} = this.state;
    return (
      <Fragment>
        <Helmet>
          <title>Hội nhóm cho bạn</title>
        </Helmet>
        { isMobile &&
        <Fragment>
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30">Hội nhóm cho bạn</TitleMobile>
          </HeaderMobile>
          <div className="group-explore">
            <div className="group-list">
              {
                isLoading && !groupList && <DivLoading/>
              }
              {
                groupList.map((item, index) => {
                  const groupAvatar = item.avatar ? imageUrl(item.avatar.url) : publicUrl('/assets/images/no-image/256x256.png')
                  return (
                      <Link to={`/groups/${item.id}`} className="group-item" key={index}>
                        <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}>
                          <div className="group-name"><span>{item.name}</span></div>
                        </div>
                        <div className="join-group">
                          <button className="btn">Tham gia</button>
                        </div>
                      </Link>
                  )
                })
              }
            </div>
          </div>
        </Fragment>
        }
        { !isMobile &&
        <div className="group-explore group-page-content paper">
          <CategorySlider/>
          <div className="section-heading">
            <h2 className="bg-white">Hội nhóm cho bạn</h2>
          </div>
          <div className="group-list">
            {
              isLoading && !groupList && <DivLoading/>
            }
            {
              groupList.map((item, index) => (
                  <GroupItem group={item} key={index}/>
              ))
            }
          </div>
          <div className="item-pagination hidden-mobile">
            {
              hasMore &&
              <Fragment>
                <div className="btn-read-more d-mobile-none">
                  <HyperLink to="#" onClick={this.onClickLoadMore}>Xem Thêm</HyperLink>
                </div>
                {
                  isMobile && <InfiniteScroll loadMore={this.onClickLoadMore} hasMore={hasMore}/>
                }
              </Fragment>
            }
          </div>
        </div>
        }
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(Explore);
