import React from 'react';
import GroupInfo from '../../GroupDetail/Tabs/ManageGroup/_components/EditGroup/GroupInfo';

class Step1 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.formEl = React.createRef();
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.props.handleGoNextStep(event);
  };

  render() {
    const {formData, files, setData, handleFileChange, handleCancel} = this.props;
    return (
      <form className="step-1" onSubmit={this.submit} ref={this.formEl}>
        <GroupInfo
          formData={formData}
          files={files}
          setData={setData}
          handleFileChange={handleFileChange}/>
        <div className="form-submit">
          <button className="btn btn-default" onClick={handleCancel}>Hủy bỏ</button>
          <button className="btn btn-primary" type="submit">Đăng ký</button>
        </div>
      </form>
    )
  }
}

export default Step1;
