import React from 'react';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

function Steps({activeStep = 0, ...others}) {
  switch (activeStep) {
    case 0:
      return <Step1 {...others}/>;
    case 1:
      return <Step2 {...others}/>;
    default:
      return <Step3 {...others}/>;
  }
}

export default Steps;
