import React from 'react';
import CommissionInfo from '../../GroupDetail/Tabs/ManageGroup/_components/EditGroup/CommissionInfo';

class Step2 extends React.PureComponent {
  constructor(props) {
    super(props);
    this.formEl = React.createRef();
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.props.handleGoNextStep(event);
  };

  render() {
    const {formData, setData, handleGoPrevStep} = this.props;
    return (
      <form  className="step-2" onSubmit={this.submit} ref={this.formEl}>
        <CommissionInfo
          formData={formData}
          setData={setData}/>
        <div className="form-submit">
          <button className="btn btn-default" onClick={handleGoPrevStep}>Quay lại</button>
          <button className="btn btn-primary" type="submit">Đăng ký</button>
        </div>
      </form>
    )
  }
}

export default Step2;
