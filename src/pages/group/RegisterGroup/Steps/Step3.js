import React from 'react';
import {Link} from 'react-router-dom';

class Step3 extends React.PureComponent {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="register-completed">
        {/*<div className="no-data">*/}
        {/*<img src={successIcon} alt={`Thành công`}/>*/}
        {/*<p>Đã gửi đơn đăng ký!</p>*/}
        {/*<p>Đơn đăng ký của bạn đã được gửi đến Outlet. Chúng tôi sẽ xem xét và phê duyệt nếu đơn của bạn đủ điều kiện*/}
        {/*hoạt động.</p>*/}
        {/*<p>Xin cảm ơn!</p>*/}
        {/*</div>*/}
        <div className="form-submit text-center">
          <Link to="/groups" className="btn btn-primary" type="submit">Trở về</Link>
        </div>
      </div>
    )
  }
}

export default Step3;
