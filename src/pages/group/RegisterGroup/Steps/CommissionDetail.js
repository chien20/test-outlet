import React, {Fragment} from 'react';
import HyperLink from '../../../../components/HyperLink/HyperLink';
import {isMobile} from "../../../../common/helpers/browser";

class CommissionDetail extends React.PureComponent {
  componentDidMount() {
    this.checkData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {relatedDataKey} = this.props;
    if (prevProps.formData !== this.props.formData) {
      if (prevProps.formData[relatedDataKey] !== this.props.formData[relatedDataKey]) {
        this.checkData();
      }
    }
  }

  checkData = () => {
    const {dataKey, relatedDataKey, formData} = this.props;
    if (formData[relatedDataKey] && formData[dataKey].length === 0) {
      this.handleAddCommissionDetail(dataKey, 100)();
    } else if (!formData[relatedDataKey] && formData[dataKey].length > 0) {
      this.handleRemoveAllCommissionDetail();
    }
  };

  handleAddCommissionDetail = (key, value = 0) => () => {
    const {formData, setData} = this.props;
    setData({
      [key]: [...formData[key], value]
    });
  };

  handleRemoveCommissionDetail = (key, index) => () => {
    const {formData, setData} = this.props;
    const data = [...formData[key]];
    data.splice(index, 1);
    setData({
      [key]: data
    });
  };

  handleRemoveAllCommissionDetail = () => {
    const {setData, dataKey} = this.props;
    setData({
      [dataKey]: []
    });
  };

  handleUpdateCommissionDetail = (key, index) => (event) => {
    let value = event.target.value;
    const {formData, setData} = this.props;
    const data = [...formData[key]];
    try {
      value = parseFloat(value);
    } catch (e) {
      value = 0;
    }
    data[index] = value;
    setData({
      [key]: data
    });
  };

  isValid = () => {
    const {formData, dataKey} = this.props;
    let total = 0;
    if (!formData[dataKey] || !formData[dataKey].length) {
      return false;
    }
    formData[dataKey].forEach(item => {
      total += item;
    });
    return total === 100;
  };

  render() {
    const {formData, dataKey, objectName} = this.props;
    if (!formData[dataKey] || !formData[dataKey].length) {
      return null;
    }
    return (
      <Fragment>
        {
          !isMobile &&
          <div className="form-section">
            <label className="form-section-label">Tỷ lệ thưởng cho {objectName} các cấp</label>
            {
              dataKey === 'reseller_commission_detail' &&
              <p>
                <span className="text-underline">Giải thích</span>:<br/>
                <strong>Đại lý cấp 1</strong> là những người do bạn (chủ hội nhóm) giới thiệu vào hội nhóm, <strong>đại lý
                cấp 2</strong> là những người do <strong>đại lý cấp 1</strong> giới thiệu...<br/>
                Bằng cách thiết lập tỷ lệ dưới đây, bạn có thể thiết lập hoa hồng nhận được của mỗi cấp đại lý.
              </p>
            }
            {
              dataKey === 'affiliate_commission_detail' &&
              <p>
                <span className="text-underline">Giải thích</span>:<br/>
                Giả sử người <strong>A</strong> là người mua hàng, <strong>B</strong> là người giới thiệu
                người <strong>A</strong> vào hội nhóm, <strong>C</strong> là người giới thiệu
                người <strong>B</strong> vào hội nhóm,...<br/>
                Khi đó <strong>B</strong> là người giới thiệu cấp 1 của <strong>A</strong>, <strong>C</strong> là
                người giới thiệu cấp 2 của <strong>A</strong>,...<br/>
                Bằng cách thiết lập tỷ lệ dưới đây, bạn có thể thiết lập hoa hồng nhận được của mỗi cấp giới thiệu.
              </p>
            }
            {
              !this.isValid() &&
              <div className="alert alert-danger">
                <strong>Lỗi</strong>
                <div>Tổng các giá trị trong mục này phải bằng 100%</div>
              </div>
            }
            {
              formData[dataKey].map((item, index) => (
                  <div className="form-group row" key={index}>
                    <div className="col-sm-8">
                      <label>Cấp {index + 1}</label>
                      {
                        index > 0 &&
                        <HyperLink className="delete-link" onClick={this.handleRemoveCommissionDetail(dataKey, index)}>
                          (Xóa)
                        </HyperLink>
                      }
                    </div>
                    <div className="input-group col-sm-4">
                      <input
                          type="number"
                          className="form-control"
                          value={item}
                          name={dataKey}
                          required={true}
                          min={0}
                          max={100}
                          onChange={this.handleUpdateCommissionDetail(dataKey, index)}/>
                      <div className="input-group-append">
                        <div className="input-group-text">%</div>
                      </div>
                      <div className="invalid-feedback">
                        Vui lòng nhập tỷ lệ thưởng cho {objectName} cấp {index + 1}.
                      </div>
                    </div>
                  </div>
              ))
            }
            <HyperLink onClick={this.handleAddCommissionDetail(dataKey)}>
              + Thêm cấp
            </HyperLink>
          </div>
        }
        {
          isMobile &&
          <div className="form-section">
            <label className="form-section-label">Tỷ lệ thưởng cho {objectName} các cấp</label>
            {
              !this.isValid() &&
              <div className="alert alert-danger">
                <strong>Lỗi</strong>
                <div>Tổng các giá trị trong mục này phải bằng 100%</div>
              </div>
            }
            {
              formData[dataKey].map((item, index) => (
                  <div className="form-group" key={index}>
                    <div className="wrapper">
                      <div>
                        <label>Cấp {index + 1}</label>
                        {
                          index > 0 &&
                          <HyperLink className="delete-link" onClick={this.handleRemoveCommissionDetail(dataKey, index)}>
                            (Xóa)
                          </HyperLink>
                        }
                      </div>
                      <div className="input-group">
                        <input
                            type="number"
                            className="form-control"
                            value={item}
                            name={dataKey}
                            required={true}
                            min={0}
                            max={100}
                            onChange={this.handleUpdateCommissionDetail(dataKey, index)}/>
                        <div className="input-group-append">
                          <div className="input-group-text">%</div>
                        </div>
                        <div className="invalid-feedback">
                          Vui lòng nhập tỷ lệ thưởng cho {objectName} cấp {index + 1}.
                        </div>
                      </div>
                    </div>
                  </div>
              ))
            }
            <HyperLink className="add-level-link" onClick={this.handleAddCommissionDetail(dataKey)}>
              + Thêm cấp
            </HyperLink>
          </div>
        }
      </Fragment>
    )
  }
}

export default CommissionDetail;
