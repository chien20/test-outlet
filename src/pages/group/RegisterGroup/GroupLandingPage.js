import React, {Fragment} from 'react';
import './GroupLandingPage.scss';
import './../GroupHome/GroupHomePC.scss'
import {publicUrl} from '../../../common/helpers';
import {isMobile} from "../../../common/helpers/browser";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import {Link} from "react-router-dom";

class GroupLandingPage extends React.PureComponent {
  render() {
    const {handleClose} = this.props;
    const backgroundImage = publicUrl(`/assets/images/backgrounds/object.svg`)
    return (
      <div className="group-landing-page group-page-content paper">
        {
          isMobile &&
            <Fragment>
              <HeaderMobile>
                <IconBackMobile/>
                <TitleMobile>Hội nhóm</TitleMobile>
              </HeaderMobile>
              <div className="page-content" style={{backgroundImage: `url(${backgroundImage})`}}>
                <div className="wrapper">
                  <div className="title" >
                    <h3 className="text-uppercase">Khám phá hội nhóm của bạn</h3>
                    <p className="color-gray">Hàng ngàn ưu đãi</p>
                  </div>
                  <div className="d-flex d-flex-column align-items-center">
                    <ul className="title-sub">
                      <li><i className="fas fa-check"/> Ưu đãi giảm giá</li>
                      <li><i className="fas fa-check"/> Giao hàng miễn phí</li>
                      <li><i className="fas fa-check"/> Chia sẻ bạn bè</li>
                    </ul>
                  </div>
                  <div className="actions">
                    <button className="btn action-bg-yellow">
                      <Link to={`/groups/explore`}>Tham gia ngay</Link>
                    </button>
                    <button className="btn action-bg-blue" onClick={handleClose}>Tạo hội nhóm</button>
                  </div>
                </div>
              </div>
            </Fragment>


        }
        {
          !isMobile &&
          <div className="page-content">
            <div className="groups-section">
              <div className="group-banner">
                <div className="group-banner-caption">
                  <div className="content">
                    <p className="font-weight">Khám phá</p>
                    <p className="font-weight font-size-xl">hội nhóm của bạn</p>
                    <p className="font-size-small color-gray">Hàng ngàn ưu đãi</p>
                  </div>
                </div>
                <div className="group-banner-img">
                  <img src={publicUrl(`/assets/images/group-object.svg`)} alt="object"/>
                </div>
              </div>
            </div>
            <div className="intro">
              <div className="slogan-wrap">
                <div className="slogan">
                  <h3>Phát triển kinh doanh với</h3>
                  <h2>Outlet Hội Nhóm</h2>
                </div>
                <img src={publicUrl('/assets/images/icons/icon-store.svg')} alt="Store"/>
              </div>
            </div>
            <div className="indicators">
              <div className="indicator">
                <img src={publicUrl('/assets/images/icons/icon-team.svg')} alt="Users"/>
                <h3>10+ triệu</h3>
                <p>Khách hàng</p>
              </div>
              <div className="indicator">
                <img src={publicUrl('/assets/images/icons/icon-shop.svg')} alt="Shops"/>
                <h3>100k+</h3>
                <p>Người bán</p>
              </div>
              <div className="indicator">
                <img src={publicUrl('/assets/images/icons/icon-truck.svg')} alt="Shipment"/>
                <h3>Giao hàng 64</h3>
                <p>Tỉnh thành</p>
              </div>
              <div className="indicator">
                <img src={publicUrl('/assets/images/icons/icon-maintenance.svg')} alt="Categories"/>
                <h3>1000+</h3>
                <p>Ngành hàng</p>
              </div>
            </div>
            <div className="btn-wrap">
              <button className="btn btn-primary" onClick={handleClose}>Đăng ký</button>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default GroupLandingPage;
