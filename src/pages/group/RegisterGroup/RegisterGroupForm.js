import React, {Fragment} from 'react';
import './RegisterGroupForm.scss';
import {showAlert} from '../../../common/helpers';
import {registerGroupAPI} from '../../../api/groups';
import {OBJECT_TYPES} from '../../../common/constants/objectTypes';
import {uploadMediaAPI} from '../../../api/media';
import {isMobile} from "../../../common/helpers/browser";
import IconBackMobile from "../../../components/HeaderMobile/IconBackMobile/IconBackMobile";
import TitleMobile from "../../../components/HeaderMobile/TitleMobile/TitleMobile";
import HeaderMobile from "../../../components/HeaderMobile/HeaderMobile";
import Step1 from './Steps/Step1';
import Step3 from './Steps/Step3';

class RegisterGroupForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: '',
        description: '',
        privacy: 10, // 10: public, 20: private
        service_fee: 20,
        manage_fee: 30,
        reseller_commission: 0,
        customer_commission: 50,
        affiliate_commission: 0,
        affiliate_commission_detail: [70, 30],
        reseller_commission_detail: [100],
        use_eplaza_service: true,
        agree_condition: true,
        questions_list: [], //optional
        verify_type: 10, //(10: SDT, 20: Email)
        policy: '',
        media: [],
      },
      files: {
        avatar_file: null,
        cover_file: null,
        contract_file: null,
        authority_file: null
      },
      isLoading: false,
      isSuccess: false,
      activeStep: 0,
    };
  }

  setData = (data) => {
    this.setState(prevState => ({
      formData: {...prevState.formData, ...data}
    }));
  };

  handleFileChange = (name) => (event) => {
    const files = event.target.files;
    if (files.length > 0) {
      const file = files[0];
      this.setState(prevState => {
        return {
          ...prevState,
          files: {
            ...prevState.files,
            [name]: file
          }
        }
      });
    }
  };

  handleSave = async () => {
    const {formData: {media, ...postData}, files} = this.state;
    try {
      const {data: {id: groupId}} = await registerGroupAPI(postData);
      if (files.avatar_file) {
        const data = {
          object_type: OBJECT_TYPES.Group,
          object_id: groupId,
          type: 'avatar'
        };
        await uploadMediaAPI(files.avatar_file, data);
      }
      let errorCount = 0;
      const newMedia = media || [];
      if (newMedia.length) {
        for (let i = 0; i < newMedia.length; i++) {
          try {
            await uploadMediaAPI(newMedia[i], {
              object_type: OBJECT_TYPES.Group,
              object_id: groupId,
              type: newMedia[i].use_type,
            });
          } catch (error) {
            errorCount++;
          }
        }
      }
      this.setState({
        isLoading: false,
        isSuccess: true,
      });
      showAlert({
        type: 'success',
        message: `Đăng ký thành công, chờ phê duyệt!`
      });
    } catch (error) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
      this.setState({
        isLoading: false,
        isSuccess: false
      });
    }
  };

  handleGoNextStep = (event) => {
    event.preventDefault();
    this.handleSave();
  };

  handleGoPrevStep = (event) => {
    event.preventDefault();
    const {activeStep} = this.state;
    if (activeStep > 0) {
      this.setState({
        activeStep: activeStep - 1
      });
    }
  };

  render() {
    const {handleOpenLandingPage} = this.props;
    const {formData, files, isLoading, isSuccess, activeStep} = this.state;
    return (
      <div className="register-group-form group-page-content paper">
        {
          isMobile &&
          <HeaderMobile>
            <IconBackMobile/>
            <TitleMobile className="pr-30">Tạo hội nhóm</TitleMobile>
          </HeaderMobile>
        }
        {
          !isMobile &&
          <div className="page-title">
            <h1>Tạo hội nhóm</h1>
          </div>
        }
        <div className="page-content">
          {
            isSuccess &&
            <Fragment>
              <div className="alert alert-success" role="alert">
                <h4 className="alert-heading">Đã gửi đơn đăng ký!</h4>
                <p>Bạn đã gửi đơn đăng ký thành công, vui lòng chờ Outlet phê duyệt!</p>
              </div>
              <Step3/>
            </Fragment>
          }
          {
            !isSuccess &&
            <Step1
              activeStep={activeStep}
              formData={formData}
              files={files}
              setData={this.setData}
              isLoading={isLoading}
              handleFileChange={this.handleFileChange}
              handleCancel={handleOpenLandingPage}
              handleGoPrevStep={this.handleGoPrevStep}
              handleGoNextStep={this.handleGoNextStep}
              handleSave={this.handleSave}
            />
          }
        </div>
      </div>
    )
  }
}

export default RegisterGroupForm;
