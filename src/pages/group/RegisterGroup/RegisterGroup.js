import React from 'react';
import GroupLandingPage from './GroupLandingPage';
import RegisterGroupForm from './RegisterGroupForm';

class RegisterGroup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showLandingPage: sessionStorage.getItem('showGroupLandingPage') !== '1'
    };
  }

  handleOpenLandingPage = () => {
    sessionStorage.setItem('showGroupLandingPage', '0');
    this.setState({
      showLandingPage: true
    });
  };

  handleCloseLandingPage = () => {
    sessionStorage.setItem('showGroupLandingPage', '1');
    this.setState({
      showLandingPage: false
    });
  };

  render() {
    const {showLandingPage} = this.state;
    if (showLandingPage) {
      return <GroupLandingPage handleClose={this.handleCloseLandingPage}/>
    }
    return <RegisterGroupForm handleOpenLandingPage={this.handleOpenLandingPage}/>
  }
}

export default RegisterGroup;