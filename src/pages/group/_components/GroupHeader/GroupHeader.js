import React from 'react';
import {imageUrl, publicUrl} from '../../../../common/helpers';
import './GroupHeader.scss';
import ESwiper from '../../../../components/ESwiper/ESwiper';

const defaultSliders = [
  {
    url: publicUrl('/assets/images/samples/cover.jpg'),
    title: 'Cover'
  }
];

class GroupHeader extends React.PureComponent {
  render() {
    const {group} = this.props;
    const sliders = group?.covers?.length ? group.covers : defaultSliders;
    const avatar = group.avatar ? imageUrl(group.avatar.url) : publicUrl('/assets/images/samples/user_avatar_1.jpg');
    return (
      <div className="group-header">
        <ESwiper sliders={sliders} className="__slider"/>
        <div className="group-avatar"
             style={{backgroundImage: `url(${avatar})`}}/>
        <div className="group-name">{group.name}</div>
      </div>
    );
  }
}

export default GroupHeader;
