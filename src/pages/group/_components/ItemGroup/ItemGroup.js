import React from "react";
import {Link} from "react-router-dom";
import {imageUrl, publicUrl} from "../../../../common/helpers";
import "./ItemGroup.scss"

class ItemGroup extends React.PureComponent {
  render() {
    const {group, updateBtn, memberCount} = this.props;
    const groupAvatar = group.avatar ? imageUrl(group.avatar.url) : publicUrl('/assets/images/no-image/256x256.png');
    let groupUrl = `/groups/${group.id}/`;
    return(
        <div className="item-group">
          <div className="group-item-wrapper">
            <Link to={groupUrl}>
              <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}/>
            </Link>

            <div className="group-info">
              <Link to={groupUrl}>
                <div className="group-name"><span>{group.name}</span></div>
              </Link>
              <div className="group-members">{memberCount !== undefined ? `${memberCount} thành viên` : ''}</div>
              <div className="group-created">Đã tham gia vào 2 tháng trước</div>
              {
                updateBtn &&
                    <Link to={groupUrl} className="update-btn">Cập nhật</Link>
              }
            </div>
          </div>
        </div>
    )
  }
}

export default ItemGroup;