import React from 'react';

function Restricted({onRestrict}) {
  React.useEffect(() => {
    if (onRestrict) {
      onRestrict();
    }
  }, []);
  return null;
}

function PermissionChecker({
  userPermissions = [],
  requiredPermissions = [],
  children = null,
  onRestrict = null,
  isOwner = false
}) {
  if (isOwner) {
    return children;
  }
  if (!userPermissions || !userPermissions.length) {
    return <Restricted onRestrict={onRestrict}/>;
  }
  let hasPermission = false;
  requiredPermissions.forEach(permissionId => {
    if (userPermissions.indexOf(permissionId) >= 0) {
      hasPermission = true;
    }
  });
  if (!hasPermission) {
    return <Restricted onRestrict={onRestrict}/>;
  }
  return children;
}

export default PermissionChecker;
