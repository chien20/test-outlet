import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import './Sidebar.scss';
import Adv from '../../../../components/Adv/Adv';

class Sidebar extends React.PureComponent {
  render() {
    const {user} = this.props;
    return (
      <Fragment>
        <div className="widget group-sidebar">
          <h3 className="widget-title">Hội nhóm</h3>
          <ul className="nav-left">
            {/*<li>*/}
            {/*<Link to="/groups/">Bảng tin</Link>*/}
            {/*</li>*/}
            {
              user.auth.isAuthenticated &&
              <li>
                <Link to="/groups">Hội nhóm của tôi</Link>
              </li>
            }
            <li>
              <Link to="/groups/explore">Khám phá nhóm mới</Link>
            </li>
            {
              user.auth.isAuthenticated &&
              <li>
                <Link to="/groups/register/">Tạo nhóm mới</Link>
              </li>
            }
            <li>
              <Link to="/groups/faq">Hướng dẫn sử dụng</Link>
            </li>
          </ul>
        </div>
        <Adv position="left-sidebar"/>
        {/*{*/}
        {/*user.auth.isAuthenticated &&*/}
        {/*<div className="widget group-sidebar">*/}
        {/*<h3 className="widget-title">Nhóm yêu thích</h3>*/}
        {/*<ul className="nav-left">*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*<li>*/}
        {/*<Link to="/g/">Tập thể lớp 9A</Link>*/}
        {/*</li>*/}
        {/*</ul>*/}
        {/*</div>*/}
        {/*}*/}
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(Sidebar);
