import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Modal} from 'react-bootstrap';
import Broadcaster from '../../../../common/helpers/broadcaster';
import BROADCAST_EVENT from '../../../../common/constants/broadcastEvents';
import {getProductListAPI} from '../../../../api/products';
import {showAlert} from '../../../../common/helpers';
import Pagination from '../../../../components/Pagination/Pagination';
import ProductTable from './ProductTable';
import {shareProductToGroupAPI} from '../../../../api/groups';
import './ShareProductsModal.scss';

const PAGE_SIZE = 5;

class ShareProductsModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isLoading: false,
      selectedIds: [],
      productList: [],
      productPrices: {},
      totalItems: 0,
      page: 0
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
    this.unmounted = false;
  }

  componentDidMount() {
    Broadcaster.on(BROADCAST_EVENT.SHARE_PRODUCT_TO_GROUP, this.handleOpen);
  }

  componentWillUnmount() {
    this.unmounted = true;
    Broadcaster.off(BROADCAST_EVENT.SHARE_PRODUCT_TO_GROUP, this.handleOpen);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.isOpen !== this.state.isOpen && this.state.isOpen) {
      this.getListProduct();
    }
  }

  getListProduct() {
    const {user} = this.props;
    const {keyword, page} = this.state;
    if (!user.store_id) {
      return;
    }
    const params = {
      store_id: user.store_id,
      current_page: page,
      page_size: PAGE_SIZE,
      q: keyword || ''
    };
    this.setState({
      isLoading: true
    });
    getProductListAPI(params, {__auth: true}).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(() => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách sản phẩm!`
      });
    });
  }

  handleOpen = (data = {}) => {
    if (this.unmounted) {
      return;
    }
    const s = JSON.parse(JSON.stringify(this.defaultState));
    this.setState({
      s,
      isOpen: true,
      ...data
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  onPageChanged = (page) => {
    this.setState({
      page: page - 1
    }, this.getListProduct);
  };

  handleSelectAll = (checked) => {
    this.setState(prevState => {
      const {productList} = prevState;
      const selectedIds = [...prevState.selectedIds];
      if (checked) {
        productList.forEach(product => {
          if (selectedIds.indexOf(product.id) < 0) {
            selectedIds.push(product.id);
          }
        });
      } else {
        productList.forEach(product => {
          const index = selectedIds.indexOf(product.id);
          if (index >= 0) {
            selectedIds.splice(index, 1);
          }
        });
      }
      return {
        selectedIds
      };
    });
  };

  handleSelect = (productId, isCheckAll = false) => (event) => {
    const checked = event.target.checked;
    if (isCheckAll) {
      return this.handleSelectAll(checked);
    }
    this.setState(prevState => {
      const selectedIds = [...prevState.selectedIds];
      if (checked) {
        if (selectedIds.indexOf(productId) < 0) {
          selectedIds.push(productId);
        } else {
          return null;
        }
      } else {
        if (selectedIds.indexOf(productId) >= 0) {
          const index = selectedIds.indexOf(productId);
          selectedIds.splice(index, 1);
        } else {
          return null;
        }
      }
      return {
        selectedIds
      };
    });
  };

  handlePriceChange = (productId, optionId = null) => (event) => {
    let value = event.target.value;
    try {
      value = parseInt(value);
      if (Number.isNaN(value)) {
        value = 0;
      }
    } catch (e) {
      value = 0;
    }
    this.setState(prevState => {
      const productPrices = {...prevState.productPrices};
      if (optionId) {
        if (!productPrices[productId]) {
          productPrices[productId] = {};
        }
        productPrices[productId][optionId] = value;
      } else {
        productPrices[productId] = value;
      }
      return {
        productPrices
      };
    });
  };

  handleSubmit = async () => {
    const {groupId} = this.props;
    const {selectedIds, productPrices} = this.state;
    const postData = [];
    selectedIds.forEach(productId => {
      const item = {
        product_id: productId,
        price: 0,
        product_option_ids: [],
        price_options: []
      };
      if (productPrices[productId]) {
        if (typeof productPrices[productId] === 'object') {
          Object.keys(productPrices[productId]).forEach(optionId => {
            item.product_option_ids.push(parseInt(optionId));
            item.price_options.push(productPrices[productId][optionId] || 0);
            delete item.price;
          });
        } else {
          item.price = productPrices[productId];
          delete item.product_option_ids;
          delete item.price_options;
        }
      }
      postData.push(item);
    });
    shareProductToGroupAPI(groupId, postData).then(() => {
      showAlert({
        type: 'success',
        message: 'Đã đăng, chờ phê duyệt!'
      });
      this.handleClose();
    }).catch(error => {
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`
      });
    });
  };

  render() {
    const {isOpen, isLoading, productList, totalItems, selectedIds, page, productPrices} = this.state;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <Modal size="lg"
             backdrop="static"
             show={isOpen}
             onHide={this.handleClose}
             className="share-products-modal"
             centered>
        <Modal.Header closeButton>
          <Modal.Title>Đăng sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            !productList.length && !isLoading &&
            <p>Bạn chưa có sản phẩm nào!&nbsp;
              <Link to="/user/store/products/upload">Click vào đây</Link> để upload sản phẩm mới!</p>
          }
          {
            !!productList.length &&
            <Fragment>
              <p>
                Vui lòng chọn sản phẩm và nhập giá chia sẻ vào hội nhóm.
              </p>
              <ProductTable
                productList={productList}
                productPrices={productPrices}
                selectedIds={selectedIds}
                handlePriceChange={this.handlePriceChange}
                handleSelect={this.handleSelect}/>
              <div className="bottom">
                <div className="item-pagination hidden-mobile">
                  {
                    totalPage > 1 &&
                    <Pagination
                      totalPages={totalPage}
                      currentPage={page + 1}
                      onPageChanged={this.onPageChanged}/>
                  }
                </div>
              </div>
            </Fragment>
          }
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-default"
            onClick={this.handleClose}
            disabled={isLoading}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}
            disabled={selectedIds.length === 0}>Xong
          </button>
        </Modal.Footer>
      </Modal>
    )
  }
}

ShareProductsModal.propTypes = {
  groupId: PropTypes.any.isRequired
};

const mapStateToProps = (state) => ({
  user: state.user.info
});

export default connect(mapStateToProps)(ShareProductsModal);
