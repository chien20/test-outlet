import React from 'react';
import {numberAsCurrentcy} from '../../../../common/helpers/format';
import './ProductTable.scss';

const ProductTableRow = ({product, productPrices, handlePriceChange, handleSelect = null, isSelected = false}) => {
  if (!product) {
    return null;
  }
  const options = JSON.parse(JSON.stringify(product.options || []));
  const productImageUrl = product.avatar_thumbnail_url;
  const productImage = productImageUrl ? <img src={productImageUrl} alt={product.name}/> : '';
  if (options.length === 0) {
    return (
      <tr>
        {
          handleSelect &&
          <td>
            <input type="checkbox" checked={isSelected} onChange={handleSelect(product.id)}/>
          </td>
        }
        <td className="text-left">
          {productImage} {product.name}
        </td>
        <td>-</td>
        <td>{product.sku || ''}</td>
        <td>{numberAsCurrentcy(product.price || 0)}đ</td>
        <td>
          {
            isSelected &&
            <input type="text" value={productPrices[product.id] || 0} onChange={handlePriceChange(product.id)}/>
          }
        </td>
        <td>{product.qty || 0}</td>
      </tr>
    )
  }
  if (options && options.length && product.option_groups && product.option_groups.length) {
    options.forEach(item => {
      if (item.groups_index && item.groups_index.length) {
        if (item.groups_index.length === 1 && product.option_groups[0].values) {
          item.name = product.option_groups[0].values[item.groups_index[0]];
        } else if (item.groups_index.length === 2
          && product.option_groups[0].values
          && product.option_groups[1].values) {
          item.name = `${product.option_groups[0].values[item.groups_index[0]]} - ${product.option_groups[1].values[item.groups_index[1]]}`;
        }
      }
    });
  }
  const rows = [];
  const count = options.length;
  const firstOption = options.shift();
  let i = 0;
  rows.push(
    <tr key={i}>
      {
        handleSelect &&
        <td rowSpan={count}>
          <input type="checkbox" checked={isSelected} onChange={handleSelect(product.id)}/>
        </td>
      }
      <td className="text-left" rowSpan={count}>
        {productImage} {product.name}
      </td>
      <td>{firstOption.name}</td>
      <td>{firstOption.sku || ''}</td>
      <td>{numberAsCurrentcy(firstOption.price || 0)}đ</td>
      <td>
        {
          isSelected &&
          <input type="text"
                 value={productPrices[product.id] && productPrices[product.id][firstOption.id] ? productPrices[product.id][firstOption.id] : 0}
                 onChange={handlePriceChange(product.id, firstOption.id)}/>
        }
      </td>
      <td>{firstOption.qty || 0}</td>
    </tr>
  );
  i++;
  options.forEach(item => {
    rows.push(
      <tr key={i}>
        <td>{item.name}</td>
        <td>{item.sku || ''}</td>
        <td>{numberAsCurrentcy(item.price || 0)}đ</td>
        <td>
          {
            isSelected &&
            <input type="text"
                   value={productPrices[product.id] && productPrices[product.id][item.id] ? productPrices[product.id][item.id] : 0}
                   onChange={handlePriceChange(product.id, item.id)}/>
          }
        </td>
        <td>{item.qty || 0}</td>
      </tr>
    );
    i++;
  });
  return rows;
};

const ProductTable = ({productList, productPrices, handlePriceChange, handleSelect = null, selectedIds = []}) => {
  let isCheckAll = false;
  if (handleSelect) {
    const ids = {};
    selectedIds.forEach(id => {
      ids[id] = true;
    });
    let hasUnselect = false;
    productList.forEach(product => {
      if (!ids[product.id]) {
        hasUnselect = true;
      }
    });
    isCheckAll = !hasUnselect;
  }
  return (
    <div className="product-table share-products-table table-responsive">
      <table className="table table-bordered">
        <thead>
        <tr>
          {
            handleSelect &&
            <th>
              <input type="checkbox" checked={isCheckAll} onChange={handleSelect(null, true)}/>
            </th>
          }
          <th>Tên sản phẩm</th>
          <th>Phân loại hàng</th>
          <th>SKU</th>
          <th>Giá</th>
          <th><div className="share-price-col">Giá chia sẻ</div></th>
          <th>Số lượng</th>
        </tr>
        </thead>
        <tbody>
        {
          productList.map((item, index) => (
            <ProductTableRow
              key={index}
              product={item}
              productPrices={productPrices}
              handlePriceChange={handlePriceChange}
              handleSelect={handleSelect}
              isSelected={selectedIds.indexOf(item.id) >= 0}/>
          ))
        }
        </tbody>
      </table>
    </div>
  )
};

export default ProductTable;