import React from 'react';
import PropTypes from 'prop-types';
import HeaderMobile from '../../../../components/HeaderMobile/HeaderMobile';
import IconBackMobile from '../../../../components/HeaderMobile/IconBackMobile/IconBackMobile';
import TitleMobile from '../../../../components/HeaderMobile/TitleMobile/TitleMobile';
import {Tab, Tabs} from 'react-bootstrap';
import './GroupHomeMobile.scss';
import GroupItem from '../../../../components/Group/GroupItem/GroupItem';
import {Link} from "react-router-dom";
import {imageUrl, publicUrl} from "../../../../common/helpers";
import IconAddMobile from "../../../../components/HeaderMobile/IconAddMobile/IconAddMobile";

class GroupHomeMobile extends React.PureComponent {
  render() {
    const {groups, suggestGroups, myGroups, myJoinedGroups, groupMemberCount} = this.props;
    return (
      <div className="group-home-mobile">
        <HeaderMobile>
          <IconBackMobile/>
          <TitleMobile className="pr-30">Hội nhóm</TitleMobile>
          <IconAddMobile linkTo={`/groups/register/`}/>
        </HeaderMobile>
        <Tabs defaultActiveKey="all">
          <Tab eventKey="all" title="Tất Cả">
            {
              !!groups.length &&
              <div className="group-list">
                {
                  groups.map((item, index) => (
                    <GroupItem
                      group={item}
                      showCreatedDate={true}
                      showMemberCount={true}
                      memberCount={groupMemberCount[item.id]}
                      key={index}
                      className="horizontal"
                    />
                  ))
                }
                <Link className="group-item horizontal" to={`/groups/register/`}>
                  <div className="group-item-wrapper add-new">
                    <div className="group-avatar">
                      <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                    </div>
                    <div className="group-info">
                      <p>Tạo hội nhóm mới</p>
                    </div>
                  </div>
                </Link>
              </div>
            }
          </Tab>
          <Tab eventKey="joined" title="Đã Tham Gia">
            {
              !!myJoinedGroups.length &&
              <div className="group-list">
                {
                  myJoinedGroups.map((item, index) => (
                    <GroupItem
                      group={item}
                      showCreatedDate={true}
                      showMemberCount={true}
                      memberCount={groupMemberCount[item.id]}
                      key={index}
                      className="horizontal"
                    />
                  ))
                }
                <Link className="group-item horizontal" to={`/groups/register/`}>
                  <div className="group-item-wrapper add-new">
                    <div className="group-avatar">
                      <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                    </div>
                    <div className="group-info">
                      <p>Tạo hội nhóm mới</p>
                    </div>
                  </div>
                </Link>
              </div>
            }
          </Tab>
          <Tab eventKey="owner" title="Của Tôi">
            {
              !!myGroups.length &&
              <div className="group-list">
                {
                  myGroups.map((item, index) => (
                    <GroupItem
                      group={item}
                      showCreatedDate={true}
                      showMemberCount={true}
                      memberCount={groupMemberCount[item.id]}
                      key={index}
                      className="horizontal"
                    />
                  ))
                }
                <Link className="group-item horizontal" to={`/groups/register/`}>
                  <div className="group-item-wrapper add-new">
                    <div className="group-avatar">
                      <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                    </div>
                    <div className="group-info">
                      <p>Tạo hội nhóm mới</p>
                    </div>
                  </div>
                </Link>
              </div>
            }
          </Tab>
        </Tabs>
        <div className="suggest-groups">
          <div className="section-heading">
            <h3 className="text-uppercase">Hội nhóm hoạt động nổi bật</h3>
          </div>
          <div className="section-inner">
            {
              suggestGroups.length &&
              <div className="row group-list">
                {
                  suggestGroups.map((item, index) => {
                    const groupAvatar = item.avatar ? imageUrl(item.avatar.url) : publicUrl('/assets/images/no-image/256x256.png')
                    return (
                      <Link to={`groups/${item.id}`} className="group-item" key={index}>
                        <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}>
                          <div className="group-info">
                            <div className="group-name">{item.name}</div>
                            <div className="group-members">{groupMemberCount[item.id] !== undefined ? `${groupMemberCount[item.id]} thành viên` : ''}</div>
                          </div>
                        </div>
                        <div className="join-group">
                          <button className="btn">Tham gia</button>
                        </div>
                      </Link>
                    )
                  })
                }
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

GroupHomeMobile.propTypes = {
  groups: PropTypes.array,
  myGroups: PropTypes.array,
  myJoinedGroups: PropTypes.array,
};

export default GroupHomeMobile;
