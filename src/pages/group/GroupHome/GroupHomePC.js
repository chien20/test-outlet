import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import ItemGroup from '../_components/ItemGroup/ItemGroup';

import {publicUrl} from "../../../common/helpers";
import "./GroupHomePC.scss"
import {Tab, Tabs} from "react-bootstrap";
import {Link} from "react-router-dom";
import GroupItem from "../../../components/Group/GroupItem/GroupItem";


class GroupHomePc extends React.PureComponent {
  render() {
    const {groups, suggestGroups, myGroups, myJoinedGroups, groupMemberCount} = this.props;
    return (
      <div className="group-home-pc">
        <div className="groups-section">
          <div className="group-banner">
            <div className="group-banner-caption">
              <div className="content">
                <p className="font-weight">Khám phá</p>
                <p className="font-weight font-size-xl">hội nhóm của bạn</p>
                <p className="font-size-small color-gray">Hàng ngàn ưu đãi</p>
              </div>
            </div>
            <div className="group-banner-img">
              <img src={publicUrl(`/assets/images/group-object.svg`)} alt="object"/>
            </div>
          </div>
        </div>
        <div className="groups-section">
          {
            !groups.length &&
            <Fragment>
              <div className="start">
                <div className="content">
                  <h3>Bạn chưa có hội nhóm nào. Hãy gia nhập hoặc tạo một hội nhóm mới...</h3>
                  <div className="actions">
                    <button className="btn action-bg-yellow m-r-20">
                      <Link to={`/groups/explore/`}>Tham gia ngay</Link>
                    </button>
                    <button className="btn action-bg-blue">
                      <Link to={`/groups/register`}>Tạo hội nhóm </Link>
                    </button>
                  </div>
                </div>
              </div>
            </Fragment>
          }
          {
            !!groups.length &&
            <Tabs className="e-tabs group-tabs" defaultActiveKey="groups-all">
              <Tab eventKey="groups-all" title="Tất cả">
                {
                  !!groups.length &&
                  <div className="group-list">
                    {
                      groups.map((item, index) => (
                        <ItemGroup
                          group={item}
                          key={index}
                          memberCount={groupMemberCount[item.id]}
                        />
                      ))
                    }
                    <div className="item-group add-new-group">
                      <div className="group-item-wrapper">
                        <div className="group-avatar">
                          <Link to={`/groups/register/`}>
                            <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                          </Link>
                        </div>
                        <div className="group-info">
                          <Link to={`/groups/register/`}>
                            <div className="group-name">Tạo hội nhóm mới</div>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                }
              </Tab>
              <Tab eventKey="groups-joined" title="Đã tham gia">
                {
                  !!myJoinedGroups.length &&
                  <div className="group-list">
                    {
                      myJoinedGroups.map((item, index) => (
                        <ItemGroup
                          group={item}
                          key={index}
                          memberCount={groupMemberCount[item.id]}
                        />
                      ))
                    }
                    <div className="item-group add-new-group">
                      <div className="group-item-wrapper">
                        <div className="group-avatar">
                          <Link to={`/groups/register/`}>
                            <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                          </Link>
                        </div>
                        <div className="group-info">
                          <Link to={`/groups/register/`}>
                            <div className="group-name">Tạo hội nhóm mới</div>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                }
              </Tab>
              <Tab eventKey="my-groups" title="Của tôi">
                {
                  !!myGroups.length &&
                  <div className="group-list">
                    {
                      myGroups.map((item, index) => (
                        <ItemGroup
                          group={item}
                          key={index}
                          updateBtn={true}
                          memberCount={groupMemberCount[item.id]}
                        />
                      ))
                    }
                    <div className="item-group add-new-group">
                      <div className="group-item-wrapper">
                        <div className="group-avatar">
                          <Link to={`/groups/register/`}>
                            <img src={publicUrl(`/assets/images/icons/add-group.svg`)} alt="them-moi"/>
                          </Link>
                        </div>
                        <div className="group-info">
                          <Link to={`/groups/register/`}>
                            <div className="group-name">Tạo hội nhóm mới</div>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                }
              </Tab>
            </Tabs>

          }
        </div>
        <div className="groups-section">
          <div className="section-heading">
            <h3>Hội nhóm hoạt động nổi bật</h3>
            <Link className="link-read-more" to={'/groups/explore/'}>Xem thêm</Link>
          </div>
          <div className="section-inner">
            {
              suggestGroups.length &&
              <div className="group-list">
                {
                  suggestGroups.map((item, index) => (
                    <GroupItem
                      group={item}
                      key={index}
                      showMemberCount={true}
                      memberCount={groupMemberCount[item.id]}
                    />
                  ))
                }
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}

GroupHomePc.propTypes = {
  groups: PropTypes.array,
  suggestGroups: PropTypes.array,
  myGroups: PropTypes.array,
  myJoinedGroups: PropTypes.array,
};

export default GroupHomePc;
