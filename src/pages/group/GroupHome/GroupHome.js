import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {getGroupMemberCountAPI, getListGroupAPI, getSuggestGroupsAPI} from '../../../api/groups';
import {showAlert} from '../../../common/helpers';
import {Redirect} from 'react-router-dom';
import {isMobile} from '../../../common/helpers/browser';
import GroupHomePc from './GroupHomePC';
import GroupHomeMobile from './GroupHomeMobile/GroupHomeMobile';


class GroupHome extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      groups: [],
      myJoinedGroups: [],
      myGroups: [],
      page: 0,
      totalItems: 0,
      isLoading: true,
      suggestGroups: [],
      groupMemberCount: {},
    };
  }

  componentDidMount() {
    this.getListGroups();
    this.getSuggestGroups().catch(error => {
      // TODO
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.page !== this.state.page) {
      this.getListGroups();
      this.getSuggestGroups().catch(error => {
        // TODO
      });
    }
  }

  getListGroups = () => {
    const {user} = this.props;
    if (!user.auth.isAuthenticated) {
      return;
    }
    const {keyword, page} = this.state;
    const params = {
      current_page: page,
      page_size: 100,
      q: keyword || '',
    };
    this.setState({
      isLoading: true,
    });
    getListGroupAPI(params).then(res => {
      const data = res.data && res.data.list ? res.data.list : [];
      const myGroups = data.filter(item => item.owner_id === user.info.id);
      const myJoinedGroups = data.filter(item => item.owner_id !== user.info.id);
      this.setState({
        isLoading: false,
        groups: data,
        myGroups,
        myJoinedGroups,
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0,
      });
      this.getGroupMemberCount(data);
    }).catch(() => {
      this.setState({
        isLoading: false,
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách hội nhóm!`,
      });
    });
  };

  getGroupMemberCount = (groups) => {
    const ids = groups.map(group => group.id);
    if (!ids.length) {
      return;
    }
    getGroupMemberCountAPI(ids).then(res => {
      this.setState(prevState => ({
        groupMemberCount: {
          ...prevState.groupMemberCount,
          ...res.data,
        },
      }));
    }).catch(error => {
      console.log(error);
      showAlert({
        type: 'error',
        message: `Không tải được thành viên hội nhóm!`,
      });
    });
  };

  getSuggestGroups = async () => {
    this.setState({
      isLoading: true,
    });
    const {data: {list: suggestGroups}} = await getSuggestGroupsAPI({
      current_page: 0,
      page_size: 8,
    });
    this.getGroupMemberCount(suggestGroups);
    this.setState({
      suggestGroups,
      isLoading: false,
    });
  }

  onPageChanged = (page) => {
    this.setState({
      page,
    });
  };

  render() {
    const {groups, suggestGroups, myJoinedGroups, myGroups, groupMemberCount} = this.state;
    const {user} = this.props;
    if (!user.auth.isAuthenticated) {
      return <Redirect to="/groups/explore/"/>;
    }
    return (
      <Fragment>
        <Helmet>
          <title>Hội nhóm</title>
        </Helmet>
        <div className="group-home group-page-content paper">
          {
            !isMobile &&
            <GroupHomePc
              groups={groups}
              suggestGroups={suggestGroups}
              myGroups={myGroups}
              myJoinedGroups={myJoinedGroups}
              groupMemberCount={groupMemberCount}
            />
          }
          {
            isMobile &&
            <GroupHomeMobile
              groups={groups}
              suggestGroups={suggestGroups}
              myGroups={myGroups}
              myJoinedGroups={myJoinedGroups}
              groupMemberCount={groupMemberCount}
            />
          }
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(GroupHome);
