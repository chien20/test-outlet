import {call, put, takeEvery} from 'redux-saga/effects'
import {GET_MENUS, GET_SETTINGS, getMenusSuccessAC, getSettingsSuccessAC, SET_LANGUAGE} from '../../actions/app/app';
import {getMenusAPI, getSettingsAPI} from '../../../api/utils';

function* setLanguageAsync() {
  yield put({type: 'SET_LANGUAGE_SUCCESS'});
}

function* getSettings() {
  try {
    const {data: {list}} = yield call(getSettingsAPI);
    yield put(getSettingsSuccessAC(list || []));
  } catch (e) {

  }
}

function* getMenus() {
  try {
    const {data: {list}} = yield call(getMenusAPI);
    yield put(getMenusSuccessAC(list || []));
  } catch (e) {

  }
}

export default function* appWatcher() {
  yield takeEvery(SET_LANGUAGE, setLanguageAsync);
  yield takeEvery(GET_SETTINGS, getSettings);
  yield takeEvery(GET_MENUS, getMenus);
}
