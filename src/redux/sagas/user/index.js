import authWatcher from './auth';
import userInfoWatcher from './info';
import notificationsWatcher from './notifications';

export default [
  authWatcher(),
  userInfoWatcher(),
  notificationsWatcher()
];
