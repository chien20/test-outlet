import {call, put, takeEvery} from 'redux-saga/effects'
import {getNotificationsAC, getNotificationsSuccessAC} from '../../actions/user/notifications';
import {getNotificationListAPI} from '../../../api/notifications';
import {showAlert} from '../../../common/helpers';
import {authenticationSuccessAC} from '../../actions/user';

function* getNotificationsFlow(action) {
  try {
    const res = yield call(getNotificationListAPI, {seen: false});
    if (res && res.data && res.data.list) {
      yield put(getNotificationsSuccessAC(res.data.list));
    }
  } catch (e) {
    if (action && action.silent === false) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  }
}

export default function* notificationsWatcher() {
  yield takeEvery(getNotificationsAC().type, getNotificationsFlow);
  yield takeEvery(authenticationSuccessAC().type, getNotificationsFlow);
}
