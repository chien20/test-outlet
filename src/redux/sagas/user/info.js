import {call, put, takeEvery} from 'redux-saga/effects'
import {showAlert} from '../../../common/helpers';
import {getCurrentUserAC, getCurrentUserFailedAC, getCurrentUserSuccessAC} from '../../actions/user/info';
import {getCurrentUserAPI} from '../../../api/users';
import {authenticationSuccessAC} from '../../actions/user';

function* getUserFlow(action) {
  try {
    const res = yield call(getCurrentUserAPI);
    if (res && res.data) {
      yield put(getCurrentUserSuccessAC(res.data));
      if (!action.isAuthenticated) {
        yield put(authenticationSuccessAC());
      }
    }
  } catch (e) {
    yield put(getCurrentUserFailedAC());
    if (action && action.silent === false) {
      showAlert({
        type: 'error',
        message: `Lỗi: ${e.message}`
      });
    }
  }
}

export default function* userInfoWatcher() {
  yield takeEvery(getCurrentUserAC().type, getUserFlow);
}
