import {put, takeEvery} from 'redux-saga/effects'
import {authenticationSuccessAC, LOGIN_SUCCESS, LOGOUT} from '../../actions/user';

function* loginSuccessFlow() {
  yield put(authenticationSuccessAC());
}

function logoutSuccessFlow() {
  // history.push('/');
}

export default function* authWatcher() {
  yield takeEvery(LOGIN_SUCCESS, loginSuccessFlow);
  yield takeEvery(LOGOUT, logoutSuccessFlow);
}
