import categoryWatcher from './category';
import usersWatcher from './users';
import bannersWatcher from './banners';

export default [
  categoryWatcher(),
  usersWatcher(),
  bannersWatcher()
];
