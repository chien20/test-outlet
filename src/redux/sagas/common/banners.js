import {call, put, takeEvery} from 'redux-saga/effects';
import {getBannersAC, getBannersSuccessAC} from '../../actions/common';
import {getBannersAPI} from '../../../api/banners';
import {getObjectsMediaAPI} from '../../../api/media';
import {OBJECT_TYPES} from '../../../common/constants/objectTypes';

function convertBanner(item) {
  switch (item.action?.type) {
    case 1:
      item.link = item.action?.params?.url || '#';
      item.target = item.action?.params?.target || '_self';
      break;
    case 2:
      item.link = `/c/c${item.action.params.category_id}`;
      break;
    case 3:
      item.link = '/products/flash-sale';
      break;
    default:
      break;
  }
  return item;
}

function* getListFlow() {
  try {
    const response = yield call(getBannersAPI, {page_size: 100});
    if (response && response.data) {
      const banners = response.data.list || [];
      const bannerIds = banners.map(item => item.id);
      if (bannerIds.length > 0) {
        const res = yield call(getObjectsMediaAPI, bannerIds, OBJECT_TYPES.Banner);
        if (res && res.data && res.data.list) {
          const map = {};
          res.data.list.forEach(item => {
            map[item.object_id] = item;
          });
          banners.forEach(item => {
            if (map[item.id]) {
              item.image = map[item.id];
            }
          });
        }
      }
      yield put(getBannersSuccessAC(banners.filter(item => item.image).map(convertBanner)));
    }
  } catch (error) {
    //TODO handle get category list error
  }
}

export default function* watcher() {
  yield takeEvery(getBannersAC().type, getListFlow);
}
