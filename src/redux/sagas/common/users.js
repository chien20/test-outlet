import {call, put, takeEvery} from 'redux-saga/effects'
import {getBasicUserInfoAC, getBasicUserInfoSuccessAC} from '../../actions/common';
import {getBasicUserInfoAPI} from '../../../api/users';

function* getBasicUserInfoFlow(action) {
  try {
    const response = yield call(getBasicUserInfoAPI, action.userId);
    if (response && response.data) {
      yield put(getBasicUserInfoSuccessAC(action.userId, response.data));
    }
  } catch (error) {
    //TODO handle get category list error
  }
}

export default function* watcher() {
  yield takeEvery(getBasicUserInfoAC().type, getBasicUserInfoFlow)
}
