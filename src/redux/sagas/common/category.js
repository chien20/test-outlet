import {call, put, takeEvery} from 'redux-saga/effects'
import {getCategoryListAC, getCategoryListSuccessAC} from '../../actions/common';
import {getCategoryListAPI} from '../../../api/products';

function* getCategoryListFlow() {
  try {
    const response = yield call(getCategoryListAPI);
    if (response && response.data) {
      yield put(getCategoryListSuccessAC(response.data.list || []));
    }
  } catch (error) {
    //TODO handle get category list error
  }
}

export default function* appWatcher() {
  yield takeEvery(getCategoryListAC().type, getCategoryListFlow)
}
