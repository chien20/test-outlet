import {all} from 'redux-saga/effects';
import appWatchers from './app';
import commonWatchers from './common';
import pagesWatchers from '../../pages/sagas';
import userWatchers from './user';

export default function* rootSaga() {
  yield all([
    ...appWatchers,
    ...commonWatchers,
    ...pagesWatchers,
    ...userWatchers
  ])
}
