import createReducer from '../../../common/utils/redux/createReducer';
import {CLOSE_MODAL, OPEN_MODAL} from '../../actions/modals/modals';

const initialState = {
  isOpen: false
};

const handlers = {
  [OPEN_MODAL]: (state, action) => {
    if (action.modalName !== 'register') {
      return state;
    }
    return {
      isOpen: true
    }
  },
  [CLOSE_MODAL]: (state, action) => {
    if (action.modalName !== 'register') {
      return state;
    }
    return {
      isOpen: false
    }
  }
};

export default createReducer(initialState, handlers);
