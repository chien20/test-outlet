import createReducer from '../../../common/utils/redux/createReducer';
import {CLOSE_MODAL, OPEN_MODAL} from '../../actions/modals/modals';

const initialState = {
  isOpen: false
};

const handlers = {
  [OPEN_MODAL]: (state, action) => {
    if (action.modalName !== 'login') {
      return state;
    }
    return {
      ...action.data,
      isOpen: true
    }
  },
  [CLOSE_MODAL]: (state, action) => {
    if (action.modalName !== 'login') {
      return state;
    }
    return {
      isOpen: false
    }
  }
};

export default createReducer(initialState, handlers);
