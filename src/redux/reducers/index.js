import app from './app';
import common from './common';
import user from './user';
import modals from './modals';
import pages from '../../pages/reducer';

export default {
  app,
  common,
  user,
  modals,
  pages
};
