import createReducer from '../../../common/utils/redux/createReducer';
import {AUTHENTICATION_SUCCESS, LOGIN_SUCCESS, LOGOUT, RENEW_ACCESS_TOKEN_SUCCESS} from '../../actions/user';
import {GET_CURRENT_USER_FAILED} from '../../actions/user/info';

const getFromStorage = () => {
  let data = {};
  if (localStorage.getItem('auth')) {
    try {
      data = JSON.parse(localStorage.getItem('auth'));
    } catch (e) {
      data = {};
    }
  }
  return data;
};

const saveToStorage = (data) => {
  const saveData = JSON.parse(JSON.stringify(data));
  delete saveData.isAuthenticated;
  localStorage.setItem('auth', JSON.stringify(saveData));
};

const initialState = {
  access_token: null,
  refresh_token: null,
  expires_time: 0,
  isAuthenticated: false,
  ...getFromStorage()
};

const handlers = {
  [RENEW_ACCESS_TOKEN_SUCCESS]: (state, action) => {
    const newState = {
      ...state,
      access_token: action.access_token,
      refresh_token: action.refresh_token
    };
    saveToStorage(newState);
    return newState;
  },
  [LOGIN_SUCCESS]: (state, action) => {
    const data = action.data;
    const newState = {
      access_token: data.access_token,
      refresh_token: data.refresh_token,
      expires_time: data.expires_time,
      isAuthenticated: true
    };
    saveToStorage(newState);
    return newState;
  },
  [AUTHENTICATION_SUCCESS]: (state) => {
    if (state.isAuthenticated) {
      return state;
    }
    return {
      ...state,
      isAuthenticated: true
    };
  },
  [GET_CURRENT_USER_FAILED]: (state) => {
    saveToStorage({});
    return {
      ...state,
      isAuthenticated: false,
      access_token: null,
      refresh_token: null,
      expires_time: 0,
    }
  },
  [LOGOUT]: (state, action) => {
    saveToStorage({});
    return {
      access_token: null,
      refresh_token: null,
      expires_time: 0,
      isAuthenticated: false
    }
  }
};

export default createReducer(initialState, handlers);

