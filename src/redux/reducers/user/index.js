import {combineReducers} from 'redux';
import auth from './auth';
import info from './info';
import notifications from './notifications';

export default combineReducers({
  auth,
  info,
  notifications
});
