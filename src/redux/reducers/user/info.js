import createReducer from '../../../common/utils/redux/createReducer';
import {LOGOUT} from '../../actions/user';
import {REGISTER_STORE_SUCCESS} from '../../../pages/user/store/_redux/actions';
import {GET_CURRENT_USER_SUCCESS} from '../../actions/user/info';

const getFromStorage = () => {
  let data = null;
  if (localStorage.getItem('user')) {
    try {
      data = JSON.parse(localStorage.getItem('user'));
    } catch (e) {
      data = null;
    }
  }
  return data;
};

const saveToStorage = (data) => {
  if (!data) {
    localStorage.removeItem('user');
    return;
  }
  localStorage.setItem('user', JSON.stringify(data));
};

const initialState = getFromStorage();

const handlers = {
  [GET_CURRENT_USER_SUCCESS]: (state, action) => {
    const newState = {
      ...state,
      ...action.data
    };
    saveToStorage(newState);
    return newState;
  },
  [LOGOUT]: () => {
    saveToStorage(null);
    return null;
  },
  [REGISTER_STORE_SUCCESS]: (state, action) => {
    const newState = {
      ...state,
      store_id: action.store_id
    };
    saveToStorage(newState);
    return newState;
  }
};

export default createReducer(initialState, handlers);
