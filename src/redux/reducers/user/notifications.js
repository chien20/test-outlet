import createReducer from '../../../common/utils/redux/createReducer';
import {LOGOUT} from '../../actions/user';
import {GET_NOTIFICATIONS_SUCCESS} from '../../actions/user/notifications';

const initialState = [];

const handlers = {
  [GET_NOTIFICATIONS_SUCCESS]: (state, action) => (
    action.notifications || []
  ),
  [LOGOUT]: () => (
    []
  )
};

export default createReducer(initialState, handlers);
