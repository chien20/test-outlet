import createReducer from '../../../common/utils/redux/createReducer';
import {GET_MENUS_SUCCESS, GET_SETTINGS_SUCCESS, SET_LANGUAGE} from '../../actions/app/app';

const initialState = {
  language: 'VI',
  settings: {},
  menus: {}
};

const handlers = {
  [SET_LANGUAGE]: (state, action) => {
    return {
      ...state,
      language: action.language
    };
  },
  [GET_SETTINGS_SUCCESS]: (state, action) => {
    const settings = {};
    action.settings.forEach(item => {
      settings[item.key] = `${item.value}`.replace(/(?:\r\n|\r|\n)/g, '<br>');
    });
    return {
      ...state,
      settings
    }
  },
  [GET_MENUS_SUCCESS]: (state, action) => {
    const menus = {};
    action.menus.forEach(item => {
      if (!menus[item.position]) {
        menus[item.position] = [];
      }
      menus[item.position].push(item);
    });
    return {
      ...state,
      menus
    }
  }
};

export default createReducer(initialState, handlers);
