import {combineReducers} from 'redux';
import category from './category';
import alert from './alert';
import users from './users';
import banners from './banners';

export default combineReducers({
  category,
  alert,
  users,
  banners
});
