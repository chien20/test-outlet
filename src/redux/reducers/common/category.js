import createReducer from '../../../common/utils/redux/createReducer';
import {GET_CATEGORY_LIST_SUCCESS} from '../../actions/common';

const initialState = {
  list: [],
  tree: []
};

const buildTreeRecursive = (parent, list, depth = 1) => {
  parent.children = [];
  if (!parent.parents) {
    parent.parents = [];
  }
  if (depth > 5) {
    return;
  }
  list.forEach(item => {
    if (item.parent_id === parent.id) {
      item.parents = [...parent.parents, parent.id];
      buildTreeRecursive(item, list, depth + 1);
      parent.children.push(item);
    }
  });
};

const getCategoryTree = (categoryList) => {
  const tree = [];
  if (!categoryList || !Array.isArray(categoryList)) {
    return tree;
  }
  const list = JSON.parse(JSON.stringify(categoryList));
  list.forEach(item => {
    if (!item.parent_id) {
      buildTreeRecursive(item, list);
      tree.push(item);
    }
  });
  return tree;
};

const buildCategoryMapRecursive = (node, map = {}) => {
  map[node.id] = node;
  if (!node.parents) {
    node.parents = [];
  }
  if (node.children) {
    node.children.forEach(child => {
      child.parents = [...node.parents, node.id];
      buildCategoryMapRecursive(child, map);
    });
  }
};

const getCategoryMap = (tree) => {
  const map = {};
  tree.forEach(item => {
    buildCategoryMapRecursive(item, map);
  });
  return map;
};

const handlers = {
  [GET_CATEGORY_LIST_SUCCESS]: (state, action) => {
    const tree = getCategoryTree(action.categoryList);
    return {
      ...state,
      list: action.categoryList,
      tree: tree,
      map: getCategoryMap(tree)
    }
  }
};

export default createReducer(initialState, handlers);
