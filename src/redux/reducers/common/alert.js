import createReducer from '../../../common/utils/redux/createReducer';
import {CLEAN_ALERTS, CLOSE_ALERT, SHOW_ALERT} from '../../actions/common';
import {uuidv4} from '../../../common/helpers';

const initialState = {
  list: []
};

const defaultAlertConfig = {
  id: '',
  message: '',
  container: '',
  html: '',
  dismissible: true,
  timeout: 5000,
  renewed_at: 0
};

const handlers = {
  [SHOW_ALERT]: (state, action) => {
    const alert = {
      ...defaultAlertConfig,
      ...action.data
    };
    if (!alert.id) {
      alert.id = uuidv4();
    }
    const index = state.list.findIndex(item => item.id === alert.id);
    if (index >= 0) {
      const list = [...state.list];
      list[index] = {...list[index], ...alert};
      if (list[index].timeout) {
        list[index].renewed_at = new Date().getTime();
      }
      return {
        ...state,
        list
      }
    }
    return {
      ...state,
      list: [...state.list, alert]
    }
  },
  [CLOSE_ALERT]: (state, action) => {
    const index = state.list.findIndex(item => item.id === action.id);
    if (index >= 0) {
      const list = [...state.list];
      list.splice(index, 1);
      return {
        ...state,
        list
      }
    }
    return state;
  },
  [CLEAN_ALERTS]: (state, action) => {
    const containers = action.containers || [];
    if (!Array.isArray(containers) || !containers.length) {
      return state;
    }
    const list = [...state.list];
    let count = 0;
    for (let i = list.length - 1; i >= 0; i--) {
      if (containers.indexOf(list[i].container) >= 0) {
        list.splice(i, 1);
        count++;
      }
    }
    if (count > 0) {
      return {
        ...state,
        list
      }
    }
    return state;
  }
};

export default createReducer(initialState, handlers);
