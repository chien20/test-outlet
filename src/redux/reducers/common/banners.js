import createReducer from '../../../common/utils/redux/createReducer';
import {GET_BANNERS_SUCCESS} from '../../actions/common';

const initialState = {
  list: [],
  byPositions: {}
};

const handlers = {
  [GET_BANNERS_SUCCESS]: (state, action) => {
    const positions = {};
    (action.data || []).forEach(item => {
      if (!positions[item.position]) {
        positions[item.position] = [];
      }
      positions[item.position].push(item);
    });
    return {
      ...state,
      list: action.data,
      byPositions: positions
    }
  }
};

export default createReducer(initialState, handlers);
