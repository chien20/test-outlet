import createReducer from '../../../common/utils/redux/createReducer';
import {GET_BASIC_USER_INFO, GET_BASIC_USER_INFO_SUCCESS} from '../../actions/common';

const initialState = {
  basicInfo: {}
};

const handlers = {
  [GET_BASIC_USER_INFO]: (state, action) => {
    return {
      ...state,
      basicInfo: {
        ...state.basicInfo,
        [action.userId]: {
          ...state.basicInfo[action.userId],
          isFetching: true
        }
      }
    }
  },
  [GET_BASIC_USER_INFO_SUCCESS]: (state, action) => {
    return {
      ...state,
      basicInfo: {
        ...state.basicInfo,
        [action.userId]: {
          ...state.basicInfo[action.userId],
          ...action.data,
          isFetching: false
        }
      }
    }
  }
};

export default createReducer(initialState, handlers);

