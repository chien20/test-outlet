export const GET_BANNERS = 'GET_BANNERS';
export const GET_BANNERS_SUCCESS = 'GET_BANNERS_SUCCESS';

export const getBannersAC = () => ({
  type: GET_BANNERS
});

export const getBannersSuccessAC = (data) => ({
  type: GET_BANNERS_SUCCESS,
  data
});
