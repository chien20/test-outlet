export const SHOW_ALERT = 'SHOW_ALERT';
export const CLOSE_ALERT = 'CLOSE_ALERT';
export const CLEAN_ALERTS = 'CLEAN_ALERTS';

export const showAlertAC = (data) => ({
  type: SHOW_ALERT,
  data
});

export const closeAlertAC = (id) => ({
  type: CLOSE_ALERT,
  id
});

export const cleanAlertAC = (containers) => ({
  type: CLEAN_ALERTS,
  containers
});
