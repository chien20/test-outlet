export const GET_CATEGORY_LIST = 'GET_CATEGORY_LIST';
export const GET_CATEGORY_LIST_SUCCESS = 'GET_CATEGORY_LIST_SUCCESS';

export const getCategoryListAC = () => ({
  type: GET_CATEGORY_LIST
});

export const getCategoryListSuccessAC = (categoryList) => ({
  type: GET_CATEGORY_LIST_SUCCESS,
  categoryList
});
