export const GET_BASIC_USER_INFO = 'GET_BASIC_USER_INFO';
export const GET_BASIC_USER_INFO_SUCCESS = 'GET_BASIC_USER_INFO_SUCCESS';

export const getBasicUserInfoAC = (userId) => ({
  type: GET_BASIC_USER_INFO,
  userId: userId
});

export const getBasicUserInfoSuccessAC = (userId, data) => ({
  type: GET_BASIC_USER_INFO_SUCCESS,
  userId: userId,
  data
});
