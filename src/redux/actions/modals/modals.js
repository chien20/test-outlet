export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const openModal = (modalName, data = null) => ({
  type: OPEN_MODAL,
  modalName,
  data
});

export const closeModal = (modalName, data = null) => ({
  type: CLOSE_MODAL,
  modalName,
  data
});
