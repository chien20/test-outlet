export const RENEW_ACCESS_TOKEN_SUCCESS = 'RENEW_ACCESS_TOKEN_SUCCESS';
export const LOGOUT = 'LOGOUT';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const AUTHENTICATION_SUCCESS = 'AUTHENTICATION_SUCCESS';

export const renewAccessTokenSuccess = (accessToken, refreshToken) => ({
  type: RENEW_ACCESS_TOKEN_SUCCESS,
  accessToken,
  refreshToken
});

export const logout = () => ({
  type: LOGOUT
});

export const loginSuccess = (data) => {
  return {
    type: LOGIN_SUCCESS,
    data
  }
};

export const authenticationSuccessAC = () => ({
  type: AUTHENTICATION_SUCCESS
});
