export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const GET_CURRENT_USER_SUCCESS = 'GET_CURRENT_USER_SUCCESS';
export const GET_CURRENT_USER_FAILED = 'GET_CURRENT_USER_FAILED';

export const getCurrentUserAC = (isAuthenticated = false) => ({
  type: GET_CURRENT_USER,
  isAuthenticated
});

export const getCurrentUserSuccessAC = (data) => ({
  type: GET_CURRENT_USER_SUCCESS,
  data
});

export const getCurrentUserFailedAC = () => ({
  type: GET_CURRENT_USER_FAILED,
});
