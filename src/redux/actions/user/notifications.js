export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS';
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS';

export const getNotificationsAC = (silent = true) => ({
  type: GET_NOTIFICATIONS,
  silent
});

export const getNotificationsSuccessAC = (notifications) => ({
  type: GET_NOTIFICATIONS_SUCCESS,
  notifications
});
