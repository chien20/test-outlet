export const SET_LANGUAGE = 'SET_LANGUAGE';
export const GET_SETTINGS = 'GET_SETTINGS';
export const GET_MENUS = 'GET_MENUS';
export const GET_SETTINGS_SUCCESS = 'GET_SETTINGS_SUCCESS';
export const GET_MENUS_SUCCESS = 'GET_MENUS_SUCCESS';

export const setLanguage = (language) => ({
  type: SET_LANGUAGE,
  language
});

export const getSettingsAC = () => ({
  type: GET_SETTINGS
});

export const getMenusAC = () => ({
  type: GET_MENUS
});

export const getSettingsSuccessAC = (settings) => ({
  type: GET_SETTINGS_SUCCESS,
  settings
});

export const getMenusSuccessAC = (menus) => ({
  type: GET_MENUS_SUCCESS,
  menus
});
