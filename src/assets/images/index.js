import angryIcon from './smiley/angry.svg';
import desperateIcon from './smiley/desperate.svg';
import happyIcon from './smiley/happy.svg';
import inLoveIcon from './smiley/in-love.svg';
import scepticIcon from './smiley/sceptic.svg';
export {
  angryIcon,
  desperateIcon,
  happyIcon,
  inLoveIcon,
  scepticIcon
};
