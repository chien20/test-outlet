import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Store from './redux/store';
import {history} from './common/utils/router/history';
import App from './App';
import './assets/css/bootstrap/bootstrap.scss';
import './assets/css/style.scss';
import './common/helpers/logger';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';

ReactDOM.render(
  <ErrorBoundary>
    <Provider store={Store}>
      <Router history={history}>
        <App/>
      </Router>
    </Provider>
  </ErrorBoundary>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
