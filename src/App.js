import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {Switch, withRouter} from 'react-router-dom';
import {isMobile} from './common/helpers/browser';
import {getBannersAC, getCategoryListAC} from './redux/actions/common';
import Broadcaster from './common/helpers/broadcaster';
import BROADCAST_EVENT from './common/constants/broadcastEvents';
import AppLayout from './layouts/AppLayout';
import AppRoute from './layouts/AppRoute';
import PrivateAppRoute from './layouts/PrivateAppRoute';
import Modals from './components/modals/Modals';
import AlertGroup from './components/Alert/AlertGroup';
import Error404 from './pages/error/Error404';
import CartLoading from './pages/order/_components/CartTable/CartLoading';
import {getCartSilentAC} from './pages/order/_redux/actions';
import {getNotificationsAC} from './redux/actions/user/notifications';
import {getCurrentUserAC} from './redux/actions/user/info';
import ProductCategory from './pages/product/ProductCategory/ProductCategory';
import ProductDetail from './pages/product/ProductDetail/ProductDetail';
import StaticPage from './pages/static/StaticPage';
import ShopDetail from './pages/product/ShopDetail/ShopDetail';
import Login from './pages/account/Login';
import VerifyEmail from './pages/account/VerifyEmail/VerifyEmail';
import ResetPassword from './pages/account/ResetPassword/ResetPassword';
import {getMenusAC, getSettingsAC} from './redux/actions/app';
import Categories from './pages/categories/Categories';
import CheckoutAddressesMobile from "./pages/order/_components/CheckoutAddresses/CheckoutAddressesMobile";
import AddressEditMobile from "./pages/user/account/Addresses/AddressEditMobile";
import RegisterShop from "./pages/registerShop/RegisterShop";
import CertDetail from './pages/static/CertDetail';
import ProductListFilter from "./pages/product/ProductListFilter/ProductListFilter";
import HelpCenter from './pages/help/HelpCenter';
import FAQQuestion from './pages/help/FAQQuestion';
import Socket from './common/utils/network/Socket';
import ForgotPassword from './pages/account/ForgotPassword/ForgotPassword';
import Register from './pages/account/Register/Register';
import RegisterModal from './components/modals/RegisterModal/RegisterModal';

const Home = React.lazy(() => import('./pages/home/Home'));
const FlashSale = React.lazy(() => import('./pages/product/FlashSale/FlashSale'));
const BestSelling = React.lazy(() => import('./pages/product/BestSelling/BestSellingProducts'));
const SearchPage = React.lazy(() => import('./pages/product/SearchPage/SearchPage'));
const UserPage = React.lazy(() => import('./pages/user/UserPage'));
const Cart = React.lazy(() => import('./pages/order/Cart/Cart'));
const Checkout = React.lazy(() => import('./pages/order/Checkout/Checkout'));
const CheckoutComplete = React.lazy(() => import('./pages/order/Checkout/CheckoutComplete'));
const GroupPage = React.lazy(() => import('./pages/group/GroupPage'));
const ViewedProducts = React.lazy(() => import('./pages/product/Viewed/ViewedProducts'));

class App extends React.PureComponent {
  componentWillMount() {
    const {auth, user, getCategoryList, getBanners, getCartSilent, getNotificationSilent, getCurrentUser, dispatch} = this.props;
    if (isMobile) {
      document.getElementsByTagName('body')[0].classList.add('is-mobile');
    }
    if (user && auth) {
      if (!auth.isAuthenticated) {
        getCurrentUser();
      } else {
        getNotificationSilent();
        getCartSilent();
      }
    }
    getCategoryList();
    getBanners();
    dispatch(getSettingsAC());
    dispatch(getMenusAC());
  }

  componentDidMount() {
    Broadcaster.on(BROADCAST_EVENT.LAYOUT_LOADED, this.onLayoutLoaded);
    setTimeout(this.onLayoutLoaded, 5000);
    if (this.props.auth.isAuthenticated) {
      Socket.connect();
    }
  }

  componentWillUnmount() {
    Broadcaster.off(BROADCAST_EVENT.LAYOUT_LOADED, this.onLayoutLoaded);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
    if (this.props.auth?.isAuthenticated !== prevProps.auth?.isAuthenticated) {
      if (this.props.auth.isAuthenticated) {
        Socket.connect();
      } else {
        Socket.disconnect();
      }
    }
  }

  onLayoutLoaded = () => {
    const loadingEl = document.getElementById('app-loading');
    if (loadingEl) {
      loadingEl.classList.add('close');
      setTimeout(() => {
        loadingEl.remove();
      }, 500);
    }
  };

  render() {
    return (
      <Fragment>
        <Switch>
          <AppRoute
            exact
            path="/"
            component={Home}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/categories"
            component={Categories}
            layout={AppLayout}
          />
          <AppRoute
            exact
            path="/register-shop"
            component={RegisterShop}
            layout={AppLayout}
          />
          <AppRoute
            exact
            path="/p/:slug"
            component={StaticPage}
            layout={AppLayout}/>
          <AppRoute
            exact
            path="/cert/:id"
            component={CertDetail}
            layout={AppLayout}
          />
          <AppRoute
            exact
            path="/login"
            component={Login}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/forgot-password"
            component={ForgotPassword}
            layout={AppLayout}
            fallback={<div>Loading...</div>}
          />
          <AppRoute
            exact
            path="/register"
            component={Register}
            layout={AppLayout}
            fallback={<div>Loading...</div>}
          />
          <AppRoute
            exact
            path="/users/verify-email"
            component={VerifyEmail}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <PrivateAppRoute
            path="/user"
            component={UserPage}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/products/flash-sale"
            component={FlashSale}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/products/best-selling"
            component={BestSelling}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/products/history/:page?"
            component={ViewedProducts}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            exact
            path="/tim-kiem/:q/:page?"
            component={SearchPage}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            path="/groups/"
            component={GroupPage}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <AppRoute
            path="/reset-password/:token"
            component={ResetPassword}
            layout={AppLayout}/>
          <AppRoute
            exact
            path="/:alias/c:id"
            component={ProductCategory}
            layout={AppLayout}/>
          <AppRoute
            exact
            path="/:alias/c:id/:page?"
            component={ProductCategory}
            layout={AppLayout}/>
          <AppRoute
            exact
            path="/:alias/p:id"
            component={ProductDetail}
            layout={AppLayout}/>
          <AppRoute
            path="/shop/:id"
            component={ShopDetail}
            layout={AppLayout}/>
          <AppRoute
            exact
            path="/shop/:id/:page?"
            component={ShopDetail}
            layout={AppLayout}/>
          <PrivateAppRoute
            exact
            path="/cart"
            component={Cart}
            layout={AppLayout}
            fallback={<CartLoading/>}/>
          <PrivateAppRoute
            exact
            path="/checkout/:invoiceId/complete"
            component={CheckoutComplete}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <PrivateAppRoute
            exact
            path="/checkout"
            component={Checkout}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <PrivateAppRoute
            exact
            path="/checkout-address"
            component={CheckoutAddressesMobile}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <PrivateAppRoute
            exact
            path="/checkout-address/edit"
            component={AddressEditMobile}
            layout={AppLayout}
            fallback={<div>Loading...</div>}/>
          <PrivateAppRoute
            excat
            path="/product-list-filter"
            component={ProductListFilter}
            layout={AppLayout}
            fallback={<div>Loading...</div>}
          />
          <AppRoute
            exact
            path="/help-center"
            component={HelpCenter}
            layout={AppLayout}
          />
          <AppRoute
            exact
            path="/help-center/questions/:id"
            component={FAQQuestion}
            layout={AppLayout}
          />
          <AppRoute
            component={Error404}
            layout={AppLayout}
          />
        </Switch>
        <Modals/>
        <AlertGroup/>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.user.auth,
  user: state.user.info
});

const mapDispatchToProps = (dispatch) => ({
  getCurrentUser: () => dispatch(getCurrentUserAC()),
  getCategoryList: () => dispatch(getCategoryListAC()),
  getBanners: () => dispatch(getBannersAC()),
  getCartSilent: () => dispatch(getCartSilentAC()),
  getNotificationSilent: () => dispatch(getNotificationsAC(true)),
  dispatch
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
