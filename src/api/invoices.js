import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const INVOICES_API_URL = `${process.env.REACT_APP_API_URL}/invoices`;

export const getInvoiceListAPI = (params) => {
  return ResponseHandler(HTTPClient.get(`${INVOICES_API_URL}`, params));
};

export const getInvoiceDetailAPI = (invoice_id) => {
  return ResponseHandler(HTTPClient.get(`${INVOICES_API_URL}/${invoice_id}`));
};
