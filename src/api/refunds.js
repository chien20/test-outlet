import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';

const API_URL = `${process.env.REACT_APP_API_URL}/refunds`;

export const createRefundAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${API_URL}`, data));
};

export const getRefundsAPI = (data) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}`, data));
};

export const getRefundDetailAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/${id}`));
};

export const updateRefundAPI = (id, data) => {
  return ResponseHandler(HTTPClient.put(`${API_URL}/${id}`, data));
};

export const getRefundMetadataAPI = () => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/meta`,));
};
