import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';
import {getCartModel} from './models/cart';

const CART_API_URL = `${process.env.REACT_APP_API_URL}/cart`;

export const addToCartAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${CART_API_URL}`, data));
};

export const updateCartAPI = (data) => {
  return ResponseHandler(HTTPClient.put(`${CART_API_URL}`, data));
};

export const deleteCartAPI = (data) => {
  return ResponseHandler(HTTPClient.delete(`${CART_API_URL}`, {
    data
  }));
};

export const getCartAPI = () => {
  return ResponseHandler(HTTPClient.get(`${CART_API_URL}`, null, {__showLoading: false}), getCartModel);
};

export const getCartTotalAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${CART_API_URL}/total`, data));
};
