import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';

const API_URL = `${process.env.REACT_APP_API_URL}`;

export const getFAQCategoriesAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/faq/categories`, params));
};

export const getFAQCategoryAPI = (id, params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/faq/categories/${id}`, params));
};

export const getFAQQuestionsAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/faq/questions`, params));
};

export const getFAQQuestionAPI = (id, params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/faq/questions/${id}`, params));
};
