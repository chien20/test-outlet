import HTTPClient from '../common/utils/network/HTTPClient';

const STATIC_PAGES_API_URL = `${process.env.REACT_APP_API_URL}/static-pages`;

export const getStaticPageDetailAPI = (slug) => {
  return HTTPClient.get(`${STATIC_PAGES_API_URL}/${slug}`);
};
