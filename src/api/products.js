import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';
import {
  getCategoryListModel,
  getProductDetailModel,
  getProductFeatureListModel,
  getProductListModel,
  getProductRatingsModel
} from './models/products';
import {transformProduct, transformProducts} from '../common/helpers';

const PRODUCTS_API_URL = `${process.env.REACT_APP_API_URL}/products`;
const BRANDS_API_URL = `${process.env.REACT_APP_API_URL}/brands`;
const RATINGS_API_URL = `${process.env.REACT_APP_API_URL}/ratings`;
const STORE_API_URL = `${process.env.REACT_APP_API_URL}/stores`;

export const getCategoryListAPI = () => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/categories`, null, {__auth: false}), getCategoryListModel);
};

export const uploadProductAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${PRODUCTS_API_URL}`, data));
};

export const deleteProductAPI = (productId) => {
  return ResponseHandler(HTTPClient.delete(`${PRODUCTS_API_URL}`, {data: [productId]}));
};

export const updateProductAPI = (productId, data) => {
  return ResponseHandler(HTTPClient.put(`${PRODUCTS_API_URL}/${productId}`, data));
};

export const getProductDetailAPI = (productId, params = null, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}`, params, {__auth: true, ...config}), getProductDetailModel).then(res => {
    if (res && res.data && res.data) {
      transformProduct(res.data);
    }
    return res;
  });
};

export const getProductListAPI = (params = null, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}`, params, {__auth: false, ...config}), getProductListModel).then(res => {
    if (res && res.data && res.data.list) {
      transformProducts(res.data.list);
    }
    return res;
  });
};

export const getProductsByIdsAPI = (productIds, config = {}) => {
  return ResponseHandler(HTTPClient.post(`${PRODUCTS_API_URL}/by-ids`, productIds, {__auth: false, ...config}), getProductListModel).then(res => {
    if (res && res.data && res.data.list) {
      transformProducts(res.data.list);
    }
    return res;
  });
};

export const getProductFeatureListAPI = (category_id) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/features`, {
    category_id,
    current_page: 0,
    page_size: 100
  }, {
    __auth: false
  }), getProductFeatureListModel);
};

export const checkUserBoughtProductAPI = (productId, userId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/bought/${userId}`, params));
};

export const rateProductAPI = (productId, data) => {
  return ResponseHandler(HTTPClient.post(`${PRODUCTS_API_URL}/${productId}/ratings`, data));
};

export const getProductRatingsAPI = (productId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/ratings`, params), getProductRatingsModel);
};

export const getProductRatingStatsAPI = (productId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/ratings/stats`, params));
};

export const getProductRatingStatsDetailAPI = (productId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/ratings/stats_count`, params));
};

export const deleteProductRatingAPI = (ids) => {
  return ResponseHandler(HTTPClient.delete(`${RATINGS_API_URL}`, {data: ids}));
};

export const getRatingListAPI = (params) => {
  return ResponseHandler(HTTPClient.get(`${RATINGS_API_URL}`, params));
};

export const getBrandsAPI = (params) => {
  return ResponseHandler(HTTPClient.get(`${BRANDS_API_URL}`, params));
};

export const getRatingsForShopAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${STORE_API_URL}/ratings`, params));
};

/************ LIKE **************/
export const getProductLikesAPI = (productId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/likes`, params));
};

export const likeProductAPI = (productId, like) => {
  return ResponseHandler(HTTPClient.put(`${PRODUCTS_API_URL}/${productId}/likes`, {like}));
};

/********* POINT *************/

export const getProductPointByGroupAPI = (productId) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${productId}/points`, null));
};
