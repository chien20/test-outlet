import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const API_URL = `${process.env.REACT_APP_API_URL}`;
const UTILS_API_URL = `${process.env.REACT_APP_API_URL}`;

export const getSettingsAPI = () => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/settings`, null, {__auth: false}));
};

export const getMenusAPI = () => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/menus/items`, null, {__auth: false}));
};

export const registerShop = (data) => {
  return ResponseHandler(HTTPClient.post(`${UTILS_API_URL}/utils/register-shop`, data, {__auth: false}));
};
