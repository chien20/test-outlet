export const sampleProduct = {
  'priceId': 169,
  'userId': 479,
  'isDeleted': false,
  'images': [{
    'id': 43,
    'thumbnail_url': '/upload/product/7b09524a-3487-4853-a89e-70e47d5a8401.jpg',
    'url': '/upload/product/7b09524a-3487-4853-a89e-70e47d5a8401.jpg'
  }, {
    'id': 44,
    'thumbnail_url': '/upload/product/a7e5f3ef-02c8-4352-ae83-aef82da84b63.jpg',
    'url': '/upload/product/a7e5f3ef-02c8-4352-ae83-aef82da84b63.jpg'
  }, {
    'id': 45,
    'thumbnail_url': '/upload/product/07d4b700-77db-49c2-9282-66e0bc5de771.jpg',
    'url': '/upload/product/07d4b700-77db-49c2-9282-66e0bc5de771.jpg'
  }],
  'status': 10,
  'id': 68,
  'name': 'Điện Thoại Iphone',
  'category_id': 15,
  'store_id': 475,
  'short_description': 'Thiết kế với màu sắc trẻ trung\n\nTích hợp micro tiện lợi\n\nÂm thanh siêu trầm, cảm nhận chân thực\n\nChất liệu aluminum alloy cao cấp',
  'full_description': 'Thiết kế trẻ trung\nTai Nghe Wutsun JS200 Stereo Earphone Sport kiểu dáng thời trang, màu sắc trẻ trung và được làm từ chất liệu aluminum alloy cao cấp, cho độ bền cao, kết cấu hoàn hảo. Sản phẩm có 2 màu sắc để lựa chọn là đỏ và nâu, phù hợp với sở thích của bạn.\n\n\n\n\nChất âm siêu trầm\nÂm thanh siêu trầm, giúp người nghe cảm nhận âm thanh chân thật và nghe nhạc vượt ra ngoài các giai điệu và đoạn điệp khúc để hội tụ âm điệu đầy đủ. Thưởng thức âm thanh kỹ thuật số tiên tiến cho âm nhạc và trò chơi ưa thích cũng như các cuộc gọi rõ ràng., kiểu dáng thể thao, phù hợp cho cả nam và nữ.\n\n\n\nTích hợp micro phone tiện lợi\nTai nghe tích hợp mirco phone hỗ trợ đàm thoại được trang bị jack cắm 3.5mm, tương thích với nhiều thiết bị di động như điện thoại, laptop, PC, máy nghe nhạc… Cùng với thiết kế kiểu Ergonomics nhét tai tinh tế có lớp đệm êm ái giúp người dùng thoải mái hơn trong khi nghe nhạc lâu. Ngoài ra, các nút điều khiển âm lượng trên dây cáp giúp thuận tiện hơn trong khi dùng. \n\n\n\n * Giá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Tuy nhiên tuỳ vào từng loại sản phẩm hoặc phương thức, địa chỉ giao hàng mà có thể phát sinh thêm chi phí khác như phí vận chuyển, phụ phí hàng cồng kềnh, ..',
  'price': 320000.0,
  'original_price': 350000.0,
  'sku': 'TAINGHEBLT213',
  'qty': 1000,
  'height': 10,
  'width': 10,
  'length': 10,
  'weight': 20,
  'features': [],
  'option_groups': [{
    'name': 'Màu',
    'values': ['Xanh', 'Đỏ']
  }],
  'options': [{
    'id': 47,
    'sku': 'TRE',
    'qty': 20,
    'price': 320000.0,
    'groups_index': [0]
  }, {
    'id': 48,
    'sku': 'FGHT',
    'qty': 30,
    'price': 320000.0,
    'groups_index': [1]
  }],
  'lang_id': 0,
  'created_at': '2019-05-15T08:24:26.000+0000',
  'updated_at': '2019-05-15T09:03:18.000+0000',
  'slug': 'ien-thoai-iphone'
};
