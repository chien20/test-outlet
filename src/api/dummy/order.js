export const sampleOrder = {
  'receiver': {
    'full_name': 'Hoàng Thái Học',
    'address': '20 Mỹ Đình',
    'phone': '0392920886',
    'province_id': '01TTT',
    'district_id': '019HH',
    'ward_id': '00625'
  },
  'buyer': {
    'full_name': 'Hoàng Thái Hà',
    'address': '',
    'phone': '0392929999'
  },
  'payment_method_id': 0,
  'shipping_method_id': 0,
  'invoice_info': {
    'company_name': 'Công Ty Cổ Phần Smiletech Công Nghệ Số',
    'tax_code': '0108417906',
    'address': 'Số 166, đường Giáp Bát, P. Giáp Bát, Quận Hoàng Mai, Hà Nội'
  },
  'cart_info': [{
    'product_option_id': 47,
    'product_id': 68,
    'qty': 1,
    product_snapshot: {
      'priceId': 169,
      'userId': 479,
      'isDeleted': false,
      'images': [{
        'id': 43,
        'thumbnail_url': '/upload/product/7b09524a-3487-4853-a89e-70e47d5a8401.jpg',
        'url': '/upload/product/7b09524a-3487-4853-a89e-70e47d5a8401.jpg'
      }, {
        'id': 44,
        'thumbnail_url': '/upload/product/a7e5f3ef-02c8-4352-ae83-aef82da84b63.jpg',
        'url': '/upload/product/a7e5f3ef-02c8-4352-ae83-aef82da84b63.jpg'
      }, {
        'id': 45,
        'thumbnail_url': '/upload/product/07d4b700-77db-49c2-9282-66e0bc5de771.jpg',
        'url': '/upload/product/07d4b700-77db-49c2-9282-66e0bc5de771.jpg'
      }],
      'status': 10,
      'id': 68,
      'name': 'Điện Thoại Iphone',
      'category_id': 15,
      'store_id': 475,
      'short_description': 'Thiết kế với màu sắc trẻ trung\n\nTích hợp micro tiện lợi\n\nÂm thanh siêu trầm, cảm nhận chân thực\n\nChất liệu aluminum alloy cao cấp',
      'full_description': 'Thiết kế trẻ trung\nTai Nghe Wutsun JS200 Stereo Earphone Sport kiểu dáng thời trang, màu sắc trẻ trung và được làm từ chất liệu aluminum alloy cao cấp, cho độ bền cao, kết cấu hoàn hảo. Sản phẩm có 2 màu sắc để lựa chọn là đỏ và nâu, phù hợp với sở thích của bạn.\n\n\n\n\nChất âm siêu trầm\nÂm thanh siêu trầm, giúp người nghe cảm nhận âm thanh chân thật và nghe nhạc vượt ra ngoài các giai điệu và đoạn điệp khúc để hội tụ âm điệu đầy đủ. Thưởng thức âm thanh kỹ thuật số tiên tiến cho âm nhạc và trò chơi ưa thích cũng như các cuộc gọi rõ ràng., kiểu dáng thể thao, phù hợp cho cả nam và nữ.\n\n\n\nTích hợp micro phone tiện lợi\nTai nghe tích hợp mirco phone hỗ trợ đàm thoại được trang bị jack cắm 3.5mm, tương thích với nhiều thiết bị di động như điện thoại, laptop, PC, máy nghe nhạc… Cùng với thiết kế kiểu Ergonomics nhét tai tinh tế có lớp đệm êm ái giúp người dùng thoải mái hơn trong khi nghe nhạc lâu. Ngoài ra, các nút điều khiển âm lượng trên dây cáp giúp thuận tiện hơn trong khi dùng. \n\n\n\n * Giá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Tuy nhiên tuỳ vào từng loại sản phẩm hoặc phương thức, địa chỉ giao hàng mà có thể phát sinh thêm chi phí khác như phí vận chuyển, phụ phí hàng cồng kềnh, ..',
      'price': 320000.0,
      'original_price': 350000.0,
      'sku': 'TAINGHEBLT213',
      'qty': 1000,
      'height': 10,
      'width': 10,
      'length': 10,
      'weight': 20,
      'features': [],
      'option_groups': [{
        'name': 'Màu',
        'values': ['Xanh', 'Đỏ']
      }],
      'options': [{
        'id': 47,
        'sku': 'TRE',
        'qty': 20,
        'price': 320000.0,
        'groups_index': [0]
      }, {
        'id': 48,
        'sku': 'FGHT',
        'qty': 30,
        'price': 320000.0,
        'groups_index': [1]
      }],
      'lang_id': 0,
      'created_at': '2019-05-15T08:24:26.000+0000',
      'updated_at': '2019-05-15T09:03:18.000+0000',
      'slug': 'ien-thoai-iphone'
    }
  }, {
    'product_option_id': 50,
    'product_id': 66,
    'qty': 2,
    product_snapshot: {
      'priceId': 405,
      'userId': 479,
      'isDeleted': false,
      'images': [{
        'id': 31,
        'thumbnail_url': '/upload/product/b988faa1-6000-4d2e-9262-755a518e761f.jpg',
        'url': '/upload/product/b988faa1-6000-4d2e-9262-755a518e761f.jpg'
      }, {
        'id': 32,
        'thumbnail_url': '/upload/product/5b3a77fd-657f-400e-987a-3b8ec81052fb.jpg',
        'url': '/upload/product/5b3a77fd-657f-400e-987a-3b8ec81052fb.jpg'
      }, {
        'id': 33,
        'thumbnail_url': '/upload/product/35bb6ea9-cfa4-46d0-890b-39ac7d62f7f7.jpg',
        'url': '/upload/product/35bb6ea9-cfa4-46d0-890b-39ac7d62f7f7.jpg'
      }, {
        'id': 34,
        'thumbnail_url': '/upload/product/16900069-0a2f-4119-91fb-bef358103a61.jpg',
        'url': '/upload/product/16900069-0a2f-4119-91fb-bef358103a61.jpg'
      }, {
        'id': 35,
        'thumbnail_url': '/upload/product/796e9b60-ee66-41a8-ada3-15e0f6548ce5.jpg',
        'url': '/upload/product/796e9b60-ee66-41a8-ada3-15e0f6548ce5.jpg'
      }, {
        'id': 36,
        'thumbnail_url': '/upload/product/0861c1f1-d688-42f2-8e4b-a72deeff62cb.jpg',
        'url': '/upload/product/0861c1f1-d688-42f2-8e4b-a72deeff62cb.jpg'
      }, {
        'id': 37,
        'thumbnail_url': '/upload/product/01f365b2-1cd2-4d55-a2c6-d146adbd2dba.jpg',
        'url': '/upload/product/01f365b2-1cd2-4d55-a2c6-d146adbd2dba.jpg'
      }, {
        'id': 38,
        'thumbnail_url': '/upload/product/ad582d5b-db57-41c3-bd0f-64fd2dd6cf17.jpg',
        'url': '/upload/product/ad582d5b-db57-41c3-bd0f-64fd2dd6cf17.jpg'
      }],
      'status': 10,
      'id': 66,
      'name': 'Tai Nghe Wutsun JS200 Stereo Earphone Sport',
      'category_id': 15,
      'store_id': 475,
      'short_description': 'Thiết kế với màu sắc trẻ trung\n\nTích hợp micro tiện lợi\n\nÂm thanh siêu trầm, cảm nhận chân thực\n\nChất liệu aluminum alloy cao cấp',
      'full_description': 'Thiết kế trẻ trung\nTai Nghe Wutsun JS200 Stereo Earphone Sport kiểu dáng thời trang, màu sắc trẻ trung và được làm từ chất liệu aluminum alloy cao cấp, cho độ bền cao, kết cấu hoàn hảo. Sản phẩm có 2 màu sắc để lựa chọn là đỏ và nâu, phù hợp với sở thích của bạn.\n\n\n\n\nChất âm siêu trầm\nÂm thanh siêu trầm, giúp người nghe cảm nhận âm thanh chân thật và nghe nhạc vượt ra ngoài các giai điệu và đoạn điệp khúc để hội tụ âm điệu đầy đủ. Thưởng thức âm thanh kỹ thuật số tiên tiến cho âm nhạc và trò chơi ưa thích cũng như các cuộc gọi rõ ràng., kiểu dáng thể thao, phù hợp cho cả nam và nữ.\n\n\n\nTích hợp micro phone tiện lợi\nTai nghe tích hợp mirco phone hỗ trợ đàm thoại được trang bị jack cắm 3.5mm, tương thích với nhiều thiết bị di động như điện thoại, laptop, PC, máy nghe nhạc… Cùng với thiết kế kiểu Ergonomics nhét tai tinh tế có lớp đệm êm ái giúp người dùng thoải mái hơn trong khi nghe nhạc lâu. Ngoài ra, các nút điều khiển âm lượng trên dây cáp giúp thuận tiện hơn trong khi dùng. \n\n\n\n * Giá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Tuy nhiên tuỳ vào từng loại sản phẩm hoặc phương thức, địa chỉ giao hàng mà có thể phát sinh thêm chi phí khác như phí vận chuyển, phụ phí hàng cồng kềnh, ..',
      'price': 320000.0,
      'original_price': 350000.0,
      'sku': 'TAINGHEBLT213',
      'qty': 1000,
      'height': 10,
      'width': 10,
      'length': 10,
      'weight': 20,
      'features': [{
        'global_feature_id': 20,
        'value': 'KO',
        'name': 'Xuất Xứ'
      }],
      'option_groups': [{
        'name': 'Màu',
        'values': ['Xanh', '']
      }],
      'options': [{
        'id': 50,
        'sku': 'SKKK',
        'qty': 10,
        'price': 320000.0,
        'groups_index': [0]
      }],
      'lang_id': 0,
      'created_at': '2019-05-15T08:24:26.000+0000',
      'updated_at': '2019-06-01T15:19:18.000+0000',
      'slug': 'tai-nghe-wutsun-js200-stereo-earphone-sport'
    }
  }],
  'use_receiver_info': false,
  'use_invoice_info': true
};
