import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const EVENTS_API_URL = `${process.env.REACT_APP_API_URL}/events`;

export const getRunningEventsAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${EVENTS_API_URL}`, params, {__auth: false}));
};

export const getEventDetailAPI = (id, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${EVENTS_API_URL}/${id}`, params, {__auth: false}));
};
