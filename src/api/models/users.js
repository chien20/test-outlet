export const getCurrentUserModel = {
  id: '',
  full_name: '',
  email: '',
  phone: '',
  avatar: '', // url ảnh avatar
  cover: '', // url
  gender: 0, // 0: nữ, 1: nam, 2: khác
  dob: '',
  store_id: '',
  has_password: false,
  email_verified: false,
  lang_id: 0,
  status: 0,
  created_at: '',
  updated_at: '',

  old_password: '',
  new_password: '',
  re_type_password: ''
};

export const getBasicUserInfoModel = {
  id: '',
  full_name: '',
  avatar: '', // url ảnh avatar
  cover: '', // url
  gender: 0, // 0: nữ, 1: nam, 2: khác
};

