export const commonListResponseModel = (list = []) => ({
  list,
  pageInfo: {
    current_page: 0,
    page_size: 0,
    total_items: 0,
    offset: ''
  }
});
