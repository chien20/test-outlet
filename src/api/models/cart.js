import {getProductDetailModel} from './products';
import {getStoreDetailModel} from './stores';

export const getCartModel = [
  {
    product_option_id: 0,
    __product_option_idNullable: true,
    product: getProductDetailModel,
    store: getStoreDetailModel,
    price: 0,
    qty: 0
  }
];
