export const getStoreDetailModel = {
  address: "",
  created_at: "",
  description: "",
  district_id: "",
  email: "",
  id: 0,
  name: "",
  phone: "",
  province_id: "",
  published: 0,
  slug: "",
  status: 0,
  updated_at: "",
  user_id: 0,
  business_info: {
    business_registration_number: '',
    date_registration: null,
    issued_by: '',
    tax_code: '',
    type: 0, //0: doanh nghiep, 10: ho kinh doanh, 20: ca nhan
  },
};
