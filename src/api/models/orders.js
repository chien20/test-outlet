import {getProductDetailModel} from './products';
import {commonListResponseModel} from './helpers';

export const getOrderDetailModel = {
  receiver: {
    full_name: '',
    address: '',
    phone: '',
    province_id: '',
    district_id: '',
    ward_id: ''
  },
  use_receiver_info: true, // Nếu true thì thông tin người mua sử dụng như thông tin người nhận
  buyer: {
    full_name: '',
    phone: ''
  },
  payment_method_id: 0, // 0: COD, 10: credit card, 20: ATM
  shipping_method_id: 0, // 0: Giao hàng tiết kiệm
  use_invoice_info: false, // Nếu true thì sử dụng thông tin xuất hóa đơn
  invoice_info: {
    company_name: '',
    tax_code: '',
    address: ''
  },
  status: 0,
  id: 0,
  created_at: '',
  updated_at: '',
  cart_info: [
    {
      product_option_id: '',
      product_id: '',
      qty: 0,
      product_snapshot: getProductDetailModel
    }
  ]
};

export const getOrderListDataModel = commonListResponseModel([
  getOrderDetailModel
]);
