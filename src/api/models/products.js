import {commonListResponseModel} from './helpers';

export const getCategoryListModel = commonListResponseModel(
  [
    {
      id: '',
      name: '',
      description: '',
      parent_id: 0,
      icon_url: '',
      avatar_url: '',
      created_at: '',
      updated_at: ''
    }
  ]
);

export const getProductFeatureListModel = commonListResponseModel(
  [
    {
      id: '',
      name: '',
      category_id: '',
      category_name: '',
      value_type: ''  // Gồm: "integer", "double", "varchar"
    }
  ]
);

export const getProductListModel = commonListResponseModel(
  [
    {
      id: '',
      name: '',
      price: 0,
      original_price: 0,
      sell_count: 0,
      option_groups: [
        { // => i
          id: '',
          name: '',
          values: []
        }
      ],
      options: [
        { // i=0; j=0
          id: '',
          sku: '',
          qty: 0,
          price: 0,
          original_price: 0,
          groups_index: [0, 0]
        }
      ],
      images: [
        {
          id: '',
          thumbnail_url: '',
          url: ''
        }
      ],
      rating: 0,
      status: 0, // 0: Not reviewed yet, 10: review OK - published, 30: review not OK - need to update
      event: { // Nếu product nằm trong 1 event, nếu không thì để null
        event_name: '',
        product_info: {
          product_id: 0,
          price: 0,
          product_option_ids: [],
          price_options: []
        }
      },
      __eventNullable: true
    }
  ]
);

export const getProductDetailModel = {
  id: 0,
  name: '',
  category_id: 0,
  store_id: 0,
  full_description: '',
  short_description: '',
  refurn_term: '',
  price: 0,
  original_price: 0,
  sku: '',
  qty: 0,
  features: [
    {
      global_feature_id: 0,
      name: '',
      value: ''
    }
  ],
  option_groups: [
    { // => i
      id: '',
      name: '',
      values: []
    }
  ],
  options: [
    { // i=0; j=0
      id: '',
      sku: '',
      qty: 0,
      price: 0,
      original_price: 0,
      groups_index: [0, 0]
    }
  ],
  created_at: '',
  updated_at: '',
  avatar_id: 0,
  images: [
    {
      id: '',
      thumbnail_url: '',
      url: ''
    }
  ],
  rating: 0,
  status: 0, // 0: Not reviewed yet, 10: review OK - published, 30: review not OK - need to update
  event: { // Nếu product nằm trong 1 event, nếu không thì để null
    event_name: '',
    product_info: {
      product_id: 0,
      price: 0,
      product_option_ids: [],
      price_options: []
    }
  },
  __eventNullable: true
};

export const getProductRatingsModel = commonListResponseModel(
  [
    {
      id: 0,
      rate: 0,
      comment: '',
      tags: [],
      images: [], // Same as product images
      user_info: {
        id: 0,
        full_name: 'Hoàng Thái Học',
        avatar: ''
      }
    }
  ]
);
