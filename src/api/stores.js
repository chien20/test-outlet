import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';
import {getStoreDetailModel} from './models/stores';

const STORES_API_URL = `${process.env.REACT_APP_API_URL}/stores`;

export const registerStoreAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${STORES_API_URL}`, data));
};

export const getStoreDetailAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${id}`, null, {__auth: false}), getStoreDetailModel);
};

export const updateStoreAPI = (id, data) => {
  return ResponseHandler(HTTPClient.put(`${STORES_API_URL}/${id}`, data));
};

export const getCountActiveProductAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${id}/products/count`));
};

export const getStoreAvgRatingAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${id}/ratings/avg`, null, {__auth: false}));
};

export const getStoreCountRatingAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${id}/ratings/count`, null, {__auth: false}));
};

export const getStoreCategoriesAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${id}/categories`, {
    current_page: 0,
    page_size: 1000
  }, {__auth: false}));
};

export const addStoreShippingFeeAPI = (storeId, data) => {
  return ResponseHandler(HTTPClient.post(`${STORES_API_URL}/${storeId}/deliveries`, data));
};

export const getStoreShippingFeeAPI = (storeId, id) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${storeId}/deliveries/${id}`));
};

export const updateStoreShippingFeeAPI = (storeId, id, data) => {
  return ResponseHandler(HTTPClient.put(`${STORES_API_URL}/${storeId}/deliveries/${id}`, data));
};

export const deleteStoreShippingFeeAPI = (storeId, id) => {
  return ResponseHandler(HTTPClient.delete(`${STORES_API_URL}/${storeId}/deliveries/${id}`));
};

export const getStoreShippingFeesAPI = (storeId, params) => {
  return ResponseHandler(HTTPClient.get(`${STORES_API_URL}/${storeId}/deliveries`, params));
};
