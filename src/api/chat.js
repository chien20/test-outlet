import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const CHAT_API_URL = `${process.env.REACT_APP_API_URL}/chat`;

export const getConversationsAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${CHAT_API_URL}/inboxes`, params));
};

export const getConversationMessagesAPI = (userId, params = {}, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${CHAT_API_URL}/inboxes/user/${userId}`, params, config));
};

export const sendMessageAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${CHAT_API_URL}/messages`, data, {__showLoading: false}));
};
