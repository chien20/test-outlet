import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';

const API_URL = `${process.env.REACT_APP_API_URL}`;

export const getCertsAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/certificates`, params));
};

export const getCertDetailAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/certificates/${id}`));
};

export const createCertRequestAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${API_URL}/certificates/requests`, data));
};

export const getCertRequestsAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${API_URL}/certificates/requests`, params));
};

export const deleteCertRequestsAPI = (id) => {
  return ResponseHandler(HTTPClient.delete(`${API_URL}/certificates/requests/${id}`));
};
