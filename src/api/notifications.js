import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const NOTIFICATIONS_API_URL = `${process.env.REACT_APP_API_URL}/notifications`;

export const getNotificationListAPI = (params) => {
  return ResponseHandler(HTTPClient.get(`${NOTIFICATIONS_API_URL}`, params));
};

export const updateNotificationSeenStatusAPI = (id, seen) => {
  return ResponseHandler(HTTPClient.put(`${NOTIFICATIONS_API_URL}/${id}`, {seen: seen}));
};

export const deleteNotificationAPI = (ids) => {
  return ResponseHandler(HTTPClient.delete(`${NOTIFICATIONS_API_URL}`, {data: ids}));
};
