import axios from 'axios';

const API_URL = `https://hiac.vn/DesktopModules/CmsView/api/OdrEvent`;

export const testHIACAPI = () => {
  return axios.get(`${API_URL}/test`);
};

export const registerHIACEventAPI = (data) => {
  return axios.get(`${API_URL}/RegisterNewEvent`, {params: data});
};
