import HTTPClient from '../common/utils/network/HTTPClient';

const LOCATION_API_URL = `${process.env.REACT_APP_API_URL}/location`;

export const getListProvinceAPI = () => {
  return HTTPClient.get(`${LOCATION_API_URL}/provinces`);
};

export const getListDistrictAPI = (province_id) => {
  return HTTPClient.get(`${LOCATION_API_URL}/provinces/${province_id}/districts`);
};

export const getListWardAPI = (district_id) => {
  return HTTPClient.get(`${LOCATION_API_URL}/districts/${district_id}/wards`);
};
