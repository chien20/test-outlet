import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';

const MEDIA_API_URL = `${process.env.REACT_APP_API_URL}/media`;

export const uploadMediaAPI = (file, data) => {
  const formData = new FormData();
  formData.set('file_upload', file);
  if (data && typeof data === 'object') {
    const keys = Object.keys(data);
    keys.forEach(k => {
      formData.set(k, data[k]);
    });
  }
  return HTTPClient.post(`${MEDIA_API_URL}`, formData, {
    headers: {'Content-Type': 'multipart/form-data'}
  });
};

export const deleteMediaAPI = (ids) => {
  return HTTPClient.delete(`${MEDIA_API_URL}`, {data: ids});
};

export const getObjectMediaAPI = (objectId, objectType, type = null, params = undefined) => {
  return ResponseHandler(HTTPClient.get(`${MEDIA_API_URL}`, {
    ...params,
    object_id: objectId,
    object_type: objectType,
    type,
  }, {
    __auth: false
  }));
};

export const getObjectsMediaAPI = (objectIds, objectType, type = null) => {
  return ResponseHandler(HTTPClient.post(`${MEDIA_API_URL}/multiple`, {
    object_ids: objectIds,
    object_type: objectType,
    type
  }, {
    __auth: false,
    params: {
      page_size: 100,
    }
  }));
};
