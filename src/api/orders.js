import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';
import {getOrderDetailModel, getOrderListDataModel} from './models/orders';

const ORDERS_API_URL = `${process.env.REACT_APP_API_URL}/orders`;

export const submitOrderAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${ORDERS_API_URL}`, data));
};

export const getOrderListAPI = (params) => {
  return ResponseHandler(HTTPClient.get(`${ORDERS_API_URL}`, params), getOrderListDataModel);
};

export const getOrderDetailAPI = (order_id) => {
  return ResponseHandler(HTTPClient.get(`${ORDERS_API_URL}/${order_id}`), getOrderDetailModel);
};

export const updateOrderDetailAPI = (order_id, data) => {
  return ResponseHandler(HTTPClient.put(`${ORDERS_API_URL}/${order_id}`, data));
};

export const updateOnePayStatusAPI = (search = '') => {
  return HTTPClient.get(`${process.env.REACT_APP_API_URL}/onepay/ipn${search}`, undefined, {__ignoreMetadata: true});
};
