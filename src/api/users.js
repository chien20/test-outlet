import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';
import {getBasicUserInfoModel, getCurrentUserModel} from './models/users';

const USERS_API_URL = `${process.env.REACT_APP_API_URL}/users`;

export const registerPhoneNumberAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/register-phone`, null, {params: data}));
};

export const verifyPhoneNumberAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/register-phone/verify`, null, {params: data}));
};

export const verifyEmailAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/verify-email`, data, {params: data}));
};

export const registerAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/register`, data));
};

export const registerByEmailAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/register-email`, data));
};

export const loginAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/login`, data, {__auth: false}));
  // data.grant_type = 'password';
  //
  // return XMLHTTPRequestClient.post(`${process.env.REACT_APP_API_URL}/oauth/token`, data, {
  //   headers: {'Authorization': 'Basic Y2xpZW50SWQ6c2VjcmV0'}
  // });
};

export const loginByGoogleAPI = (params) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/login/google`, params, {__auth: false}));
}

export const loginByFacebookAPI = (params) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/login/facebook`, params, {__auth: false}));
}

export const getCurrentUserAPI = (config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/current`, null, config), getCurrentUserModel);
};

export const forgotPasswordAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/forgot-password`, data));
};

export const resetPasswordAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/reset-password`, data));
};

export const updateUserProfileAPI = (data) => {
  return ResponseHandler(HTTPClient.put(`${USERS_API_URL}/current`, data));
};

export const updateUserPasswordAPI = (data) => {
  return HTTPClient.put(`${USERS_API_URL}/current/password`, data);
};

export const updateUserAvatarAPI = (file, data = null) => {
  const formData = new FormData();
  formData.set('upload_file', file);
  if (data && typeof data === 'object') {
    const keys = Object.keys(data);
    keys.forEach(k => {
      formData.set(k, data[k]);
    });
  }
  return HTTPClient.put(`${USERS_API_URL}/current/avatar`, formData, {
    headers: {'Content-Type': 'multipart/form-data'}
  });
};

export const getBasicUserInfoAPI = (userId, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/info/${userId}`, null, config), getBasicUserInfoModel);
};

export const subscribeEmailAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/subscribe`, data));
};

export const updatePhoneAPI = (data) => {
  return ResponseHandler(HTTPClient.put(`${USERS_API_URL}/update-phone`, data));
};

/************* Addresses ************/

export const getAddressesAPI = (config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/addresses`, null, config));
};

export const getAddressDetailAPI = (id, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/addresses/${id}`, null, config));
};

export const addAddressesAPI = (data, config = {}) => {
  return ResponseHandler(HTTPClient.post(`${USERS_API_URL}/addresses`, data, config));
};

export const updateAddressesAPI = (id, data, config = {}) => {
  return ResponseHandler(HTTPClient.put(`${USERS_API_URL}/addresses/${id}`, data, config));
};

export const deleteAddressesAPI = (id, config = {}) => {
  return ResponseHandler(HTTPClient.delete(`${USERS_API_URL}/addresses/${id}`, config));
};

/******** Points ***********/
export const getUserPointsAPI = (params = null, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/points`, params, config));
};

export const getUserPointsHistoryAPI = (params = null, config = {}) => {
  return ResponseHandler(HTTPClient.get(`${USERS_API_URL}/points/history`, params, config));
};
