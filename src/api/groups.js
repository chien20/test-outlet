import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const GROUPS_API_URL = `${process.env.REACT_APP_API_URL}/groups`;
const POSTS_API_URL = `${GROUPS_API_URL}/posts`;

export const registerGroupAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/register`, data));
};

export const getListGroupAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}`, params));
};

export const getGroupMemberCountAPI = (groupIds) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/members/multiple`, groupIds));
};

export const getSuggestGroupsAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/suggest`, params));
};

export const getGroupDetailAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${id}`));
};

export const updateGroupAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}`, data));
};

export const deleteGroupAPI = (groupIds) => {
  return ResponseHandler(HTTPClient.delete(`${GROUPS_API_URL}`, {data: groupIds}));
};

export const getListGroupMembersAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/members`, params));
};

export const updateGroupMemberStatusAPI = (groupId, userId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}/members/${userId}`, data));
};

export const deleteGroupMemberAPI = (groupId, userId, data) => {
  return ResponseHandler(HTTPClient.delete(`${GROUPS_API_URL}/${groupId}/members/${userId}`, {data: data}));
};

export const getListGroupRolesAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/roles`, params));
};

export const getRoleDetailAPI = (groupId, roleId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/roles/${roleId}`, params));
};

export const createGroupRoleAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/${groupId}/roles`, data));
};

export const updateGroupRoleAPI = (groupId, roleId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}/roles/${roleId}`, data));
};

export const deleteGroupRolesAPI = (groupId, roleIds) => {
  return ResponseHandler(HTTPClient.delete(`${GROUPS_API_URL}/${groupId}/roles`, {data: roleIds}));
};

export const getGroupMemberPermissionsAPI = (groupId) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/permissions`));
};

export const inviteGroupMembersAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/invite`, data));
};

export const assignRoleForMemberAPI = (groupId, member_id, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}/members/${member_id}/role`, data));
};

export const getInviteInfoAPI = (id) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/invite/${id}`));
};

export const getInvitesAPI = (groupId) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/invite`));
};

export const acceptInviteInfoAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/invite/accept`, data));
};

export const requestJoinGroupAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/${groupId}/request`, data));
};

export const getListGroupActivitiesAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/activities`, params));
};

export const reportGroupItemAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/${groupId}/reports`, data));
};

/******* PRODUCT *******/
export const getListGroupProductAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/products`, params));
};

export const approveGroupProductAPI = (groupId, productId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}/products/${productId}`, data));
};

export const updateReviewFlagAPI = (groupId, productId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/${groupId}/products/${productId}/reviewed`, data));
};

export const shareProductToGroupAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/${groupId}/products`, data));
};

/******* POSTS *******/
export const getGroupPostsAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/posts`, params));
};

export const getGroupPostDetailAPI = (groupId, postId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/posts/${postId}`, params));
};

export const addGroupPostAPI = (groupId, data) => {
  return ResponseHandler(HTTPClient.post(`${GROUPS_API_URL}/${groupId}/posts`, data));
};

export const getGroupPostsForOwnerAPI = (groupId, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/owner/${groupId}/posts`, params));
};

export const updateGroupPostStatusAPI = (groupId, postId, data) => {
  return ResponseHandler(HTTPClient.put(`${GROUPS_API_URL}/owner/${groupId}/posts/${postId}`, data));
};

export const deleteGroupPostAPI = (groupId, postId) => {
  return ResponseHandler(HTTPClient.delete(`${GROUPS_API_URL}/${groupId}/posts/${postId}`));
};

export const getGroupPostLikesAPI = (postId) => {
  return ResponseHandler(HTTPClient.get(`${POSTS_API_URL}/${postId}/like`));
};

export const likeGroupPostAPI = (groupId, postId, data) => {
  return ResponseHandler(HTTPClient.put(`${POSTS_API_URL}/${postId}/like`, data));
};

/******* COMMENTS *******/
export const addCommentToPostAPI = (postId, data) => {
  return ResponseHandler(HTTPClient.post(`${POSTS_API_URL}/${postId}/comments`, data));
};

export const getPostCommentsAPI = (postId, params = null) => {
  return ResponseHandler(HTTPClient.get(`${POSTS_API_URL}/${postId}/comments`, params));
};

export const deletePostCommentAPI = (postId, commentId) => {
  return ResponseHandler(HTTPClient.delete(`${POSTS_API_URL}/${postId}/comments/${commentId}`));
};

export const updatePostCommentAPI = (postId, commentId, data) => {
  return ResponseHandler(HTTPClient.put(`${POSTS_API_URL}/${postId}/comments/${commentId}`, data));
};


/******* STATS ************/
export const getGroupStatsMembersCountAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/members/count`, params));
};

export const getGroupStatsMembersByTimeAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/members/time`, params));
};

export const getGroupStatsMembersByRoleAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/members/role`, params));
};

export const getGroupStatsMembersByLevelAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/members/level`, params));
};

export const getGroupStatsProductsCountAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/products/count`, params));
};

export const getGroupStatsProductsActiveAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/products/active`, params));
};

export const getGroupStatsProductsNewAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/products/new`, params));
};

export const getGroupStatsProductsUpdateAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/products/update`, params));
};

export const getGroupStatsProductsDeleteAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/products/delete`, params));
};

export const getGroupStatsRevenueByTimeAPI = (groupId) => (params) => {

  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/revenue/time`, params));
};

export const getGroupStatsRevenueByMembersAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/revenue/members`, params));
};

export const getGroupStatsRevenueByProductsAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/revenue/products`, params));
};

export const getGroupStatsRevenueCountAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/revenue/count`, params));
};

export const getGroupStatsCommissionByTimeAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/commission/time`, params));
};

export const getGroupStatsCommissionByMembersAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/commission/members`, params));
};

export const getGroupStatsCommissionByProductsAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/commission/members`, params));
};

export const getGroupStatsOrdersCountAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/orders/count`, params));
};

export const getGroupStatsOrdersByTimeAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/orders/time`, params));
};

export const getGroupStatsOrdersByMembersAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/orders/members`, params));
};

export const getGroupStatsOrdersByProductsAPI = (groupId) => (params) => {
  return ResponseHandler(HTTPClient.get(`${GROUPS_API_URL}/${groupId}/stats/orders/members`, params));
};

/******* END STATS *********/
