import HTTPClient from '../common/utils/network/HTTPClient';
import ResponseHandler from '../common/utils/network/ResponseHandler';

const VOUCHERS_API_URL = `${process.env.REACT_APP_API_URL}/vouchers`;
const PRODUCTS_API_URL = `${process.env.REACT_APP_API_URL}/products`;

export const getVouchersAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${VOUCHERS_API_URL}`, params));
};

export const getVoucherDetailAPI = (id, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${VOUCHERS_API_URL}/${id}`, params));
};

export const getVoucherInfoAPI = (params = {}) => {
  return ResponseHandler(HTTPClient.get(`${VOUCHERS_API_URL}/info`, params));
};

export const addVoucherAPI = (data) => {
  return ResponseHandler(HTTPClient.post(`${VOUCHERS_API_URL}`, data));
};

export const updateVoucherAPI = (id, data) => {
  return ResponseHandler(HTTPClient.put(`${VOUCHERS_API_URL}/${id}`, data));
};

export const deleteVoucherAPI = (id) => {
  return ResponseHandler(HTTPClient.delete(`${VOUCHERS_API_URL}/${id}`));
};

export const getProductVouchersAPI = (id, params = {}) => {
  return ResponseHandler(HTTPClient.get(`${PRODUCTS_API_URL}/${id}/vouchers`, params));
};
