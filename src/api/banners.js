import ResponseHandler from '../common/utils/network/ResponseHandler';
import HTTPClient from '../common/utils/network/HTTPClient';

const BANNERS_API_URL = `${process.env.REACT_APP_API_URL}/banners`;

export const getBannersAPI = (params = null) => {
  return ResponseHandler(HTTPClient.get(`${BANNERS_API_URL}`, params, {__auth: false}));
};
