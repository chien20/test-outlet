/**
 * Handle request's response
 * @param request
 * @param dataModel
 * @constructor
 */
import {handleFieldErrors} from '../../helpers';

const ResponseHandler = (request, dataModel = null) => {
  return request.then(res => {
    return standardizedResponse(res, dataModel);
  }).catch((error) => {
    if (error && error.error_fields && error.error_fields.length) {
      handleFieldErrors(error.error_fields);
    }
    throw error
  });
};

function standardizedResponse(response, dataModel) {
  if (!response || typeof response !== 'object' || !response.data || !dataModel) {
    return response;
  }
  response = assignObject(response, {
    data: dataModel
  });
  return response;
}

function assignObject(res, model) {
  res = JSON.parse(JSON.stringify(res));
  for (let i in model) {
    if (!model.hasOwnProperty(i)) {
      continue;
    }

    // Ignore __{i}Nullable
    if (i.indexOf('__') === 0) {
      continue;
    }

    // If res[i] is empty
    if (!res.hasOwnProperty(i) || res[i] === undefined || res[i] === null) {
      // If this property can be null
      const __i = `__${i}Nullable`;
      if (model[__i]) {
        continue;
      }
      // Otherwise
      res[i] = getAssignValue(model[i]);
      continue;
    }

    // If res[i] is not empty

    if (Array.isArray(model[i])) {
      if (!Array.isArray(res[i])) {
        console.error('Invalid type of response or data model property');
        res[i] = [];
      }
      if (model[i].length > 0) {
        for (let j = 0; j < res[i].length; j++) {
          res[i][j] = assignObject(res[i][j], model[i][0]);
        }
      }
      continue;
    }

    if (model[i] && typeof model[i] === 'object') {
      if (typeof res[i] !== 'object') {
        console.error('Invalid type of response or data model property');
        res[i] = {};
      }
      res[i] = assignObject(res[i], model[i]);
    }

    // Other types (string, number, boolean...) => keep current value
  }

  return res;
}

function getAssignValue(modelValue) {
  if (Array.isArray(modelValue)) {
    return [];
  }
  if (typeof modelValue === 'object' && !!modelValue) {
    const obj = JSON.parse(JSON.stringify(modelValue));
    Object.keys(obj).forEach(key => {
      obj[key] = getAssignValue(obj[key]);
    });
    return obj;
  }
  return modelValue;
}


export default ResponseHandler;