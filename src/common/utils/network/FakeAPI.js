import Nprogress from 'nprogress';

const FakeAPI = (responseData, timeout = 500) => {
  const response = {
    data: responseData,
    metadata: {
      code: 200,
      message: 'Success',
      message_code: 'SUCCESS',
      error_fields: []
    }
  };
  Nprogress.start();
  return new Promise(resolve => {
    setTimeout(() => {
      Nprogress.done();
      resolve(response);
    }, timeout);
  })
};

export default FakeAPI;
