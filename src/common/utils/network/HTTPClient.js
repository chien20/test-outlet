import axios from 'axios';
import Nprogress from 'nprogress';
import Store from '../../../redux/store';
import {logout, renewAccessTokenSuccess} from '../../../redux/actions/user';
import logger from '../../helpers/logger';

const REFRESH_TOKEN_URL = `${process.env.REACT_APP_API_URL}/users/refresh-token`;
const CACHE_EXPIRY_TIME = 30 * 60 * 1000; // Milliseconds

export class HTTPClient {
  /**
   * Is refreshing the access token?
   * @type {boolean}
   */
  static isRefreshingAccessToken = false;

  /**
   * List of callback to recall after renew access token success
   * @type {Array}
   */
  static subscribers = [];

  /**
   * Default configuration
   * @type {*}
   */
  static defaultConfig = {
    __showLoading: true,
    __isRetry: false,
    __auth: true,
    __cache: false,
    __ignoreMetadata: false,
    __injectTimestamp: true,
  };

  /**
   * The constructor
   */
  constructor() {
    const axiosInstance = axios.create();
    axiosInstance.interceptors.request.use(this.handleRequest);
    axiosInstance.interceptors.response.use(this.handleSuccess, this.handleError);
    this.axiosInstance = axiosInstance;
  }

  /****************************************************************************************************************
   ************************************* PRIVATE METHODS **********************************************************
   ****************************************************************************************************************/

  handleRequest = (config) => {
    return config;
  };

  /**
   * Handle request success
   * @param response
   * @return {*}
   */
  handleSuccess = (response) => {
    const {data, config} = response;
    if (!data || !data.code || data.code !== 200) {
      if (config && config.__ignoreMetadata) {
        return data;
      }
      return Promise.reject(data);
    }
    return data;
  };

  /**
   * Handle request error
   * @param error
   * @return {Promise<>}
   */
  handleError = (error) => {
    const {config} = error;
    switch (error.response.status) {
      case 401:
        let refresh = false;

        if (!HTTPClient.isRefreshingAccessToken) {
          refresh = true;
        }

        return new Promise((resolve, reject) => {
          if (refresh) {
            this.handleRefreshAccessToken(error, reject);
          }

          HTTPClient.subscribers.push(access_token => {
            config.headers['Authorization'] = `Bearer ${access_token}`;
            resolve(this.axiosInstance({
              ...config,
              __isRetry: true
            }));
          });
        });
      default:
        break;
    }
    try {
      const logResponse = {...error?.response};
      const logConfig = {...error?.config};
      ['config', 'request'].forEach(key => {
        delete logResponse[key];
      });
      ['adapter', 'transformRequest', 'transformResponse', 'validateStatus'].forEach(key => {
        delete logConfig[key];
      });
      const messages = ['[API-ERROR]'];
      if (logResponse.status) {
        messages.push(`[${logResponse.status}]`);
      }
      if (logConfig.url) {
        messages.push(`[${logConfig.url}]`);
      }
      const err = new Error(error.message);
      err.name = messages.join('');
      logger.sendReport(err, {
        config: logConfig,
        response: logResponse,
      });
    } catch (e) {

    }
    return Promise.reject(error);
  };

  /**
   * Renew the access token
   * @return {*}
   */
  handleRefreshAccessToken = (originalError, reject) => {
    // Set flag
    HTTPClient.isRefreshingAccessToken = true;
    // Renew the token
    const user = Store.getState().user || {};
    if (!user || !user.auth || !user.auth.refresh_token) {
      this.handleLogout();
      reject(originalError);
    }
    return this.axiosInstance.post(this.getUrl(REFRESH_TOKEN_URL), {
      'refresh_token': user.auth.refresh_token,
      'access_token': user.auth.access_token
    }).then(response => {
      HTTPClient.isRefreshingAccessToken = false;
      Store.dispatch(renewAccessTokenSuccess(response.access_token, response.refresh_token));
      this.onAccessTokenFetched(response.data.access_token);
      return response;
    }).catch(() => {
      HTTPClient.isRefreshingAccessToken = false;
      this.handleLogout();
      reject(originalError);
    });
  };

  /**
   * Handle access token fetched successfully
   * @param access_token
   */
  onAccessTokenFetched = (access_token) => {
    HTTPClient.subscribers = HTTPClient.subscribers.filter(callback => callback(access_token));
  };

  /**
   * Handle logout
   */
  handleLogout = () => {
    Store.dispatch(logout());
  };

  /**
   * Build the request url
   * @param {string} url
   * @return {string}
   */
  getUrl = (url = '') => {
    return url;
  };

  /**
   * Build the config
   * @param config
   * @return {*}
   */
  getConfig = (config = {}) => {
    const requestConfig = {...HTTPClient.defaultConfig, ...config};

    if (!requestConfig.headers) {
      requestConfig.headers = {};
    }

    if (requestConfig.__auth) {
      const user = Store.getState().user || {};
      if (user && user.auth && user.auth.access_token) {
        requestConfig.headers['Authorization'] = `Bearer ${user.auth.access_token}`;
      }
    }

    // Process the url
    requestConfig['url'] = this.getUrl(config['url']);

    return requestConfig;
  };

  getCacheKey = (config) => {
    let objJsonStr = JSON.stringify(config);
    return 'cache_' + Buffer.from(objJsonStr).toString('base64');
  };

  setCache = (config, response) => {
    if (!config || !response) {
      return false;
    }
    const saveObj = {
      expiration: new Date().getTime() + CACHE_EXPIRY_TIME,
      response: response
    };
    sessionStorage.setItem(this.getCacheKey(config), JSON.stringify(saveObj));
  };

  getCache = (config) => {
    const value = sessionStorage.getItem(this.getCacheKey(config));
    let obj = null;
    try {
      obj = JSON.parse(value);
    } catch (e) {
      obj = {};
    }
    if (!obj) {
      return null;
    }
    const t = new Date().getTime();
    if (obj.expiration && obj.expiration < t) {
      return null;
    }
    return obj.response;
  };

  /****************************************************************************************************************
   ************************************* PUBLIC METHODS ***********************************************************
   ****************************************************************************************************************/

  /**
   * Make a request
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  request = (config = {}) => {
    config = this.getConfig(config);
    if (config.__cache) {
      const responseCached = this.getCache(config);
      if (responseCached) {
        return Promise.resolve(responseCached);
      }
    }
    if (config.__showLoading) {
      Nprogress.start();
    }
    return this.axiosInstance.request(config).then(response => {
      if (config.__cache && response) {
        this.setCache(config, response);
      }
      if (config.__showLoading) {
        Nprogress.done();
      }
      return response;
    }).catch(error => {
      if (config.__showLoading) {
        Nprogress.done();
      }
      return Promise.reject(error);
    });
  };

  /**
   * Make GET request
   * @param url
   * @param params
   * @param config
   * @return {AxiosPromise<any>}
   */
  get = (url, params = null, config = {}) => {
    if (!params) {
      params = {};
    }
    if (config.__injectTimestamp !== false) {
      params.t = new Date().getTime();
    }
    return this.request({
      method: 'get',
      url,
      params,
      ...config
    });
  };

  /**
   * Make POST request
   * @param url
   * @param data
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  post = (url, data = null, config = {}) => {
    return this.request({
      method: 'post',
      url,
      data,
      ...config
    });
  };

  /**
   * Make PUT request
   * @param url
   * @param data
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  put = (url, data = null, config = {}) => {
    return this.request({
      method: 'put',
      url,
      data,
      ...config
    });
  };

  /**
   * Make DELETE request
   * @param url
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  delete = (url, config = {}) => {
    return this.request({
      method: 'delete',
      url,
      ...config
    });
  };

  /**
   * Get AxiosCancelTokenSource to cancel a request
   * @return {CancelTokenSource}
   */
  static getCancelTokenSource = () => {
    const CancelToken = axios.CancelToken;
    return CancelToken.source();
  };
}

export default new HTTPClient();
