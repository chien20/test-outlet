import Store from '../../../redux/store';
import Nprogress from 'nprogress';

const XMLHTTPRequestClient = () => {
  const defaultConfig = {
    method: 'GET',
    responseType: 'json',
    __showLoading: true,
    __isRetry: false,
    __auth: true,
    __cache: false
  };

  const getConfig = (config = {}) => {
    return {...defaultConfig, ...config}
  };

  const getHeaders = (config = {}) => {
    const defaultHeader = {};
    if (config.__auth) {
      const user = Store.getState().user || {};
      if (user && user.auth && user.auth.access_token) {
        defaultHeader['Authorization'] = `Bearer ${user.auth.access_token}`;
      }
    }
    return {...defaultHeader, ...config.headers};
  };

  const getParamsString = (params) => {
    if (!params || typeof params !== 'object') {
      return '';
    }
    const str = [];
    const keys = Object.keys(params);
    keys.forEach(key => {
      str.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
    });
    return str.join('&');
  };

  const getFormData = (data) => {
    const formData = new FormData();
    if (data && typeof data === 'object') {
      const keys = Object.keys(data);
      keys.forEach(key => {
        formData.set(key, data[key]);
      });
    }
    return formData;
  };

  const request = (config = {}) => {
    config = getConfig(config || {});

    // Data
    const formData = getFormData(config.data || {});

    // Headers
    const headers = getHeaders(config);

    // Params
    const params = getParamsString(config.params);

    const request = new XMLHttpRequest();

    if (config.__showLoading) {
      Nprogress.start();
    }

    return new Promise(function (resolve, reject) {

      request.onreadystatechange = function () {
        // Only run if the request is complete
        if (request.readyState !== 4) {
          return;
        }

        if (config.__showLoading) {
          Nprogress.done();
        }

        // Process the response
        if (request.status >= 200 && request.status < 300) {
          // If successful
          resolve(request.response);
        } else {
          // If failed
          reject(request.response);
        }
      };

      request.open(config.method, `${config.url}${params ? `?${params}` : ''}`, true);

      request.responseType = config.responseType || 'json';

      const keys = Object.keys(headers);
      if (keys.length > 0) {
        keys.forEach(key => {
          request.setRequestHeader(key, headers[key]);
        });
      }

      request.send(formData);
    });
  };

  /**
   * Make GET request
   * @param url
   * @param params
   * @param config
   * @return {AxiosPromise<any>}
   */
  const get = (url, params = null, config = {}) => {
    return request({
      method: 'get',
      url,
      params,
      ...config
    });
  };

  /**
   * Make POST request
   * @param url
   * @param data
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  const post = (url, data = null, config = {}) => {
    return request({
      method: 'post',
      url,
      data,
      ...config
    });
  };

  /**
   * Make PUT request
   * @param url
   * @param data
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  const put = (url, data = null, config = {}) => {
    return request({
      method: 'put',
      url,
      data,
      ...config
    });
  };

  /**
   * Make DELETE request
   * @param url
   * @param config
   * @return {Promise<AxiosResponse<any>>}
   */
  const deleteHandler = (url, config = {}) => {
    return request({
      method: 'delete',
      url,
      ...config
    });
  };

  return {
    get,
    post,
    put,
    delete: deleteHandler
  }
};

export default new XMLHTTPRequestClient();
