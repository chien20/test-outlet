import {io} from 'socket.io-client';
import Store from '../../../redux/store';
import Broadcaster from '../../helpers/broadcaster';

const Socket = (() => {
  let socket = null;

  const connect = () => {
    if (socket) {
      disconnect();
    }

    socket = io(`${process.env.REACT_APP_SOCKET_URL}`, {
      path: '/sock',
    });

    socket.on('connect', function () {
      const user = Store.getState().user || {};
      if (!user || !user.auth || !user.auth.access_token) {
        disconnect();
        return;
      }
      socket.emit('authenticate', user.auth.access_token);
    });

    socket.onAny(function (event, data) {
      console.log(event, data);
      Broadcaster.broadcast(`socket.${event}`, data);
    });
  };

  const disconnect = () => {
    if (!socket) {
      return;
    }
    socket.disconnect();
    socket = null;
  };

  const emit = function () {
    if (!socket) {
      return;
    }
    socket.emit(...arguments);
  };

  return {
    connect,
    disconnect,
    emit,
  };
})();

export default Socket;
