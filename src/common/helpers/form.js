import $ from 'jquery';
import {getErrorMessage} from './language';

export const handleInputTextChanged = (key, setDataCallback, type = 'string', prevValue = undefined) => (event) => {
  let value = typeof event === 'string' ? event : event.target.value;
  if (type === 'int') {
    try {
      value = parseInt(value);
      if (Number.isNaN(value)) {
        value = '';
      }
    } catch (e) {
      value = prevValue !== undefined ? prevValue : 0;
    }
  } else if (type === 'float') {
    try {
      value = parseFloat(value);
      if (Number.isNaN(value)) {
        value = '';
      }
    } catch (e) {
      value = prevValue !== undefined ? prevValue : 0;
    }
  }
  if (setDataCallback && typeof setDataCallback === 'function') {
    setDataCallback({[key]: value});
  }
};

export const handleEditorChanged = (key, setDataCallback) => (value) => {
  setDataCallback({[key]: value});
};

export const handleRadioChanged = (key, setDataCallback, defaultValue = '') => (event) => {
  if (event && event.target) {
    const checked = event.target.checked;
    let inputValue = event.target.value;
    if (inputValue === 'false') {
      inputValue = false;
    }
    if (inputValue === 'true') {
      inputValue = true;
    }
    const value = checked ? inputValue : defaultValue;
    if (setDataCallback && typeof setDataCallback === 'function') {
      setDataCallback({[key]: value});
    }
  }
};

export const handleCheckBoxChanged = (key, setDataCallback, defaultValue = '', type = 'string') => (event) => {
  if (event && event.target) {
    const checked = event.target.checked;
    let value = checked ? event.target.value : defaultValue;
    if (type === 'int') {
      try {
        value = parseInt(value);
      } catch (e) {

      }
    } else if (type === 'float') {
      try {
        value = parseFloat(value);
      } catch (e) {

      }
    } else if (type === 'bool') {
      value = checked;
    }
    if (setDataCallback && typeof setDataCallback === 'function') {
      setDataCallback({[key]: value});
    }
  }
};

/**
 * Handle react select change
 * @param key
 * @param valueKey
 * @param setFormDataCallback
 * @param isMulti
 * @return {Function}
 */
export const handleSelectChange = (key, valueKey = 'id', setFormDataCallback, isMulti = false) => (selectedOption) => {
  if (isMulti) {
    if (selectedOption && Array.isArray(selectedOption)) {
      setFormDataCallback({[key]: selectedOption.map(item => item[valueKey])});
    } else {
      setFormDataCallback({[key]: []});
    }
    return;
  }
  setFormDataCallback({[key]: selectedOption[valueKey]});
};

export const checkDirtyFields = (data, initialData) => {
  const dirtyFields = {};
  const keys = Object.keys(initialData);
  keys.forEach(key => {
    if (initialData[key] !== data[key]) {
      dirtyFields[key] = true;
    }
  });
  return dirtyFields;
};

export const handleFieldErrors = (errorFields) => {
  if (!Array.isArray(errorFields)) {
    return;
  }
  let isFocused = false;
  errorFields.forEach(item => {
    if (item.field_name) {
      let el = $(`input[name=${item.field_name}]`);
      if (!el.length) {
        el = $(`select[name=${item.field_name}]`);
      }
      if (el.length) {
        const message = getErrorMessage(item.message_code || item.message, item.field_name);
        const parent = el.parent();
        const feedBackEl = parent.find('.invalid-feedback');
        el[0].setCustomValidity(message);
        el.closest('form').addClass('was-validated');
        if (feedBackEl.length === 0) {
          parent.append(`<div class="invalid-feedback">${message}</div>`);
        } else {
          feedBackEl.text(message);
        }
        if (!isFocused && el.scrollIntoView) {
          el.scrollIntoView();
        }
      }
    }
  });
};
