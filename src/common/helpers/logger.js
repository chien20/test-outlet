import * as Sentry from "@sentry/react";
import {Integrations} from "@sentry/tracing";

export default (function logger() {
  let sentryAvailable = false;

  const init = function () {
    if (process.env.REACT_APP_SENTRY_DSN) {
      sentryAvailable = true;
      Sentry.init({
        dsn: process.env.REACT_APP_SENTRY_DSN,
        integrations: [
          new Integrations.BrowserTracing(),
        ],
        tracesSampleRate: 1.0,
      });
    }
  };

  const sendReport = function (error, extra = undefined) {
    if (sentryAvailable) {
      Sentry.captureException(error, {
        extra,
      });
    }
  };

  init();

  return {
    sendReport,
  };
})();
