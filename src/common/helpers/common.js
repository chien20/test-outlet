import qs from 'qs';

export const publicUrl = (uri) => {
  return `${process.env.PUBLIC_URL}${uri}`;
};

export const imageUrl = (uri) => {
  if (!uri || `${uri}`.startsWith('https://') || `${uri}`.startsWith('http://')) {
    return uri;
  }
  return `${process.env.REACT_APP_API_URL}${uri}`;
};

export const uuidv4 = () => {
  let bytes = window.crypto.getRandomValues(new Uint8Array(32));
  const randomBytes = () => (bytes = bytes.slice(1)) && bytes[0];

  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    // eslint-disable-next-line no-mixed-operators
    (c ^ randomBytes() & 15 >> c / 4).toString(16)
  );
};

export function randomInt(min, max = 65535) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function timeDiff(time) {
  let result = '';
  const t = Date.parse(time);
  const now = new Date().getTime();
  const diff = Math.floor((now - t) / 1000);
  const months = Math.floor(diff / (60 * 60 * 24 * 30));
  if (months > 0) {
    result = `${months} tháng`;
  } else {
    const day = Math.floor(diff / (60 * 60 * 24));
    if (day > 0) {
      result = `${day} ngày`;
    } else {
      result = '';
    }
  }
  return result;
}

/**
 * Get file extension from file name
 * @param fileName
 * @return {*}
 */
export const getFileExt = (fileName) => {
  const arr = `${fileName}`.split('.');
  let ext = arr.pop();
  if (!ext) {
    return null;
  }
  return ext.toLowerCase();
};

/**
 * Get query params from location.search
 * @param search
 * @param options
 * @return {object}
 */
export const getQueryParams = (search = undefined, options = {}) => {
  if (search === undefined) {
    search = document.location.search;
  }
  return qs.parse(search, {ignoreQueryPrefix: true, ...options});
};

/**
 * Buid query string
 * @param params
 * @param options
 * @return {string|*}
 */
export const buildQueryString = (params, options = {}) => {
  return qs.stringify(params, {options: true, ...options});
};
