import {replaceAll} from './string';
import {format as formatDateFn} from 'date-fns';

export const numberAsCurrentcy = (num, defaultValue = '') => {
  if (num === undefined || num === null || !num.toLocaleString) {
    return defaultValue;
  }
  return num.toLocaleString();
};

export function convertToSlug(str = '') {
  if (typeof str !== 'string') {
    return '';
  }
  return str.toLowerCase()
  // eslint-disable-next-line
    .replace(/[\x00-\x2F\x3A-\x40\x5B-\x60\x7B-\xBF]+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

export function convertNotification(text = '', params = null, actions = null) {
  if (!text || typeof text !== 'string') {
    return '';
  }
  if (!params || typeof params !== 'object' || Object.keys(params).length === 0) {
    return text;
  }
  const keys = Object.keys(params);
  let html = text;
  keys.forEach(key => {
    if (key === 'username') {
      html = replaceAll(html, `{${key}}`, `<strong class="text-secondary">${params[key]}</strong>`);
      return;
    }
    if (key === 'order_code') {
      html = replaceAll(html, `{${key}}`, `<strong class="text-orange">${params[key]}</strong>`);
      return;
    }
    html = replaceAll(html, `{${key}}`, `<strong>${params[key]}</strong>`);
  });
  return html;
}

export function formatDate(dateStr, format = 'dd/MM/yyyy') {
  if (!dateStr) {
    return '';
  }
  const date = new Date(dateStr);
  return formatDateFn(date, format);
}