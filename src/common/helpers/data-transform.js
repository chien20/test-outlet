import {convertToSlug} from './format';
import {imageUrl, publicUrl} from './common';

const NO_IMAGE = publicUrl('/assets/images/no-image/256x256.png');

function getDiscount(original, current) {
  let discount_price = original - current;
  let discount = 0;
  if (discount_price > 0) {
    discount = Math.floor(discount_price * 100 / original);
  }
  return discount;
}

function getPrices(product) {
  let isFlashSale = false;

  const optionPriceMap = {};

  if (product.event && product.event.product_info) {
    isFlashSale = true;
    if (product.event.product_info.product_option_ids && product.event.product_info.product_option_ids.length) {
      product.event.product_info.product_option_ids.forEach((id, index) => {
        optionPriceMap[id] = product.event.product_info.price_options[index] || 0;
      });
    } else {
      product.price = product.event.product_info.price;
    }
  }

  let priceMin = product.price || 0;
  let priceMax = product.price || 0;
  let originalPriceMin = product.original_price || 0;
  let originalPriceMax = product.original_price || 0;
  let discountMin = getDiscount(originalPriceMin, priceMin);
  let discountMax = discountMin;

  if (product.options && product.options.length) {
    product.options.forEach(item => {
      if (optionPriceMap[item.id] !== undefined) {
        item.price = optionPriceMap[item.id];
      }

      if (item.price < priceMin) {
        priceMin = item.price || 0;
        originalPriceMin = item.original_price || 0;
      }
      if (item.price > priceMax) {
        priceMax = item.price || 0;
        originalPriceMax = item.original_price || 0;
      }
      const discount = getDiscount(item.original_price || 0, item.price || 0);
      if (discountMin === 0 && discount !== 0) {
        discountMin = discount;
      } else if (discount < discountMin && discount > 0) {
        discountMin = discount;
      }
      if (discount > discountMax) {
        discountMax = discount;
      }
    });
  }

  return {
    original: {
      min: originalPriceMin,
      max: originalPriceMax
    },
    price: {
      min: priceMin,
      max: priceMax
    },
    discount: {
      min: discountMin,
      max: discountMax
    },
    isFlashSale: isFlashSale
  };
}

export function transformProduct(product) {
  if (!product || typeof product !== 'object') {
    return product;
  }
  // Url
  const slug = product.slug ? product.slug : convertToSlug(product.name);
  product.url = `/${slug}/p${product.id}`;

  // Avatar
  product.avatar_url = NO_IMAGE;
  product.avatar_thumbnail_url = NO_IMAGE;
  if (product.images && product.images.length) {
    product.images.forEach(item => {
      item.url = imageUrl(item.url);
      item.thumbnail_url = imageUrl(item.thumbnail_url);
    });
    product.avatar_url = product.images[0].url;
    product.avatar_thumbnail_url = product.images[0].thumbnail_url;
  } else {
    product.images.push({
      id: null,
      url: NO_IMAGE,
      thumbnail_url: NO_IMAGE
    });
  }

  product.prices = getPrices(product);
}

export function transformProducts(products) {
  if (!products || !Array.isArray(products)) {
    return products;
  }
  products.forEach(product => {
    transformProduct(product);
  });
}
