export const replaceAll = function (str, search, replacement) {
  return str.replace(new RegExp(search, 'g'), replacement);
};

export const defaultSanitizeHTMLConfig = {
  allowedTags: ['img', 'br', 'p', 'span', 'ul', 'li', 'ol', 'div'],
  selfClosing: ['img', 'br'],
  allowedAttributes: {
    'img': ['src', 'alt']
  }
};

export const safeHTML = (html, config = defaultSanitizeHTMLConfig) => {
  const sanitizeHTML = window.sanitizeHtml;
  return sanitizeHTML(html, config);
};
