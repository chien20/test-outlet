import {closeAlertAC, showAlertAC} from '../../redux/actions/common';
import Store from '../../redux/store';

export const showAlert = (data) => {
  Store.dispatch(showAlertAC(data));
};

export const closeAlert = (id) => {
  Store.dispatch(closeAlertAC(id));
};