import {ERROR_CODES} from '../constants/errorCodes';

export const FIELD_NAME = {
  phone: 'Số điện thoại',
  email: 'Email'
};

export const getErrorMessage = (code, fieldName = '') => {
  if (!code || !ERROR_CODES[code]) {
    return code;
  }
  let msg = ERROR_CODES[code];
  if (fieldName && FIELD_NAME[fieldName]) {
    msg = `${FIELD_NAME[fieldName]} ${msg.toLowerCase()}`;
  }
  return msg;
};
