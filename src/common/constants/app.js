export const REGISTER_TYPES = {
  NORMAL: 1,
  FACEBOOK: 2,
  GOOGLE: 3
};

export const NOTIFICATION_ACTIONS = {
  OPEN_ORDER_DETAIL: 20,
  OPEN_ORDER_DETAIL_FOR_SHOP: 21,
  OPEN_GROUP_INVITATION: 55,
};

export const REGISTER_GROUP_STEPS = [
  {
    title: 'Thông tin hội nhóm'
  },
  {
    title: 'Thiết lập tỷ lệ điểm thưởng'
  },
  {
    title: 'Hoàn thành đăng ký'
  }
];

export const GROUP_PERMISSIONS_BY_NAME = {
  XEM_THONG_TIN_GIOI_THIEU: {
    id: 1,
    name: 'Xem thông tin giới thiệu',
    key: 'view_basic_info'
  },
  XEM_THONG_TIN_HOA_HONG: {
    id: 2,
    name: 'Xem thông tin hoa hồng',
    key: 'view_commission_info'
  },
  SUA_THONG_TIN_GIOI_THIEU: {
    id: 3,
    name: 'Sửa thông tin giới thiệu',
    key: 'edit_basic_info'
  },
  XEM_SAN_PHAM: {
    id: 4,
    name: 'Xem sản phẩm',
    key: 'view_product'
  },
  DANG_SAN_PHAM: {
    id: 5,
    name: 'Đăng sản phẩm',
    key: 'add_product'
  },
  KIEM_DUYET_SAN_PHAM: {
    id: 6,
    name: 'Kiểm duyệt sản phẩm',
    key: 'manage_product'
  },
  XEM_BAI_VIET: {
    id: 7,
    name: 'Xem bài viết',
    key: 'view_post'
  },
  DANG_BAI_VIET: {
    id: 8,
    name: 'Đăng bài viết',
    key: 'add_post'
  },
  BINH_LUAN: {
    id: 9,
    name: 'Bình luận',
    key: 'add_comment'
  },
  KIEM_DUYET_BINH_LUAN: {
    id: 10,
    name: 'Kiểm duyệt bình luận',
    key: 'manage_comment'
  },
  KIEM_DUYET_BAI_VIET: {
    id: 11,
    name: 'Kiểm duyệt bài viết',
    key: 'manage_post'
  },
  XEM_THANH_VIEN: {
    id: 12,
    name: 'Xem thành viên trong nhóm',
    key: 'view_member'
  },
  MOI_THANH_VIEN: {
    id: 13,
    name: 'Mời thành viên mới',
    key: 'invite_member'
  },
  KIEM_DUYET_THANH_VIEN: {
    id: 14,
    name: 'Kiểm duyệt thành viên',
    key: 'manage_member'
  },
  XEM_BAO_CAO_THONG_KE: {
    id: 15,
    name: 'Xem báo cáo, thống kê',
    key: 'view_stats'
  }
};

export const BUSINESS_TYPES = [
  {
    id: 0,
    name: 'Doanh nghiệp',
  },
  {
    id: 10,
    name: 'Hộ kinh doanh cá thể',
  },
  {
    id: 20,
    name: 'Cá nhân',
  },
];

export const IMAGE_EXTENSIONS = ['jpe', 'jpg', 'jpeg', 'png', 'gif'];
