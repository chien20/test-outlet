import React from 'react';
import {Link} from 'react-router-dom';
import HyperLink from '../HyperLink/HyperLink';
import {numberAsCurrentcy} from '../../common/helpers/format';
import './ProductTable.scss';

const ProductStatus = ({status}) => {
  switch (status) {
    case 0:
      return <span>Chờ phê duyệt</span>;
    case 10:
      return <span className="text-green">Đã phê duyệt</span>;
    case 30:
      return <span className="text-red">Đã bị từ chối</span>;
    default:
      return '';
  }
};

const ProductTableRow = ({product, handleDelete, handleSelect = null, isSelected = false}) => {
  if (!product) {
    return null;
  }
  const options = JSON.parse(JSON.stringify(product.options || []));
  const productImageUrl = product.images && product.images.length ? product.images[0].thumbnail_url : '';
  const productImage = productImageUrl ?
    <img className="product-image" src={productImageUrl} alt={product.name}/> : '';
  if (options.length === 0) {
    return (
      <tr>
        {
          handleSelect &&
          <td>
            <input type="checkbox" checked={isSelected} onChange={handleSelect(product.id)}/>
          </td>
        }
        <td className="text-left">
          <div className="product-name-wrapper">
            <div className="product-image-wrapper">
              {productImage}
            </div>
            <div className="product-name">{product.name}</div>
          </div>
        </td>
        <td>-</td>
        <td>{product.sku || ''}</td>
        <td>{numberAsCurrentcy(product.price || 0)}đ</td>
        <td>{product.qty || 0}</td>
        <td><ProductStatus status={product.status}/></td>
        {
          handleDelete &&
          <td>
            <div className="action-buttons">
              <Link to={`/user/store/products/update/${product.id}`} className="text-success"><i
                className="fas fa-edit"/></Link>
              <HyperLink onClick={handleDelete(product)} className="text-danger"><i
                className="fas fa-trash"/></HyperLink>
            </div>
          </td>
        }
      </tr>
    );
  }
  if (options && options.length && product.option_groups && product.option_groups.length) {
    options.forEach(item => {
      if (item.groups_index && item.groups_index.length) {
        if (item.groups_index.length === 1 && product.option_groups[0].values) {
          item.name = product.option_groups[0].values[item.groups_index[0]];
        } else if (item.groups_index.length === 2
          && product.option_groups[0].values
          && product.option_groups[1].values) {
          item.name = `${product.option_groups[0].values[item.groups_index[0]]} - ${product.option_groups[1].values[item.groups_index[1]]}`;
        }
      }
    });
  }
  const rows = [];
  const count = options.length;
  const firstOption = options.shift();
  let i = 0;
  rows.push(
    <tr key={i}>
      {
        handleSelect &&
        <td rowSpan={count}>
          <input type="checkbox" checked={isSelected} onChange={handleSelect(product.id)}/>
        </td>
      }
      <td className="text-left" rowSpan={count}>
        <div className="product-name-wrapper">
          <div className="product-image-wrapper">
            {productImage}
          </div>
          <div className="product-name">{product.name}</div>
        </div>
      </td>
      <td>{firstOption.name}</td>
      <td>{firstOption.sku || ''}</td>
      <td>{numberAsCurrentcy(firstOption.price || 0)}đ</td>
      <td>{firstOption.qty || 0}</td>
      <td rowSpan={count}><ProductStatus status={product.status}/></td>
      {
        handleDelete &&
        <td rowSpan={count}>
          <div className="action-buttons">
            <Link to={`/user/store/products/update/${product.id}`} className="text-success"><i
              className="fas fa-edit"/></Link>
            <HyperLink onClick={handleDelete(product)} className="text-danger"><i
              className="fas fa-trash"/></HyperLink>
          </div>
        </td>
      }
    </tr>
  );
  i++;
  options.forEach(item => {
    rows.push(
      <tr key={i}>
        <td>{item.name}</td>
        <td>{item.sku || ''}</td>
        <td>{numberAsCurrentcy(item.price || 0)}đ</td>
        <td>{item.qty || 0}</td>
      </tr>
    );
    i++;
  });
  return rows;
};

const ProductTable = ({productList, handleDelete = null, handleSelect = null, selectedIds = []}) => {
  let isCheckAll = false;
  if (handleSelect) {
    const ids = {};
    selectedIds.forEach(id => {
      ids[id] = true;
    });
    let hasUnselect = false;
    productList.forEach(product => {
      if (!ids[product.id]) {
        hasUnselect = true;
      }
    });
    isCheckAll = !hasUnselect;
  }
  return (
    <div className="product-table table-responsive select-products-table">
      <table className="table table-bordered">
        <thead>
        <tr>
          {
            handleSelect &&
            <th>
              <input type="checkbox" checked={isCheckAll} onChange={handleSelect(null, true)}/>
            </th>
          }
          <th>Tên sản phẩm</th>
          <th>Phân loại hàng</th>
          <th>SKU</th>
          <th>Giá</th>
          <th>Số lượng</th>
          <th>Trạng thái</th>
          {
            handleDelete &&
            <th>Sửa</th>
          }
        </tr>
        </thead>
        <tbody>
        {
          productList.map((item, index) => (
            <ProductTableRow
              key={index}
              product={item}
              handleDelete={handleDelete}
              handleSelect={handleSelect}
              isSelected={selectedIds.indexOf(item.id) >= 0}/>
          ))
        }
        </tbody>
      </table>
    </div>
  );
};

export default ProductTable;
