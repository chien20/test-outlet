import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Modal} from 'react-bootstrap';
import {getProductListAPI} from '../../api/products';
import {showAlert} from '../../common/helpers';
import Pagination from '../../components/Pagination/Pagination';
import ProductTable from './ProductTable';
import Store from '../../redux/store/Store';

const PAGE_SIZE = 5;

class SelectProductModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isLoading: false,
      selectedIds: [],
      productList: [],
      totalItems: 0,
      page: 0
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
    this.resolver = null;
    this.unmounted = false;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.isOpen !== this.state.isOpen && this.state.isOpen) {
      this.getListProduct();
    }
  }

  getListProduct() {
    const user = Store.getState().user.info;
    const {keyword, page} = this.state;
    const params = {
      type: 'own',
      store_id: user.store_id,
      current_page: page,
      page_size: PAGE_SIZE,
      q: keyword || ''
    };
    this.setState({
      isLoading: true
    });
    getProductListAPI(params, {__auth: true}).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách sản phẩm!`
      });
    });
  }

  handleOpen = (data = {}, onClose = null) => {
    if (this.unmounted) {
      return;
    }
    this.resolver = onClose;
    const s = JSON.parse(JSON.stringify(this.defaultState));
    this.setState({
      s,
      isOpen: true,
      ...data
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  onPageChanged = (page) => {
    this.setState({
      page: page - 1
    }, this.getListProduct);
  };

  handleSelectAll = (checked) => {
    this.setState(prevState => {
      const {productList} = prevState;
      const selectedIds = [...prevState.selectedIds];
      if (checked) {
        productList.forEach(product => {
          if (selectedIds.indexOf(product.id) < 0) {
            selectedIds.push(product.id);
          }
        });
      } else {
        productList.forEach(product => {
          const index = selectedIds.indexOf(product.id);
          if (index >= 0) {
            selectedIds.splice(index, 1);
          }
        });
      }
      return {
        selectedIds
      };
    });
  };

  handleSelect = (productId, isCheckAll = false) => (event) => {
    const checked = event.target.checked;
    if (isCheckAll) {
      return this.handleSelectAll(checked);
    }
    this.setState(prevState => {
      const selectedIds = [...prevState.selectedIds];
      if (checked) {
        if (selectedIds.indexOf(productId) < 0) {
          selectedIds.push(productId);
        } else {
          return null;
        }
      } else {
        if (selectedIds.indexOf(productId) >= 0) {
          const index = selectedIds.indexOf(productId);
          selectedIds.splice(index, 1);
        } else {
          return null;
        }
      }
      return {
        selectedIds
      };
    });
  };

  handleSubmit = () => {
    const {selectedIds} = this.state;
    if (this.resolver) {
      this.resolver(selectedIds);
    }
    this.handleClose();
  };

  render() {
    const {isOpen, isLoading, productList, totalItems, selectedIds, page} = this.state;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <Modal size="lg"
             backdrop="static"
             show={isOpen}
             onHide={this.handleClose}
             className="select-products-modal"
             centered>
        <Modal.Header closeButton>
          <Modal.Title>Đăng sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            !productList.length && !isLoading &&
            <p>Bạn chưa có sản phẩm nào!&nbsp;
              <Link to="/user/store/products/upload">Click vào đây</Link> để upload sản phẩm mới!</p>
          }
          {
            !!productList.length &&
            <Fragment>
              <ProductTable
                productList={productList}
                handleDelete={this.handleDelete}
                selectedIds={selectedIds}
                handleSelect={this.handleSelect}/>
              <div className="bottom">
                <div className="item-pagination hidden-mobile">
                  {
                    totalPage > 1 &&
                    <Pagination
                      totalPages={totalPage}
                      currentPage={page + 1}
                      onPageChanged={this.onPageChanged}/>
                  }
                </div>
              </div>
            </Fragment>
          }
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-default"
            onClick={this.handleClose}
            disabled={isLoading}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}>Xong
          </button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default SelectProductModal;
