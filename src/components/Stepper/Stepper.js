import React from 'react';
import './Stepper.scss';

class Stepper extends React.PureComponent {
  render() {
    const {steps, activeStep} = this.props;
    return (
      <div className="stepper-horizontal">
        {
          steps.map((item, index) => (
            <div className={`md-step ${index <= activeStep && 'active'}`} key={index}>
              <div className="md-step-circle"><span>{index + 1}</span></div>
              <div className="md-step-title">{item.title}</div>
              <div className="md-step-bar-left"/>
              <div className="md-step-bar-right"/>
            </div>
          ))
        }
      </div>

    )
  }
}

export default Stepper;