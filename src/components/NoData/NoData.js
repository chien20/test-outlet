import React from 'react';
import './NoData.scss';
import cartIcon from '../../assets/images/icons/icon-empty.svg';

const NoData = ({children, title = 'Không có dữ liệu', description = ''}) => {
  return (
    <div className="no-data">
      <div className="img-wrap">
        <img src={cartIcon} alt={title}/>
      </div>
      {title && <p className="title">{title}</p>}
      {description && <p className="description">{description}</p>}
      {children}
    </div>
  )
};

export default NoData;
