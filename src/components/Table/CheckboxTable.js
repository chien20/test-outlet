import React from 'react';
import checkboxHOC from 'react-table/lib/hoc/selectTable';
import CheckboxTablePropTypes from './propTypes/CheckboxTablePropTypes';
import CommonTable from './CommonTable';

const CheckboxTableContainer = checkboxHOC(CommonTable);

class CheckboxTable extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selection: [],
      selectAll: false
    };
    this.checkboxTable = React.createRef();
  }

  componentDidUpdate(prevProps, prevState, context) {
    if (prevState.selection !== this.state.selection || prevState.selectAll !== this.state.selectAll) {
      this.onSelectChanged(this.state.selection, this.state.selectAll);
    }
  }

  onSelectChanged = (selection, selectAll) => {
    const {onSelectChanged} = this.props;
    if (onSelectChanged) {
      onSelectChanged(selection, selectAll);
    }
  };

  toggleSelection = (id, shift, row) => {
    const {keyField, data} = this.props;
    if (!row) {
      return;
    }
    const key = row[keyField];
    const {selection: prevSelection} = this.getSelection();
    // start off with the existing state
    let selection = [...prevSelection];
    let selectAll = true;
    const keyIndex = selection.indexOf(key);
    // check to see if the key exists
    if (keyIndex >= 0) {
      // it does exist so we will remove it using destructing
      selection = [
        ...selection.slice(0, keyIndex),
        ...selection.slice(keyIndex + 1)
      ];
      selectAll = false; // Uncheck "Select all"
    } else {
      // it does not exist so add it
      selection.push(key);
      // Check select all state
      data.forEach(item => {
        if (selection.indexOf(item[keyField]) < 0) {
          selectAll = false;
        }
      });
    }
    // update the state
    this.setSelection(selection, selectAll);
  };

  toggleAll = () => {
    /*
      'toggleAll' is a tricky concept with any filterable table
      do you just select ALL the records that are in your data?
      OR
      do you only select ALL the records that are in the current filtered data?

      The latter makes more sense because 'selection' is a visual thing for the user.
      This is especially true if you are going to implement a set of external functions
      that act on the selected information (you would not want to DELETE the wrong thing!).

      So, to that end, access to the internals of ReactTable are required to get what is
      currently visible in the table (either on the current page or any other page).

      The HOC provides a method call 'getWrappedInstance' to get a ref to the wrapped
      ReactTable and then get the internal state and the 'sortedData'.
      That can then be iterrated to get all the currently visible records and set
      the selection state.
    */
    const {keyField} = this.props;
    const {selectAll: prevSelectAll} = this.getSelection();
    const selectAll = !prevSelectAll;
    const selection = [];
    if (selectAll) {
      // we need to get at the internals of ReactTable
      const wrappedInstance = this.checkboxTable.current.getWrappedInstance();
      // the 'sortedData' property contains the currently accessible records based on the filter and sort
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      // we just push all the IDs onto the selection array
      currentRecords.forEach(item => {
        selection.push(item._original[keyField]);
      });
    }
    this.setSelection(selection, selectAll);
  };

  isSelected = key => {
    /*
      Instead of passing our external selection state we provide an 'isSelected'
      callback and detect the selection state ourselves. This allows any implementation
      for selection (either an array, object keys, or even a Javascript Set object).
    */
    if (this.props.isSelected) {
      return this.props.isSelected(key);
    }
    const {selection} = this.getSelection();
    return selection.includes(key);
  };

  getSelection = () => {
    if (this.props.selection !== undefined) {
      return {
        selection: this.props.selection,
        selectAll: this.props.selectAll
      };
    }
    return {
      selection: this.state.selection,
      selectAll: this.state.selectAll
    };
  };

  setSelection = (selection, selectAll) => {
    if (this.props.selection !== undefined) {
      return this.props.onSelectChange(selection, selectAll);
    }
    this.setState({
      selection,
      selectAll
    });
  };

  render() {
    const {selectAll} = this.getSelection();
    return (
      <CheckboxTableContainer
        ref={this.checkboxTable}
        className="common-table"
        selectAll={selectAll}
        isSelected={this.isSelected}
        toggleSelection={this.toggleSelection}
        toggleAll={this.toggleAll}
        {...this.props}/>
    )
  }
}

CheckboxTable.propTypes = CheckboxTablePropTypes;

CheckboxTable.defaultProps = {
  keyField: 'id'
};

export default CheckboxTable;
