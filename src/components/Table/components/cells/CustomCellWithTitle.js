import React from 'react';

const CustomCellWithTitle = ({title, content, className = ''}) => {
  return (
    <div className={`custom-cell-with-title ${className}`}>
      <div className="cell-title">{title}</div>
      <div className="cell-content">{content}</div>
    </div>
  );
};

export default CustomCellWithTitle;