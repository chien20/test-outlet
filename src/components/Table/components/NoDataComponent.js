import React, {Fragment} from 'react';

const NoDataComponent = ({children}) => {
  return (
    <Fragment>
      {
        children &&
        <div className="rt-noData">
          {children}
        </div>
      }
    </Fragment>
  )
};

export default NoDataComponent;
