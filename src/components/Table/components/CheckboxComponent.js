import React from 'react';

const onInputChange = () => {

};

const onClickInput = (e) => {
  e.stopPropagation();
};

const onClickLabel = (props) => (e) => {
  e.stopPropagation();
  if (props.onClick) {
    const {shiftKey} = e;
    props.onClick(props.id, shiftKey, props.row);
  }
};

const CheckboxComponent = props => {
  return (
    <div className="checkbox">
      <label>
        <input
          type="checkbox"
          checked={props.checked}
          onChange={onInputChange}
          onClick={onClickInput}/>
        <span
          onClick={onClickLabel}/>
      </label>
    </div>
  )
};

export default CheckboxComponent;
