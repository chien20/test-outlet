import React from 'react';
import ReactTable from 'react-table';
import CommonTablePropTypes from './propTypes/CommonTablePropTypes';
import 'react-table/react-table.css';
import './CommonTable.scss';

const CommonTable = React.forwardRef((props, ref) => (
  <ReactTable
    ref={ref}
    {...props}
    className={`common-table ${props.className ? props.className : ''}`}/>
));

CommonTable.propTypes = CommonTablePropTypes;

CommonTable.defaultProps = {
  keyField: 'id',
  defaultPageSize: 10,
  pageSizeOptions: [10, 20, 50, 100],
  rowsText: 'hàng',
  ofText: '/',
  pageText: 'Trang',
  noDataText: 'Không có dữ liệu',
  nextText: 'Trang sau',
  previousText: 'Trang trước',
  pageJumpText: 'Đi đến trang',
  rowsSelectorText: 'hàng mỗi trang',
  sortable: false,
  minRows: 2,
};

export default CommonTable;
