import React from 'react';
import PropTypes from 'prop-types';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {isMobile} from '../../common/helpers/browser';
import {publicUrl} from '../../common/helpers';

const RenderButton = (customProps) => (props) => {
  const isDisabled = customProps.isDisabled || props.isDisabled || props.isProcessing || !props.isSdkLoaded;
  if (isMobile) {
    return  (
      <button className="btn btn-facebook" onClick={props.onClick} disabled={isDisabled}>
        <img src={publicUrl(`./assets/images/icons/socials/facebook.svg`)} alt="facebook"/>
      </button>
    )
  }
  return (
    <button className="btn btn-facebook" onClick={props.onClick} disabled={isDisabled}>
      <i className="icon fab fa-facebook-f"/> {customProps.text}
    </button>
  );
};

class ButtonFacebookLogin extends React.PureComponent {
  render() {
    const {callback, onClick, ...others} = this.props;
    return (
      <FacebookLogin
        appId={process.env.REACT_APP_FACEBOOK_APP_ID}
        autoLoad={false}
        fields="name,email,picture"
        scope="public_profile,email"
        onClick={onClick}
        language="vi_VN"
        responseType="code"
        render={RenderButton(others)}
        callback={callback}
      />
    )
  }
}

ButtonFacebookLogin.propTypes = {
  text: PropTypes.string,
  callback: PropTypes.func,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
};

ButtonFacebookLogin.defaultProps = {
  text: 'Đăng nhập với Facebook'
};

export default ButtonFacebookLogin;
