import React from 'react';
import GoogleLogin from 'react-google-login';
import PropTypes from 'prop-types';
import {isMobile} from '../../common/helpers/browser';
import {publicUrl} from '../../common/helpers';

const RenderButton = (customProps) => (props) => {
  if (isMobile) {
    return (
      <button className="btn btn-google" onClick={props.onClick} disabled={customProps.isDisabled || props.disabled}>
        <img src={publicUrl(`./assets/images/icons/socials/google.svg`)} alt="google"/>
      </button>
    );
  }
  return (
    <button className="btn btn-google" onClick={props.onClick} disabled={customProps.isDisabled || props.disabled}>
      <i className="icon fab fa-google"/> {customProps.text}
    </button>
  );
};

class ButtonGoogleLogin extends React.PureComponent {
  onFailure = (res) => {
    const {onFailure} = this.props;
    if (res?.error === 'idpiframe_initialization_failed') {
      return;
    }
    if (onFailure) {
      onFailure(res);
    }
  };

  render() {
    const {onSuccess, onFailure, ...rest} = this.props;
    return (
      <GoogleLogin
        autoLoad={false}
        clientId={process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID}
        onSuccess={onSuccess}
        onFailure={this.onFailure}
        render={RenderButton(rest)}
        responseType="token"
        scope="profile email"
        cookiePolicy="single_host_origin"
      />
    );
  }
}

ButtonGoogleLogin.propTypes = {
  text: PropTypes.string,
  onSuccess: PropTypes.func,
  onFailure: PropTypes.func,
  isDisabled: PropTypes.bool,
};

ButtonGoogleLogin.defaultProps = {
  text: 'Đăng nhập với Google'
};

export default ButtonGoogleLogin;
