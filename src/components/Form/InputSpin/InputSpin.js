import React from 'react';
import PropTypes from 'prop-types';
import './InputSpin.scss';

const onChangeDefault = () => {

};

class InputSpin extends React.PureComponent {
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.max !== this.props.max || prevProps.min !== this.props.min) {
      this.correctValue();
    }
  }

  correctValue = () => {
    const {min, max} = this.props;
    const value0 = this.getValue(this.props.value);
    let value = value0;
    if (value < min) {
      value = min;
    }
    if (value > max) {
      value = max;
    }
    if (value !== value0) {
      this.props.onChange(value);
    }
  };

  handleInputChange = (event) => {
    const {isDisabled, min, max} = this.props;
    if (isDisabled) {
      return;
    }
    let value = this.getValue(event.target.value);
    if (value < min) {
      value = min;
    }
    if (value > max) {
      value = max;
    }
    this.props.onChange(value);
  };

  getValue = (value) => {
    if (!value) {
      return 0;
    }
    if (typeof value === 'number') {
      return value;
    }
    if (typeof value === 'string' && !isNaN(value)) {
      return parseInt(value);
    }
    return 0;
  };

  handleIncrease = () => {
    const {isDisabled, max, onChange} = this.props;
    if (isDisabled) {
      return;
    }
    const value = this.getValue(this.props.value);
    const nextValue = value < max ? value + 1 : max;
    onChange(nextValue);
  };

  handleDecrease = () => {
    const {isDisabled, min, onChange} = this.props;
    if (isDisabled) {
      return;
    }
    const value = this.getValue(this.props.value);
    const nextValue = value > min ? value - 1 : min;
    onChange(nextValue);
  };

  render() {
    const {value, min, max, isDisabled} = this.props;
    return (
      <div className={`input-spin ${isDisabled ? 'disabled' : ''}`}>
        <div className="input-group">
          <div className="input-group-prepend">
            <button className="btn" type="button" onClick={this.handleDecrease}>-</button>
          </div>
          <input
            type="text"
            className="form-control no-spin"
            value={value}
            onChange={this.handleInputChange}
            min={min}
            max={max}
            disabled={isDisabled}/>
          <div className="input-group-append">
            <button className="btn" type="button" onClick={this.handleIncrease}>+</button>
          </div>
        </div>
      </div>
    )
  }
}

InputSpin.propTypes = {
  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  onChange: PropTypes.func,
  isDisabled: PropTypes.bool
};

InputSpin.defaultProps = {
  value: 1,
  min: 1,
  max: 1000,
  isDisabled: false,
  onChange: onChangeDefault
};

export default InputSpin;
