import React from 'react';
import ReactSelect from 'react-select';
import PropTypes from 'prop-types';
import ReactDom from 'react-dom';

class Select extends React.PureComponent {
  constructor(props) {
    super(props);
    this.elRef = React.createRef();
  }

  componentDidMount() {
    this.autoFocus();
  }

  autoFocus = () => {
    if (!this.props.autoFocus) {
      return;
    }
    if (this.elRef.current && this.elRef.current) {
      const el = ReactDom.findDOMNode(this.elRef.current);
      if (el && el.focus) {
        setTimeout(() => {
          el.focus();
        });
      }
    }
  };

  getOptionValue = (option) => {
    const {optionValueKey} = this.props;
    return option ? option[optionValueKey] : '';
  };

  getOptionLabel = (option) => {
    const {optionLabelKey} = this.props;
    return option ? option[optionLabelKey] : '';
  };

  render() {
    const {value, options, onChange, className, isSearchable, placeholder, isDisabled, ...others} = this.props;
    return (
      <ReactSelect
        ref={this.elRef}
        value={value}
        options={options}
        onChange={onChange}
        classNamePrefix='cf'
        placeholder={placeholder}
        isDisabled={isDisabled}
        getOptionLabel={this.getOptionLabel}
        getOptionValue={this.getOptionValue}
        isSearchable={isSearchable}
        className={`${className}`}
        {...others}/>
    )
  }
}

Select.propTypes = {
  value: PropTypes.any,
  options: PropTypes.array,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  isDisabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
  autoFocus: PropTypes.bool,
  menuPosition: PropTypes.string,
  maxMenuHeight: PropTypes.number,
  optionLabelKey: PropTypes.string,
  optionValueKey: PropTypes.string,
  className: PropTypes.string
};

Select.defaultProps = {
  className: '',
  placeholder: '',
  isDisabled: false,
  isSearchable: true,
  autoFocus: false,
  optionLabelKey: 'name',
  optionValueKey: 'id'
};

export default Select;
