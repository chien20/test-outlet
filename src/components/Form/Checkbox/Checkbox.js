import React, {useState} from 'react';
import {uuidv4} from '../../../common/helpers';

const Checkbox = ({checked = true, className = '', label, onChange}) => {
  const [id] = useState(uuidv4());
  return (
    <div className={`custom-control custom-checkbox ${className}`}>
      <input
        type="checkbox"
        className="custom-control-input"
        id={id}
        checked={checked}
        onChange={onChange}/>
      <label
        className="custom-control-label"
        htmlFor={id}>
        {label}
      </label>
    </div>
  )
};

export default Checkbox;
