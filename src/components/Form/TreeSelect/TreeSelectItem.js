import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

class TreeSelectItem extends React.PureComponent {
  render() {
    const {
      options,
      selectedItem,
      selectedPath,
      onChange,
      depth,
      rootLabel,
      childLabel,
      wrapperComponent: Wrapper,
      ...others
    } = this.props;
    let value = null;
    if (selectedItem && selectedPath) {
      value = options.find(item => selectedPath.indexOf(item.id) >= 0);
    }
    return (
      <Fragment>
        <Wrapper
          value={value}
          depth={depth}
          rootLabel={rootLabel}
          childLabel={childLabel}>
          <Select
            value={value}
            options={options}
            onChange={onChange}
            {...others}/>
        </Wrapper>
        {
          value && value.children && !!value.children.length &&
          <TreeSelectItem
            {...this.props}
            key={`${value.id}`}
            options={value.children}
            depth={depth + 1}/>
        }
      </Fragment>
    )
  }
}

TreeSelectItem.propTypes = {
  options: PropTypes.array,
  selectedItem: PropTypes.any,
  selectedPath: PropTypes.array,
  onChange: PropTypes.func,
  depth: PropTypes.number,
  rootLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  childLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  wrapperComponent: PropTypes.any
};

export default TreeSelectItem;