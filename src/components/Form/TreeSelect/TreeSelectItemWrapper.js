import React from 'react';

const TreeSelectItemWrapper = ({depth, rootLabel, childLabel, children}) => (
  <div className="form-row">
    <div className="form-group">
      <label>{!depth ? rootLabel : childLabel}</label>
      {
        children
      }
    </div>
  </div>
);

export default TreeSelectItemWrapper;