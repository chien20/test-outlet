import React from 'react';
import PropTypes from 'prop-types';
import TreeSelectItem from './TreeSelectItem';
import TreeSelectItemWrapper from './TreeSelectItemWrapper';

class TreeSelect extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null,
      selectedPath: [],
      treeMap: {}
    }
  }

  componentWillMount() {
    this.buildMap(this.props.options);
  }

  componentDidMount() {
    this.onSelectNode(this.props.value);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.options !== nextProps.options) {
      this.buildMap(nextProps.options);
    }
    if (this.props.value !== nextProps.value) {
      if (nextProps.value !== this.state.selectedItem) {
        this.onSelectNode(nextProps.value);
      }
    }
  }

  buildMap = (options) => {
    const treeMap = {};
    const tree = options || [];
    tree.forEach(item => {
      this.traverseTree(item, treeMap);
    });
    this.setState({
      treeMap
    });
  };

  traverseTree = (node, treeMap = {}, path = []) => {
    if (!node) {
      return;
    }
    treeMap[node.id] = {
      path: [...path, node.id],
      item: node
    };
    if (!Array.isArray(node.children) || !node.children.length) {
      return;
    }
    node.children.forEach(child => {
      this.traverseTree(child, treeMap, treeMap[node.id].path);
    });
  };

  onSelectNode = (selectedOption) => {
    const {onChange} = this.props;
    if (!selectedOption || !this.state.treeMap[selectedOption.id]) {
      this.setState({
        selectedItem: null,
        selectedPath: []
      }, () => {
        if (onChange) {
          onChange(this.state.selectedItem);
        }
      });
      return;
    }
    this.setState({
      selectedItem: this.state.treeMap[selectedOption.id].item, // Make sure this item is in the options list instead of trust from props
      selectedPath: this.state.treeMap[selectedOption.id].path
    }, () => {
      if (onChange) {
        const {value} = this.props;
        const {selectedItem} = this.state;
        if (value && selectedItem && value.id === selectedItem.id) {
          return;
        }
        onChange(this.state.selectedItem);
      }
    });
  };

  render() {
    const {selectedItem, selectedPath} = this.state;
    const {options, value, onChange, ...others} = this.props;
    return (
      <TreeSelectItem
        key={'root'}
        options={options}
        selectedItem={selectedItem}
        selectedPath={selectedPath}
        onChange={this.onSelectNode}
        depth={0}
        {...others}/>
    )
  }
}

TreeSelect.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
  rootLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  childLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  wrapperComponent: PropTypes.any
};

TreeSelect.defaultProps = {
  rootLabel: '',
  childLabel: '',
  wrapperComponent: TreeSelectItemWrapper
};

export default TreeSelect;
