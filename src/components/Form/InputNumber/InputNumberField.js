import React from 'react';
import {useField} from 'formik';
import InputNumber from './InputNumber';

const InputNumberField = ({name, ...others}) => {
  const [field, meta, helpers] = useField(name);

  const {value} = meta;
  const {setValue} = helpers;

  return (
    <InputNumber {...others} value={value} onChange={setValue}/>
  )
};

export default InputNumberField;
