import React from 'react';

/**
 * Format to display
 * Input: 100000.1
 * Output: 100.000,1
 * @param value
 * @returns {string}
 */
export function formatCurrency(value) {
  if (!value && value !== 0 && value !== '0') {
    return '';
  }
  const arr = `${value}`.split('.');
  arr[0] = arr[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return arr.join(',');
}

/**
 * Convert to standard number format
 * Input: 100.000,1
 * Output: 100000.1
 * @param onChange
 * @returns {function(...[*]=)}
 */
export const handleInputCurrencyChange = (onChange) => (value) => {
  const arr = `${value}`.split(',');
  arr[0] = arr[0].replace(/(\.*)/g, '');
  if (arr[0]) {
    arr[0] = `${arr[0] * 1}`;
  }
  onChange(arr.join('.'));
};

const InputNumber = ({value, onChange, ...others}) => {
  const handleChange = React.useCallback((event) => {
    handleInputCurrencyChange(onChange)(event.target.value || '');
  }, [onChange]);

  return (
    <input
      type="text"
      {...others}
      value={formatCurrency(value)}
      onChange={handleChange}
    />
  );
};

export default InputNumber;
