import React from 'react';
import PropTypes from 'prop-types';
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Autoplay, EffectFade} from 'swiper';
import 'swiper/swiper.scss';

// setting slider banner shop
SwiperCore.use([Autoplay]);
SwiperCore.use([EffectFade]);

const params = {
  slidesPerView: 1,
  spaceBetween: 30,
  loop: true,
  autoplay: {
    delay: 6000,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  }
};

class ESwiper extends React.PureComponent {
  render() {
    const {sliders, className} = this.props;
    return (
      <Swiper
        className={className}
        effect="fade"
        {...params}
      >
        {
          sliders.map((item, index) =>
            <SwiperSlide key={index}>
              <div className="slider-item" style={{backgroundImage: `url(${item.url})`}}/>
            </SwiperSlide>
          )
        }
      </Swiper>
    )
  }
}

ESwiper.propTypes = {
  sliders: PropTypes.array.isRequired,
  className: PropTypes.string,
};

ESwiper.defaultProps = {
  className: '',
};

export default ESwiper;
