import React from 'react';
import PropTypes from 'prop-types';
import HyperLink from '../HyperLink/HyperLink';
import './Pagination.scss';
import {isMobile} from '../../common/helpers/browser';

class Pagination extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      total: 0
    };
  }

  componentWillMount() {
    this.calculate(this.props);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.totalPages !== this.props.totalPages
      || nextProps.totalItems !== this.totalItems
      || nextProps.pageSize !== this.props.pageSize) {
      this.calculate(nextProps);
    }
  }

  calculate = (props) => {
    const {totalPages, totalItems, pageSize} = props;
    let total = totalPages;
    if (total === undefined) {
      if (totalItems && pageSize) {
        total = Math.floor(totalItems / pageSize);
      }
    }
    if (!total) {
      this.setState({
        items: [],
        total: 0
      });
      return;
    }
    const items = [];
    for (let i = 1; i <= total; i++) {
      items.push({
        page: i,
        label: i
      });
    }
    this.setState({
      items,
      total
    });
  };

  onClickPage = (page) => () => {
    const {onPageChanged} = this.props;
    if (onPageChanged) {
      onPageChanged(page);
    }
  };

  render() {
    const {items, total} = this.state;
    const currentPage = parseInt(this.props.currentPage) || 1;
    if (!items.length) {
      return null;
    }
    const maxPage = isMobile ? 4 : 10;
    const startIndex = currentPage - maxPage / 2 > 0 ? currentPage - maxPage / 2 : 0;
    const displayItems = items.slice(startIndex, startIndex + maxPage - 1);
    const left = displayItems[0].page > 2;
    const right = displayItems[displayItems.length - 1].page < total;
    return (
      <nav aria-label="Page navigation">
        <ul className="pagination">
          {
            currentPage && currentPage > 1 &&
            <li
              className="page-item"
              onClick={this.onClickPage(currentPage - 1)}>
              <HyperLink className="page-link"><i className="fas fa-angle-double-left"/></HyperLink>
            </li>
          }
          {
            displayItems[0].page > 1 &&
            <li
              className={`page-item`}
              onClick={this.onClickPage(1)}>
              <HyperLink className="page-link">1</HyperLink>
            </li>
          }
          {
            left && <li className="page-item"><span className="page-link">...</span></li>
          }
          {
            displayItems.map((item, index) => (
              <li
                className={`page-item ${`${item.page}` === `${currentPage}` ? 'active' : ''}`}
                key={index}
                onClick={this.onClickPage(item.page)}>
                <HyperLink className="page-link">{item.label}</HyperLink>
              </li>
            ))
          }
          {
            right && <li className="page-item"><span className="page-link">...</span></li>
          }
          {
            currentPage && currentPage < total &&
            <li
              className="page-item"
              onClick={this.onClickPage(currentPage + 1)}>
              <HyperLink className="page-link"><i className="fas fa-angle-double-right"/></HyperLink>
            </li>
          }
        </ul>
      </nav>
    )
  }
}

Pagination.propTypes = {
  totalPages: PropTypes.number,
  totalItems: PropTypes.number,
  pageSize: PropTypes.number,
  currentPage: PropTypes.any,
  onPageChanged: PropTypes.func
};

export default Pagination;