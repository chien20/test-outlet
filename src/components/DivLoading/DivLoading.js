import React from 'react';
import './DivLoading.scss';

const DivLoading = ({width = '100%', height = '50px', style = {}, wrapperStyle = {}}) => (
  <div className="div-loading-wrapper" style={wrapperStyle}>
    <div className="div-loading" style={{
      width,
      height,
      ...style
    }}/>
  </div>
);

export default DivLoading;
