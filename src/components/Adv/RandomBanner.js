import React from 'react';
import {randomInt} from '../../common/helpers';
import Banner from './Banner';

const RandomBanner = ({banners, className}) => {
  const index = randomInt(0, banners.length - 1);
  const banner = banners[index];
  return (
    <div className={className}>
      <Banner banner={banner}/>
    </div>
  )
};

export default RandomBanner;