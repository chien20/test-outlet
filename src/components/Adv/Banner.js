import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {imageUrl} from '../../common/helpers';

class Banner extends React.PureComponent {
  render() {
    const {banner} = this.props;
    if (!banner || !banner.image || !banner.image.url) {
      return null;
    }
    return (
      <Link to={banner.link} target={banner.target}>
        <img src={imageUrl(banner.image.url)} alt={banner.title}/>
      </Link>
    )
  }
}

Banner.propTypes = {
  banner: PropTypes.any
};

export default Banner;