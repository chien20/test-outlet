import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import IntervalBanner from './IntervalBanner';
import RandomBanner from './RandomBanner';

class Adv extends React.PureComponent {
  render() {
    const {bannerByPositions, position, type, ...others} = this.props;
    if (!bannerByPositions || !bannerByPositions[position] || !bannerByPositions[position].length) {
      return null;
    }
    const banners = bannerByPositions[position];
    if (type === 'all') {
      return <IntervalBanner banners={banners} {...others}/>
    }
    return <RandomBanner banners={banners} {...others}/>
  }
}

Adv.propTypes = {
  position: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['random', 'all']),
  className: PropTypes.string
};

Adv.defaultProps = {
  type: 'all',
  className: 'advs'
};

const mapStateToProps = (state) => ({
  bannerByPositions: state.common.banners.byPositions
});

export default connect(mapStateToProps)(Adv);