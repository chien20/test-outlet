import React from 'react';
import Banner from './Banner';

class IntervalBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      banners: [],
      index: 0
    };
    this.interVal = null;
  }

  componentDidMount() {
    this.setBanners();
    this.interVal = setInterval(this.changeBanner, 6000);
  }

  componentWillUnmount() {
    if (this.interVal) {
      clearInterval(this.interVal);
      this.interVal = null;
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.banners !== this.props.banners) {
      this.setBanners();
    }
  }

  setBanners = () => {
    const {banners} = this.props;
    this.setState({
      banners: banners,
      index: 0
    });
  };

  changeBanner = () => {
    this.setState(prevState => ({
      index: (prevState.index + 1) % prevState.banners.length
    }));
  };

  render() {
    const {className} = this.props;
    const {banners, index} = this.state;
    if (!banners[index]) {
      return null;
    }
    return (
      <div className={className}>
        <Banner banner={banners[index]}/>
      </div>
    )
  }
}

export default IntervalBanner;