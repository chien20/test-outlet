import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './CountDown.scss';

class CountDown extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      days: 0,
      hours: 0,
      minutes: 0,
      seconds: 0
    };
    this.timer = null;
  }

  componentDidMount() {
    if (this.props.endTime && typeof this.props.endTime.getTime === 'function') {
      this.count();
      this.timer = setInterval(this.count, 1000);
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  count = () => {
    const now = new Date().getTime();
    const end = this.props.endTime.getTime();
    const distance = end - now;
    if (distance < 0) {
      return;
    }
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);
    this.setState({
      days,
      hours,
      minutes,
      seconds
    });
  };

  render() {
    const {hideDays} = this.props;
    const {days, hours, minutes, seconds} = this.state;
    const daysStr = days < 10 ? `0${days}` : days;
    const hoursStr = hours < 10 ? `0${hours}` : hours;
    const minutesStr = minutes < 10 ? `0${minutes}` : minutes;
    const secondsStr = seconds < 10 ? `0${seconds}` : seconds;
    let showDay = !!hideDays;
    if (hideDays === 'auto' && days === 0) {
      showDay = false;
    }
    return (
      <div className="count-down">
        {
          showDay &&
          <Fragment>
            <span className="days">{daysStr}</span>
            <span className="divider">:</span>
          </Fragment>
        }
        <span className="hours">{hoursStr}</span>
        <span className="divider">:</span>
        <span className="minutes">{minutesStr}</span>
        <span className="divider">:</span>
        <span className="seconds">{secondsStr}</span>
      </div>
    )
  }
}

CountDown.propTypes = {
  endTime: PropTypes.any,
  hideDays: PropTypes.any
};

CountDown.defaultProps = {
  hideDays: 'auto'
};

export default CountDown;
