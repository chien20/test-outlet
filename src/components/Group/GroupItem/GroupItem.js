import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {imageUrl, publicUrl} from '../../../common/helpers/index';
import {Link} from 'react-router-dom';
import './GroupItem.scss';
import {formatDate} from '../../../common/helpers/format';
import {isMobile} from "../../../common/helpers/browser";

class GroupItem extends React.PureComponent {
  render() {
    const {group, className, showMemberCount, showCreatedDate, memberCount} = this.props;
    const groupAvatar = group.avatar ? imageUrl(group.avatar.url) : publicUrl('/assets/images/no-image/256x256.png');
    let groupUrl = `/groups/${group.id}/`;
    return (
      <Link to={groupUrl} className={`group-item ${className}`}>
        <div className="group-item-wrapper">
          {
            isMobile &&
              <Fragment>
                <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}>
                  <div className="tags">
                    {
                      group.approved_status === 0 &&
                      <div className="tag tag-gray">Chờ phê duyệt</div>
                    }
                    {
                      group.approved_status === 10 &&
                      <div className="tag tag-red">Bị từ chối</div>
                    }
                    {
                      group.status === 20 &&
                      <div className="tag tag-red">Khóa</div>
                    }
                    {
                      group.status === 30 &&
                      <div className="tag tag-red">Bị chặn</div>
                    }
                  </div>
                </div>
                <div className="group-item-info">
                  <div className="group-name"><span>{group.name}</span></div>
                  {
                    showMemberCount &&
                    <div className="group-members">{memberCount !== undefined ? `${memberCount} thành viên` : ''}</div>
                  }
                  {
                    showCreatedDate &&
                    <div className="group-created-date">Ngày lập: {formatDate(group.created_at)}</div>
                  }
                </div>
              </Fragment>
          }
          {
            !isMobile &&
              <Fragment>
                <div className="group-avatar" style={{backgroundImage: `url(${groupAvatar})`}}>
                  <div className="tags">
                    {
                      group.approved_status === 0 &&
                      <div className="tag tag-gray">Chờ phê duyệt</div>
                    }
                    {
                      group.approved_status === 10 &&
                      <div className="tag tag-red">Bị từ chối</div>
                    }
                    {
                      group.status === 20 &&
                      <div className="tag tag-red">Khóa</div>
                    }
                    {
                      group.status === 30 &&
                      <div className="tag tag-red">Bị chặn</div>
                    }
                  </div>
                </div>
                <div className="group-item-info">
                  <div className="group-name"><span>{group.name}</span></div>
                  {
                    showMemberCount &&
                    <div className="group-members">{memberCount !== undefined ? `${memberCount} thành viên` : ''}</div>
                  }
                  {
                    showCreatedDate &&
                    <div className="group-created-date">Ngày lập: {formatDate(group.created_at)}</div>
                  }
                </div>
              </Fragment>
          }
        </div>
      </Link>
    )
  }
}

GroupItem.propTypes = {
  group: PropTypes.object,
  isAdmin: PropTypes.bool,
  className: PropTypes.string,
  showCreatedDate: PropTypes.bool,
  showMemberCount: PropTypes.bool,
};

GroupItem.defaultProps = {
  className: '',
};

export default GroupItem;
