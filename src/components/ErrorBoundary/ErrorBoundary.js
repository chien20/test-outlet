import React from 'react';
import logger from '../../common/helpers/logger';

class ErrorBoundary extends React.PureComponent {
  state = {hasError: false};

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return {hasError: true};
  }

  componentDidCatch(error, errorInfo) {
    logger.sendReport(error, {errorInfo});
  }

  render() {
    if (this.state.hasError) {
      return <div className="error">Something went wrong.</div>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
