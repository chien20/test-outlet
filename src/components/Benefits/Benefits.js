import React from "react";
import "./Benefits.scss"
import {showAlert} from "../../common/helpers";
import {getStoreDetailAPI} from "../../api/stores";


class Benefits extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      store: null,
    }
  }

  componentDidMount() {
    this.getStore();
  }

  getStoreAsync = async () => {
    const {storeId} = this.props;
    const {data: store} = await getStoreDetailAPI(storeId);
    return store;
  };

  getStore = () => {
    this.getStoreAsync().then(store => {
      this.setState({
        isLoading: false,
        store: store,
      });
    }).catch((e) => {
      console.error(e);
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: 'Không tải được thông tin cửa hàng'
      })
    });
  };

  render() {
    const {store} = this.state;
    if (!store) {
      return null;
    }
    const reviewed = store.reviewed;
    const authentic = store.other_info && store.other_info.authentic;
    const refund = store.other_info && store.other_info.refund;
    const refunds = store.other_info && store.other_info.refunds;
    return (
      <ul className="list-benefit-shop">
        {
          reviewed && <li><i className="ico-16"/> <span>Chứng nhận bởi Outlet</span></li>
        }
        {
          authentic && <li><i className="ico-17"/> <span>Cam kết chính hãng 100%</span></li>
        }
        {
          (refund || refunds) && <li><i className="ico-18"/> <span>Hoàn tiền 100% nếu hàng giả</span></li>
        }
      </ul>
    )
  }
}

export default Benefits;
