import React from 'react';
import {Link} from 'react-router-dom';

const FAQTopic = ({category}) => {
  return (
    <div className="topic">
      <h2>{category?.name}</h2>
      <div className="topic-content">
        <ul className="link-list">
          {
            category.questions.map((item, index) => (
              <li key={index}>
                <Link to={`/help-center/questions/${item.id}`}>{item.name}</Link>
              </li>
            ))
          }
        </ul>
      </div>
    </div>
  );
};

export default FAQTopic;
