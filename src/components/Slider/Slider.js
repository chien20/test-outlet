import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import 'slick-carousel';
import './Slider.scss';
import {Link} from 'react-router-dom';

class Slider extends React.PureComponent {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    const mainSliderConfig = {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      infinite: true,
      autoplay: true
    };
    $(this.ref.current).slick(mainSliderConfig);
  }

  render() {
    const {className, items} = this.props;
    return (
      <div className={`slider ${className}`} ref={this.ref}>
        {
          items.map((item, index) => (
            <div className="item" key={index}>
              {
                item.link &&
                <Link to={item.link} target={item.target === '_blank' ? '_blank' : ''}>
                  <img src={item.imageUrl} alt={item.title}/>
                </Link>
              }
              {
                !item.link &&
                <img src={item.imageUrl} alt={item.title}/>
              }
            </div>
          ))
        }
      </div>
    )
  }
}

Slider.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array
};

Slider.defaultProps = {
  className: '',
  items: []
};

export default Slider;