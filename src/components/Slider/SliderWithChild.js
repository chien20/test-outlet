import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import './Slider.scss';

class SliderWithChild extends React.PureComponent {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    setTimeout(() => {
      const mainSliderConfig = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        infinite: true,
        autoplay: true
      };
      $(this.ref.current).slick(mainSliderConfig);
    });
  }

  render() {
    const {className, children} = this.props;
    return (
      <div className={`slider ${className}`} ref={this.ref}>
        {
          children
        }
      </div>
    )
  }
}

SliderWithChild.propTypes = {
  className: PropTypes.string
};

SliderWithChild.defaultProps = {
  className: ''
};

export default SliderWithChild;