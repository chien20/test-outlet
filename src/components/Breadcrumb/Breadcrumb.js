import React from 'react';
import {Link} from 'react-router-dom';
import './Breadcrumb.scss';
import {isMobile} from '../../common/helpers/browser';

export const BreadcrumbItem = ({isActive, path, text, className = ''}) => (
  <li className={`breadcrumb-item ${isActive ? 'active' : ''} ${className}`}>
    {
      !path ? text : <Link to={path}>{text}</Link>
    }
  </li>
);

export const Breadcrumb = ({children}) => (
  <section className="breadcrumb-section">
    <div className="container">
      <nav aria-label="breadcrumb">
        <ol className={`breadcrumb ${isMobile ? 'breadcrumb-mobile' : ''}`}>
          <BreadcrumbItem path="/" text={<><i className="fas fa-home"/> Trang chủ</>} className="home-item"/>
          {children}
        </ol>
      </nav>
    </div>
  </section>
);
