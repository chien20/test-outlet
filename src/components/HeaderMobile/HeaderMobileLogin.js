import React from 'react';
import './HeaderMobileLogin.scss';
import IconBackMobile from '../../components/HeaderMobile/IconBackMobile/IconBackMobile';

const HeaderMobileLogin = () => {
  return (
    <div className="header-mobile-login">
      <div className="header_mobile-bg top"></div>
      <div className="header_mobile-bg bottom"></div>
      <IconBackMobile/>
    </div>
  );
};

export default HeaderMobileLogin;