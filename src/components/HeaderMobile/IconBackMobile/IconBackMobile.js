import React from 'react';
import {publicUrl} from '../../../common/helpers';
import history from '../../../common/utils/router/history';

const goBack = () => {
  history.goBack();
};

const IconBackMobile = () => (
  <div className="header-icon icon-back" onClick={goBack}>
    <img src={publicUrl('/assets/images/icons/icon-back.svg')} alt="Back"/>
  </div>
);

export default IconBackMobile;
