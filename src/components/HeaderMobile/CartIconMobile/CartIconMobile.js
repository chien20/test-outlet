import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import bagIcon from '../../../assets/images/icons/icon-bag.svg';
import bagIconBlack from '../../../assets/images/icons/icon-bag-black.svg';
import {Link} from 'react-router-dom';
import './CartIconMobile.scss'; 

class CartIconMobile extends React.PureComponent {
  render() {
    const {color, cart} = this.props;
    return (
      <Link to="/cart" className="cart-icon-mobile badge-container">
        <img
          src={color === 'black' ? bagIconBlack : bagIcon}
          className="search-icon"
          alt="Icon2"
        />
        {
          cart.qty > 0 &&
          <span className="badge">{cart.qty}</span>
        }
      </Link>
    );
  }
}

CartIconMobile.propTypes = {
  color: PropTypes.oneOf(['black', 'white']),
};

CartIconMobile.defaultProps = {
  color: 'black',
};

const mapStateToProps = (state) => ({
  cart: state.pages.order.cart
});

export default connect(mapStateToProps)(CartIconMobile);