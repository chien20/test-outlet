import React from "react";
import './IconAddMobile.scss';
import {publicUrl} from "../../../common/helpers";
import {Link} from "react-router-dom";

class IconAddMobile extends React.PureComponent {
  render() {
    const {linkTo} = this.props;
    return (
      <div className="header-icon icon-add">
        <Link to={linkTo}>
          <img src={publicUrl('/assets/images/icons/icon-add.svg')} alt="add-icon"/>
        </Link>
      </div>
    )
  }
}

export default IconAddMobile;