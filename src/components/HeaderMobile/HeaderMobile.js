import React from 'react';
import './HeaderMobile.scss';

const HeaderMobile = ({className = '', children}) => {
  return (
    <div className={`header-mobile ${className}`}>
      {children}
    </div>
  );
};

export default HeaderMobile;