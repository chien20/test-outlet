import React from 'react';
import shareIcon from '../../../assets/images/icons/baseline-share-24px.svg'
import './ShareIconMobile.scss'


class ShareIconMobile extends React.PureComponent{
    render() {
        const encodedUrl = encodeURI(window.location.href);
        return (
            <a
                className="header-icon share-icon-mobile"
                href={`https://www.facebook.com/sharer/sharer.php?u=${encodedUrl}`}
                target="_blank"
                rel="noopener noreferrer">
                <img src={ shareIcon } alt="ShareIcon"/>
            </a>
        )

    }
}
export default ShareIconMobile;