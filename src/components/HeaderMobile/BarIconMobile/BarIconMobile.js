import React from 'react';
import './BarIconMobile.scss';
import {Dropdown} from 'react-bootstrap';
import barIcon from '../../../assets/images/icons/Dot-vertical.svg';
import history from '../../../common/utils/router/history';

const CustomToggle = React.forwardRef(({children, onClick}, ref) => (
  <div
    ref={ref}
    onClick={onClick}
  >
    <img src={barIcon} alt={'icon'}/>
  </div>
));

class BarIconMobile extends React.PureComponent {
  onClickMenu = (event) => {
    event.preventDefault();
    history.push(event.target.getAttribute('href'));
  };

  render() {
    return (
      <Dropdown className="header-icon bar-icon">
        <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
          Custom toggle
        </Dropdown.Toggle>
        <Dropdown.Menu className="dropdown-bar-icon">
          <Dropdown.Item href="/" onClick={this.onClickMenu}>
            Trang chủ
          </Dropdown.Item>
          <Dropdown.Item href="/categories" onClick={this.onClickMenu}>
            Danh mục sản phẩm
          </Dropdown.Item>
          <Dropdown.Item href="/user" onClick={this.onClickMenu}>
            Tài khoản
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  }
}

export default BarIconMobile
