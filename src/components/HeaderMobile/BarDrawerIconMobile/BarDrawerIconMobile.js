import React from 'react';
import {publicUrl} from "../../../common/helpers";
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import HyperLink from '../../../components/HyperLink/HyperLink';
import Avatar from '../../../components/Avatar/Avatar';
import {logout} from '../../../redux/actions/user';
import {isMobile} from '../../../common/helpers/browser';
import "./BarDrawerIconMobile.scss";

class BarDrawerIconMobile extends React.PureComponent {
  render() {
    const {user} = this.props;
    return (
      <>
      <div className="bar-drawer-mobile">
        <input type="checkbox" className="bar_checkbox" name="" id="bar_checkbox"/>
          <label for="bar_checkbox" className="bar_overlay"></label>
          <label for="bar_checkbox">
            <img
              src={publicUrl('/assets/images/header-mobile/menu-bars.svg')}
              className="bar-drawer-icon"
              alt=""
            />
          </label>

        <div className="bar_mobile">
          <div className="bar_user">
            {
              user && 
              <>
              <Avatar className="bar_user-avatar" size={isMobile ? 45 : 40} src={user ? user.avatar : ''}/>
              <div className="bar_info">
                <h3>{user.full_name}</h3>
                <p>Xin chào</p>
              </div>
              </>
            }
            {
              !user && 
              <>
              {/* <img alt="avatar" className="bar_user-avatar" src={publicUrl('/assets/images/header-mobile/avatar-test.png')}/> */}
              <div className="mg-l-22"></div>
              <div className="bar_info">
                <h3>
                  <Link to="/login">Đăng nhập</Link>
                </h3>
                <p>Xin chào</p>
              </div>
              </>
            }
          </div>
          <ul className="bar_list">
              <li className="bar_list-item active">Trang chủ</li>
              <li className="bar_list-item">Khuyến mãi hôm nay</li>
              <li className="bar_list-item">Hàng bán chạy</li>
              <li className="bar_list-item">Sản phẩm đã xem</li>
              <li className="bar_list-item border-bt">Theo dõi đơn hàng</li>
              <li className="bar_list-item">Cài đặt</li>
              <li className="bar_list-item">Trợ giúp</li>
          </ul>
        </div>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.info
});

export default withRouter(connect(mapStateToProps)(BarDrawerIconMobile));