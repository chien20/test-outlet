import React from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged} from '../../../common/helpers';
import searchIconMobile from '../../../../src/assets/images/icons/icon-search-mobile.svg';
import './SearchBoxMobile.scss';
import history from '../../../common/utils/router/history';

class SearchBoxMobile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      keyword: ''
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  handleSearch = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const {keyword} = this.state;
    if (!keyword) {
      return;
    }
    history.push('/tim-kiem/' + encodeURI(keyword));
  };

  render() {
    const {placeholder} = this.props;
    const {keyword} = this.state;
    return (
      <form className="search-box-mobile" onSubmit={this.handleSearch}>
        <input
          type="text"
          name="keyword"
          value={keyword}
          placeholder={placeholder}
          onChange={handleInputTextChanged('keyword', this.setData)}
        />
        <button type="submit" className="">
          <img
            src={searchIconMobile}
            className="search-icon"
            alt="Icon"
          />
        </button>
      </form>
    );
  }
}

SearchBoxMobile.propTypes = {
  onSearch: PropTypes.func,
  placeholder: PropTypes.string
};

SearchBoxMobile.defaultProps = {
  placeholder: 'Bạn đang tìm sản phẩm gì?'
};

export default SearchBoxMobile;
