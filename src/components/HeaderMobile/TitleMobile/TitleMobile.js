import React from 'react';
import './TitleMobile.scss';

const TitleMobile = ({children, className = ''}) => (
  <div className={`title-mobile ${className}`}>{children}</div>
);

export default TitleMobile;
