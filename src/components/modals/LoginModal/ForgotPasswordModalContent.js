import React, {Fragment} from 'react';
import {Button, Modal} from 'react-bootstrap';
import {forgotPasswordAPI} from '../../../api/users';
import {showAlert} from '../../../common/helpers';

class ForgotPasswordModalContent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      receiver: '',
      isLoading: false,
      isSuccess: null
    };
  }

  handleInputChange = (event) => {
    this.setState({
      receiver: event.target.value
    });
  };

  handleForgotPassword = () => {
    const {receiver} = this.state;
    this.setState({
      isLoading: true,
      isSuccess: null
    });
    forgotPasswordAPI({
      email_phone: receiver
    }).then((res) => {
      this.setState({
        isLoading: false,
        isSuccess: res.data,
      });
    }).catch(error => {
      this.setState({
        isLoading: false,
        isSuccess: false,
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error'
      });
    });
  };

  handleGoBack = () => {
    this.props.setData({isForgotPassword: false});
  };

  render() {
    const {receiver, isLoading, isSuccess} = this.state;
    const isBtnContinueDisabled = !receiver || isLoading;
    return (
      <Fragment>
        <Modal.Header closeButton>
          <Modal.Title>Quên mật khẩu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            isSuccess === true &&
            <div className="alert alert-success">
              Mật khẩu đã được gửi đến email/số điện thoại của bạn! Vui lòng kiểm tra và đăng nhập bằng mật khẩu mới!
            </div>
          }
          {
            isSuccess === false &&
            <div className="alert alert-danger">
              Lỗi! Không lấy lại được mật khẩu. Có thể số điện thoại hoặc email bạn nhập không tồn tại trên hệ thống!
              <p>Xin vui lòng kiểm tra lại!</p>
            </div>
          }
          {
            isSuccess !== true &&
            <div className="form-group">
              <label>Email/Số điện thoại:</label>
              <input type="text" className="form-control" value={receiver} onChange={this.handleInputChange}/>
            </div>
          }
        </Modal.Body>
        <Modal.Footer> 
          <Button variant={isSuccess !== true ? 'default' : 'primary'} onClick={this.handleGoBack}>
            {isSuccess !== true ? 'Quay lại' : 'Đăng nhập'}
          </Button>
          {
            isSuccess !== true &&
            <Button variant="primary" onClick={this.handleForgotPassword} disabled={isBtnContinueDisabled}>
              Tiếp tục
            </Button>
          }
        </Modal.Footer>
      </Fragment>
    )
  }
}

export default ForgotPasswordModalContent;
