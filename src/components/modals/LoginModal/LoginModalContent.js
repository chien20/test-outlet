import React from 'react';
import {connect} from 'react-redux';
import {Button, Modal} from 'react-bootstrap';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {loginAPI, loginByFacebookAPI, loginByGoogleAPI} from '../../../api/users';
import {loginSuccess} from '../../../redux/actions/user';
import HyperLink from '../../HyperLink/HyperLink';
import {getCurrentUserSuccessAC} from '../../../redux/actions/user/info';
import ButtonFacebookLogin from '../../Button/ButtonFacebookLogin';
import ButtonGoogleLogin from '../../Button/ButtonGoogleLogin';

class LoginModalContent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email_phone: '',
        password: '',
        fb_access_token: '',
        google_id: '',
        google_access_token: ''
      },
      isLoading: false,
      isLoginSuccess: null,
      isForgotPassword: false
    };
    this.formEl = React.createRef();
  }

  /**
   * Set form data
   * @param data
   */
  setData = (data) => {
    if (!data || typeof data !== 'object') {
      return;
    }
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  handleLogin = () => {
    const {data: {email_phone, password}} = this.state;
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    loginAPI({
      email_phone,
      password
    }).then(this.handleLoginResponse).catch(this.handleLoginResponse);
  };

  handleForgotPassword = () => {
    this.props.setData({isForgotPassword: true});
  };

  handleClickFacebookLogin = () => {
    console.log('a');
  };

  handleFacebookLogin = (res) => {
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    if (res?.accessToken) {
      loginByFacebookAPI({
        access_token: res.accessToken,
      })
      .then(this.handleLoginResponse)
      .catch(this.handleLoginResponse);
    } else {
      this.handleLoginResponse();
    }
  };

  handleGoogleLogin = (res) => {
    this.setState({
      isLoading: true,
      isLoginSuccess: null
    });
    if (res?.accessToken) {
      loginByGoogleAPI({
        access_token: res.accessToken,
      })
      .then(this.handleLoginResponse)
      .catch(this.handleLoginResponse);
    } else {
      this.handleLoginResponse();
    }
  };

  handleLoginResponse = (res) => {
    const {handleClose, handleLoginSuccess, handleGetCurrentUserSuccess} = this.props;
    if (res?.data?.oauth) {
      this.setState({
        isLoading: false,
        isLoginSuccess: true
      });
      showAlert({
        message: 'Đăng nhập thành công!',
        type: 'success'
      });
      handleLoginSuccess(res.data.oauth);
      handleGetCurrentUserSuccess(res.data.info, true);
      handleClose();
    } else {
      this.setState({
        isLoading: false,
        isLoginSuccess: false
      });
      if (res?.error === 'invalid_grant') {
        showAlert({
          message: `Đăng nhập không thành công: Sai tên truy cập hoặc mật khẩu`,
          type: 'error'
        });
      } else {
        showAlert({
          message: `Đăng nhập thất bại!`,
          type: 'error'
        });
      }
    }
  };

  submit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    if (this.formEl.current.checkValidity() === false) {
      this.formEl.current.classList.add('was-validated');
      return;
    }
    this.handleLogin();
  };

  render() {
    const {handleClose} = this.props;
    const {data, isLoading} = this.state;
    const isBtnLoginDisabled = isLoading || !data.email_phone || !data.password;
    return (
      <form onSubmit={this.submit} ref={this.formEl}>
        <Modal.Header closeButton>
          <Modal.Title>Đăng nhập</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-group">
            <label>Email/Số điện thoại</label>
            <input type="text"
                   value={data.email_phone}
                   onChange={handleInputTextChanged('email_phone', this.setData)}
                   className="form-control"
                   required={true}/>
            <div className="invalid-feedback">
              Vui lòng nhập email hoặc số điện thoại
            </div>
          </div>
          <div className="form-group">
            <label>Mật khẩu</label>
            <input type="password"
                   value={data.password}
                   onChange={handleInputTextChanged('password', this.setData)}
                   className="form-control"
                   required={true}/>
            <div className="invalid-feedback">
              Vui lòng nhập mật khẩu
            </div>
          </div>
          <p><HyperLink className="text-primary" onClick={this.handleForgotPassword}>Quên mật khẩu?</HyperLink></p>
          <p className="text-center">HOẶC</p>
          <div className="by-socials text-center">
            <ButtonFacebookLogin
              isDisabled={isLoading}
              onClick={this.handleClickFacebookLogin}
              callback={this.handleFacebookLogin}
            />
            <ButtonGoogleLogin
              isDisabled={isLoading}
              onSuccess={this.handleGoogleLogin}
              onFailure={this.handleGoogleLogin}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="default" onClick={handleClose}>
            Hủy bỏ
          </Button>
          <button className="btn btn-primary" onClick={this.submit} disabled={isBtnLoginDisabled}>
            Đăng nhập
          </button>
        </Modal.Footer>
      </form>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  handleLoginSuccess: (data) => dispatch(loginSuccess(data)),
  handleGetCurrentUserSuccess: (data) => dispatch(getCurrentUserSuccessAC(data))
});

export default connect(null, mapDispatchToProps)(LoginModalContent);
