import React from 'react';
import {Modal} from 'react-bootstrap';
import {connect} from 'react-redux';
import LoginModalContent from './LoginModalContent';
import {closeModal} from '../../../redux/actions/modals/modals';
import ForgotPasswordModalContent from './ForgotPasswordModalContent';

class LoginModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isForgotPassword: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data && nextProps.data.isForgotPassword) {
      this.setState({
        isForgotPassword: nextProps.data.isForgotPassword
      });
    } else {
      this.setState({
        isForgotPassword: false
      });
    }
  }

  setData = (data) => {
    this.setState(prevState => ({
      ...prevState, ...data
    }));
  };

  render() {
    const {isOpen, handleClose} = this.props;
    const {isForgotPassword} = this.state;
    return (
      <Modal size="md" show={isOpen} onHide={handleClose} className="login-modal common-modal" centered>
        {
          !isForgotPassword && <LoginModalContent handleClose={handleClose} setData={this.setData}/>
        }
        {
          isForgotPassword && <ForgotPasswordModalContent handleClose={handleClose} setData={this.setData}/>
        }
      </Modal>
    )
  }
}

const mapStateToProps = (state) => ({
  isOpen: state.modals.login.isOpen,
  data: state.modals.login
});

const mapDispatchToProps = (dispatch) => ({
  handleClose: () => dispatch(closeModal('login'))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);