import React, {Fragment} from 'react';
import {Button, Modal} from 'react-bootstrap';
import {handleInputTextChanged, showAlert} from '../../../common/helpers';
import {registerByEmailAPI} from '../../../api/users';
import AlertContainer from '../../Alert/AlertContainer';
import {Link} from 'react-router-dom';

class RegisterByEmail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      full_name: '',
      isSubmitting: false,
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  registerByPhone = () => {
    this.props.setData({
      registerBy: 'phone'
    });
  };

  handleSubmit = () => {
    const {handleClose} = this.props;
    const {email, password, full_name} = this.state;
    this.setState({
      isSubmitting: true
    });
    registerByEmailAPI({
      email,
      password,
      full_name
    }).then(() => {
      handleClose();
      showAlert({
        message: 'Đăng ký thành công! Bạn có thể đăng nhập!',
        type: 'success'
      });
      this.setState({
        isSubmitting: false
      });
    }).catch(error => {
      let message = `Lỗi! ${error.message}`;
      if (error.error_fields) {
        error.error_fields.forEach(e => {
          if (e.field_name === 'email' && e.message === 'FIELD_EXIST') {
            message = 'Email đã tồn tại!';
          }
        });
      }
      showAlert({
        message,
        type: 'error',
        timeout: 0,
        id: 'register-by-email',
        container: 'register-by-email'
      });
      this.setState({
        isSubmitting: false
      });
    });
  };

  render() {
    const {handleClose} = this.props;
    const {email, password, full_name, isSubmitting} = this.state;
    const isBtnSubmitDisabled = !email || !password || !full_name || isSubmitting;
    return (
      <Fragment>
        <Modal.Header closeButton>
          <Modal.Title>Đăng ký</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AlertContainer container="register-by-email"/>
          <div className="form-group">
            <label>Họ tên <sup>(*)</sup></label>
            <input type="text"
                   value={full_name}
                   onChange={handleInputTextChanged('full_name', this.setData)}
                   className="form-control"
                   required/>
          </div>
          <div className="form-group">
            <label>Email <sup>(*)</sup></label>
            <input type="text"
                   value={email}
                   onChange={handleInputTextChanged('email', this.setData)}
                   className="form-control"
                   required/>
          </div>
          <div className="form-group">
            <label>Mật khẩu <sup>(*)</sup></label>
            <input type="password"
                   value={password}
                   onChange={handleInputTextChanged('password', this.setData)}
                   className="form-control"
                   required/>
          </div>
          <p className="agree-policy">Bằng việc đăng ký, bạn đã đồng ý với <Link to="/p/dieu-khoan-dich-vu" target="_blank">Điều khoản
            dịch vụ</Link> và <Link to="/p/chinh-sach-bao-mat" target="_blank">Chính sách bảo mật</Link> của chúng tôi.</p>
          {/*<p>*/}
          {/*<HyperLink className="text-primary" onClick={this.registerByPhone}>Đăng ký bằng số điện thoại</HyperLink>*/}
          {/*</p>*/}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="default" onClick={handleClose}>
            Hủy bỏ
          </Button>
          <Button variant="primary"
                  disabled={isBtnSubmitDisabled}
                  onClick={this.handleSubmit}>
            Đăng ký
          </Button>
        </Modal.Footer>
      </Fragment>
    )
  }
}

export default RegisterByEmail;
