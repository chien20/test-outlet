import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged} from '../../../common/helpers';
import AlertContainer from '../../Alert/AlertContainer';

class RegisterStep2 extends React.PureComponent {
  render() {
    const {data, setData, isLoading} = this.props;
    return (
      <Fragment>
        <AlertContainer container="register-step2"/>
        <div className="form-group">
          <label>Họ tên <sup>(*)</sup></label>
          <input type="text"
                 value={data.full_name}
                 onChange={handleInputTextChanged('full_name', setData)}
                 disabled={isLoading}
                 className="form-control"
                 required/>
        </div>
        <div className="form-group">
          <label>Email</label>
          <input type="email"
                 value={data.email}
                 onChange={handleInputTextChanged('email', setData)}
                 disabled={isLoading}
                 className="form-control"
                 required/>
        </div>
        <div className="form-group">
          <label>Mật khẩu <sup>(*)</sup></label>
          <input type="password"
                 value={data.password}
                 onChange={handleInputTextChanged('password', setData)}
                 disabled={isLoading}
                 className="form-control"
                 required/>
        </div>
        <div className="form-group">
          <label>Nhập lại mật khẩu <sup>(*)</sup></label>
          <input type="password"
                 value={data.re_password}
                 onChange={handleInputTextChanged('re_password', setData)}
                 disabled={isLoading}
                 className="form-control"
                 required/>
        </div>
      </Fragment>
    )
  }
}

RegisterStep2.propTypes = {
  setData: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  isLoading: PropTypes.bool
};

export default RegisterStep2;
