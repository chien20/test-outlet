import React, {Fragment} from 'react';
import {REGISTER_TYPES} from '../../../common/constants/app';
import {registerAPI, registerPhoneNumberAPI, verifyPhoneNumberAPI} from '../../../api/users';
import {showAlert} from '../../../common/helpers';
import RegisterStep1 from './RegisterStep1';
import RegisterStep2 from './RegisterStep2';
import {Link} from 'react-router-dom';
import {Button, Modal} from 'react-bootstrap';

class RegisterByPhone extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        full_name: '',
        email: '',
        register_token: '', // Required, get from #1.2
        password: '',
        fb_id: '',
        fb_access_token: '',
        google_id: '',
        google_access_token: '',
        re_password: '',
        phone: '',
        otp: ''
      },
      isSentOTP: false,
      isEmailExisted: false,
      isLoading: false,
      registerType: REGISTER_TYPES.NORMAL,
      step: 1
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.isOpen !== nextProps.isOpen && nextProps.isOpen) {
      this.setState(JSON.parse(JSON.stringify(this.defaultState)));
    }
    if (this.props.isOpen !== nextProps.isOpen && !nextProps.isOpen) {
      this.props.cleanAlert(['register-step1', 'register-step2'])
    }
  }

  /**
   * Set form data
   * @param data
   */
  setData = (data) => {
    if (!data || typeof data !== 'object') {
      return;
    }
    this.setState(prevState => ({
      data: {...prevState.data, ...data}
    }));
  };

  /**
   * Handle user click continue button
   */
  handleContinue = () => {
    const {step} = this.state;
    if (step === 1) {
      this.handleVerifyPhone();
      return;
    }
    this.handleRegister();
  };

  handleSendOTP = () => {
    this.setState({
      isLoading: true,
      isSentOTP: false
    });
    const {data: {phone}} = this.state;
    registerPhoneNumberAPI({phone}).then(() => {
      this.setState({
        isSentOTP: true,
        isLoading: false,
      });
      showAlert({
        type: 'success',
        message: 'Đã gửi mã xác nhận, vui lòng kiểm tra điện thoại!',
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Lỗi: ${error.message}`,
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    });
  };

  handleVerifyPhone = () => {
    const {data: {phone, otp}} = this.state;
    this.setState({
      isLoading: true
    });
    verifyPhoneNumberAPI({
      phone,
      otp
    }).then(res => {
      this.setState(prevState => ({
        isLoading: false,
        data: {
          ...prevState.data,
          register_token: res.data.register_token
        },
        step: 2
      }));
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error',
        timeout: 0,
        container: 'register-step1',
        id: 'register-step1'
      });
    });
  };

  handleRegister = () => {
    const {data} = this.state;
    const {handleClose} = this.props;
    this.setState({
      isLoading: true
    });
    registerAPI(data).then(() => {
      this.setState({
        isLoading: false
      });
      handleClose();
      showAlert({
        message: 'Đăng ký thành công! Bạn có thể đăng nhập!',
        type: 'success'
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        message: `Lỗi! ${error.message}`,
        type: 'error',
        timeout: 0,
        id: 'register-step2',
        container: 'register-step2'
      });
    });
  };

  registerByEmail = () => {
    this.props.setData({
      registerBy: 'email'
    });
  };

  render() {
    const {handleClose} = this.props;
    const {step, data, isSentOTP, isEmailExisted, isLoading} = this.state;
    let isBtnContinueDisabled = isLoading || isEmailExisted;
    if (step === 1) {
      if (!data.phone || !data.otp) {
        isBtnContinueDisabled = true;
      }
    }
    if (step === 2) {
      if (!data.full_name || !data.password || !data.re_password || data.password !== data.re_password) {
        isBtnContinueDisabled = true;
      }
    }
    return (
      <Fragment>
        <Modal.Header closeButton>
          <Modal.Title>Đăng ký</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            step === 1 &&
            <RegisterStep1
              data={data}
              isSentOTP={isSentOTP}
              setData={this.setData}
              registerByEmail={this.registerByEmail}
              handleSendOTP={this.handleSendOTP}
              isLoading={isLoading}/>
          }
          {
            step === 2 &&
            <RegisterStep2
              data={data}
              setData={this.setData}
              isLoading={isLoading}/>
          }
          <p className="agree-policy">Bằng việc đăng ký, bạn đã đồng ý với <Link to="/p/dieu-khoan-dich-vu">Điều khoản
            dịch vụ</Link> và <Link to="/p/chinh-sach-bao-mat">Chính sách bảo mật</Link> của chúng tôi.</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="default" onClick={handleClose}>
            Hủy bỏ
          </Button>
          <Button variant="primary"
                  disabled={isBtnContinueDisabled}
                  onClick={this.handleContinue}>
            Tiếp tục
          </Button>
        </Modal.Footer>
      </Fragment>
    )
  }
}

export default RegisterByPhone;
