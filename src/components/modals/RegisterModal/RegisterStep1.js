import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {handleInputTextChanged} from '../../../common/helpers';
import AlertContainer from '../../Alert/AlertContainer';
import HyperLink from '../../HyperLink/HyperLink';

class RegisterStep1 extends React.PureComponent {
  render() {
    const {data, setData, handleSendOTP, isLoading, isSentOTP, registerByEmail} = this.props;
    return (
      <Fragment>
        <AlertContainer container="register-step1"/>
        <div className="form-group">
          <label>Số điện thoại <sup>(*)</sup></label>
          <div className="input-group">
            <input type="text"
                   value={data.phone}
                   onChange={handleInputTextChanged('phone', setData)}
                   disabled={isLoading}
                   className="form-control"
                   required/>
            <div className="input-group-append">
              <button
                className="btn btn-secondary"
                onClick={handleSendOTP}
                disabled={!data.phone || isLoading || isSentOTP}>Gửi mã xác nhận
              </button>
            </div>
            {/*<div className="invalid-feedback">*/}
            {/*Số điện thoại này đã được sử dụng!*/}
            {/*</div>*/}
          </div>
        </div>
        <div className="form-group">
          <label>Mã xác nhận <sup>(*)</sup></label>
          <input type="text"
                 value={data.otp}
                 onChange={handleInputTextChanged('otp', setData)}
                 disabled={isLoading || !isSentOTP}
                 className="form-control"
                 required/>
        </div>
        <p><HyperLink className="text-primary" onClick={registerByEmail}>Đăng ký bằng email</HyperLink></p>
        <p className="text-center">HOẶC</p>
        <div className="by-socials text-center">
          <button className="btn btn-facebook"><i className="icon fab fa-facebook-f"/> Đăng ký bằng Facebook</button>
          <button className="btn btn-google"><i className="icon fab fa-google"/> Đăng ký bằng Google</button>
        </div>
      </Fragment>
    )
  }
}

RegisterStep1.propTypes = {
  setData: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  handleSendOTP: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isSentOTP: PropTypes.bool
};

export default RegisterStep1;
