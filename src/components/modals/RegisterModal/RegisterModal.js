import React from 'react';
import {connect} from 'react-redux';
import {Modal} from 'react-bootstrap';
import {closeModal} from '../../../redux/actions/modals/modals';
import {cleanAlertAC} from '../../../redux/actions/common';
import RegisterByEmail from './RegisterByEmail';
import './RegisterModal.scss';

class RegisterModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      registerBy: 'email'
    };
  }

  setData = (data) => {
    this.setState(data);
  };

  render() {
    const {isOpen, handleClose} = this.props;
    const {registerBy} = this.state;
    return (
      <Modal size="md" show={isOpen} onHide={handleClose} className="register-modal common-modal" centered>
        {/*{*/}
        {/*registerBy === 'phone' && <RegisterByPhone handleClose={handleClose} setData={this.setData}/>*/}
        {/*}*/}
        {
          registerBy === 'email' && <RegisterByEmail handleClose={handleClose} setData={this.setData}/>
        }
      </Modal>
    )
  }
}

const mapStateToProps = (state) => ({
  isOpen: state.modals.register.isOpen
});

const mapDispatchToProps = (dispatch) => ({
  handleClose: () => dispatch(closeModal('register')),
  cleanAlert: (containers) => dispatch(cleanAlertAC(containers))
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterModal);
