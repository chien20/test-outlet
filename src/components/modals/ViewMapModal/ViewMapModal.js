import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'react-bootstrap';
import LocationPicker from '../../GoogleMap/LocationPicker';
import LoadScript from '../../GoogleMap/LoadScript';

class ViewMapModal extends React.PureComponent {
  state = {
    isOpen: false,
  }

  handleOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    const {address, geocoding, title} = this.props;
    const {isOpen} = this.state;
    return (
      <Modal size="lg" show={isOpen} onHide={this.handleClose} className="alert-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            isOpen &&
            <LoadScript>
              <LocationPicker
                address={address}
                geocoding={geocoding}
              />
            </LoadScript>
          }
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-default" onClick={this.handleClose}>
            Đóng
          </button>
        </Modal.Footer>
      </Modal>
    )
  }
}

ViewMapModal.propTypes = {
  title: PropTypes.string,
  address: PropTypes.string,
  geocoding: PropTypes.object,
};

export default ViewMapModal;
