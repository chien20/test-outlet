import React, {Fragment} from 'react';
import RegisterModal from './RegisterModal/RegisterModal';
import LoginModal from './LoginModal/LoginModal';

const Modals = () => (
  <Fragment>
    <RegisterModal/>
    <LoginModal/>
  </Fragment>
);
export default Modals;