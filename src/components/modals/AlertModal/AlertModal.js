import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'react-bootstrap';
import Broadcaster from '../../../common/helpers/broadcaster';

class AlertModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      title: '',
      message: '',
      isLoading: false,
      buttons: []
    };
    this.unmounted = false;
  }

  componentDidMount() {
    Broadcaster.on(this.props.eventName, this.handleOpen);
  }

  componentWillUnmount() {
    this.unmounted = true;
    Broadcaster.off(this.props.eventName, this.handleOpen);
  }

  handleOpen = (data = {}) => {
    if (this.unmounted) {
      return;
    }
    this.setState({
      isOpen: true,
      ...data
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    const {isOpen, title, message, buttons, isLoading} = this.state;
    return (
      <Modal size="md" show={isOpen} onHide={this.handleClose} className="alert-modal" centered>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {message || this.props.children}
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-default" onClick={this.handleClose} disabled={isLoading}>
            Hủy bỏ
          </button>
          {
            buttons.map((item, index) => (
              <button
                className={item.className}
                onClick={item.onClick}
                key={index}
                disabled={isLoading}>{item.name}</button>
            ))
          }
        </Modal.Footer>
      </Modal>
    )
  }
}

AlertModal.propTypes = {
  eventName: PropTypes.string.isRequired
};

export default AlertModal;
