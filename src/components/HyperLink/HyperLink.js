import React from 'react';

const HyperLink = ({onClick, children, className, underConstruction, ...others}) => {
  const handleClick = (event) => {
    event.preventDefault();
    if (underConstruction) {
      alert('Tính năng đang được phát triển!');
    }
    if (onClick) {
      onClick(event);
    }
  };

  return <a href="/" onClick={handleClick} className={className} {...others}>{children}</a>
};

export default HyperLink;
