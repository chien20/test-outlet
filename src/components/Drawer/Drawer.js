import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import './Drawer.scss';
import {KEY_CODE} from '../../common/constants/keyCode';
import HyperLink from '../HyperLink/HyperLink';

class Drawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isRemoved: false
    };
  }

  componentDidMount() {
    window.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (event) => {
    if (!this.props.isOpen || !this.props.handleClose) {
      return;
    }
    if (event.keyCode === KEY_CODE.ESC) {
      this.props.handleClose();
    }
  };

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.isOpen !== nextProps.isOpen) {
      if (nextProps.isOpen) {
        this.setState({
          isRemoved: true
        });
      } else {
        setTimeout(() => {
          this.setState({
            isRemoved: false
          });
        }, 300);
      }
    }
  }

  onBackdropClick = () => {
    const {handleClose} = this.props;
    if (handleClose) {
      handleClose();
    }
  };

  render() {
    const {isOpen, width, children, className, handleClose, title} = this.props;
    const {isRemoved} = this.state;
    const drawerStyle = {
      width: width,
      right: -1 - width
    };
    return (
      <Fragment>
        <div className={`drawer ${className} ${isOpen ? 'opened' : ''}`}
             style={drawerStyle}>
          <div className="drawer-header">
            <h2>{title}</h2>
            <HyperLink className="close-button" onClick={handleClose}><i className="fas fa-times"/></HyperLink>
          </div>
          {
            isRemoved ? children : ''
          }
        </div>
        {
          isRemoved &&
          <div className={`drawer-backdrop ${isRemoved && !isOpen ? 'closing' : ''}`}
               onClick={this.onBackdropClick}/>
        }
      </Fragment>
    )
  }
}

Drawer.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  className: PropTypes.string,
  width: PropTypes.number,
  title: PropTypes.string
};

Drawer.defaultProps = {
  isOpen: false,
  className: '',
  width: 200,
  title: ''
};

export default Drawer;
