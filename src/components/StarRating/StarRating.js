import React from 'react';
import PropTypes from 'prop-types';
import _debounce from 'lodash/debounce';
import './StarRating.scss';

class StarRating extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tmpValue: 0
    };
  }

  onClickStar = (index) => () => {
    const {onChange, isDisabled} = this.props;
    if (isDisabled) {
      return;
    }
    if (onChange) {
      onChange(index + 1);
    }
  };

  onHoverStar = (index) => (event) => {
    const {onHoverStar, isDisabled} = this.props;
    if (isDisabled) {
      return;
    }
    if (onHoverStar) {
      onHoverStar(index, event);
    }
    this.setState({
      tmpValue: index + 1
    });
  };

  onMouseOutStar = (event) => {
    const {value, isDisabled, onMouseOut, onHoverStar} = this.props;
    if (isDisabled) {
      return;
    }
    if (onMouseOut) {
      onMouseOut(event);
    }
    if (onHoverStar) {
      onHoverStar(Math.floor(value - 1), event);
    }
    this.setState({
      tmpValue: 0
    });
  };

  handleMouseOut = _debounce(this.onMouseOutStar, 100);

  render() {
    const {value, isDisabled} = this.props;
    const {tmpValue} = this.state;
    const displayValue = tmpValue || value;
    const intValue = Math.floor(displayValue);
    const hasHalf = intValue !== displayValue;
    const stars = [];
    let index = 0;
    for (let i = 0; i < intValue; i++) {
      stars.push(
        <i className="fas fa-star active"
           key={i}
           onMouseOver={this.onHoverStar(index)}
           onClick={this.onClickStar(index)}/>
      );
      index++;
    }
    if (hasHalf) {
      stars.push(
        <i className="fas fa-star-half-alt active"
           key={stars.length}
           onMouseOver={this.onHoverStar(index)}
           onClick={this.onClickStar(index)}/>
      );
      index++;
    }
    while (stars.length < 5) {
      stars.push(
        <i className="far fa-star"
           key={stars.length}
           onMouseOver={this.onHoverStar(index)}
           onClick={this.onClickStar(index)}/>
      );
      index++;
    }
    return (
      <div
        className={`star-rating ${isDisabled ? 'disabled' : ''}`}
        onMouseLeave={this.handleMouseOut}>
        {
          stars
        }
      </div>
    )
  }
}

StarRating.propTypes = {
  value: PropTypes.number,
  isDisabled: PropTypes.bool,
  onChange: PropTypes.func,
  onHoverStar: PropTypes.func,
  onMouseOut: PropTypes.func
};

StarRating.defaultProps = {
  value: 0,
  isDisabled: false
};

export default StarRating;