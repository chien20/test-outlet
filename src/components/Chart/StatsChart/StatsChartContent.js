import React from 'react';
import {Bar, Line} from 'react-chartjs-2';

const StatsChartContent = ({type = 'bar', ...others}) => {
  switch (type) {
    case 'bar':
      return <Bar {...others}/>;
    case 'line':
      return <Line {...others}/>;
    default:
      return null;
  }
};

export default StatsChartContent;
