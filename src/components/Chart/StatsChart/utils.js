export const defaultColors = [
  '#E53A40',
  '#30A9DE',
  '#EFDC05',
  '#379392',
  '#FFBC42',
  '#8F2D56',
  '#56A902',
  '#54546c'
];

export const defaultBackgroundColors = [
  'rgba(229, 58, 64, 0.3)',
  'rgba(48, 169, 222, 0.3)',
  'rgba(229, 230, 5, 0.3)',
  'rgba(55, 147, 146, 0.3)',
  'rgba(255, 188, 66, 0.3)',
  'rgba(143, 45, 86, 0.3)',
  'rgba(86, 169, 2, 0.3)',
  'rgba(84, 84, 108, 0.3)',
];

export const defaultData = {
  labels: [],
  datasets: [{
    label: '',
    data: [],
    backgroundColor: defaultColors,
    borderColor: defaultColors,
    borderWidth: 1
  }]
};

export const defaultOptions = {
  legend: {
    display: false
  },
  tooltips: {
    callbacks: {
      label: function (tooltipItem) {
        return tooltipItem.yLabel;
      }
    }
  }
};
