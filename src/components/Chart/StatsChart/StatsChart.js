import React from 'react';
import PropTypes from 'prop-types';
import ChartTimeSelect from './ChartTimeSelect';
import StatsChartContent from './StatsChartContent';
import {defaultBackgroundColors, defaultColors, defaultData, defaultOptions} from './utils';
import './StatsChart.scss';

class StatsChart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: 'bar',
      data: defaultData,
      options: defaultOptions,
      view: 'day',
      updateTime: 0
    }
  }

  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.view !== this.state.view) {
      this.getData();
    }
  }

  getData = async () => {
    const {handleRequest, transformResponse, optionLabelKey, optionValueKey, type} = this.props;
    if (!handleRequest) {
      return;
    }
    const {view} = this.state;
    const params = {
      view
    };
    let {data: res} = await handleRequest(params);
    if (typeof res === 'object' && res.list) {
      res = res.list;
    }
    if (transformResponse) {
      res = transformResponse(res);
    }
    const datasets = {
      label: '',
      data: [],
      backgroundColor: defaultBackgroundColors,
      borderColor: defaultColors
    };
    if (type === 'line') {
      datasets.backgroundColor = defaultBackgroundColors[0];
      datasets.borderColor = defaultColors[0];
      datasets.pointBorderColor = defaultColors[0];
    } else {
      datasets.borderWidth = 2;
    }
    const data = {
      labels: [],
      datasets: [datasets]
    };
    if (res) {
      res.forEach(item => {
        data.labels.push(item[optionLabelKey]);
        datasets.data.push(item[optionValueKey]);
      });
    }
    this.setState({
      data: data,
      updateTime: new Date().getTime()
    });
  };

  onSelectTime = (view) => {
    this.setState({
      view: view
    });
  };

  render() {
    const {label, type, selectView} = this.props;
    const {data, options, view, updateTime} = this.state;
    return (
      <div className="stats-chart">
        {label && <h3 className="chart-label">{label}</h3>}
        <div className="chart-content">
          {
            selectView && <ChartTimeSelect onSelect={this.onSelectTime} view={view}/>
          }
          <StatsChartContent
            key={updateTime}
            data={data}
            options={options}
            type={type}/>
        </div>
      </div>
    )
  }
}

StatsChart.propTypes = {
  type: PropTypes.string,
  selectView: PropTypes.bool,
  handleRequest: PropTypes.func,
  transformResponse: PropTypes.func,
  optionLabelKey: PropTypes.string,
  optionValueKey: PropTypes.string,
  timeLabel: PropTypes.bool
};

StatsChart.defaultProps = {
  type: 'bar',
  selectTime: false,
  optionLabelKey: 'time',
  optionValueKey: 'total',
  timeLabel: true
};

export default StatsChart;