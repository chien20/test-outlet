import React from 'react';
import {Button, ButtonGroup} from 'react-bootstrap';

const defaultListTime = [
  {
    label: 'Ngày',
    value: 'day'
  },
  {
    label: 'Tuần',
    value: 'week'
  },
  {
    label: 'Tháng',
    value: 'month'
  },
  {
    label: 'Năm',
    value: 'year'
  }
];

const handleClick = (onSelect, view) => () => {
  if (onSelect) {
    onSelect(view);
  }
};

const ChartTimeSelect = ({view = 'day', listTime = defaultListTime, onSelect}) => (
  <ButtonGroup aria-label="Chart-time select" className="chart-time-select">
    {
      listTime.map((item, index) => (
        <Button
          key={index}
          variant="default"
          className={`${view === item.value ? 'active' : ''}`}
          onClick={handleClick(onSelect, item.value)}>
          {item.label}
        </Button>
      ))
    }
  </ButtonGroup>
);

export default ChartTimeSelect;
