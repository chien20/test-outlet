import React from 'react';
import './AlertGroup.scss';
import AlertContainer from './AlertContainer';

const AlertGroup = () => (
  <div className="alert-group">
    <AlertContainer container=""/>
  </div>
);

export default AlertGroup;
