import React from 'react';
import PropTypes from 'prop-types';
import {Alert as BSAlert} from 'react-bootstrap';

class Alert extends React.PureComponent {
  constructor(props) {
    super(props);
    this.timer = null;
  }

  componentDidMount() {
    this.setTimer();
  }

  componentWillUnmount() {
    this.cancelTimer();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.renewed_at !== nextProps.renewed_at && nextProps.timeout) {
      this.cancelTimer();
      this.setTimer();
    }
  }

  setTimer = () => {
    const {timeout, handleClose} = this.props;
    if (timeout) {
      this.timer = setTimeout(() => {
        handleClose();
      }, timeout);
    }
  };

  cancelTimer = () => {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  };

  render() {
    const {type, dismissible, message, handleClose, ...others} = this.props;
    const variant = type === 'error' ? 'danger' : type;
    return (
      <BSAlert
        variant={variant}
        dismissible={dismissible}
        onClose={handleClose}
        {...others}>
        {
          message
        }
      </BSAlert>
    )
  }
}

Alert.propTypes = {
  message: PropTypes.string,
  type: PropTypes.oneOf([
    'primary',
    'secondary',
    'success',
    'danger',
    'error',
    'warning',
    'info',
    'light',
    'dark',
  ]),
  show: PropTypes.bool,
  handleClose: PropTypes.func,
  dismissible: PropTypes.bool,
  bsPrefix: PropTypes.string,
  closeLabel: PropTypes.string,
  timeout: PropTypes.number
};

Alert.defaultProps = {
  message: '',
  type: 'secondary',
  show: true,
  dismissible: true,
  bsPrefix: 'alert',
  closeLabel: 'Đóng',
  timeout: 5000
};

export default Alert;