import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Alert from './Alert';
import {closeAlertAC} from '../../redux/actions/common';

class AlertContainer extends React.PureComponent {
  handleClose = (id) => () => {
    this.props.handleClose(id);
  };

  render() {
    const {alertList, container} = this.props;
    const displayedList = alertList.filter(item => item.container === container);
    return (
      <Fragment>
        {
          displayedList.map((item) => (
            <Alert
              key={item.id}
              {...item}
              handleClose={this.handleClose(item.id)}/>
          ))
        }
      </Fragment>
    )
  }
}

AlertContainer.propTypes = {
  container: PropTypes.string
};

AlertContainer.defaultProps = {
  container: ''
};

const mapStateToProps = (state) => ({
  alertList: state.common.alert.list
});

const mapDispatchToProps = (dispatch) => ({
  handleClose: (id) => dispatch(closeAlertAC(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AlertContainer);