import React from 'react';
import PropType from 'prop-types';

class QRCode extends React.PureComponent {
  constructor(props) {
    super(props);
    this.elRef = React.createRef();
    this.qrCode = null;
  }

  componentDidMount() {
    this.init();
  }

  init = () => {
    if (!window.QRCode) {
      return;
    }
    const {text, width, height} = this.props;
    this.qrCode = new window.QRCode(this.elRef.current, {
      text: text,
      width: width,
      height: height
    });
  };

  render() {
    return (
      <div className="qr-code" ref={this.elRef}/>
    )
  }
}

QRCode.propTypes = {
  text: PropType.string,
  width: PropType.number,
  height: PropType.number
};

QRCode.defaultProps = {
  text: '',
  width: 96,
  height: 96
};

export default QRCode;
