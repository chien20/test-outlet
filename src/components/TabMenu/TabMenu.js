import React from 'react';
import './TabMenu.scss';
import {Link, withRouter} from 'react-router-dom';

const TabMenuNav = ({className = '', children}) => {
  return (
    <div className={`${className} nav tab-menu`}>
      {children}
    </div>
  );
};

const TabMenuItem = withRouter(({href, isExternal = false, children, target, location}) => {
  const isActive = !isExternal && (location?.pathname === href || `${location?.pathname}`.includes(href));
  return (
    <div className="nav-item">
      {
        isExternal &&
        <a href={href} className="nav-link" target={target}>{children}</a>
      }
      {
        !isExternal &&
        <Link to={href} className={`nav-link ${isActive ? 'active' : ''}`}>{children}</Link>
      }
    </div>
  );
});

export {
  TabMenuNav,
  TabMenuItem,
};
