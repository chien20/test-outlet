import React from "react";

const SliderProps = {
  renderCenterLeftControls: ({previousSlide}) => {
    // if (currentSlide === 0) {
    //   return null;
    // }
    return (
        <button
            className="nuka-slider-btn-prev"
            onClick={previousSlide}
        >
          <i className="fas fa-chevron-left"/>
        </button>
    )
  },
  renderCenterRightControls: ({nextSlide}) => {
    // if (currentSlide + slidesToShow >= slideCount) {
    //   return null;
    // }
    return (
        <button className="nuka-slider-btn-next"
            onClick={nextSlide}
        >
          <i className="fas fa-chevron-right"/>
        </button>
    );
  },
  defaultControlsConfig: {
    pagingDotsStyle: {
      display: 'none'
    }
  }
}
export default SliderProps;