import React from 'react';
import PropTypes from 'prop-types';
import {isInViewport} from '../../common/helpers';
import throttle from 'lodash/throttle';

class InfiniteScroll extends React.PureComponent {
  constructor(props) {
    super(props);
    this.elRef = React.createRef();
  }

  componentDidMount() {
    this.register();
  }

  componentWillUnmount() {
    this.unregister();
  }

  register() {
    window.removeEventListener('scroll', this.onScroll);
    window.addEventListener('scroll', this.onScroll);
  }

  unregister() {
    window.removeEventListener('scroll', this.onScroll);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (this.props.hasMore !== nextProps.hasMore) {
      if (!nextProps.hasMore) {
        this.unregister();
      } else {
        this.register();
      }
    }
  }

  checkInViewPort = () => {
    if (isInViewport(this.elRef.current)) {
      this.props.loadMore();
    }
  };

  onScroll = throttle(this.checkInViewPort, 200);

  render() {
    return (
      <div className="show-more" ref={this.elRef}/>
    )
  }
}

InfiniteScroll.propTypes = {
  loadMore: PropTypes.func.isRequired,
  loader: PropTypes.any
};

export default InfiniteScroll;