import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';
import AddProductItem from './AddProductItem';
import SelectProductsModal from './SelectProductsModal/SelectProductsModal';
import './SelectProducts.scss';
import {getProductsByIdsAPI} from '../../api/products';

class SelectProducts extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      products: {}
    };
    this.selectProductsModal = React.createRef();
  }

  componentDidMount() {
    this.fetchProducts();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.productIds !== this.state.productIds) {
      this.fetchProducts();
    }
  }

  fetchProducts = () => {
    const {productIds} = this.props;
    const {products} = this.state;
    const ids = productIds.filter(id => !products[id]);
    if (!ids.length) {
      return;
    }
    getProductsByIdsAPI(ids).then(res => {
      if (res && res.data && res.data.list) {
        const data = {};
        res.data.list.forEach(item => {
          data[item.id] = item;
        });
        this.setState(prevState => ({
          products: {...prevState.products, ...data}
        }));
      }
    }).catch(error => {

    });
  };

  onClickAdd = () => {
    const {productIds} = this.props;
    this.selectProductsModal.current.handleOpen({
      selection: productIds
    }, this.onSelectProducts);
  };

  onSelectProducts = (selection) => {
    const {productIds} = this.props;
    const newIds = [...productIds];
    let changed = false;
    selection.forEach(id => {
      if (newIds.indexOf(id) < 0) {
        newIds.push(id);
        changed = true;
      }
    });
    if (changed) {
      this.onProductsChange(newIds);
    }
  };

  handleDeleteProduct = (index) => () => {
    const {productIds} = this.props;
    const newIds = [...productIds];
    newIds.splice(index, 1);
    this.onProductsChange(newIds);
  };

  onProductsChange = (productIds) => {
    if (this.props.onChange) {
      this.props.onChange(productIds);
    }
  };

  render() {
    const {productIds} = this.props;
    const {products} = this.state;
    return (
      <Fragment>
        <div className="select-products">
          <div className="--product-list">
            {
              productIds.map((id, index) => (
                <ProductItem
                  key={index}
                  id={id}
                  product={products[id]}
                  handleDelete={this.handleDeleteProduct(index)}/>
              ))
            }
            <AddProductItem onClickAdd={this.onClickAdd}/>
          </div>
        </div>
        <SelectProductsModal ref={this.selectProductsModal}/>
      </Fragment>
    );
  }
}

SelectProducts.propTypes = {
  productIds: PropTypes.array,
  onChange: PropTypes.func
};

export default SelectProducts;
