import React from 'react';
import {Modal} from 'react-bootstrap';
import {getProductListAPI} from '../../../api/products';
import {showAlert} from '../../../common/helpers/index';
import Pagination from '../../Pagination/Pagination';
import Store from '../../../redux/store/Store';
import CheckboxTable from '../../Table/CheckboxTable';
import './SelectProductsModal.scss';

const PAGE_SIZE = 5;

class SelectProductsModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isLoading: false,
      productList: [],
      totalItems: 0,
      page: 0,
      keyword: '',
      selection: [],
      selectAll: false
    };
    this.defaultState = JSON.parse(JSON.stringify(this.state));
    this.resolver = null;
    this.unmounted = false;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.isOpen !== this.state.isOpen && this.state.isOpen) {
      this.getListProduct();
    }
  }

  getListProduct() {
    const user = Store.getState().user.info;
    const {keyword, page} = this.state;
    const params = {
      type: 'own',
      store_id: user.store_id,
      current_page: page,
      page_size: PAGE_SIZE,
      q: keyword || '',
      status: 10
    };
    this.setState({
      isLoading: true
    });
    getProductListAPI(params, {__auth: true}).then(res => {
      this.setState({
        isLoading: false,
        productList: res.data && res.data.list ? res.data.list : [],
        totalItems: res.data && res.data.page_info ? res.data.page_info.total_items : 0
      });
    }).catch(error => {
      this.setState({
        isLoading: false
      });
      showAlert({
        type: 'error',
        message: `Không tải được danh sách sản phẩm!`
      });
    });
  }

  handleOpen = (data = {}, onClose = null) => {
    if (this.unmounted) {
      return;
    }
    this.resolver = onClose;
    const s = JSON.parse(JSON.stringify(this.defaultState));
    this.setState({
      s,
      isOpen: true,
      ...data
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  onPageChanged = (page) => {
    this.setState({
      page: page - 1
    }, this.getListProduct);
  };

  onSelectChange = (selection, selectAll) => {
    this.setState({
      selection,
      selectAll
    });
  };

  handleInputSearchChange = (event) => {
    this.setState({
      keyword: event.target.value
    });
  };

  handleSearch = (event) => {
    event.preventDefault();
    this.getListProduct();
  };

  columns = [
    {
      Header: 'Tên sản phẩm',
      id: 'name',
      accessor: (item) => (
        <div className="product-name">
          {item.name}
        </div>
      )
    },
    {
      Header: 'Phân loại hàng',
      id: 'variants',
      width: 150,
      accessor: (item) => (
        <div className="product-variants">
          {item.options.length ? `${item.options.length} biến thể` : ''}
        </div>
      )
    },
    {
      Header: 'SKU',
      accessor: 'sku',
      width: 150,
      className: 'text-center'
    },
    {
      Header: 'Số lượng',
      accessor: 'qty',
      width: 100,
      className: 'text-center'
    }
  ];

  handleSubmit = () => {
    const {selection} = this.state;
    if (this.resolver) {
      this.resolver(selection);
    }
    this.handleClose();
  };

  render() {
    const {isOpen, isLoading, productList, totalItems, keyword, page, selection, selectAll} = this.state;
    const totalPage = Math.ceil(totalItems / PAGE_SIZE);
    return (
      <Modal size="lg"
             backdrop="static"
             show={isOpen}
             onHide={this.handleClose}
             className="select-products-modal"
             centered>
        <Modal.Header closeButton>
          <Modal.Title>Chọn sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className="search-box" onSubmit={this.handleSearch}>
            <input
              type="text"
              className="form-control"
              value={keyword}
              placeholder="Nhập tên sản phẩm cần tìm"
              onChange={this.handleInputSearchChange}/>
            <button type="submit" className="btn btn-primary search-icon">
              <i className="fa fa-search"/> Tìm kiếm
            </button>
          </form>
          <CheckboxTable
            data={productList}
            columns={this.columns}
            selection={selection}
            selectAll={selectAll}
            onSelectChange={this.onSelectChange}
            showPagination={false}
            defaultPageSize={PAGE_SIZE}/>
          {
            !!productList.length &&
            <div className="bottom">
              <div className="item-pagination hidden-mobile">
                {
                  totalPage > 1 &&
                  <Pagination
                    totalPages={totalPage}
                    currentPage={page + 1}
                    onPageChanged={this.onPageChanged}/>
                }
              </div>
            </div>
          }
        </Modal.Body>
        <Modal.Footer>
          <button
            className="btn btn-default"
            onClick={this.handleClose}
            disabled={isLoading}>
            Hủy bỏ
          </button>
          <button
            className="btn btn-success"
            onClick={this.handleSubmit}>Xong
          </button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default SelectProductsModal;
