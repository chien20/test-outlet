import React from 'react';

const ProductItem = ({product, handleDelete}) => (
  <div className="--product-item">
    {
      product &&
      <img src={product.avatar_thumbnail_url} alt={product.name}/>
    }
    <span className="delete-btn" onClick={handleDelete}><i className="fas fa-trash-alt"/></span>
  </div>
);

export default ProductItem;
