import React from 'react';

const AddProductItem = ({onClickAdd}) => (
  <div className="--add-product-item" onClick={onClickAdd}>
    <span>+</span>
  </div>
);

export default AddProductItem;
