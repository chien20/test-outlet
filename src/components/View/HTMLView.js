import React from 'react';
import PropTypes from 'prop-types';
import {safeHTML} from '../../common/helpers';

class HTMLView extends React.PureComponent {
  render() {
    const {html, className} = this.props;
    return (
      <div className={`${className} html-view`} dangerouslySetInnerHTML={{__html: safeHTML(html)}}>
      </div>
    )
  }
}

HTMLView.propTypes = {
  html: PropTypes.string,
  className: PropTypes.string
};

HTMLView.defaultProps = {
  className: ''
};

export default HTMLView;
