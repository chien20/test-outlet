import React from 'react';
import PropTypes from 'prop-types';
import CKE from 'ckeditor4-react';
import {defaultConfig} from './configs';
import './CKEditor.scss';

class CKEditor extends React.PureComponent {
  onChange = (event) => {
    this.props.onChange(event.editor.getData() || '');
  };

  render() {
    const {value, config, ...others} = this.props;
    return (
      <CKE
        {...others}
        config={config}
        data={value}
        onChange={this.onChange}/>
    )
  }
}

CKEditor.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  config: PropTypes.object
};

CKEditor.defaultProps = {
  value: '',
  config: defaultConfig
};

export default CKEditor;