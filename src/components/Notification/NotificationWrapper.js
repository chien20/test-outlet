import React from 'react';
import {NOTIFICATION_ACTIONS} from '../../common/constants/app';
import {Link} from 'react-router-dom';

const NotificationWrapper = ({action = null, children, ...others}) => {
  if (!action || typeof action !== 'object' || !action.type) {
    return (
      <div {...others}>
        {children}
      </div>
    );
  }
  switch (action.type) {
    case NOTIFICATION_ACTIONS.OPEN_ORDER_DETAIL:
      return (
        <Link
          to={`/user/orders/view/${action.params.order_id}`}
          {...others}
        >
          {children}
        </Link>
      );
    case NOTIFICATION_ACTIONS.OPEN_ORDER_DETAIL_FOR_SHOP:
      return (
        <Link
          to={`/user/store/orders/view/${action.params.order_id}`}
          {...others}
        >
          {children}
        </Link>
      );
    case NOTIFICATION_ACTIONS.OPEN_GROUP_INVITATION:
      return (
        <Link
          to={`/groups/${action.params.group_id}/invite/${action.params.group_invite_id}`}
          {...others}
        >
          {children}
        </Link>
      );
    default:
      return (
        <div {...others}>
          {children}
        </div>
      );
  }
};

export default NotificationWrapper;
