import React from 'react';

const NotificationImage = ({url = ''}) => {
  if (!url) {
    return <div className="notification-image"/>
  }
  const style = {
    backgroundImage: `url(${url})`
  };
  return <div className="notification-image" style={style}/>
};

export default NotificationImage;
