import React from 'react';
import {convertNotification} from '../../common/helpers/format';

export const NotificationContent = (data) => {
  return <p className="notification-content" dangerouslySetInnerHTML={{__html: convertNotification(data.text, data.params)}}/>;
};
