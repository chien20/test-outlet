import React from 'react';

const LoadScript = ({children}) => {
  const [isLoaded, setIsLoaded] = React.useState(false);
  const setIsLoadedRef = React.useRef(setIsLoaded);
  setIsLoadedRef.current = setIsLoaded;

  React.useEffect(() => {
    // Create the script tag, set the appropriate attributes
    if (document.getElementById('google-map-script')) {
      setIsLoadedRef.current(true);
      return;
    }
    const script = document.createElement('script');
    script.id = 'google-map-script';
    script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}&callback=initMap&libraries=places`;
    script.defer = true;

    // Attach your callback function to the `window` object
    window.initMap = function () {
      setIsLoadedRef.current(true);
    };

    // Append the 'script' element to 'head'
    document.head.appendChild(script);
  }, [setIsLoadedRef]);

  if (!isLoaded) {
    return null;
  }

  return children;
};

export default React.memo(LoadScript);
