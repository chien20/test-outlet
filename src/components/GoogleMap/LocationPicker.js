import React from 'react';
import PropTypes from 'prop-types';
import {uuidv4} from '../../common/helpers';
import {isMobile} from '../../common/helpers/browser';

class LocationPicker extends React.PureComponent {
  state = {
    map: null,
    marker: null,
    id: uuidv4(),
  };

  markerRef = React.createRef();

  componentDidMount() {
    this.initData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.province !== this.props.province || prevProps.district !== this.props.district || prevProps.ward !== this.props.ward) {
      this.handleProvinceChanged();
    }
  }

  initData = () => {
    const {id} = this.state;
    const google = window.google;
    const map = new google.maps.Map(document.getElementById(id), {
      zoom: 13,
    });
    this.setState({
      map,
    }, () => {
      const {geocoding} = this.props;
      if (geocoding && geocoding.address && geocoding.position) {
        this.createOrUpdateMarker(geocoding.position, geocoding.address, 14);
      } else {
        this.handleProvinceChanged(map);
      }
    });
  };

  handleProvinceChanged = () => {
    const {province, district, ward} = this.props;
    const {map} = this.state;
    if (!map || !province || !province.id) {
      return;
    }
    const google = window.google;

    const names = [province.name];
    if (district.id) {
      names.push(district.name);
    }
    if (ward && ward.id) {
      names.push(ward.name);
    }

    const request = {
      query: names.reverse().join(', '),
      fields: ['name', 'geometry'],
    };

    const service = new google.maps.places.PlacesService(map);

    service.findPlaceFromQuery(request, (results, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        this.createOrUpdateMarker(results[0].geometry.location, request.query, names.length + 12);
      }
    });
  };

  createOrUpdateMarker = (position, title, zoom = undefined) => {
    const {markerRef} = this;
    const {geocoding, onAddressChanged} = this.props;
    const {map} = this.state;
    if (!markerRef.current) {
      const google = window.google;
      const geocoder = new google.maps.Geocoder();
      markerRef.current = new google.maps.Marker({
        position,
        map: map,
        title,
        draggable: true,
        animation: google.maps.Animation.DROP,
      });
      if (onAddressChanged) {
        if (!geocoding && position && typeof position.lat === 'function') {
          onAddressChanged({
            address: title,
            position: {
              lat: position.lat(),
              lng: position.lng(),
            },
          });
        }
        markerRef.current.addListener("dragend", (mapsMouseEvent) => {
          const latlng = mapsMouseEvent.latLng;
          geocoder.geocode({'location': latlng}, (results, status) => {
            if (status === 'OK') {
              if (results[0]) {
                markerRef.current.setTitle(results[0].formatted_address);
                onAddressChanged({
                  address: results[0].formatted_address,
                  position: {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng(),
                  }
                });
              } else {
                console.log('No results found');
              }
            } else {
              console.log('Geocoder failed due to: ' + status);
            }
          });
        });
      }
    } else {
      markerRef.current.setPosition(position);
      markerRef.current.setTitle(title);
    }
    map.setCenter(position);
    if (zoom) {
      map.setZoom(zoom);
    }
  };

  render() {
    const {id} = this.state;
    return (
      <div className="location-picker" id={id} style={{width: '100%', height: isMobile ? 300 : 450}}/>
    );
  }
}

LocationPicker.propTypes = {
  province: PropTypes.object,
  district: PropTypes.object,
  ward: PropTypes.object,
  geocoding: PropTypes.object,
  onAddressChanged: PropTypes.func,
};

export default LocationPicker;
