import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {numberAsCurrentcy} from '../../common/helpers/format';
import {publicUrl} from '../../common/helpers';
import ProductFavorite from '../../pages/product/_components/ProductFavorite/ProductFavorite';
import './ProductItem.scss';

class ProductItem extends React.PureComponent {
  render() {
    const {product, showQtyLeft} = this.props; 
    const prices = product.prices;
    const imageBg = {
      backgroundImage: `url(${product.avatar_thumbnail_url})`
    };
    return (
      <div className="product-item">
        <div className="product-image">
          <Link to={product.url}>
            <span className="product-image-wrapper" style={imageBg}/>
          </Link>
          {
            prices.discount.max > 0 &&
            <div className="sale">-{prices.discount.max}%</div>
          }
        </div>
        <div className="product-text">
          <h3 className="product-name">
            <Link to={product.url}>
              {prices.isFlashSale && <i className="fas fa-fire-alt flash-sale-icon"/>}
              {product.name}
            </Link>
          </h3>
          <div className="item-price">
            <div className="price-left">
              <p className="new-price">
                {numberAsCurrentcy(prices.price.min)}đ
                {
                  prices.price.max > prices.price.min &&
                  <Fragment> - {numberAsCurrentcy(prices.price.max)}đ</Fragment>
                }
              </p>
              {/*{*/}
              {/*  (prices.price.min !== prices.original.min || prices.price.max !== prices.original.max) &&*/}
              {/*  <p className="old-price">*/}
              {/*    {numberAsCurrentcy(prices.original.min)}đ*/}
              {/*    {*/}
              {/*      prices.original.max > prices.original.min &&*/}
              {/*      <Fragment> - {numberAsCurrentcy(prices.original.max)}đ</Fragment>*/}
              {/*    }*/}
              {/*  </p>*/}
              {/*}*/}
            </div>
            <ProductFavorite product={product}/>
          </div>
          {
            showQtyLeft &&
              <Fragment>
                {
                  product.qty > 0 &&
                  <div className="item-qty">Còn {product.qty} sản phẩm</div>
                }
                {
                  product.qty === 0 &&
                  <div className="item-qty gray">Sản phẩm đã hết</div>
                }
              </Fragment>
          }
        </div>
      </div>
    )
  }
}

ProductItem.propTypes = {
  product: PropTypes.any,
  showQtyLeft: PropTypes.bool,
};

export default ProductItem;
