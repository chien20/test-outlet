import React from 'react';
import './Avatar.scss';
import {imageUrl, publicUrl} from '../../common/helpers';

const Avatar = ({src, size = 40, className, alt, style, canEdit = false, handleClickEdit = null, ...others}) => {
  src = src ? imageUrl(src) : publicUrl('/assets/images/no-image/avatar.png');
  let avatarStyle = {};
  if (size) {
    avatarStyle = {
      width: size,
      height: size,
      lineHeight: `${size}px`
    };
  }
  if (style && typeof style === 'object') {
    avatarStyle = {...style, ...avatarStyle};
  }
  return (
    <div className={`avatar ${className ? className : ''}`}
         style={avatarStyle}>
      <img {...others}
           src={src}
           alt={alt}/>
      {
        canEdit &&
        <div className="avatar-overlay" onClick={handleClickEdit}><i className="fas fa-edit"/></div>
      }
    </div>
  )
};

export default Avatar;
