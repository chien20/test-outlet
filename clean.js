const path = require('path');
const {readdir, readFile, unlink} = require('fs').promises;
const {resolve} = path;

async function getFiles(dir) {
  const dirents = await readdir(dir, {withFileTypes: true});
  const files = await Promise.all(dirents.map((dirent) => {
    const res = resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : {path: res, name: dirent.name};
  }));
  return Array.prototype.concat(...files);
}

async function findUsage(imageFile, srcFiles) {
  for (let i = 0; i < srcFiles.length; i++) {
    const content = await readFile(srcFiles[i].path);
    if (content && content.indexOf(imageFile.name) >= 0) {
      return true;
    }
  }
  return false;
}

function printProgress(progress) {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write('Processing ' + progress + '%');
}

async function clean() {
  console.log('Cleaning...');
  const imageFiles = await getFiles(path.join(__dirname, 'public', 'assets', 'images'));
  const srcFiles = await getFiles(path.join(__dirname, 'src'));
  console.log(imageFiles.length + ' image files found.');
  const unusedFiles = [];
  for (let i = 0; i < imageFiles.length; i++) {
    const imageFile = imageFiles[i];
    printProgress(Math.round((i + 1) * 100 / imageFiles.length));
    const isUsed = await findUsage(imageFile, srcFiles);
    if (!isUsed) {
      await unlink(imageFile.path);
      unusedFiles.push(imageFile);
    }
  }
  console.log('\nDeleted ' + unusedFiles.length + ' unused files.');
}

clean().catch(e => {
  console.error(e);
});
